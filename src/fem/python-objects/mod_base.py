"""
Python copy of mod_base.f90
"""

import numpy as np
import utilities as ut
import mod_master_el as me

#----------------------------------------------------------------------

class t_base():
  def __init__(self,data):
    self.k    = data['k']
    self.me   = me.t_me( data['me'] )
    self.m    = data['m']
    self.ms   = data['ms']
    self.deg  = data['deg']
    self.degs = data['degs']

    d  = self.me.d
    m  = self.m
    ms = self.ms

    ut.copy_array_field( self , 'xig'  , data , (  d,m    ) )
    ut.copy_array_field( self , 'wg'   , data , (    m ,  ) )
    ut.copy_array_field( self , 'stab' , data , ( -1,ms   ) , shift=-1)
    ut.copy_array_field( self , 'xigs' , data , (d-1,ms   ) )
    ut.copy_array_field( self , 'wgs'  , data , (    ms   ) )
    ut.copy_array_field( self , 'xigb' , data , ( d ,ms,-1) )
    ut.copy_array_field( self , 'wgb'  , data , (    ms,-1) )

    self.pk   = data['pk']
    pk = self.pk
    ut.copy_array_field(self, 'p'      , data , (  pk,m    ) )
    ut.copy_array_field(self, 'gradp'  , data , (d,pk,m    ) )
    ut.copy_array_field(self, 'pb'     , data , (  pk,ms,-1) )
    ut.copy_array_field(self, 'gradpb' , data , (d,pk,ms,-1) )

#----------------------------------------------------------------------

