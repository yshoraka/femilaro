"""
Compute the dispersion relation for the Landau damping test case.

For each wavenumber k, the solution is a superposition of many modes;
nevertheless considering the first mode, as done here, provides a
reasonable approximation. The first mode is defined by the largest
imaginary part of the pole omega_j of D(k,omega).

The initial condition is

 f = 1/(sqrt(2*pi)*vth) * (1 + alpha*cos(k*x)) * exp(-0.5*(v/vth)**2)

"""

import numpy as np
import scipy as sp
import pylab as pp
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


#----------------------------------------------------------------------
# Define some general functions

def Z(zeta):
  """
  Plasma dispersion function Z(zeta)
  """
  from scipy.special import erfi
  return np.sqrt(np.pi) * np.exp(-zeta**2) * (1.0j-erfi(zeta))

def Z1(zeta):
  """
  Z'(zeta)
  """
  return -2.0*(1.0+zeta*Z(zeta))

def Z2(zeta):
  """
  Z''(zeta)
  """
  return -2.0*(Z(zeta)+zeta*Z1(zeta))
#----------------------------------------------------------------------

#----------------------------------------------------------------------
# Define D(k,omega)

def D(k,omega, vth):
  """
  Denominator for the Landau damping problem

  k,omega: variables of the transformed problem
  vth:     thermal velocity
  """
  kvth = k*vth
  return 1.0 - 1.0/(2.0*kvth**2) * Z1( omega/(np.sqrt(2.0)*kvth) )

def D_omega(k,omega, vth):
  """
  Derivative of D(k,omega)
  """
  kvth = k*vth
  return - 1.0/(2.0*np.sqrt(2.0)*kvth**3) \
          * Z2( omega/(np.sqrt(2.0)*kvth) )

#----------------------------------------------------------------------


# Define here the interval to be scanned for omega
x = np.linspace(-6.0,6.0,200)
y = np.linspace(-6.0,1.0,200)
xx,yy = np.meshgrid(x,y)
zz = xx + yy*1.0j

# Select k and vth
k = 0.5 *3
#vth = 1.0/np.sqrt(0.25)
vth = 1.0
#vth = 0.1
# Amplitude of the perturbation
#alpha = 0.005
alpha = 0.01

# Contour plot to locate the dominant mode
pp.contour(xx,yy,abs(D(k,zz,vth)),np.linspace(0,1,50))
pp.show()

# Set here a tentative value for the dominant pole, and possibly for
# the following ones
OMEGA = np.array([ 1.40-0.15j , 1.79-1.14j , 2.20-1.68j , 2.54-2.06j , \
                   2.80-2.40j , 3.10-2.70j , 3.35-2.95j , 3.60-3.25j , \
		   3.80-3.45j , 4.00-3.70j , 4.20-3.90j , 4.35-4.10j , \
		   4.50-4.25j , 4.75-4.45j , 4.90-4.65j , 5.05-4.80j ])
OMEGA = np.array([ 2.0-1.2j ]);
RES   = 0.0*OMEGA

i = 0
for omega in OMEGA:
  
  # Newton method to refine the pole
  delta = 100.0
  while(delta>1.0e-12):
    omegan = omega - D(k,omega,vth)/D_omega(k,omega,vth)
    delta = np.abs(omegan-omega)
    print("increment:  delta = %e" % delta)
    omega = omegan

  print("%d-th pole:  omega_j = %f %fj" % (i,omega.real,omega.imag))

  # Notice that omega is the frequency of the electric field, the
  # electric energy oscillates twice as fast.
  OMEGA[i] = omega

  # To complete the computation, we need for each omega the associated
  # residual.

  okv = omega/(k*vth) 
  okvpi = okv/np.sqrt(2.0)
  Zo = Z( okvpi )
  RES[i] = alpha/2.0 * k*vth**2 * Zo / ( 2.0*okvpi - (1.0-okv**2) *Zo )
  print("Residual coefficient:  R = %f e^(i%f)" % \
         (np.abs(RES[i]),np.angle(RES[i])))

  i += 1

EE = "E(x,t) = 4 *( "
# Reconstruct the electric field
for i in range(0,OMEGA.size):

  o_r, o_i = OMEGA[i].real, OMEGA[i].imag
  r  , phi = np.abs(RES[i]),np.angle(RES[i])
  EE += "%f * exp(%f*t).*cos(%f*t-%f)" % \
        (r, o_i, o_r, phi)
  if i<OMEGA.size-1:
    EE += " + "

Energy = EE + " )^2 * %f" % (2.0*np.pi/k)
EE += " ) .* sin(%f*x)" % k

print("")
print(EE)
print("")

# NOTE: the following energy corresponds to one wavelength
o_r, o_i = OMEGA[0].real, OMEGA[0].imag
r  , phi = np.abs(RES[0]),np.angle(RES[0])
print("Energy for the dominating mode:")
print("1/2*||E||^2 = %f * exp(%f*t)*(1+cos(%f*t-%f))" %         \
      (np.pi/k * 8.0 * r**2 / 2.0, 2.0*o_i, 2.0*o_r, 2.0*phi) )

print("")
print("Complete energy:")
print(Energy)

