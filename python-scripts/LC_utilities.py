"""
Some useful functions for the liquid crystal problem (harmonic map).
"""

import numpy as np
import scipy as sp
import scipy.sparse
import scipy.sparse.linalg

#----------------------------------------------------------------------

def u_energy(base,grid,dofs,u):
  """
  Compute the energy |\nabla u|^2 for a liquid crystal field u
  """

  Gp = np.transpose( base.gradp , (2,1,0) )

  Eu = 0.0
  for e in grid.e:

    # transform the phi gradients: [ quad node, base, d/dx component ]
    Gpe = np.dot( Gp , e.bi )

    # get the element solution: [ u component, base ]
    ue = u[:,dofs.dofs[:,e.i]]

    # gradient of u: [ u component, quad node, d/dx component ]
    Gue = np.dot( ue , Gpe )

    # accumulate the energy
    Eu += e.det_b * np.sum( base.wg * np.sum( Gue**2 , axis=(0,2) ) )

  return Eu

#----------------------------------------------------------------------

def u_error_norms(base,grid,dofs,uex,Guex,u):
  """
  Compute some error norms for the orientation field u

  uex : computes u(x) with rows for u components and columns for points
  Guex : Grad(u), [ u component , d/dx component , point ]
  """

  Gp = np.transpose( base.gradp , (2,1,0) )

  e_li = 0.0 # L^\infty
  e_l1 = 0.0 # L^1
  e_l2 = 0.0 # L^2
  e_h1 = 0.0 # H^1
  for e in grid.e:

    # quadrature nodes in physical space
    xige = np.dot( e.b , base.xig ) + e.x0[:,np.newaxis]

    # exact solution
    uexe  =  uex( xige )
    Guexe = Guex( xige )

    # numerical solution: see also u_energy
    une = u[:,dofs.dofs[:,e.i]] # nodal values
    ue  = np.dot(               une ,     base.p )
    Gue = np.transpose( np.dot( une , np.dot( Gp , e.bi ) ) , (0,2,1) )

    # compute the errors
    du  =  uexe -  ue
    dGu = Guexe - Gue
    e_li = max( e_li , max([ max(abs(dul)) for dul in du ]) )
    e_l1 += e.det_b * np.sum( base.wg * np.sum( abs(du), axis=(0,)  ) )
    e_l2 += e.det_b * np.sum( base.wg * np.sum( du**2  , axis=(0,)  ) )
    e_h1 += e.det_b * np.sum( base.wg * np.sum( dGu**2 , axis=(0,1) ) )

  e_h1 += e_l2

  e_l2 = np.sqrt(e_l2)
  e_h1 = np.sqrt(e_h1)

  return e_li, e_l1, e_l2, e_h1

#----------------------------------------------------------------------

def q_error_norms(base,grid,dofs,qex,Gqex,q):
  """
  Compute some error norms for the Lagrange multiplier q
  """

  Gp = np.transpose( base.gradp , (2,1,0) )

  e_li = 0.0 # L^\infty
  e_l1 = 0.0 # L^1
  e_l2 = 0.0 # L^2
  e_h1 = 0.0 # H^1
  for e in grid.e:

    # quadrature nodes in physical space
    xige = np.dot( e.b , base.xig ) + e.x0[:,np.newaxis]

    # exact solution
    qexe  =  qex( xige )
    Gqexe = Gqex( xige )

    # numerical solution: see also u_energy
    qne = q[dofs.dofs[:,e.i]] # nodal values
    qe  = np.dot( qne ,     base.p )
    Gqe = np.dot( qne , np.dot( Gp , e.bi ) ).T
    
    # compute the errors
    dq  =  qexe -  qe
    dGq = Gqexe - Gqe
    e_li = max( e_li , max(abs(dq)) )
    e_l1 += e.det_b * np.sum( base.wg * abs(dq) )
    e_l2 += e.det_b * np.sum( base.wg * dq**2 )
    e_h1 += e.det_b * np.sum( base.wg * np.sum( dGq**2 , axis=(0,) ) )

  e_h1 += e_l2

  e_l2 = np.sqrt(e_l2)
  e_h1 = np.sqrt(e_h1)

  return e_li, e_l1, e_l2, e_h1

#----------------------------------------------------------------------

def q_Riestz_H1_representation(base,grid,dofs,qex,q):
  """
  Given q in (H^1)', we compute its Riestz representation in H^1.

  In practice, this amounts to solving the linear problem

  \int_\Omega ( Grad(rq) \cdot Grad(v) + rq v ) = \int_\Omega q v
  
  When computing the representation of an error, the right hand side
  must be modified as  \int_\Omega (qex - q)  where qex is not in
  general polynomial. Set qex to a value different from None to
  provide an exact solution; see the definition of compute_dq in the
  code.

  Note:  coo_matrix((data, (i, j)), [shape=(M, N)])  is the standard
  way to assemble a sparse matrix, implying summation for repeated
  entries.
  """

  pk = base.pk # number of base functions

  if qex is None:
    compute_dq = lambda b, xig, x0, qg : qg
  else:
    compute_dq = lambda b, xig, x0, qg : \
      qex( np.dot( b , xig ) + x0[:,np.newaxis] ) - qg

  # phi gradients: [ base, quad node , d/dx component ]
  Gp = np.transpose( base.gradp , (1,2,0) )

  # global matrix: index arrays and coefficients
  ijl = np.repeat( np.reshape(np.arange(0,pk),(-1,1)) , pk , axis=1 )
  ij = np.empty( (grid.ne,pk,pk) , dtype=int   )
  xx = np.empty( (grid.ne,pk,pk) , dtype=float )

  # right hand side
  bb = np.zeros( dofs.ndofs , dtype=float )

  # element contributions
  aak = np.empty( (pk,pk) , dtype=float )
  bk  = np.empty( (pk,  ) , dtype=float )
  for e,edofs , ije,xxe in zip( grid.e , dofs.dofs.T , ij , xx ):

    # element quadrature weights 
    wg = e.det_b * base.wg

    # Grad(phi)|_K: [ base, quad node, d/dx component ]
    Gpe = np.dot( Gp , e.bi )

    # element solution
    qg = np.dot( q[edofs] , base.p )

    # rhs, depending on the presence of qex
    qrhs = compute_dq( e.b, base.xig, e.x0, qg )

    # local matrix
    for i,(pi,Gpi) in enumerate(zip(base.p,Gpe)):
      wgpi  = wg               * pi
      wgGpi = wg[:,np.newaxis] * Gpi # using broadcasting
      for j,(pj,Gpj) in enumerate(zip(base.p,Gpe)):
        aak[i,j] = np.sum( wgpi * pj ) + np.sum( wgGpi * Gpj )
      bk[i] = np.sum( wgpi * qrhs )

    # store into the global matrix and rhs
    ije[...] = edofs[ijl]
    xxe[...] = aak
    bb[edofs] += bk

  # build and solve the linear system
  iii = np.reshape(             ij,         -1)
  jjj = np.reshape(np.transpose(ij,(0,2,1)),-1) # as iii, reordered
  xxx = np.reshape(             xx,         -1)
  aaa = sp.sparse.coo_matrix( ( xxx , (iii,jjj) ) ,
                              (dofs.ndofs,dofs.ndofs) ).tocsc()

  return scipy.sparse.linalg.spsolve(aaa,bb)

#----------------------------------------------------------------------

# exact solutions

C = np.pi
gamma = 0.01
k = 1
h = 2

t = 1

kpi = np.pi*k
hpi = np.pi*h
kpi2 = kpi**2
hpi2 = hpi**2
Cexp = C*np.exp( -gamma*(kpi2+hpi2)*t )

def theta(x):
  return Cexp * np.cos(kpi*x[0,:]) * np.cos(hpi*x[1,:])

def thetax(x):
  return Cexp * (-kpi*np.sin(kpi*x[0,:])) * np.cos(hpi*x[1,:])

def thetay(x):
  return Cexp * np.cos(kpi*x[0,:]) * (-hpi*np.sin(hpi*x[1,:]))

def thetaxx(x):
  return Cexp * (-kpi2*np.cos(kpi*x[0,:])) * np.cos(hpi*x[1,:])

def thetayy(x):
  return Cexp * np.cos(kpi*x[0,:]) * (-hpi2*np.cos(hpi*x[1,:]))

def thetaxy(x):
  return Cexp * (-kpi*np.sin(kpi*x[0,:])) * (-hpi*np.sin(hpi*x[1,:]))


def uex(x):
  th = theta(x)
  return np.array( [ np.cos(th) , np.sin(th) ] )
  

def Guex(x):

  th = theta(x)
  thx = thetax(x)
  thy = thetay(x)

  st = np.sin(th)
  ct = np.cos(th)
  
  return np.array( [ [ -st*thx , -st*thy ] , \
                     [  ct*thx ,  ct*thy ] ] )


def qex(x):
  return -( thetax(x)**2 + thetay(x)**2 )

def Gqex(x):
  
  thx = thetax(x)
  thy = thetay(x)
  thxx = thetaxx(x)
  thxy = thetaxy(x)
  thyy = thetayy(x)

  return np.array( [ -2*(thx*thxx + thy*thxy) , \
                     -2*(thx*thxy + thy*thyy) ])

#----------------------------------------------------------------------

# filebn = "../run/LC-convtest/h032-dt=07-grid.octxt"
# G = open(filebn+"-grid.octxt")
# B = open(filebn+"-base.octxt")
# U = open(filebn+"-res-0020.octxt")
#
# GRID = read_octxt.read_all_vars(G)
# BASE = read_octxt.read_all_vars(B)
# UVAR = read_octxt.read_all_vars(U)
#
# base = mod_base.t_base( BASE["base"] )
# grid = mod_grid.t_grid( GRID["grid"] )
# dofs = mod_cgdofs.t_cgdofs( GRID["dofs"] )
#
# LC_utilities.u_error_norms(base,grid,dofs,LC_utilities.uex,LC_utilities.Guex,UVAR["u"])
# LC_utilities.q_error_norms(base,grid,dofs,LC_utilities.qex,LC_utilities.Gqex,UVAR["q"])
# req = LC_utilities.q_Riestz_H1_representation(base,grid,dofs,LC_utilities.qex,UVAR["q"])
# LC_utilities.q_error_norms(base,grid,dofs,lambda x: 0,lambda x: 0,req)
# 
# filebn = "../run/LC-convtest/h032-dt=0X"
# 
# G = open(filebn+"-grid.octxt"); B = open(filebn+"-base.octxt"); U = open(filebn+"-res-0020.octxt")
# GRID = read_octxt.read_all_vars(G); BASE = read_octxt.read_all_vars(B); UVAR = read_octxt.read_all_vars(U)
# base = mod_base.t_base( BASE["base"] ); grid = mod_grid.t_grid( GRID["grid"] ); dofs = mod_cgdofs.t_cgdofs( GRID["dofs"] )
# 
# Uh.close(); Uh = open("../run/LC-convtest/h032-dt=07-res-0020.octxt"); UhVAR = read_octxt.read_all_vars(Uh)
# LC_utilities.u_error_norms(base,grid,dofs,lambda x:0,lambda x: 0,UVAR["u"]-UhVAR["u"])
# LC_utilities.q_error_norms(base,grid,dofs,lambda x:0,lambda x: 0,UVAR["q"]-UhVAR["q"])
# 
# G.close(); B.close(); U.close()
# 
# plot_grid.plt.style.use('grayscale')
# plot_grid.plt.xlim((1e-3,0.15))
# plot_grid.plt.ylim((8e-7,0.15))
# plot_grid.plt.loglog(dt,Eu128[:,3],'*-',markersize=20)
# plot_grid.plt.loglog(dt,Eu64[:,3],'s-',markersize=7.5)
# plot_grid.plt.loglog(dt,Eu32[:,3],'d-',markersize=10)
# plot_grid.plt.loglog(dt,Eu16[:,3],'o-',markersize=10)
# plot_grid.add_slope_reference(2,3e-2,0.15,8e-7)
# plot_grid.plt.tight_layout()
# plot_grid.plt.show()
# 
# plot_grid.plt.style.use('grayscale')
# plot_grid.plt.xlim((1e-3,0.15))
# plot_grid.plt.ylim((2e-2,8))
# plot_grid.plt.loglog(dt,Eq128[:,0],'*-',markersize=20)
# plot_grid.plt.loglog(dt,Eq64[:,0],'s-',markersize=7.5)
# plot_grid.plt.loglog(dt,Eq32[:,0],'d-',markersize=10)
# plot_grid.plt.loglog(dt,Eq16[:,0],'o-',markersize=10)
# plot_grid.add_slope_reference(1,3e-2,0.15,2e-2)
# plot_grid.plt.tight_layout()
# plot_grid.plt.show()
# 
