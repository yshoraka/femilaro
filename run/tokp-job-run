#!/bin/bash


#--------------------------CONFIGURATIONS-----------------------------

# Name of the executable
execroot="./../bin"
executable="b-sol-minimal"

# Number of cores
num_cores=2
# Number of MPI processes (per core) - maximum 8
num_MPI_proc=8

# Name specifier (used to identify different experiments)
name_spec="simple-TOKP-test"

# Input file
input_file=${executable}.in

# Required time: seconds | hours:minutes:seconds
#  -> see time_specifier in "man sge_types"
real_time="24:00:00"

# MPI module
mpi_module="impi/5.0.3"

#------------------------END CONFIGURATIONS---------------------------


#-----------------------------MAIN OUTPUT-----------------------------

# PBS script to submit the job
job_file="qsub_script_to_run_${executable}.sub"

# job name
job_qual="${num_cores}x${num_MPI_proc}"
job_name="${executable}-${job_qual}"

# stdout file
job_out="out-${executable}-${name_spec}-N${job_qual}.txt"

#---------------------------END MAIN OUTPUT---------------------------


# We make a copy of the input file so that the original file can be
# changed even while the job is waiting in the queue.
input_file_copy=""
if [ -n "${input_file}" ]; # notice that we need "" in the test!
 then
  input_file_copy="temp-${job_name}-${RANDOM}.in"
  cp ${input_file} "${input_file_copy}"
fi

# Total number of processes: num_cores*num_MPI_proc
tot_num_procs=`echo ${num_cores} ${num_MPI_proc} \
               | awk '{ N = \$1*\$2 ; print N }'`

# Write the PBS input file
cat > ${job_file} << EOF
#!/bin/bash
#
# Execute the job in the current working directory
#$ -cwd
#
# Join standard error and output
#$ -j y
#
# Set the runtime as "real time"
#$ -l h_rt=${real_time}
#
# Select the "parallel programming environment"
#  -> use "qconf -spl" to see a list of the available environments
#  -> use "qconf -sp <env-name>" for a description of the environments
#$ -pe impi_hydra ${tot_num_procs}
#
# Never send an e-mail
#$ -m n
#$ -M marco.restelli@ipp.mpg.de
#
# Job name
#$ -N ${name_spec}
#
# Output file
#$ -o ${job_out}
#
# Project name, determines the job priority in the queue
#$ -P tokp

echo
echo \
"#><><><><><><><><><><><><><><><> Job file <><><><><><><><><><><><><><>#"
cat ${job_file}
echo \
"#><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>#"
echo

echo \
"**************** Preamble of the job ${name_spec} *********************"

echo
echo "hostname: \`hostname\`"
echo "    date: \`date\`"
echo "     pwd: \`pwd\`"

echo
echo "   SGE_ARCH: \${SGE_ARCH}"
echo "   HOSTNAME: \${HOSTNAME}"
echo "   HOSTTYPE: \${HOSTTYPE}"
echo "     JOB_ID: \${JOB_ID}"
echo "      QUEUE: \${QUEUE}"
echo "PE_HOSTFILE: \`cat \${PE_HOSTFILE}\`"

echo
echo \
"***************** Starting the job ${name_spec} ***********************"
echo

module load ${mpi_module}
mpiexec -perhost ${num_MPI_proc} -l -np ${tot_num_procs} \\
   ${execroot}/${executable} ${input_file_copy}

echo
echo "DONE! -- date: \`date\`"

EOF

# qsub appends the output, which can be confusing
if [ -e "${job_out}" ]
 then
  rm ${job_out}
fi

qsub ${job_file}

