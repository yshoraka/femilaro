function xi = bubble_xiadv(xy)
% Compute the xi coefficient for the bubble test case. The
% coefficients m,n,eps can be set in the function itself.
 m = 3;
 n = 5;
 eps = 0.5;

 xi = ( (xy(1,:).^m)/m + (xy(2,:).^n)/n )/eps;

return

