!! Copyright (C) 2016 Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> Hybrid 2D counterpart of \c mod_cgdofs
!!
!! The main simplification here is that, for a 2D grid, the grid
!! entities represented in \fref{mod_h2d_grid,t_h2d_grid} are enough
!! to describe all the degrees of freedom. Hence, there is no need to
!! build new \f$e\f$-dimensional entities for \f$e=0,\ldots,d\f$.
!<----------------------------------------------------------------------
module mod_h2d_cgdofs

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_integer, wp_mpi,   &
   mpi_status_size,       &
   mpi_irecv, mpi_isend,  &
   mpi_request_null,      &
   mpi_wait, mpi_waitall, &
   mpi_bcast

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave, &
   read_octave,  &
   read_octave_al

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_h2d_grid, t_ddc_h2d_grid

 use mod_h2d_base, only: &
   mod_h2d_base_initialized, &
   t_h2d_base

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_h2d_cgdofs_constructor, &
   mod_h2d_cgdofs_destructor,  &
   mod_h2d_cgdofs_initialized, &
   t_h2d_cgdofs, new_cgdofs, clear, &
   write_octave, &
   ! Only for testing ----
   mod_h2d_cgdofs_constructor_test

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Element degrees of freedom
 type :: t_edofs
  integer, allocatable :: edofs(:)
 end type t_edofs

 !> Collection of global degrees of freedom.
 !!
 !! All the comments for \fref{mod_cgdofs,t_cgdofs} applies also here.
 type :: t_h2d_cgdofs
  integer :: d, m
  integer :: ndofs
  integer, allocatable :: ndofsd(:)
  type(t_edofs), allocatable :: dofs(:)
  real(wp), allocatable :: x(:,:)
  !integer, allocatable :: dir_dofs(:,:)
  !integer, allocatable :: idir_dofs(:,:)
  !integer, allocatable :: nat_dofs(:)
 end type t_h2d_cgdofs

! Module variables

 ! public members
 logical, protected :: &
   mod_h2d_cgdofs_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_h2d_cgdofs'

 interface write_octave
   module procedure write_h2d_dofs_struct
 end interface write_octave

 interface clear
   module procedure cgdofs_clear
 end interface clear

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_h2d_cgdofs_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
      (mod_mpi_utils_initialized.eqv..false.) .or. &
      (mod_octave_io_initialized.eqv..false.) .or. &
       (mod_h2d_grid_initialized.eqv..false.) .or. &
       (mod_h2d_base_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_cgdofs_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_cgdofs_initialized = .true.
 end subroutine mod_h2d_cgdofs_constructor

 subroutine mod_h2d_cgdofs_constructor_test()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
      (mod_mpi_utils_initialized.eqv..false.) .or. &
   ! not needed if we avoid write -> mod_octave_io_initialized
       (mod_h2d_grid_initialized.eqv..false.) .or. &
       (mod_h2d_base_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_cgdofs_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_cgdofs_initialized = .true.
 end subroutine mod_h2d_cgdofs_constructor_test

!-----------------------------------------------------------------------
 
 subroutine mod_h2d_cgdofs_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_h2d_cgdofs_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_h2d_cgdofs_initialized = .false.
 end subroutine mod_h2d_cgdofs_destructor

!-----------------------------------------------------------------------

 subroutine new_cgdofs(dofs , grid,base , comm)
  type(t_h2d_grid),   intent(in)  :: grid
  type(t_h2d_base),   intent(in)  :: base
  !type(t_bcs),      intent(in)  :: bcs
  type(t_h2d_cgdofs), intent(out) :: dofs
  ! optional ddc specific arguments
  integer,            intent(in),  optional :: comm
  !type(t_ddc_grid),   intent(in),  optional :: ddc_grid
  !type(t_ddc_cgdofs), intent(out), optional :: ddc_dofs

  integer :: i, iv, is,iel, ie, id, i1,i2
  integer :: edofs_cnt

   !--------------------------------------------------------------------
   ! Set here some fields which are easily computed
   dofs%d = 2
   dofs%m = 2
   allocate( dofs%ndofsd(0:2) )
   dofs%ndofsd(0) = grid%nv ! vertex dofs
   ! side dofs, read from the first element in base
   dofs%ndofsd(1) = base%e(base%lb)%me%s_lagnodes(1)%nn*grid%ns
   ! element dofs
   dofs%ndofsd(2) = 0
   do ie=1,grid%ne
     associate( eme => base%e(grid%e(ie)%poly)%me )
     dofs%ndofsd(2) = dofs%ndofsd(2) + &
       ( size(eme%xnodes,2) - grid%e(ie)%poly*(1+eme%s_lagnodes(1)%nn) )
     end associate
   enddo
   dofs%ndofs = sum( dofs%ndofsd )

   allocate( dofs%dofs(grid%ne) )
   do ie=1,grid%ne
     associate( eme => base%e(grid%e(ie)%poly)%me )
     allocate( dofs%dofs(ie)%edofs( size( eme%xnodes,2 ) ) )
     end associate
   enddo
   allocate( dofs%x( dofs%m , dofs%ndofs ) )

   !--------------------------------------------------------------------
   ! Vertex degrees of freedom
   do iv=1,grid%nv
     dofs%x(:,iv) = grid%v(iv)%x
   enddo
   do ie=1,grid%ne
     dofs%dofs(ie)%edofs(1:grid%e(ie)%poly) = grid%e(ie)%iv
   enddo

   !--------------------------------------------------------------------
   ! Side degrees of freedom
   associate(  xs1 => base%e(base%lb)%me%s_lagnodes(1)%x(1,:) , &
               ns1 => base%e(base%lb)%me%s_lagnodes(1)%nn     , &
              stab => base%e(base%lb)%me%s_lagnodes(1)%stab     )
   do is=1,grid%ns
     i1 = grid%nv + (is-1)*ns1 + 1
     i2 = grid%nv +  is   *ns1
     do id=1,2 ! dimension
       dofs%x(id,i1:i2) = (1.0_wp-xs1)*grid%s(is)%v(1)%p%x(id) &
                        +         xs1 *grid%s(is)%v(2)%p%x(id) 
     enddo
     do iel=1,2 ! local elements
       associate(  ie => grid%s(is)%ie(iel)  , &
                  isl => grid%s(is)%isl(iel) )
       if(ie.gt.0) & ! the side can be on the boundary
         ! include the local side shift and the node permutation
         dofs%dofs(ie)%edofs( grid%e(ie)%poly + (isl-1)*ns1            &
                     + stab(grid%e(ie)%pi(isl),:) ) = (/( i, i=i1,i2 )/)
       end associate
     enddo
   enddo
   end associate

   !--------------------------------------------------------------------
   ! Element degrees of freedom
   edofs_cnt = 0 ! counter for internal dofs (elements are different)
   do ie=1,grid%ne
     associate( poly => grid%e(ie)%poly              )
     associate( eme  => base%e(poly)%me              )
     associate( nn   => eme%s_lagnodes(1)%nn         )
     associate( xni  => eme%xnodes(:,poly*(1+nn)+1:) ) ! internal dofs

     i1 = sum(dofs%ndofsd(0:1)) + edofs_cnt + 1
     i2 = sum(dofs%ndofsd(0:1)) + edofs_cnt + size(xni,2)
     edofs_cnt = edofs_cnt + size(xni,2)

     dofs%x(:,i1:i2) = grid%e(ie)%me%map( xni )

     dofs%dofs(ie)%edofs( grid%e(ie)%poly*(1+nn)+1 : ) = (/(i,i=i1,i2)/)

     end associate
     end associate
     end associate
     end associate
   enddo

 end subroutine new_cgdofs

!-----------------------------------------------------------------------

 pure subroutine cgdofs_clear(dofs)
  type(t_h2d_cgdofs), intent(out) :: dofs

  character(len=*), parameter :: &
    this_sub_name = 'cgdofs_clear'

   ! allocatable components automatically deallocated

 end subroutine cgdofs_clear

!-----------------------------------------------------------------------

 subroutine write_h2d_dofs_struct(dofs,var_name,fu)
  integer, intent(in) :: fu
  type(t_h2d_cgdofs), intent(in) :: dofs
  character(len=*), intent(in) :: var_name
 
  integer :: i
  character(len=*), parameter :: &
    this_sub_name = 'write_h2d_dofs_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 6' ! number of fields

   ! field 01 : d
   write(fu,'(a)')      '# name: d'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(dofs%d,'<cell-element>',fu)

   ! field 02 : m
   write(fu,'(a)')      '# name: m'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(dofs%m,'<cell-element>',fu)

   ! field 03 : ndofs
   write(fu,'(a)')      '# name: ndofs'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(dofs%ndofs,'<cell-element>',fu)

   ! field 04 : ndofsd
   write(fu,'(a)')      '# name: ndofsd'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(dofs%ndofsd,'r','<cell-element>',fu)

   ! field 05 : dofs
   write(fu,'(a)')      '# name: dofs'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   write(fu,'(a)')      '# name: <cell-element>'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(*(i0,:," "))') (/ 1 , size(dofs%dofs) /)
   do i=1,size(dofs%dofs)
     call write_octave(dofs%dofs(i)%edofs,'r','<cell-element>',fu)
   enddo

   ! field 06 : x
   write(fu,'(a)')      '# name: x'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(dofs%x,'<cell-element>',fu)

 end subroutine write_h2d_dofs_struct

!-----------------------------------------------------------------------

end module mod_h2d_cgdofs

