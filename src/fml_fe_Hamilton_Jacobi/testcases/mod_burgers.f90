!! Copyright (C) 2014 Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Burgers equation.
!!
!! \n
!!
!! Burgers equation as stated in <a
!! href="http://dx.doi.org/10.1016/j.jcp.2010.09.022">[Yan, Osher,
!! JCP, 2011]</a>.
!!
!! The equation is
!! \f{displaymath}{
!!  \partial_t \Phi + \frac{1}{2}\left( 1 + \sum_{i=1}^d
!!  \partial_{x^i}\Phi \right)^2 = 0
!! \f}
!! with initial condition
!! \f{displaymath}{
!!  \Phi(0,\underline{x}) = -\cos\left( \frac{\pi}{d}\sum_{i=1}^d x^i
!!  \right).
!! \f}
!<----------------------------------------------------------------------
module mod_burgers_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_burgers_test_constructor, &
   mod_burgers_test_destructor,  &
   mod_burgers_test_initialized, &
   test_name,        &
   test_description, &
   coeff_init, &
   coeff_h,    &
   coeff_a

 private

!-----------------------------------------------------------------------

 ! public members
 character(len=*), parameter   :: test_name = "burgers"
 character(len=100), protected :: test_description(1)

 logical, protected :: mod_burgers_test_initialized = .false.

 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_burgers'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_burgers_test_constructor(d)
  integer, intent(in) :: d

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_burgers_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   write(test_description(1),'(a,i1)') &
     'Burgers equation, dimension ',d

   mod_burgers_test_initialized = .true.
 end subroutine mod_burgers_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_burgers_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_burgers_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_burgers_test_initialized = .false.
 end subroutine mod_burgers_test_destructor

!-----------------------------------------------------------------------

 pure function coeff_init(x) result(phi0)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: phi0(size(x,2))

  real(wp), parameter :: pi = 3.1415926535897932384626433832795028_wp

   phi0 = -cos( pi/real(size(x,1),wp) * sum(x,1) )
   
 end function coeff_init

!-----------------------------------------------------------------------

 pure function coeff_h(p) result(h)
  real(wp), intent(in) :: p(:,:)
  real(wp) :: h(size(p,2))

   h = 0.5_wp * ( 1.0_wp + sum(p,1) )**2

 end function coeff_h

!-----------------------------------------------------------------------

 !> Stabilization coefficients \f$\underline{\alpha}\f$
 !!
 !! The stabilization coefficients are given by
 !! \f{displaymath}{
 !!  \alpha^i = \max( | \partial_{p^i} H | ),
 !! \f}
 !! yielding
 !! \f{displaymath}{
 !!  \alpha^i = \max\left( \left| 1 + \sum_{j=1}^d p^j \right| \right).
 !! \f}
 !! Here, for each point, we build the box
 !! \f{displaymath}{
 !!  \mathcal{B} = [\min(p^1)\,,\max(p^1)]\times \ldots \times
 !!     [\min(p^d)\,,\max(p^d)]
 !! \f}
 !! and compute the maximum on the corners of \f$\mathcal{B}\f$.
 pure function coeff_a(p) result(a)
  real(wp), intent(in) :: p(:,:,:)
  real(wp) :: a(size(p,2),size(p,3))

  integer :: l, i, j
  real(wp) :: box( size(p,2) , 2**size(p,2) )

   do l=1,size(p,3) ! loop over the points
     
     ! The corners of the box are given taking all the possible
     ! combinations of p^+ and p^-
     do i=0,2**size(p,2)-1
       do j=0,size(p,2)-1
         box(j+1,i+1) = p( mod(i/(2**j),2)+1 , j+1,l)
       enddo
     enddo

     ! The coefficient is the same for all the variables
     a(:,l) = maxval( abs( 1.0_wp + sum(box,1) ) )

   enddo

 end function coeff_a

!-----------------------------------------------------------------------

end module mod_burgers_test

