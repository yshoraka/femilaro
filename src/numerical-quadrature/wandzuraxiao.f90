program wandzuraxiao

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   write_octave, &
   read_octave,  &
   read_octave_al

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor

 use mod_psymbasis, only: &
   mod_psymbasis_constructor, &
   mod_psymbasis_destructor

 use mod_orbits, only: &
   mod_orbits_constructor, &
   mod_orbits_destructor,  &
   collect_xyw

 use mod_wandsystem, only: &
   mod_wandsystem_constructor, &
   mod_wandsystem_destructor,  &
   xstatus,   &
   lhs,       &
   jf,        &
   status_set

 use mod_gmres, only: &
   mod_gmres_constructor, &
   mod_gmres_destructor

 use mod_newton, only: &
   mod_newton_constructor, &
   mod_newton_destructor,  &
   newton

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! integer :: ierr, ierrr
 integer :: order, fu, ierr, noc1, noc2, noc3, nargin
 logical :: reached_maxiter
 integer :: niter
 real(wp), allocatable :: orx(:), ory(:), orw(:)
 real(wp), allocatable :: res(:), xx(:), yy(:), ww(:)
 character(len=100) :: input_file
 character(len=100) :: output_file
 character(len=*), parameter :: this_prog_name='wandzuraxiao'
 character(len=100000) :: message(4)
 character(len=13) :: cformat
 
!-----------------------------------------------------------------------
! Call constructors
 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_sympoly_constructor()

 call mod_gmres_constructor()

 call mod_newton_constructor()
!-----------------------------------------------------------------------

 ! 0) parse input arguments
 nargin = command_argument_count()
 select case(nargin)
  case(0)
   write(*,*) 'Input file name:'
   read(*,'(A)') input_file
   write(*,*) 'Output file name'
   read(*,'(A)') output_file
  case(2)
   call get_command_argument(1,value=input_file)
   call get_command_argument(2,value=output_file)
  case default
   write(message(1),'(a)') 'Wrong number of arguments.'
   write(message(2),'(a)') ' Required arguments are:'
   write(message(3),'(a)') '  input file (.octxt)'
   write(message(4),'(a)') '  output file (.octxt)'
   call error(this_prog_name,'',message(1:4))
 end select

 ! 1) read the data
 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file), &
      status='old',action='read',form='formatted',iostat=ierr)
  call read_octave(order,'order',fu) ! order of the quadrature rule
  call read_octave(noc1,'noc1',fu) ! # traj. class 1
  call read_octave(noc2,'noc2',fu) ! # traj. class 2
  call read_octave(noc3,'noc3',fu) ! # traj. class 3
  call read_octave_al(orx,'orx',fu) ! x coord. for the orbits
  call read_octave_al(ory,'ory',fu) ! y coord. for the orbits
  call read_octave_al(orw,'orw',fu) ! weights
 close(fu,iostat=ierr)

 ! 2) construct polynomial basis and orbits
 call mod_psymbasis_constructor(order)

 call mod_orbits_constructor(noc1,noc2,noc3,orw,orx,ory)

 ! 3) solve the nonlinear problem
 call mod_wandsystem_constructor()

 call newton(xstatus,lhs,jf,status_set,5e-32_wp,.true.,1000,100, &
             reached_maxiter,niter,res)

 write(message(1),'(A)') 'Solution of the nonlinear system'
 write(message(2),'(A,I3,A,L1)') ' # iterations: ',niter, &
                        '; reached maximum:',reached_maxiter
 write(cformat,'(A,I5,A)') '(A,',niter+1,'E9.1)'
 write(message(3),cformat) ' residuals:',res
 call info(this_prog_name,'',message(1:3))

 ! 4) collect the results
 call collect_xyw(xx,yy,ww)
 ww = ww/(3.0_wp*sqrt(3.0_wp)/4.0_wp) ! sum = 1
 open(fu,file=trim(output_file), &
      status='replace',action='write',form='formatted',iostat=ierr)
  call write_octave(xx,'c','xG',fu)
  call write_octave(yy,'c','yG',fu)
  call write_octave(ww,'c','wG',fu)
 close(fu,iostat=ierr)

 write(message(1),'(A,A,A)') 'Output witten in "',trim(output_file),'".'
 call info(this_prog_name,'',message(1))

!-----------------------------------------------------------------------
! Call destructors
 call mod_wandsystem_destructor()

 call mod_orbits_destructor()

 call mod_psymbasis_destructor()

 call mod_newton_destructor()

 call mod_gmres_destructor()

 call mod_sympoly_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 call mod_kinds_destructor()

 call mod_messages_destructor()
!-----------------------------------------------------------------------

end program wandzuraxiao

