function [Ux,gUx] = elem_interp(base,e,U,x)
% [Ux,gUx] = elem_interp(base,e,U,x)
%
% Element based interpolation: e is an element, U is a row vector with
% the modal degrees of freedom and x is the array of the points where
% the interpolated values are sought. The output are the function
% values Ux and the function gradients gUx.

 % Remap on the reference element
 xx = zeros(size(x));
 for i=1:e.d
   xx(i,:) = x(i,:) - e.x0(i);
 end
 xi = e.bi*xx;

 % evaluate the basis function and the gradients
 np = size(x,2);
 PHI  = zeros(    base.pk,np);
 gPHI = zeros(e.d,base.pk,np);
 for i=1:base.pk
   PHI(i,:) = ev_pol(base.p_s{i},xi);
   for j=1:e.d
     gPHI(j,i,:) = ev_pol(base.gradp_s{j,i},xi);
   end
 end
 for j=1:np
   gPHI(:,:,j) = e.bi' * gPHI(:,:,j);
 end
 
 Ux = U * PHI;
 gUx = zeros(e.d,np);
 for j=1:e.d
   gUx(j,:) = U * permute(gPHI(j,:,:),[2 3 1]);
 end

return
