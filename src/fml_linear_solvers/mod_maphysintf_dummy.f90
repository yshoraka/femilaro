!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Dummy version of \c mod_maphysintf to avoid linking MaPHyS.
!!
!! \n
!!
!! The constructor, destructor and clean functions can be called,
!! while the remaining functions return an error. The use statements
!! are the same as in the true module, to simplify debugging and
!! testing with and without MaPHyS.
!<----------------------------------------------------------------------
module mod_maphysintf

!-----------------------------------------------------------------------

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_sparse, only: &
   mod_sparse_initialized, &
   ! sparse types
   t_col, t_tri,&
   col2tri,     &
   clear

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_reduce, wp_mpi, mpi_sum, &
   mpi_bcast

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_output_control, only: &
   mod_output_control_initialized, &
   elapsed_format, base_name

 use mod_linsolver_base, only: &
   mod_linsolver_base_initialized, &
   c_linpb

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_maphysintf_constructor, &
   mod_maphysintf_destructor,  &
   mod_maphysintf_initialized, &
   c_maphyspb

 private

!-----------------------------------------------------------------------

 type :: mph_maphys_t
   ! For the real MaPHyS, this type is defined in *mph_maphys_type.F90
 end type mph_maphys_t

 type, extends(c_linpb), abstract :: c_maphyspb
  logical :: distributed
  integer :: gn
  type(t_col), pointer :: m
  real(wp), pointer :: rhs(:)
  integer, pointer :: gij(:)
  logical :: transposed_mat = .false.
  integer :: mpi_comm
  logical :: write_mat = .false.
  integer, allocatable, private :: l2g_map(:)
  type(mph_maphys_t), private :: mph
  logical, private :: sys_set = .false.
 contains
  procedure, pass(s) :: factor => maphys_factor
  procedure, pass(s) :: solve  => maphys_solve
  procedure, pass(s) :: clean  => maphys_clean
  procedure, nopass :: working_implementation => maphys_wi
  procedure, nopass :: xassign => xassign_dummy
 end type c_maphyspb

 logical, protected :: &
   mod_maphysintf_initialized = .false.
 character(len=*), parameter :: &
   errormsg = &
     'This is not a real MaPHyS interface and should not be called.'
 character(len=*), parameter :: &
   this_mod_name = 'mod_maphysintf_dummy'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_maphysintf_constructor(init_maphys)
  logical, intent(in), optional :: init_maphys

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
         (mod_sparse_initialized.eqv..false.) .or. &
      (mod_mpi_utils_initialized.eqv..false.) .or. &
 (mod_output_control_initialized.eqv..false.) .or. &
 (mod_linsolver_base_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_maphysintf_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_maphysintf_initialized = .true.
 end subroutine mod_maphysintf_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_maphysintf_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_maphysintf_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_maphysintf_initialized = .false.
 end subroutine mod_maphysintf_destructor

!-----------------------------------------------------------------------

  pure function maphys_wi()
   logical :: maphys_wi
    maphys_wi = .false.
  end function maphys_wi

!-----------------------------------------------------------------------

 subroutine maphys_factor(s,phase)
  class(c_maphyspb), intent(inout) :: s
  character(len=*), intent(in), optional :: phase
 
  character(len=*), parameter :: &
    this_sub_name = 'maphys_factor'

   call error(this_sub_name,this_mod_name,errormsg)
 
 end subroutine maphys_factor
 
!-----------------------------------------------------------------------
 
 subroutine maphys_solve(s,x,x1d)
  class(c_maphyspb), intent(inout) :: s
  class(c_stv),     intent(inout), optional :: x
  real(wp),         intent(inout), optional :: x1d(:)

  character(len=*), parameter :: &
    this_sub_name = 'maphys_solve'

   call error(this_sub_name,this_mod_name,errormsg)
 
 end subroutine maphys_solve
 
!-----------------------------------------------------------------------
 
 subroutine maphys_clean(s)
  class(c_maphyspb), intent(inout) :: s

  character(len=*), parameter :: &
    this_sub_name = 'maphys_clean'

   ! nothing to do
 
 end subroutine maphys_clean
 
!-----------------------------------------------------------------------

 subroutine xassign_dummy(x,s,x_vec)
  real(wp),       intent(in) :: x_vec(:)
  class(c_linpb), intent(inout) :: s
  class(c_stv),   intent(inout) :: x

  character(len=*), parameter :: &
    this_sub_name = 'xassign_dummy'

   call error(this_sub_name,this_mod_name,errormsg)

 end subroutine xassign_dummy

!-----------------------------------------------------------------------

end module mod_maphysintf

