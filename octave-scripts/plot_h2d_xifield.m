function data = plot_h2d_xifield(grid,base,f,key,S,varargin)
% data = plot_h2d_xifield(grid,base,f,key,S)
%
% Plot the field "key" of the data collection f, containing the values
% of the fields at the element quadrature points.
%
% The solution is reconstructed as discussed in proj_h2d_xi2nodes and
% then plotted. According to the rank of the selected field, either a
% surface plot or a quiver (vector) plot is chosen. Tensor quantities
% presently are not supported.
%
% S: this argument is passed to quiver and quiver3 for vector plots;
%    use 0 to avoid scaling the vectors; it is ignored for scalar
%    plots
%
% Notice that quadrilateral elements are visualized as two triangles.

 if(nargin>5)
   skip = varargin{1};
 else
   skip = 1;
 endif

 % Interpolate the selected field

 fnodal = proj_h2d_xi2nodes(grid,base,f,key);

 xy = [grid.v(:).x];
 data_rank = size(fnodal.(key));
 switch length(data_rank)
  case 2 % scalar/vector plot
   switch data_rank(2)
    case 1 % scalar plot
     plot_h2d_nodalfield(grid,base,fnodal.(key))
    case 2 % 2D vector
     % quiver/quiver3 have problems with vanishing data
     if(max(abs(fnodal.(key)(:,1:2)(:))) == 0)
       printf( ...
  "\nThe selcted field is identically zero, nothing to plot.\n\n");
     else
       quiver( xy(1,1:skip:end) , xy(2,1:skip:end) , ...
         fnodal.(key)(1:skip:end,1) , fnodal.(key)(1:skip:end,2) ,S);
     endif
    case 3 % 3D vector
     if(max(abs(fnodal.(key)(:,1:3)(:))) == 0)
       printf( ...
  "\nThe selcted field is identically zero, nothing to plot.\n\n");
     else
       quiver3(xy(1,:),xy(2,:),0*xy(1,:) , ...
         fnodal.(key)(:,1),fnodal.(key)(:,2),fnodal.(key)(:,3),S);
     endif
    otherwise
     warning(["Unsupported data rank ",num2str(data_rank),"."])
   endswitch
  case 3 % tensor
   warning(["Unsupported data rank ",num2str(data_rank),"."])
  otherwise
   warning(["Unsupported data rank ",num2str(data_rank),"."])
 endswitch

 if(nargout>=1)
   data = fnodal.(key);
 endif

return

