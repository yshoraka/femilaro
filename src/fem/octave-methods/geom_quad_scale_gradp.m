function sgradp = geom_quad_scale_gradp(me,xig,gradp)

 for l=1:size(xig,2)
   % 2x2 inverse, including transposition
   jac_it(1,1) =   me.b(2,2) + me.c(2)*xig(1,l);
   jac_it(1,2) = - me.b(2,1) - me.c(2)*xig(2,l);
   jac_it(2,1) = - me.b(1,2) - me.c(1)*xig(1,l);
   jac_it(2,2) =   me.b(1,1) + me.c(1)*xig(2,l);

   % notice that the 2x2 det formula can be applied also to jac_it
   det = jac_it(1,1)*jac_it(2,2) - jac_it(1,2)*jac_it(2,1);
   jac_it = jac_it / det;

   sgradp(:,:,l) = jac_it * gradp(:,:,l);
 end

return
