# Copyright (C) 2009,2010,2011,2012  Marco Restelli
#
# This file is part of:
#   FEMilaro -- Finite Element Method toolkit
#
# FEMilaro is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# FEMilaro is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
#
# author: Marco Restelli                   <marco.restelli@gmail.com>

# List of the object files of the present folder
OBJ_VP_TEST= mod_landau_damping.o \
             mod_bump_on_tail.o \
             mod_two_stream_inst.o \
             mod_poisson_test.o
OBJ_VP= mod_f_state.o \
     mod_poisson_pb.o \
     mod_vp_ode.o \
     mod_testcases.o \
     vp_dg.o \
     $(OBJ_VP_TEST)
OBJ_VP_HJ= mod_f_state.o \
     mod_poisson_pb.o \
     mod_vp_hj_ode.o \
     mod_testcases.o \
     vp_hj_dg.o \
     $(OBJ_VP_TEST)

# General settings -----------------------------------

PWD:=$(shell pwd)
BUILDDIR:=$(BUILDDIR)/VE

# Include the fml libraries in the linker options
LIBS:= $(patsubst lib%.a,-l%,$(FML_LIBS)) $(LIBS)
LDFLAGS:=$(LDFLAGS) -L$(LIBDIR)

# VPATH includes:
#   PWD for the source files (included by default)
#   PWD/testcases for the source files of the testcases
#   BUILDDIR for the .o prerequisites
#   LIBDIR for the FML_LIBS libraries
#   BINDIR for the final executable
VPATH:=./testcases $(BUILDDIR) $(LIBDIR) $(BINDIR)

# clear unfinished targets
.DELETE_ON_ERROR:
# define the phony targets
.PHONY: clean

# Main targets ---------------------------------------

vp-dg: $(OBJ_VP)
	cd $(BUILDDIR) && \
	  $(LD) $(LDFLAGS) $(OBJ_VP) $(LIBS) -o $(BINDIR)/vp-dg

vp-hj-dg: $(OBJ_VP_HJ)
	cd $(BUILDDIR) && \
	  $(LD) $(LDFLAGS) $(OBJ_VP_HJ) $(LIBS) -o $(BINDIR)/vp-hj-dg

# General rules:
# 1) how to generate a .o from the corresponding .f90
# 2) each file here depends on the used libraries FML_LIBS
%.o: %.f90 $(FML_LIBS)
	cd $(BUILDDIR) && \
	  $(FC) $(FFLAGS) $(MODFLAG)$(LIBDIR) -c $(PWD)/$<

clean:
	$(RM) $(BUILDDIR)/*.o $(BUILDDIR)/*.mod \
	  $(BINDIR)/vp-dg

# Specific dependencies: because of USE directives ---

# Notice: here we list only the prerequisites which are not already
# included in any of the FML_LIBS libraries, which are a prerequisite
# for all the .o files listed in this Makefile and which must be
# defined in the parent Makefile.

mod_poisson_pb.o: \
  mod_f_state.o

mod_vp_ode.o: \
  mod_f_state.o \
  mod_poisson_pb.o

mod_vp_hj_ode.o: \
  mod_f_state.o \
  mod_testcases.o \
  mod_poisson_pb.o

mod_testcases.o: $(OBJ_VP_TEST)

vp_dg.o: \
  mod_f_state.o \
  mod_vp_ode.o
  
vp_hj_dg.o: \
  mod_f_state.o \
  mod_vp_hj_ode.o
