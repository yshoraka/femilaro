"""
Python copy of mod_h2d_base.f90
"""

import numpy as np
from mod_h2d_master_el import *

#----------------------------------------------------------------------

# Collect the base master element constructors
me_base_constructors = \
  [ None , None , t_h2d_me_base_tri , t_h2d_me_base_quad ]

#----------------------------------------------------------------------

class t_h2d_base():
  def __init__(self,data):
    copy_array_field( self , 'bounds' , data , (2,) )
    self.e = np.array(                                         \
      [ me_base_constructors[i-1](data["e"][i-self.bounds[0]]) \
        for i in range(self.bounds[0],self.bounds[1]+1) ] )

#----------------------------------------------------------------------

