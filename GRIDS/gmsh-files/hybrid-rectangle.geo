// Mesh size
Lx = 3.0;
Ly = 1.0;

// grid resolution
Nx = 20;
Ny = 34;

// Build the geometry
SW = newp; Point(SW) = {0.0,-Ly/2.0,0.0};
SE = newp; Point(SE) = { Lx,-Ly/2.0,0.0};
NE = newp; Point(NE) = { Lx, Ly/2.0,0.0};
NW = newp; Point(NW) = {0.0, Ly/2.0,0.0};
WW = newp; Point(WW) = {0.0,  0.0  ,0.0};
EE = newp; Point(EE) = { Lx,  0.0  ,0.0};

sSS = newc; Line(sSS) = {SW,SE}; 
sSE = newc; Line(sSE) = {SE,EE}; 
sNE = newc; Line(sNE) = {EE,NE}; 
sNN = newc; Line(sNN) = {NE,NW}; 
sNW = newc; Line(sNW) = {NW,WW}; 
sSW = newc; Line(sSW) = {WW,SW}; 
seq = newc; Line(seq) = {WW,EE}; 

NorthLoop = newreg; Line Loop(NorthLoop) = { seq,sNE,sNN,sNW};
SouthLoop = newreg; Line Loop(SouthLoop) = {sSS,sSE,-seq,sSW};

North = news; Plane Surface(North) = {NorthLoop};
South = news; Plane Surface(South) = {SouthLoop};

// Define the 1D grid on the curves
Transfinite Line{sSS} = Nx;
Transfinite Line{seq} = Nx;
Transfinite Line{sNN} = Nx;
Transfinite Line{sSW} = Ceil(Ny/2);
Transfinite Line{sSE} = Ceil(Ny/2);
Transfinite Line{sNW} = Ceil(Ny/2);
Transfinite Line{sNE} = Ceil(Ny/2);

// Specify that the mesh must be transfinite
Transfinite Surface{North};
Transfinite Surface{South} Alternate;

// Recombine triangles -> quadrilaterals in the upper region
Recombine Surface{North};

// Display boundary labels
View "Boundary labels" {
  // somehow, to use model coordinates we need a T3 object
  T3( Lx/2.0 ,    0.0 , 0.0 , 1 ){ Sprintf("%.0f",seq) };
  T3( Lx/2.0 , -Ly/2.0, 0.0 , 1 ){ Sprintf("%.0f",sSS) };
  T3( Lx/2.0 ,  Ly/2.0, 0.0 , 1 ){ Sprintf("%.0f",sNN) };
  T3(  0.0   ,  Ly/4.0, 0.0 , 1 ){ Sprintf("%.0f",sNW) };
  T3(   Lx   ,  Ly/4.0, 0.0 , 1 ){ Sprintf("%.0f",sNE) };
  T3(  0.0   , -Ly/4.0, 0.0 , 1 ){ Sprintf("%.0f",sSW) };
  T3(   Lx   , -Ly/4.0, 0.0 , 1 ){ Sprintf("%.0f",sSE) };
};

