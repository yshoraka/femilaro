!! Copyright (C) 2012  Carlo de Falco
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Carlo de Falco

!>\brief
!!
!! Define the class vector_extrapolation derived from c_stv.
!!
!! \n
!!
!! This module defines an abstract class derived from the abstract 
!! type \c c_stv which adds methods for performing orthogonalization
!! of state vectors and vector extrapolation.
!!

!<----------------------------------------------------------------------
module mod_vector_extrapolation

  !-----------------------------------------------------------------------

  use mod_messages, only: &
       mod_messages_initialized, &
       error,   &
       warning, &
       info

  use mod_kinds, only: &
       mod_kinds_initialized, &
       wp

  use mod_state_vars, only: &
       c_stv
       
  !-----------------------------------------------------------------------
  
  implicit none
  
  !-----------------------------------------------------------------------
  
  ! Module interface
  
  public :: &
       mod_vector_extrapolation_constructor, &
       mod_vector_extrapolation_destructor,  &
       mod_vector_extrapolation_initialized, &
       vector_extrapolation
  
  private
  
  !-----------------------------------------------------------------------

  ! Module types and parameters

  ! public members

  !> Vector extrapolation context

  type, abstract :: vector_extrapolation
   contains


  end type vector_extrapolation

  ! public members
  logical, protected ::                           &
       mod_vector_extrapolation = .false.
  ! private members
  character(len=*), parameter ::                  &
       this_mod_name = 'mod_vector_extrapolation'

  !-----------------------------------------------------------------------

contains

  !-----------------------------------------------------------------------

  subroutine mod_vector_extrapolation_constructor ()

    character(len=*), parameter :: &
         this_sub_name = 'constructor'

    !Consistency checks ---------------------------
    if (mod_messages_initialized.eqv..false.     .or.      &
         mod_kinds_initialized.eqv..false.       .or.      &
         mod_state_vars_initialized.eqv..false.)  then
       call error(this_sub_name,this_mod_name,             &
            'Not all the required modules are initialized.')
    endif

    if (mod_state_vars_initialized.eqv..true.) then
       call warning(this_sub_name,this_mod_name, &
            'Module is already initialized.')
    endif
    !----------------------------------------------

    mod_vector_extrapolation_initialized = .true.
  end subroutine mod_vector_extrapolation_constructor

  !-----------------------------------------------------------------------

  subroutine mod_vector_extrapolation_destructor ()
    character(len=*), parameter :: &
         this_sub_name = 'destructor'

    !Consistency checks ---------------------------
    if (mod_vector_extrapolation_initialized.eqv..false.) then
       call error (this_sub_name,this_mod_name,                &
            'This module is not initialized.')
    endif
    !----------------------------------------------

    mod_vector_extrapolation_initialized = .false.
  end subroutine mod_vector_extrapolation_destructor


end module mod_vector_extrapolation

