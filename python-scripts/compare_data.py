"""
Comparing complicated objects in python is not straightforward. This
module defines a function  compare(x,y)  which handles the most
frequent data types.

The comparison takes into account both the type and the value of the
two objects.

TODO: the output messages can be improved; a possibility would be
always returning a tuple (Bool,msg) where the msg component is
meaningful only if a difference has been found.
"""

import numpy as np

#-----------------------------------------------------------------------

def compare_iter(i1,i2,idx, spec=("data","fields")):
  """
  Compare two iterable objects.

  Often one would like writing (for dictionaries)
  
   return all( [ compare(d1[k],d2[k]) for k in d1.keys() ] )
  
  and similar for other data structures. However, this has two
  problems:
  a) it does not allow tracking which fields are different
  b) it has to evaluate all the list, even after finding a False

  This function is meant for such situations.
  """

  are_equal = True # for empty objects
  for k in idx:
    are_equal = compare( i1[k] , i2[k] )
    if not are_equal:
      print(spec[0]+': the two '+spec[1]+' "'+str(k)+'" do not match.')
      break

  return are_equal

#-----------------------------------------------------------------------

def compare_dict(d1,d2):

  unmatched_keys = set(d1.keys()) ^ set(d2.keys())
  if unmatched_keys:
    print("The following dict. keys do not match: ",unmatched_keys)
    return False
  
  return compare_iter( d1,d2 , d1.keys() , spec=("dict","fields") )

#-----------------------------------------------------------------------

def compare_np_ndarray(a1,a2):
  """
  For numpy arrays, one could use == but there are corner cases
  concerning empty arrays; using np.array_equal is safer.

  For arrays containing objects, it is better to compare the objects
  one-by-one.

  Finally, we add a check on the dtype, since in numpy 2 == 2.0 .
  """

  if not a1.dtype is a2.dtype:
    print("The following ndarrays have different dtypes:")
    print(" a1.dtype: ",a1.dtype,"; a1 = ",a1)
    print(" a2.dtype: ",a2.dtype,"; a2 = ",a2)
    return False

  if a1.dtype is np.dtype('O'):
    if a1.shape != a2.shape:
      print("Ndarray shape mismatch:")
      print(" a1.shape: ",a1.shape,"; a1 = ",a1)
      print(" a2.shape: ",a2.shape,"; a2 = ",a2)
      return False

    return compare_iter( a1,a2 , range(len(a1)) ,    \
                         spec=("ndarray","elements") )
  else:
    equal = np.array_equal(a1,a2)
    if not equal:
      print("The following ndarrays are different:")
      print(" a1.dtype: ",a1.dtype,"; a1 = ",a1)
      print(" a2.dtype: ",a2.dtype,"; a2 = ",a2)
    return equal

#-----------------------------------------------------------------------

def compare_list_tuple(l1,l2):
  if len(l1) != len(l2):
    print("List/tuple lenght mismatch:")
    print(" len(l1): ",len(l1),"; l1 = ",l1)
    print(" len(l2): ",len(l2),"; l2 = ",l2)
    return False

  spec = ( "list" if type(l1) is list else "tuple" , "elements" )
  return compare_iter( l1,l2 , range(len(l1)) , spec=spec )

#-----------------------------------------------------------------------

def compare(x,y):
  """
  The main function for data comparison.
  """

  if not type(x) is type(y):
    print("Found different types: "+str(type(x))+" vs "+str(type(y)))
    return False
  
  if type(x) is dict:
    return compare_dict(x,y)

  if type(x) is np.ndarray:
    return compare_np_ndarray(x,y)
  
  if (type(x) is list) or (type(x) is tuple):
    return compare_list_tuple(x,y)
  
  # For classes, we reduce them to dictionaries
  if hasattr(x,'__dict__'):
    return compare_dict( x.__dict__ , y.__dict__ )
    
  try:
    equal = x == y
    if not equal:
      print("The following objects are different:")
      print(" type(x): ",type(x),"; x = ",x)
      print(" type(y): ",type(y),"; y = ",y)
    return equal
  except:
    print("Unable to make a comparison")
    return None

