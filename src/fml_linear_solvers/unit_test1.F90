!**************************************************************
!  Copyright INRIA
!  Authors : 
!     CALVI project team
!  
!  This code SeLaLib (for Semi-Lagrangian-Library) 
!  is a parallel library for simulating the plasma turbulence 
!  in a tokamak.
!  
!  This software is governed by the CeCILL-B license 
!  under French law and abiding by the rules of distribution 
!  of free software.  You can  use, modify and redistribute 
!  the software under the terms of the CeCILL-B license as 
!  circulated by CEA, CNRS and INRIA at the following URL
!  "http://www.cecill.info". 
!**************************************************************

! Test the linear solvers. This test tries those linear solvers
! defined in  mod_linsolver  for which the solution can be computed as
! a 1D real array. The function  working_implementation()  is used to
! check which solvers are really implemented and which ones are simply
! dummy versions.
!
! We run here a parallel test with two processors.
!
! Compared to the  test_fml_linear_solvers2  test, here the linear
! system solution is returned as a 1D real array. This makes this
! example simpler, but also less general.

program test_fml_linear_solvers1

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   elapsed_format, base_name

 use mod_mpi_utils, only: &
   mod_mpi_utils_constructor, &
   mod_mpi_utils_destructor,  &
   mpi_logical, mpi_integer, wp_mpi, &
   mpi_comm_world, mpi_status_size,  &
   mpi_sum, mpi_max, mpi_lor, mpi_land, &
   mpi_init, mpi_init_thread,        &
   mpi_thread_single, mpi_thread_multiple, &
   mpi_finalize,                     &
   mpi_comm_size, mpi_comm_rank,     &
   mpi_barrier, mpi_reduce

 use mod_sparse, only: &
   mod_sparse_constructor, &
   mod_sparse_destructor,  &
   t_col, transpose, matmul, &
   new_tri, tri2col, clear

 use mod_linsolver, only: &
   mod_linsolver_constructor, &
   mod_linsolver_destructor,  &
   c_linpb, c_mumpspb, c_pastixpb, c_umfpackpb

 implicit none

 character(len=*), parameter :: &
   this_prog_name = 'test_fml_linear_solvers1'

 logical :: all_test_passed, g_all_test_passed
 integer :: mpi_nd, mpi_id, mpi_thread_provided, ierr
 character(len=1000) :: message

 !> global dimension of the system
 integer :: problem_global_size
 !> local to global indexes
 integer, allocatable, target :: gij(:)
 !> linear problem
 class(c_linpb), allocatable :: linpb
 !> system unknown
 real(wp), allocatable :: x(:)
 !> system matrix
 type(t_col), save, target :: mmmt
 !> system RHS
 real(wp), allocatable, target :: rhs(:)
 !> exact solution
 real(wp), allocatable :: xex(:)

 ! Linear solvers
 !> See \c mod_mumpsintf for details.
 type, extends(c_mumpspb) :: t_linpb_mumps
  ! nothing to add
 end type t_linpb_mumps
 !> See \c mod_pastixintf for details.
 type, extends(c_pastixpb) :: t_linpb_pastix
  ! nothing to add
 end type t_linpb_pastix
 !> See \c mod_umfintf for details.
 type, extends(c_umfpackpb) :: t_linpb_umfpack
  ! nothing to add
 end type t_linpb_umfpack


  !---------------------------------------------------------------------
  ! General setup
  ! - MPI and all the required modules are initialized
  call mpi_init_thread(mpi_thread_multiple,mpi_thread_provided,ierr)
  call mpi_comm_size(mpi_comm_world,mpi_nd,ierr)
  call mpi_comm_rank(mpi_comm_world,mpi_id,ierr)
  if(mpi_nd.ne.2) call error(this_prog_name,"",    &
      'This test should be run with two processors')

  call mod_messages_constructor()
  call mod_kinds_constructor()
  call mod_output_control_constructor('')
  call mod_mpi_utils_constructor()
  call mod_sparse_constructor()
  call mod_linsolver_constructor()

  !---------------------------------------------------------------------
  ! Define the linear system
  ! - each process builds its own part of the matrix; the complete
  !  matrix is the sum of the local ones
  ! - the local matrices are transposed: this is good for the
  !  iterative solvers
  problem_global_size = 5
  build_matrix: select case(mpi_id)
   case(0)
    ! local matrix
    ! M0 = [ 1   0   8   3
    !        0   2   0   0
    !        0   1   3   0
    !        0   1   0   4 ]
    mmmt = transpose( tri2col( new_tri(             &
       n = 4, m = 4,                                &
      ti = (/ 1 , 2 , 3 , 4 , 1 , 3 , 1 , 4 /) - 1, & ! zero based indx
      tj = (/ 1 , 2 , 2 , 2 , 3 , 3 , 4 , 4 /) - 1, &
      tx = (/1.0_wp,2.0_wp,1.0_wp,1.0_wp,8.0_wp,3.0_wp,3.0_wp,4.0_wp/) &
                    ) ) )
    ! local to global indexing
    allocate(gij(0:3)); gij = (/ 1 , 2 , 3 , 4 /) - 1 ! zero based
    ! local right-hand-side
    allocate(rhs(4)); rhs = (/ 37.0_wp , 4.0_wp , -28.0_wp , 5.0_wp /)
    allocate(xex(4)); xex = (/ 1.0_wp , 2.0_wp , 3.0_wp , 4.0_wp /)
   case(1)
    ! local matrix
    ! M1 = [ -3   0  -6
    !         0  -2   0
    !         2   0  -1 ]
    mmmt = transpose( tri2col( new_tri(             &
       n = 3, m = 3,                                &
      ti = (/ 1 , 3 , 2 , 1 , 3 /) - 1,             & ! zero based indx
      tj = (/ 1 , 1 , 2 , 3 , 3 /) - 1,             &
      tx = (/-3.0_wp,2.0_wp,-2.0_wp,-6.0_wp,-1.0_wp/) &
                    ) ) )
    ! local to global indexing
    allocate(gij(0:2)); gij = (/ 3 , 4 , 5 /) - 1 ! zero based
    ! local right-hand-side
    allocate(rhs(3)); rhs = (/ 0.0_wp , 5.0_wp , 1.0_wp /)
    allocate(xex(3)); xex = (/ 3.0_wp , 4.0_wp , 5.0_wp /)
  end select build_matrix
  allocate(x(size(gij)))
  ! The resulting global matrix is
  !
  ! M = [ 1   0   8   3   0
  !       0   2   0   0   0
  !       0   1   0   0  -6
  !       0   1   0   2   0
  !       0   0   2   0  -1 ]
  !
  ! with RHS and solution
  !
  !       [ 37]            [1]
  !       [  4]            [2]
  ! rhs = [-28]        x = [3]
  !       [ 10]            [4]
  !       [  1]            [5]

  !---------------------------------------------------------------------
  ! Test the linear solvers
  ! - for each solver, compute the residual
  all_test_passed = .true.
  call mpi_barrier(mpi_comm_world,ierr)

  !---------------------------------------------------------------------
  ! GMRES solver: can not be used without a c_stv result


  !---------------------------------------------------------------------
  ! MUMPS solver
  allocate(t_linpb_mumps::linpb)
  if(linpb%working_implementation()) then
    call mpi_barrier(mpi_comm_world,ierr)
    x = 0.0_wp ! initial guess (not used by MUMPS)
    select type(linpb); type is(t_linpb_mumps)
      linpb%distributed    = .true.
      linpb%poo            = 3 ! SCOTCH
      linpb%transposed_mat = .true.
      linpb%gn             = problem_global_size
      linpb%m              => mmmt
      linpb%rhs            => rhs
      linpb%gij            => gij
      linpb%mpi_comm       = mpi_comm_world
    end select
    call linpb%factor('analysis')
    call linpb%factor('factorization')
    call linpb%solve(x1d=x)
    call linpb%clean()

    write(message,'(a,e23.15)') &
      "Done MUMPS; error ", norm2( xex - x )
    call info(this_prog_name,"",message)
    if( norm2( xex - x ) .gt. 1.0e-8 ) all_test_passed = .false.
    call mpi_barrier(mpi_comm_world,ierr)
  endif
  deallocate(linpb)


  !---------------------------------------------------------------------
  ! PaStiX solver
  allocate(t_linpb_pastix::linpb)
  if(linpb%working_implementation()) then
    call mpi_barrier(mpi_comm_world,ierr)
    x = 0.0_wp ! initial guess (not used)
    select type(linpb); type is(t_linpb_pastix)
      linpb%transposed_mat = .true.
      linpb%gn             = problem_global_size
      linpb%m              => mmmt
      linpb%rhs            => rhs
      linpb%gij            => gij
      linpb%mpi_comm       = mpi_comm_world
    end select
    call linpb%factor('analysis')
    call linpb%factor('factorization')
    call linpb%solve(x1d=x)
    call linpb%clean()

    write(message,'(a,e23.15)') &
      "Done PaStiX; error ", norm2( xex - x )
    call info(this_prog_name,"",message)
    if( norm2( xex - x ) .gt. 1.0e-8 ) all_test_passed = .false.
    call mpi_barrier(mpi_comm_world,ierr)
  endif
  deallocate(linpb)


  !---------------------------------------------------------------------
  ! Clean-up
  deallocate(rhs,gij)
  call clear(mmmt)
  deallocate(x)

  call mod_linsolver_destructor()
  call mod_sparse_destructor()
  call mod_mpi_utils_destructor()
  call mod_output_control_destructor()
  call mod_kinds_destructor()
  call mod_messages_destructor()

  !---------------------------------------------------------------------
  ! Done
  call mpi_reduce(all_test_passed , g_all_test_passed , 1 , &
           mpi_logical , mpi_land , 0 , mpi_comm_world, ierr )
  if(mpi_id.eq.0) then
    if(g_all_test_passed) write(*,*) '#PASSED'
  endif

  call mpi_finalize(ierr)

end program test_fml_linear_solvers1
!-----------------------------------------------------------------------

