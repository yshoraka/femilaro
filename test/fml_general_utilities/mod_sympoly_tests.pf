module mod_sympoly_tests

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor, &
   wp

 use mod_symfun, only: &
   mod_symfun_constructor, &
   mod_symfun_destructor,  &
   c_symfun, t_funcont, &
   assignment(=),       &
   fnpack, snpack

 use mod_symmon, only: &
   mod_symmon_constructor, &
   mod_symmon_destructor

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor,  &
   mod_sympoly_initialized, &
   t_symmon,     &
   t_sympol,     &
   new_sympoly,  &
   nll_sympoly,  &
   all_sympoly,  &
   assignment(=),&
   f_to_sympoly, &
   operator(**), &
   var_change,   &
   me_int,       &
   test_t_sympol => test, & ! renamed to avoid clashing with pfunit
   ! error codes
   wrong_input,     &
   wrong_previous,  &
   wrong_monomials, &
   wrong_type,      &
   ! module internals
   get__t_sympol__nmon, &
   get__t_sympol__mons

 use mod_symfun_tests, only: &
   t_funmock

 use pfunit_mod

!-----------------------------------------------------------------------

 implicit none

!-----------------------------------------------------------------------

 public

!-----------------------------------------------------------------------

 ! Tolerance for floating point comparisons
 real(wp), parameter :: toll = 10.0_wp*epsilon(1.0_wp)

 ! Some module variables which are used in the tests
 type(t_sympol), save :: p__zero, p__one, p__two, p__1p0x, p__2p0x, &
  p__one_1p0x, p__1p0x_3p1xy2, p__0p1y_2p5xy2, p__1p0x_0p1y_5p6xy2, &
  p__2p0x_0p2y_11p2xy2, p__1p0x2_9p61x2y4_6p2x2y2,                  &
  p__38p44x2y3_12p4x2y, p__115p32x2y2_12p4x2, p_3var, p__error

 ! Mock object: add compatible with t_sympol
 type, extends(t_funmock) :: t_funmock1
 contains
  private
  procedure, public, pass(x) :: add => funmock1_add
 end type t_funmock1

 ! Mock object: not compatible with t_sympol
 type, extends(t_funmock) :: t_funmock2
 end type t_funmock2

 character(len=*), parameter :: &
   this_mod_name = 'mod_symmon_tests'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 @Before
 subroutine mod_sympoly_tests_setup()
  character(len=*), parameter :: &
    this_sub_name = 'setup'
 
   call mod_messages_constructor()
   call mod_kinds_constructor()
   call mod_symfun_constructor()

   call mod_sympoly_constructor()
 
   p__zero = new_sympoly(               &
     coefficients = (/ real(wp) :: /) , &
        exponents = reshape(            &
                    (/ integer ::  /)   &
                    , (/0,0/) ) )

   p__one = new_sympoly(              &
     coefficients = (/ 1.0_wp /) ,    &
        exponents = reshape(          &
                    (/ integer ::  /) &
                    , (/0,1/) ) )

   p__two = new_sympoly(              &
     coefficients = (/ 2.0_wp /) ,    &
        exponents = reshape(          &
                    (/ integer ::  /) &
                    , (/0,1/) ) )

   p__1p0x = new_sympoly(          &
     coefficients = (/ 1.0_wp /) , &
        exponents = reshape(       &
                    (/ 1 /)        & ! x
                    , (/1,1/) ) )

   p__2p0x = new_sympoly(          &
     coefficients = (/ 2.0_wp /) , &
        exponents = reshape(       &
                    (/ 1 /)        & ! x
                    , (/1,1/) ) )

   p__one_1p0x = new_sympoly(               &
     coefficients = (/ 1.0_wp , 1.0_wp /) , &
        exponents = reshape(    &
                    (/ 0 ,      & ! 1
                       1 /)     & ! x
                    , (/1,2/) ) )

   p__1p0x_3p1xy2 = new_sympoly(            &
     coefficients = (/ 1.0_wp , 3.1_wp /) , &
        exponents = reshape(                &
                    (/ 1 , 0 ,  & ! x
                       1 , 2 /) & ! x y^2
                    , (/2,2/) ) )

   p__0p1y_2p5xy2 = new_sympoly(            &
     coefficients = (/ 0.1_wp , 2.5_wp /) , &
        exponents = reshape(                &
                    (/ 0 , 1 ,  & ! y
                       1 , 2 /) & ! x y^2
                    , (/2,2/) ) )

   p__1p0x_0p1y_5p6xy2 = new_sympoly(                &
     coefficients = (/ 1.0_wp , 0.1_wp , 5.6_wp /) , &
        exponents = reshape(                         &
                    (/ 1 , 0 ,  & ! x
                       0 , 1 ,  & ! y
                       1 , 2 /) & ! x y^2
                    , (/2,3/) ) )

   p__2p0x_0p2y_11p2xy2 = new_sympoly(                &
     coefficients = (/ 2.0_wp , 0.2_wp , 11.2_wp /) , &
        exponents = reshape(                          &
                    (/ 1 , 0 ,  & ! x
                       0 , 1 ,  & ! y
                       1 , 2 /) & ! x y^2
                    , (/2,3/) ) )

   p__1p0x2_9p61x2y4_6p2x2y2 = new_sympoly(           &
     coefficients = (/ 1.0_wp , 9.61_wp , 6.2_wp /) , &
        exponents = reshape(                          &
                    (/ 2 , 0 ,  & ! x^2
                       2 , 4 ,  & ! x^2 y^2
                       2 , 2 /) & ! x^2 y^2
                    , (/2,3/) ) )

   p__1p0x2_9p61x2y4_6p2x2y2 = new_sympoly(           &
     coefficients = (/ 1.0_wp , 9.61_wp , 6.2_wp /) , &
        exponents = reshape(                          &
                    (/ 2 , 0 ,  & ! x^2
                       2 , 4 ,  & ! x^2 y^2
                       2 , 2 /) & ! x^2 y^2
                    , (/2,3/) ) )

   p__38p44x2y3_12p4x2y = new_sympoly(         &
     coefficients = (/ 38.44_wp , 12.4_wp /) , &
        exponents = reshape(                   &
                    (/ 2 , 3 ,  & ! x^2 y^3
                       2 , 1 /) & ! x^2 y
                    , (/2,2/) ) )

   p__115p32x2y2_12p4x2 = new_sympoly(          &
     coefficients = (/ 115.32_wp , 12.4_wp /) , &
        exponents = reshape(                    &
                    (/ 2 , 2 ,  & ! x^2 y^2
                       2 , 0 /) & ! x^2
                    , (/2,2/) ) )

   p_3var = new_sympoly(                     &
     coefficients = (/ 10.0_wp  , 1.0_wp , 1.0_wp    , 10.0_wp   , &
                        0.1_wp  , 1.0_wp , 0.2_wp    ,  0.025_wp , &
                        0.5_wp  , 2.5_wp , 0.0025_wp ,  0.05_wp  , &
                        0.26_wp , 0.1_wp /) , &
        exponents = reshape(                 &
                    (/ 1 , 0 , 2 , &
                       0 , 1 , 2 , &
                       1 , 1 , 1 , &
                       1 , 0 , 1 , &

                       0 , 2 , 1 , &
                       0 , 1 , 1 , &
                       0 , 0 , 1 , &
                       1 , 2 , 0 , &

                       1 , 1 , 0 , &
                       1 , 0 , 0 , &
                       0 , 3 , 0 , &
                       0 , 2 , 0 , &

                       0 , 1 , 0 , &
                       0 , 0 , 0 /)          &
                    , (/3,14/) ) )

   p__error = new_sympoly(                           &
     coefficients = (/ 0.0_wp , 0.0_wp , 0.0_wp /) , &
        exponents = reshape(                         &
                    (/ 0 , 0 ,  &
                       2 , 0 /) &
                    , (/2,2/) ) )

 end subroutine mod_sympoly_tests_setup

!-----------------------------------------------------------------------

 @After
 subroutine mod_sympoly_tests_teardown()
  character(len=*), parameter :: &
    this_sub_name = 'tear-down'
 
   call mod_sympoly_destructor()

   call mod_symfun_destructor()
   call mod_kinds_destructor()
   call mod_messages_destructor()
 
 end subroutine mod_sympoly_tests_teardown

!-----------------------------------------------------------------------

 ! Some helper subroutines

 ! Note: checking the types and classes should be done as part of the
 ! main tests, these helper functions work with t_sympol objects.

 subroutine assert_sympol_zero(p)
  type(t_sympol), intent(in) :: p

  type(t_symmon), allocatable :: mons(:)
  character(len=*), parameter :: &
   remark = 'Assertion in helper function "assert_sympol_zero":'

   ! To be zero, there can not be any error
   @assertEqual( 0 , test_t_sympol(p) , remark//" error condition:" )

   ! Check that all the monomials are small
   mons = get__t_sympol__mons(p) ! reallocate on assignment

   ! There are two possibilities:
   ! 1) there are no monomials at all -> test passed
   ! 2) there are some monomials -> check if the coefficients are small
   if(size(mons).gt.0) then
     @assertEqual( 0.0_wp , maxval( abs( mons%coeff ) ) , toll,remark )
   endif

 end subroutine assert_sympol_zero


 subroutine assert_sympol_not_zero(p)
  type(t_sympol), intent(in) :: p

  type(t_symmon), allocatable :: mons(:)
  character(len=*), parameter :: &
   remark = 'Assertion in helper function "assert_sympol_not_zero":'

   ! To be zero, there can not be any error
   @assertEqual( 0 , test_t_sympol(p) , remark//" error condition:" )

   ! Check that not all the monomials are small
   mons = get__t_sympol__mons(p) ! reallocate on assignment
   @assertGreaterThan(maxval( abs( mons%coeff ) ), 0.0_wp ,toll,remark)
   deallocate(mons)

 end subroutine assert_sympol_not_zero


 subroutine assert_sympol_are_equal(p1,p2)
  type(t_sympol), intent(in) :: p1, p2

   !call assert_sympol_zero( f_to_sympoly( p1 - p2 ) )
   call assert_sympol_zero( f_to_sympoly( p1%sub( p2 ) ) )

 end subroutine assert_sympol_are_equal


 subroutine assert_sympol_are_not_equal(p1,p2)
  type(t_sympol), intent(in) :: p1, p2

   call assert_sympol_not_zero( f_to_sympoly( p1%sub( p2 ) ) )

 end subroutine assert_sympol_are_not_equal

!-----------------------------------------------------------------------

 ! Some sanity check for this module: setup and helper functions

 @Test
 subroutine new_sympoly__valid_input__works_correctly()

  type(t_symmon), allocatable :: mons(:)

   ! Public components
   @assertEqual( 3 ,               p__1p0x_3p1xy2%deg )
   @assertEqual( 0 , test_t_sympol(p__1p0x_3p1xy2)    )

   ! Internal implementation
   @assertEqual( 2 , get__t_sympol__nmon(p__1p0x_3p1xy2) )
   mons = get__t_sympol__mons(p__1p0x_3p1xy2) ! using allocate on ass.
   @assertEqual( 2      ,    size(mons) )
   @assertEqual( 1.0_wp , mons(1)%coeff , toll )
   @assertEqual( 3.1_wp , mons(2)%coeff , toll )
   deallocate(mons)

 end subroutine new_sympoly__valid_input__works_correctly


 @Test
 subroutine new_sympoly__wrong_input_shape__returns_error()

  real(wp) :: coefs(3)
  integer :: exps(4,2)
  type(t_sympol) :: res

   res = new_sympoly( coefs , exps )

   @assertEqual( wrong_input , test_t_sympol(res) )

 end subroutine new_sympoly__wrong_input_shape__returns_error


 @Test
 subroutine this_test_module__equal_helper_function__works()

  call assert_sympol_are_equal( p__1p0x_3p1xy2 , p__1p0x_3p1xy2 )

 end subroutine this_test_module__equal_helper_function__works


 @Test
 subroutine this_test_module__not_equal_helper_function__works()

  call assert_sympol_are_not_equal( p__zero , p__1p0x_3p1xy2 )

 end subroutine this_test_module__not_equal_helper_function__works

!-----------------------------------------------------------------------

 ! Now the main tests

 @Test
 subroutine all_sympoly__valid_input_dimension_zero__works()

  type(t_sympol), allocatable :: polys(:)

   !allocate( polys , source=all_sympoly( d=0 , k=1 ) )
   call gfortran_bug_workaround( polys , d=0,k=1 )

   @assertEqual( 1 , size(polys) )
   call assert_sympol_are_equal( p__one , polys(1) )

 end subroutine all_sympoly__valid_input_dimension_zero__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine all_sympoly__valid_input_dimension_one__works()

  type(t_sympol), allocatable :: polys(:)

   !allocate( polys , source=all_sympoly( d=1 , k=1 ) )
   call gfortran_bug_workaround( polys , d=1,k=1 )

   @assertEqual( 2 , size(polys) )
   call assert_sympol_are_equal( p__one  , polys(1) )
   call assert_sympol_are_equal( p__1p0x , polys(2) )

 end subroutine all_sympoly__valid_input_dimension_one__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine all_sympoly__valid_input_dimension_two__works()

   @assertEqual( 3 , size(all_sympoly( d=2 , k=1 )) )

 end subroutine all_sympoly__valid_input_dimension_two__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine to_sympol_assign__valid_sympol_eq_funcont__works()

  type(t_funcont) :: wrap
  type(t_sympol)  :: x
  
   wrap = p__0p1y_2p5xy2
   x = wrap

   call assert_sympol_are_equal( x , p__0p1y_2p5xy2 )

 end subroutine to_sympol_assign__valid_sympol_eq_funcont__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine snpack_assign__funcont_eq_sympol__works()
  
  ! This test is about the interaction of t_sympol and t_funcont
  ! objects, and in principle it would be a duplicate of the general
  ! tests in mod_symfun_tests. It is mostly motivated by this compiler
  ! bug for ifort: bug in ifort
  ! https://software.intel.com/en-us/forums/topic/564995

  type(t_funcont) :: x
  
   x = new_sympoly(                         &
     coefficients = (/ 0.1_wp , 2.5_wp /) , &
        exponents = reshape(                &
                    (/ 0 , 1 ,  & ! y
                       1 , 2 /) & ! x y^2
                    , (/2,2/) ) )

   select type( px => x%f )
    type is(t_sympol)
     call assert_sympol_are_equal( px , p__0p1y_2p5xy2 )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine snpack_assign__funcont_eq_sympol__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine snpack_assign__array_funcont_eq_sympol__works()

  ! This test is very similar to
  ! snpack_assign__funcont_eq_sympol__works but using the fact that
  ! the operation is elemental.

  integer :: i
  ! This is gfortran bug
  ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=67539
  type(t_funcont) :: gfortran_bug_workaround
  type(t_funcont), allocatable :: x(:)

   allocate(x(2))
   gfortran_bug_workaround = new_sympoly(                         &
     coefficients = (/ 0.1_wp , 2.5_wp /) , &
        exponents = reshape(                &
                    (/ 0 , 1 ,  & ! y
                       1 , 2 /) & ! x y^2
                    , (/2,2/) ) )
   x = gfortran_bug_workaround

   do i=1,size(x)
     select type( px => x(i)%f )
      type is(t_sympol)
       call assert_sympol_are_equal( px , p__0p1y_2p5xy2 )
      class default
       @assertTrue( .false. , 'Wrong result type' )
     end select
   enddo

 end subroutine snpack_assign__array_funcont_eq_sympol__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__sympol_plus_sympol__valid_input_works()

  type(t_funcont) :: res

   res = p__1p0x_3p1xy2 + p__0p1y_2p5xy2

   select type( pres => res%f )
    type is(t_sympol)
     call assert_sympol_are_equal( pres , p__1p0x_0p1y_5p6xy2 )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine add__sympol_plus_sympol__valid_input_works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__sympol_plus_sympol__wrong_input_returns_error()

  type(t_funcont) :: res

   res = p__zero + p__error

   select type( pres => res%f )
    type is(t_sympol)
     @assertEqual( wrong_previous , test_t_sympol(res) )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine add__sympol_plus_sympol__wrong_input_returns_error

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__sympol_plus_cont__valid_input_works()

  type(t_funcont) :: tmp, res

   tmp = p__1p0x_3p1xy2
   !res = p__0p1y_2p5xy2 + tmp
   res = p__0p1y_2p5xy2%add( tmp )
   
   select type( pres => res%f )
    type is(t_sympol)
     call assert_sympol_are_equal( pres , p__1p0x_0p1y_5p6xy2 )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine add__sympol_plus_cont__valid_input_works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__cont_plus_sympol__valid_input_works()

  type(t_funcont) :: tmp, res

   tmp = p__1p0x_3p1xy2
   !res = tmp + p__0p1y_2p5xy2
   res = tmp%add( p__0p1y_2p5xy2 )
   
   select type( pres => res%f )
    type is(t_sympol)
     call assert_sympol_are_equal( pres , p__1p0x_0p1y_5p6xy2 )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine add__cont_plus_sympol__valid_input_works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__sympol_plus_mock1__valid_input_works()

  ! After exchaging the terms, mock1 returns a valid result.

  type(t_funmock1) :: y
  type(t_funcont) :: res

   !res = p__0p1y_2p5xy2 + y
   res = p__0p1y_2p5xy2%add( y )
   
   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock1)
       @assertTrue( f%add_called , 'Method add not called.' )
      class default
       @assertTrue( .false. , 'Wrong result type' )
     end select
   endif

 end subroutine add__sympol_plus_mock1__valid_input_works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__sympol_plus_mock2__returns_error()

  ! None of the two types is compatible with the other: error

  type(t_funmock2) :: y
  type(t_funcont) :: res

   !res = p__0p1y_2p5xy2 + y
   res = p__0p1y_2p5xy2%add( y )
   
   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock2)
       @assertEqual( 1 , f%ierr , 'Error flag not set.' )
      class default
       @assertTrue( .false. , 'Wrong result type' )
     end select
   endif

 end subroutine add__sympol_plus_mock2__returns_error

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__mock2_plus_sympol__returns_error()

  ! None of the two types is compatible with the other: error

  type(t_funmock2) :: y
  type(t_funcont) :: res

   !res = p__0p1y_2p5xy2 + y
   res = y%add( p__0p1y_2p5xy2 )
   
   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_sympol)
       @assertEqual( wrong_type , f%ierr , 'Error flag not set.' )
      class default
       @assertTrue( .false. , 'Wrong result type' )
     end select
   endif

 end subroutine add__mock2_plus_sympol__returns_error

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine sub__sympol_minus_sympol__valid_input_works()

  type(t_funcont) :: res

   !res = p__1p0x_0p1y_5p6xy2 - p__1p0x_3p1xy2
   res = p__1p0x_0p1y_5p6xy2%sub( p__1p0x_3p1xy2 )

   select type( pres => res%f )
    type is(t_sympol)
     call assert_sympol_are_equal( pres , p__0p1y_2p5xy2 )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine sub__sympol_minus_sympol__valid_input_works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine scal_prod__valid_input__works()

  type(t_funcont) :: res

   res = 2.0_wp*p__1p0x_0p1y_5p6xy2

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( pres => res%f )
      type is(t_sympol)
       call assert_sympol_are_equal( p__2p0x_0p2y_11p2xy2 , pres )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine scal_prod__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine scal_prod__wrong_input__returns_error()

  type(t_funcont) :: res

   res = 1.5_wp * p__error

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( pres => res%f )
      type is(t_sympol)
       @assertEqual( wrong_previous , test_t_sympol(pres) )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine scal_prod__wrong_input__returns_error

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine mult__sympol_times_sympol__valid_input_works()

  type(t_funcont) :: res

   res = p__1p0x_3p1xy2 * p__1p0x_3p1xy2

   select type( pres => res%f )
    type is(t_sympol)
     call assert_sympol_are_equal( pres , p__1p0x2_9p61x2y4_6p2x2y2 )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine mult__sympol_times_sympol__valid_input_works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine mult__sympol_times_sympol__error_on_invalid_input()

  type(t_funcont) :: res

   res = p__1p0x_3p1xy2 * p__error

   select type( pres => res%f )
    type is(t_sympol)
     @assertEqual( wrong_previous , test_t_sympol(pres) )
    class default
     @assertTrue( .false. , 'Wrong result type' )
   end select

 end subroutine mult__sympol_times_sympol__error_on_invalid_input

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pow__zero_power__works()

  call assert_sympol_are_equal( p__1p0x_3p1xy2**0 , p__one )

 end subroutine pow__zero_power__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pow__valid_input__works()

  call assert_sympol_are_equal( p__1p0x_3p1xy2**2 ,       &
                                p__1p0x2_9p61x2y4_6p2x2y2 )

 end subroutine pow__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pow__wrong_input__returns_error()

  type(t_sympol) :: res

   res = p__error**2
   @assertEqual( wrong_previous , test_t_sympol(res) )

 end subroutine pow__wrong_input__returns_error

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine ev_scal__valid_input__works()

  real(wp) :: p, x(2)

   x = (/ 0.5_wp , 1.0_wp /)
   p = p__1p0x_3p1xy2%ev( x )
   @assertEqual( 2.05_wp , p , toll )

 end subroutine ev_scal__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine ev_1d__valid_input__works()

  real(wp) :: p(3), x(2,3)

   x(:,1) = (/ 0.5_wp , 1.0_wp /)
   x(:,2) = (/ 1.0_wp , 1.0_wp /)
   x(:,3) = (/ 1.0_wp , 0.5_wp /)
   p = p__1p0x_3p1xy2%ev( x )
   @assertEqual( (/ 2.05_wp , 4.1_wp , 1.775_wp /) , p , toll )

 end subroutine ev_1d__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pderj__valid_input__works()

  type(t_funcont) :: dp

   ! d/dy[ x^2 + 9.61x^2y^4 + 6.2x^2y^2 ]
   dp = p__1p0x2_9p61x2y4_6p2x2y2%pderj( 2 ) 

   if(.not.allocated(dp%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( pdp => dp%f )
      type is(t_sympol)
       call assert_sympol_are_equal( p__38p44x2y3_12p4x2y , pdp )
      class default
       @assertTrue( .false. , 'Wrong result type for dp%f.' )
     end select
   endif

 end subroutine pderj__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pderjk__valid_input__works()

  type(t_funcont) :: dp

   ! d^2/dy^2[ x^2 + 9.61x^2y^4 + 6.2x^2y^2 ]
   dp = p__1p0x2_9p61x2y4_6p2x2y2%pderj( 2 , 2 ) 

   if(.not.allocated(dp%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( pdp => dp%f )
      type is(t_sympol)
       call assert_sympol_are_equal( p__115p32x2y2_12p4x2 , pdp )
      class default
       @assertTrue( .false. , 'Wrong result type for dp%f.' )
     end select
   endif

 end subroutine pderjk__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine var_change__valid_input__works()

  real(wp) :: a(2,3), b(2)

   ! The polynomial  0.1y + 2.5xy^2  becomes
   !
   !  0.1*(0.1*eta + 2*zeta + 1) 
   !   + 2.5*(xi + 0.1*eta)*(0.1*eta + 2*zeta + 1)^2

   a(1,:) = (/ 1.0_wp , 0.1_wp , 0.0_wp /)
   a(2,:) = (/ 0.0_wp , 0.1_wp , 2.0_wp /)
   b = (/ 0.0_wp , 1.0_wp /)

   call assert_sympol_are_equal(                        &
          var_change( p__0p1y_2p5xy2 , a , b ) , p_3var )

 end subroutine var_change__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine var_change__wrong_input__returns_error()

  real(wp) :: a(2,3), b(2)
  type(t_sympol) :: res

   res = var_change( p__error , a , b )
   @assertEqual( wrong_previous , test_t_sympol(res) )

 end subroutine var_change__wrong_input__returns_error

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine var_change__wrong_input_shape__returns_error()

  real(wp) :: a(2,3), b(3)
  type(t_sympol) :: res

   res = var_change( p__0p1y_2p5xy2 , a , b )
   @assertEqual( wrong_input , test_t_sympol(res) )

   res = var_change( p__0p1y_2p5xy2 , a(1:1,:) , b(1:1) )
   @assertEqual( wrong_input , test_t_sympol(res) )

 end subroutine var_change__wrong_input_shape__returns_error

!-----------------------------------------------------------------------

 ! This function obviates the following gfortran bug:
 !
 !  allocate( polys , source=all_sympoly( d=0 , k=1 ) )
 !
 ! produces an ICE.
 !
 ! Note: this bug is
 ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=67123
 subroutine gfortran_bug_workaround( polys , d,k )
  integer,                     intent(in)  :: d, k
  type(t_sympol), allocatable, intent(out) :: polys(:)

   allocate( polys( size(all_sympoly( d , k )) ) )
   polys = all_sympoly( d , k )
   
 end subroutine gfortran_bug_workaround

!-----------------------------------------------------------------------

 ! Implementation of the mock methods

 elemental function funmock1_add(x,y) result(z)
  class(t_funmock1), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: ynp
  type(t_funmock1) :: tmp

   select type(y)
    type is(t_sympol) ! OK
     tmp%add_called = .true.
     z = tmp
    class default
     if(x%try_permutation) then
       ynp = y
       ynp%f%try_permutation = .false.
       !z = ynp + x
       z = ynp%add( x )
     else
       allocate( t_funmock1 :: z%f )
       call z%f%set_err(1) ! unknown type
     endif
   end select

 end function funmock1_add
 
!-----------------------------------------------------------------------

end module mod_sympoly_tests

