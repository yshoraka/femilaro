program test_interp1d

  use mod_interp1d, only: &
       lookup, linterp1d

  use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

  use mod_octave_io

  use mod_fu_manager

  implicit none
  
  real(wp), allocatable :: bias(:,:)
  real(wp) :: values(100), yout(100)
  real(wp) :: rtmp
  integer  :: indices(100)
  integer :: ii, fu, ierr

  call new_file_unit(fu,ierr)
  open(fu,file="bias_fabio.octxt", status='old',action='read', &
       form='formatted',position='rewind',iostat=ierr)
  call read_octave_al (bias, 'bias', fu)
  close(fu,iostat=ierr)

  values(1) = 0.0_wp
  do ii=2,100
     values(ii) = values(ii-1) + .04_wp / 99.0_wp
  end do
  
  !call lookup (bias(:, 1), values, indices)
  !write (*, '(10 (i0, " "))') indices

  do ii=1, 100
     !call lookup (bias(:, 1), values(ii), indices(ii))
     rtmp = yout(ii)
     call linterp1d (bias(:, 1), bias(:, 6), 4.250000000000000E-002_wp, rtmp)
     write (*, '(i0, es19.10, es19.10)') ii, yout(ii)
  end do
  !write (*, '(100 (es17.10))') array
  !write (*, '(100 (es17.10))') yin
  !write (*, '(10 (1x, i0))') indices
  !write (*, '(10 (es19.10))') values
  !write (*, '(10 (es19.10))') yout
  
end program test_interp1d
