module mod_wandsystem
!General comments: nonlinear system for the computation of the
!quadrature nodes and weights.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_sympoly, only: &
   mod_sympoly_initialized, &
   ev

 use mod_psymbasis, only: &
   mod_psymbasis_initialized, &
   dimbasis, &
   basis,    &
   dxbasis,  &
   dybasis,  &
   intbasis

 use mod_orbits, only: &
   mod_orbits_initialized, &
   t_torbit,    &
   dimorbits,   &
   orbits,      &
   dimxstatus,  &
   xstatus,     &
   t_xsleg,     &
   xstatus_leg, &
   orbit_xy,    &
   orbit_dxdl,  &
   orbit_dydl

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_wandsystem_constructor, &
   mod_wandsystem_destructor,  &
   mod_wandsystem_initialized, &
   xstatus,   &
   lhs,       &
   jf,        &
   status_set

 private

!-----------------------------------------------------------------------

! Module types and parameters

! Module variables

 ! public members
 logical, protected ::               &
   mod_wandsystem_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_wandsystem'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_wandsystem_constructor()
  character(len=100) :: message(2)
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.)  .or. &
       (mod_kinds_initialized.eqv..false.)     .or. &
       (mod_sympoly_initialized.eqv..false.)   .or. &
       (mod_psymbasis_initialized.eqv..false.) .or. &
       (mod_orbits_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_wandsystem_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   write(message(1),'(A)') &
     'Dimension of the nonlinear system:'
   write(message(2),'(A,I3,A,I3,A)') &
     ' ',dimbasis,' equations in ',dimxstatus,' unknowns'
   call info(this_sub_name,this_mod_name,message)

   mod_wandsystem_initialized = .true.
 end subroutine mod_wandsystem_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_wandsystem_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_wandsystem_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_wandsystem_initialized = .false.
 end subroutine mod_wandsystem_destructor

!-----------------------------------------------------------------------
 
 pure function lhs(x) result(fx)
  real(wp), intent(in) :: x(:)
  real(wp) :: fx(size(x))
 
  integer :: i, j
  real(wp) :: fxi
  character(len=*), parameter :: &
    this_fun_name = 'lhs'

   do i=1,dimbasis
     fxi = 0.0_wp
     do j=1,dimorbits
       
       fxi = fxi +                                &
             orbits(j)%w * real(orbits(j)%m,wp) * &
             ev(basis(i),orbit_xy(orbits(j)))

     enddo
     fx(i) = fxi - intbasis(i)
   enddo

   ! the system might not be square
   do i=dimbasis+1,dimxstatus
     fx(i) = 0.0_wp
   enddo
 
 end function lhs
 
!-----------------------------------------------------------------------
 
 pure function jf(dx) result(jfdx)
  real(wp), intent(in) :: dx(:)
  real(wp) :: jfdx(size(dx))
 
  integer :: i, j, jo
  real(wp) :: jfdxi
  character(len=*), parameter :: &
    this_fun_name = 'jf'

   do i=1,dimbasis
     jfdxi = 0.0_wp
     do j=1,dimxstatus
       jo = xstatus_leg(j)%jo
       select case(xstatus_leg(j)%ptype(1:1))
        case('w')
         jfdxi = jfdxi +                              &
               dx(j) * real(orbits(jo)%m,wp) *        &
               ev(basis(i),orbit_xy(orbits(jo)))
        case('l')
         jfdxi = jfdxi +                              &
               orbits(jo)%w * real(orbits(jo)%m,wp) * &
               ( ev(dxbasis(i),orbit_xy(orbits(jo)))  &
                  *orbit_dxdl(xstatus_leg(j)) +       &
                 ev(dybasis(i),orbit_xy(orbits(jo)))  &
                  *orbit_dydl(xstatus_leg(j)) )*dx(j)
       end select
     enddo
     jfdx(i) = jfdxi
   enddo
 
   ! the system might not be square
   do i=dimbasis+1,dimxstatus
     jfdx(i) = 0.0_wp
   enddo

 end function jf
 
!-----------------------------------------------------------------------
 
 subroutine status_set(x)
  real(wp), intent(in) :: x(:)
 
  character(len=*), parameter :: &
    this_sub_name = 'status_set'

   xstatus = x
 
 end subroutine status_set
 
!-----------------------------------------------------------------------

end module mod_wandsystem

