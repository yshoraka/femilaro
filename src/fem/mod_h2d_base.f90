!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> \brief
!!
!! Base type for the hybrid 2D grid
!!
!! \n
!!
!! A base is defines as a collection of
!! \fref{mod_h2d_master_el,c_h2d_me_base} objects. Only those base
!! elements corresponding to element shapes actually defined in the
!! grid are generated, and they are indexed in the element collection
!! with the number of vertexes.
!<----------------------------------------------------------------------
module mod_h2d_base

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_initialized, &
   c_h2d_me_base, h2d_me_base_init, &
   t_elem_spec

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_h2d_base_constructor, &
   mod_h2d_base_destructor,  &
   mod_h2d_base_initialized, &
   t_h2d_base, new_base, clear, write_octave, &
   ! Only for testing ----
   mod_h2d_base_constructor_test

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Container type for \fref{mod_h2d_master_el,c_h2d_me_base} 
 !!
 !! This type is introduced to be able to build an array with
 !! different master element objects.
 type t_me_base_container
  class(c_h2d_me_base), allocatable :: me
 end type t_me_base_container

 !> base: collection of base master elements
 !!
 !! Notice that using a type with an array component, rather than
 !! directly an array of \c t_me_base_container type, ensures that the
 !! lower and upper bounds of the array are preserved across function
 !! calls.
 type t_h2d_base
  !> lower and upper bounds for
  !! \field_fref{mod_h2d_base,t_h2d_base,e}, stored here for
  !! convenience.
  integer :: lb, ub
  type(t_me_base_container), allocatable :: e(:)
 end type t_h2d_base

! Module variables

 ! public members
 logical, protected ::               &
   mod_h2d_base_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_h2d_base'

 interface new_base
   module procedure new_base, new_base_from_base
 end interface

 interface clear
   module procedure clear_h2d_base
 end interface

 interface write_octave
   module procedure write_h2d_base_struct
 end interface write_octave

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_h2d_base_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false. ) .or. &
          (mod_kinds_initialized.eqv..false. ) .or. &
      (mod_octave_io_initialized.eqv..false. ) .or. &
  (mod_h2d_master_el_initialized.eqv..false. ) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_base_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_base_initialized = .true.
 end subroutine mod_h2d_base_constructor

 subroutine mod_h2d_base_constructor_test()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false. ) .or. &
          (mod_kinds_initialized.eqv..false. ) .or. &
   ! not needed if we avoid write -> mod_octave_io_initialized
  (mod_h2d_master_el_initialized.eqv..false. ) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_base_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_base_initialized = .true.
 end subroutine mod_h2d_base_constructor_test

!-----------------------------------------------------------------------
 
 subroutine mod_h2d_base_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_h2d_base_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_h2d_base_initialized = .false.
 end subroutine mod_h2d_base_destructor

!-----------------------------------------------------------------------

 !> Build a new base
 !!
 !! When cloning part of an existing base, as described in
 !! \fref{mod_h2d_master_el,t_elem_spec}, we need to be able to set
 !! the \c cloned_me component of \c elem_spec for each element inside
 !! this function. This explains the intent \c INOUT for \c elem_spec.
 subroutine new_base(b,required_shapes,rqs1,rqs2,elem_spec , cloned_b)
  integer,           intent(in) :: rqs1, rqs2
  logical,           intent(in) :: required_shapes(rqs1:rqs2)
  type(t_elem_spec), intent(inout) :: elem_spec
  type(t_h2d_base),  intent(in), optional, target :: cloned_b
  type(t_h2d_base),  intent(out) :: b

  integer :: ie
  character(len=*), parameter :: &
    this_sub_name = 'new_base'

   ! Build the required elements
   allocate( b%e( rqs1 : rqs2 ) )
   b%lb = lbound(b%e,1); b%ub = ubound(b%e,1)
   do ie=b%lb,b%ub
     if(required_shapes(ie)) then
       if(present(cloned_b)) elem_spec%cloned_me => cloned_b%e(ie)%me
       call h2d_me_base_init(b%e(ie)%me,ie,elem_spec)
     endif
   enddo

 end subroutine new_base

!-----------------------------------------------------------------------

 !> Useful to generate a new FE space reusing a previous grid
 !!
 !! This is a wrapper to \c new_base that can be used when a new base
 !! (i.e. a new \c elem_spec FE definition) is required with the same
 !! polynomial master elements already defined for another base.
 !!
 !! In practice, all the arguments to \c new_base are deduced from the
 !! input argument \c bo, except for \c elem_spec.
 !!
 !! \note It is assumed that the cloned base described in
 !! \fref{mod_h2d_master_el,t_elem_spec} is the same \c bo passed as
 !! input. Notice anyway that fields from \c bo are copied in \c b
 !! <em>only if the corresponding fields in
 !! \fref{mod_h2d_master_el,t_elem_spec} specify so</em>.
 subroutine new_base_from_base(b, bo,elem_spec)
  type(t_elem_spec), intent(inout) :: elem_spec
  type(t_h2d_base),  intent(in) :: bo
  type(t_h2d_base),  intent(out) :: b

  integer :: i

   call new_base( b, &
     required_shapes = (/( allocated( bo%e(i)%me ) , i=bo%lb,bo%ub )/),&
                rqs1 = bo%lb ,                                         &
                rqs2 = bo%ub ,                                         &
           elem_spec = elem_spec ,                                     &
            cloned_b = bo )

 end subroutine new_base_from_base

!-----------------------------------------------------------------------

 pure subroutine clear_h2d_base(b)
  type(t_h2d_base), intent(inout) :: b

  integer :: ie

   ! Deallocate the master elements
   do ie=b%lb,b%ub
     call b%e(ie)%me%clear( b%e(ie)%me )
   enddo

   deallocate(b%e)
   
 end subroutine clear_h2d_base

!-----------------------------------------------------------------------

 subroutine write_h2d_base_struct(b,var_name,fu)
  integer, intent(in) :: fu
  type(t_h2d_base), intent(in) :: b
  character(len=*), intent(in) :: var_name
 
  integer :: ie
  character(len=*), parameter :: &
    this_sub_name = 'write_h2d_base_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 2' ! number of fields

   ! field 01 : bounds
   write(fu,'(a)')      '# name: bounds'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave( (/b%lb,b%ub/) ,'r','<cell-element>',fu)

   ! field 02 : e
   write(fu,'(a)')      '# name: e'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   write(fu,'(a)')      '# name: <cell-element>'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a,i7)')   '# rows: ',1
   write(fu,'(a,i7)')   '# columns: ',size(b%e)
   do ie=b%lb,b%ub
     call b%e(ie)%me%write_octave('<cell-element>',fu)
   enddo

 end subroutine write_h2d_base_struct

!-----------------------------------------------------------------------

end module mod_h2d_base

