!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> Representation of the scalar unknwon.
!!
!! \n
!!
!! The scalar unknown is represented as a two-dimensional array,
!! following the standard representation for DG methods.
!<----------------------------------------------------------------------
module mod_phi_state

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_time_integrators, only: &
   mod_time_integrators_initialized, &
   c_ode, c_ods

 use mod_tps_phs_grid, only: &
   mod_tps_phs_grid_initialized, &
   t_tps_grid

 use mod_tps_base, only: &
   mod_tps_base_initialized, &
   t_tps_base

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_phi_state_constructor, &
   mod_phi_state_destructor,  &
   mod_phi_state_initialized, &
   t_phi_state, new_phi_state, clear, &
   write_octave

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members

 !> Generic scalar unknown
 !!
 !! Some pointers to the finite element space description are also
 !! introduced; such description is shared by all the state
 !! variables.
 type, extends(c_stv) :: t_phi_state
  type(t_tps_grid), pointer :: grid => null()
  type(t_tps_base), pointer :: base => null()
  real(wp), allocatable :: phi(:,:)
 contains
  procedure, pass(x) :: incr
  procedure, pass(x) :: tims
  procedure, pass(x) :: inlt => phi_inlt
  procedure, pass(z) :: alt  => phi_alt
  procedure, pass(z) :: mlt  => phi_mlt
  procedure, pass(z) :: copy => phi_copy
  procedure, pass(x) :: scal => phi_scal
  procedure, pass(x) :: show => phi_show
 end type t_phi_state

 interface clear
   module procedure clear_phi_state
 end interface

 interface write_octave
   module procedure write_phi_state
 end interface write_octave

! Module variables

 ! public members
 logical, protected ::               &
   mod_phi_state_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_phi_state'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_phi_state_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
   (mod_octave_io_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
(mod_time_integrators_initialized.eqv..false.) .or. &
(mod_tps_phs_grid_initialized.eqv..false.) .or. &
    (mod_tps_base_initialized.eqv..false.) ) then  
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_phi_state_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_phi_state_initialized = .true.
 end subroutine mod_phi_state_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_phi_state_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_phi_state_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_phi_state_initialized = .false.
 end subroutine mod_phi_state_destructor

!-----------------------------------------------------------------------
 
 subroutine new_phi_state(obj,grid,base)
  type(t_tps_grid), intent(in), target :: grid
  type(t_tps_base), intent(in), target :: base
  type(t_phi_state), intent(out) :: obj

   obj%grid => grid
   obj%base => base
   allocate( obj%phi( base%pkd , grid%ne ) )

 end subroutine new_phi_state
 
!-----------------------------------------------------------------------
 
 pure subroutine clear_phi_state(obj)
  type(t_phi_state), intent(out) :: obj

   nullify( obj%grid )
   nullify( obj%base )
   ! allocatable components are implicitly deallocated
 
 end subroutine clear_phi_state
 
!-----------------------------------------------------------------------

 subroutine incr(x,y)
  class(c_stv), intent(in) :: y
  class(t_phi_state), intent(inout) :: x

   select type(y); type is(t_phi_state)

   x%phi = x%phi + y%phi

   end select
 end subroutine incr

!-----------------------------------------------------------------------

 subroutine tims(x,r)
  real(wp), intent(in) :: r
  class(t_phi_state), intent(inout) :: x

   x%phi = r*x%phi

 end subroutine tims

!-----------------------------------------------------------------------

 subroutine phi_inlt(x,r,y)
  real(wp),     intent(in) :: r
  class(c_stv), intent(in) :: y
  class(t_phi_state), intent(inout) :: x

   select type(y); type is(t_phi_state)

   x%phi = x%phi + r*y%phi

   end select
 end subroutine phi_inlt

!-----------------------------------------------------------------------

 !> Overloaded for efficiency
 subroutine phi_alt(z,x,r,y)
  real(wp),     intent(in) :: r
  class(c_stv), intent(in) :: x, y
  class(t_phi_state), intent(inout) :: z

   select type(x); type is(t_phi_state)
    select type(y); type is(t_phi_state)

   z%phi = x%phi + r*y%phi

    end select
   end select
 end subroutine phi_alt

!-----------------------------------------------------------------------

 !> Overloaded for efficiency
 subroutine phi_mlt(z,r,x)
  real(wp),     intent(in) :: r
  class(c_stv), intent(in) :: x
  class(t_phi_state), intent(inout) :: z

   select type(x); type is(t_phi_state)

   z%phi = r*x%phi

   end select
 end subroutine phi_mlt

!-----------------------------------------------------------------------

 subroutine phi_copy(z,x)
  class(c_stv),     intent(in)    :: x
  class(t_phi_state), intent(inout) :: z

   select type(x); type is(t_phi_state)

   z%phi  = x%phi

   end select
 end subroutine phi_copy

!-----------------------------------------------------------------------

 function phi_scal(x,y) result(s)
  class(t_phi_state), intent(in) :: x
  class(c_stv), intent(in) :: y
  real(wp) :: s
   
   select type(y); type is(t_phi_state)
    s = sum(x%phi*y%phi)
   end select

 end function phi_scal

!-----------------------------------------------------------------------

 subroutine phi_show(x)
  class(t_phi_state), intent(in) :: x
   write(*,'(a)') '------ Show a t_phi_state object ------'
   write(*,'(*(e12.3))') maxval(abs(x%phi))
   write(*,'(a)') '---------------------------------------'
 end subroutine phi_show

!-----------------------------------------------------------------------

 subroutine write_phi_state(x,var_name,fu)
  integer, intent(in) :: fu
  type(t_phi_state), intent(in) :: x
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_phi_state'

   write(fu,'(a,a)') '# name: ',var_name
   write(fu,'(a)')   '# type: struct'
   write(fu,'(a)')   '# length: 1' ! number of fields

   ! field 01 : phi
   write(fu,'(a)')   '# name: phi'
   write(fu,'(a)')   '# type: cell'
   write(fu,'(a)')   '# rows: 1'
   write(fu,'(a)')   '# columns: 1'
   call write_octave(x%phi,'<cell-element>',fu)

 end subroutine write_phi_state

!-----------------------------------------------------------------------

end module mod_phi_state

