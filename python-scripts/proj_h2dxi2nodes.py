
import numpy as np
import numpy.linalg
import scipy as sp


def proj_h2d_xi2nodes(GRID,BASE,FXI,key):

  # Check how many (scalar) fields are required (from first el.)
  #  -> the last index is for the nodes, the previous ones are for
  #    the vector/tensor components; notice that prod([]) = 1.0
  nfields_K = int( np.prod( FXI[0][0][key].shape[:-1] ) )

  FNOD_V = [ np.zeros((G.nv,nfields_K)) for G in GRID ]

  # a small class to which appending fields
  class work_data():
    pass

  # FNOD_V is a list of modifiable objects: we can work on references
  for G,B,Fxi,Fno in zip( GRID , BASE , FXI , FNOD_V ):

    # Counter of the values for each node
    cnt = np.zeros(G.nv,dtype=int)

    # Work array: a list of objects with the listed attributes
    W = [ work_data() for e in B.e ] # initialize the list
    for wi,e in zip(W,B.e):
      setattr( wi,"lm" , np.ndarray((e.pk,  e.pk   )) )
      setattr( wi,"lf" , np.ndarray((e.pk,nfields_K)) )
      setattr( wi,"fxik",np.ndarray((e.m ,nfields_K)) )
      setattr( wi,"MK"  ,[                                          \
    # when building a matrix, the innermost index is the column
    np.array(                                                       \
  [[ e.p[i,l]*e.p[j,l] for i in range(e.pk)] for j in range(e.pk) ] \
    ) for l in range(e.m) ] )                                       \
      
    # Compute the local projections
    for e,fxi in zip(G.e,Fxi):
      
      ii = e.poly - B.bounds[0]

      bme  = B.e[ii]
      lm   = W[ii].lm
      lf   = W[ii].lf
      fxik = W[ii].fxik
      MK   = W[ii].MK

      # Select the required field
      fxik[:,:] = np.reshape( fxi[key] ,(nfields_K,-1),order='F').T

      # Scaled weights
      wga = e.me.scale_wg( bme.xig , bme.wg );

      # Scaled interpolation values
      for row,w in zip(fxik,wga):
        row *= w

      # Assemble the local projection system
      lm[:,:] = 0.0
      for l in range(bme.m):
        lm +=  wga[l] * MK[l]
      # efficient evaluation of  lf = bme.p * fxik
      np.dot( bme.p , fxik , out=lf )
      
      # Lagrangian basis -> dofs are the nodal values
      fe = np.linalg.solve(lm,lf)

      Fno[ e.iv , : ] += fe
      cnt[ e.iv     ] += 1

    for row,n in zip(Fno,cnt):
      row /= n
  
  return FNOD_V

