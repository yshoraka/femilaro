# Include file for the top level Makefile

# Prefix of this test
T_PREF :=B

# Note: it is important to write explicitly the path relative to the
# including Makefile, bacause that is where these variables will be
# used.
PWD :=B
$(T_PREF)_DIR :=./$(PWD)

# Test executable
$(T_PREF)_TEXE := b.test

# Build directory for this test
$(T_PREF)_TBUILDDIR := $(TBUILDDIR)/B

# Build directory for the tested modules (needed for the .mod files)
TedBUILDDIR := $(TBUILDDIR)/../B
$(T_PREF)_TedBUILDDIR := $(TedBUILDDIR)

# Object files for this test
TSRC_PF  := $(notdir $(wildcard $(PWD)/*.pf))
TSRC_F90 := $(TSRC_PF:.pf=.F90)
TOBJ     := $(TSRC_F90:.F90=.o)
$(T_PREF)_TF90 := $(TSRC_F90)
$(T_PREF)_TOBJ := $(TOBJ)

# FEMilaro libraries
$(T_PREF)_FMLLIBS := -lfem -lode -lfml_linear_solvers -lfml_general_utilities

# FEMilaro objects (not included in _FMLLIBS )
$(T_PREF)_FMLOBJS := $(wildcard $(TedBUILDDIR)/mod_*.o)

# Dependece on other tests
$(T_PREF)_OTHERTESTINC := -I../$(FEM_DIR) $(FEM_OTHERTESTINC)
$(T_PREF)_OTHERTESTOBJ := $(patsubst %, ../$(FEM_DIR)/%, $(FEM_TOBJ)) $(FEM_OTHERTESTOBJ)

# Accumulate the general lists
T_PREFS += $(T_PREF)
T_EXES += $$($(T_PREF)_TEXE)

# List here the dependencies among test modules.

mod_sol_br1_locmat_tests.o: \
  mod_h2d_master_el_tests.o \
  mod_h2d_cgdofs_tests.o

mod_sol_br1_ode_tests.o: \
  mod_h2d_cgdofs_tests.o
