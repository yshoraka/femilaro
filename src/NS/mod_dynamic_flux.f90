!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Michele Nini                   <michelenini88@gmail.com>

!<---------------------------------------------------------------------
module mod_dynamic_flux

!----------------------------------------------------------------------

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_messages, only: &
   mod_messages_initialized, &
   error, warning, info

 use mod_grid, only: &
   mod_grid_initialized, &
   t_grid,el_linear_size

 use mod_base, only: &
   mod_base_initialized, &
   t_base

 use mod_sympoly, only: &
   mod_sympoly_initialized, &
   nll_sympoly

 use mod_atm_refstate, only: &
   mod_atm_refstate_initialized, &
   t_atm_refstate
 
 use mod_turb_flux_base, only: &
   mod_turb_flux_base_initialized, &
   c_turbmod,  c_turbmod_progs, c_turbmod_diags, &
   t_turb_diags

 use mod_dgcomp_testcases, only: &
   mod_dgcomp_testcases_initialized, &
   t_phc, phc, coeff_visc
 
 use mod_viscous_flux, only: &
   mod_viscous_flux_initialized, &
   t_viscous_flux, compute_vf_flux

!----------------------------------------------------------------------
 
 implicit none

!----------------------------------------------------------------------

! Module interface

 public :: &
   mod_dynamic_flux_constructor, &
   mod_dynamic_flux_destructor,  &
   mod_dynamic_flux_initialized, &
   df_compute_coeff_diags,       &
   compute_df_flux,              &
   t_dynamic_flux,               &
   t_dynamic_diags
   

 private

!----------------------------------------------------------------------

! Module types and parameters

 ! public members

 !> Dynamic flux
 !!
 !! The field \c grad stores the gradients of the following quatities:
 !! <ul>
 !!  <li> temperature (second index equal to 1)
 !!  <li> velocity    (second index from 2 to <tt>1+grid\%d</tt>)
 !!  <li> tracers     (second index \f$\geq\f$<tt>2+grid\%d</tt>)
 !! </ul>
 !! Some private fields are also included for the computation of the
 !! diagnostics.
 type, extends(t_viscous_flux) :: t_dynamic_flux
  contains 
  procedure, pass(tm) :: init                => df_init
  procedure, pass(tm) :: clean               => df_clean
  procedure, pass(tm) :: compute_coeff_diags => df_compute_coeff_diags
  procedure, pass(tm) :: flux                => df_flux
 end type t_dynamic_flux
 
 type, extends(c_turbmod_diags) :: t_dynamic_diags
  real(wp), allocatable :: coeffs(:,:)
 end type t_dynamic_diags

 type t_filter
  !> projectors [Gauss weights * basis functions] (complete)
  real(wp), allocatable :: pp(:,:)
  !> projectors [Gauss weights * basis functions] (partial-dynamic)
  real(wp), allocatable :: pp_dyn(:,:)
  !> projectors [Gauss weights * basis functions] (partial-multiscale)
  ! real(wp), allocatable :: pp_ms(:,:)
 end type t_filter

! Module variables
 ! public members
 logical, protected ::               &
   mod_dynamic_flux_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_dynamic_flux'
 type(t_filter) :: filter
 integer :: k_dyn, pk_dyn

!----------------------------------------------------------------------

contains

!----------------------------------------------------------------------

 subroutine mod_dynamic_flux_constructor(base, grid, n_basis_function)
  type(t_base), intent(in) :: base
  type(t_grid), intent(in) :: grid
  integer, intent(in) :: n_basis_function 
  integer :: i
  real(wp), allocatable :: filter_dyn(:,:)
  character(len=*), parameter :: &
   this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
        (mod_grid_initialized.eqv..false.) .or. &
        (mod_base_initialized.eqv..false.) .or. &
(mod_atm_refstate_initialized.eqv..false.) .or. &
(mod_turb_flux_base_initialized.eqv..false.).or.&
(mod_dgcomp_testcases_initialized.eqv..false.).or.&
(mod_viscous_flux_initialized.eqv..false.)) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_dynamic_flux_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   
   ! Create dynamic basis 
   k_dyn = n_basis_function
   pk_dyn = nll_sympoly(grid%me%d,k_dyn)
 
   ! Set the filter module variable
   allocate( filter%pp_dyn(base%m,base%m))!filter%pp_ms(base%m,base%m)
   ! dynamic low-pass filtering
   allocate( filter_dyn(pk_dyn, base%m))
   do i=1,pk_dyn
     filter_dyn(i,:) = base%wg * base%p(i,:)
   enddo
   filter%pp_dyn = matmul( transpose(filter_dyn) , &
                           base%p(1:pk_dyn,:) )
   mod_dynamic_flux_initialized = .true.  
 end subroutine mod_dynamic_flux_constructor

!----------------------------------------------------------------------

 subroutine mod_dynamic_flux_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_dynamic_flux_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_dynamic_flux_initialized = .false.
 end subroutine mod_dynamic_flux_destructor

!----------------------------------------------------------------------

 subroutine df_init(tm,progs,diags,td,grid,base,ntrcs)
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  integer, intent(in) :: ntrcs
  class(t_dynamic_flux), intent(inout) :: tm
  class(c_turbmod_progs), allocatable, intent(out) :: progs
  class(c_turbmod_diags), allocatable, intent(out) :: diags
  type(t_turb_diags), intent(out) :: td

  integer :: id, jd
  character :: vel(3)
   tm%d           = grid%d
   tm%ntrcs       = ntrcs
   tm%initialized = .true.  
   allocate(tm%wg(base%m)); tm%wg = base%wg
   allocate(tm%base_p(base%pk,base%m)); tm%base_p = base%p
  
   !diags
   allocate(t_dynamic_diags::diags)
   diags%ngrad    = 1 + tm%d + tm%ntrcs
   allocate(diags%grad(tm%d,diags%ngrad,base%pk,grid%ne))
   select type(diags); type is(t_dynamic_diags)
    allocate(diags%coeffs(6,grid%ne)); 
   end select

   !diagnostic
   vel=['u','v','w']
   td%ndiags = 11 
   allocate( td%diag_names(td%ndiags) ,  &
     td%diags(td%ndiags,base%pk,grid%ne) )
   td%diag_names(1) = '|nabla^S u + lambda*div(u)|'
   td%diag_names(2) = 'dynamic dissipation'
   do id=1,tm%d
     do jd =1,tm%d
       td%diag_names(2+jd+(id-1)*tm%d) = 'tau_'// vel(id) // vel(jd)
     enddo
   enddo
 end subroutine df_init

!----------------------------------------------------------------------

 pure &
  subroutine df_clean(tm,diags,td)
  class(t_dynamic_flux), intent(inout) :: tm
  class(c_turbmod_diags), allocatable, intent(inout) :: diags
  type(t_turb_diags), intent(out) :: td
   
   deallocate(tm%wg,tm%base_p)
   deallocate(diags) ! deallocates all the components
   ! td components deallocated automatically

 end subroutine df_clean

!----------------------------------------------------------------------

 pure &
  subroutine df_compute_coeff_diags(tm,cd , grid,base,uuu,progs,atm_ref)
  class(t_dynamic_flux), intent(in) :: tm
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  real(wp),     intent(in) :: uuu(:,:,:)
  class(t_atm_refstate), intent(in) :: atm_ref(:,:)
  class(c_turbmod_progs), allocatable, intent(in) :: progs
  class(c_turbmod_diags), intent(inout) :: cd

  ! local variables
  integer  :: d,je,id,jd,l,i
  real(wp) :: ci_num(base%m), ci_den(base%m)
  real(wp) :: uuue( size(uuu,1), size(base%p,1) )
  real(wp) :: fuuu( size(uuu,1), base%m ), &
              uuug( size(uuu,1), base%m ), frho(base%m), &
              rhog(base%m), fe(base%m), eg(base%m), &
              fm(grid%d,base%m),mg(grid%d,base%m), &
              fuu(grid%d,base%m), uug(grid%d,base%m), &
              fT(base%m),Tg(base%m) 
  real(wp) :: lm(grid%d,grid%d,base%m,2),Leom(grid%d,grid%d,base%m), &
              Leom_d(grid%d,grid%d,base%m), &
              Leom_mod(grid%d,grid%d,base%m), &
              lq(grid%d,base%m,2), Leoq(grid%d,base%m), &
              Leoj_mod(grid%d,base%m), lj(grid%d,base%m,2), &
              Leoj(grid%d,base%m),Leoq_mod(grid%d,base%m)
  real(wp) :: ldivu, fldivu, sym_d(grid%d,grid%d), &
              fsym_d(grid%d,grid%d), s2(base%m),fs2(base%m),s2_e
  real(wp) :: delta, fdelta, gdl, fgdl, c1, fc1
  real(wp) :: alfa_m(grid%d,grid%d,base%m,2), alfa_i(base%m,2), &
              alfa_q(grid%d,base%m,2), alfa_j(grid%d,base%m,2)
  real(wp) :: cj_check(grid%d,base%m), guu_check, cq_e, cj_e, &
              ci_e, cj_check_e(grid%d),cd_e
  real(wp) :: gradg(grid%d, size(cd%grad,2),base%m), & 
              fgradg(grid%d, size(cd%grad,2),base%m), &
              guu(grid%d,grid%d,base%m), &
              fguu(grid%d,grid%d,base%m), &
              gsuu(grid%d,grid%d,base%m), &
              fgsuu(grid%d,grid%d,base%m), &
              gtt(grid%d,base%m), fgtt(grid%d,base%m)

   select type(cd); class is(t_dynamic_diags)
   d = grid%d
   do je=1,grid%ne
   
     ! reset accumulation variables
     s2 = 0.0_wp
     fs2 = 0.0_wp
   
     ! local solution
     uuue = uuu(:,:,je)
        
     ! element significant length
     gdl = real(nll_sympoly(d,base%k),wp)**(1.0_wp/3.0_wp)
     fgdl = real(nll_sympoly(d,k_dyn),wp)**(1.0_wp/3.0_wp)

     delta = el_linear_size(grid%e(je)) / gdl
     fdelta = el_linear_size(grid%e(je)) / fgdl

     ! solution at Gauss nodes (recover from elsewhere??)
     ! low pass filter solution
     
     fuuu = matmul( uuue(:,1:pk_dyn), base%p(1:pk_dyn,:) )
     frho = fuuu(1,:) + atm_ref(:,je)%rho
     fe   = fuuu(2,:) + atm_ref(:,je)%e
     fm   = fuuu(2+1:2+d,:)
     do id=1,d
       fuu(id,:) = fm(id,:)/frho
     enddo
     fT = 1.0_wp/phc%cv * &
          ( fe/frho - 0.5_wp*sum(fuu**2,1) - atm_ref(:,je)%phi )
    
     ! full solution
     uuug = fuuu + matmul( uuue(:,pk_dyn+1:), &
                           base%p(pk_dyn+1:,:) )
     rhog = uuug(1,:) + atm_ref(:,je)%rho
     eg   = uuug(2,:) + atm_ref(:,je)%e
     mg   = uuug(2+1:2+d,:)
     do id=1,d
       uug(id,:) = mg(id,:)/rhog
     enddo
     Tg   = 1.0_wp/phc%cv *  &
            ( eg/rhog -0.5_wp*sum(uug**2,1) - atm_ref(:,je)%phi )

     ! 1st plane: complete solution and then filter
     ! 2nd plane: use double filtered solution       
     fgradg = 0.0_wp
     do l= 1,size(base%p,2) !m
       do i=1,pk_dyn
         fgradg(:,:,l) = fgradg(:,:,l) + cd%grad(:,:,i,je)* &
                         base%p(i,l)
       enddo
     enddo
     fgtt = fgradg(:,1,:)
     fguu = fgradg(:,1+1:1+d,:)

     gradg=fgradg
     do l= 1,size(base%p,2)
       do i= pk_dyn+1,size(base%p,1)
         gradg(:,:,l) = gradg(:,:,l) + cd%grad(:,:,i,je)*base%p(i,l)
       enddo
     enddo
     gtt = gradg(:,1,:)
     guu = gradg(:,1+1:1+d,:)

     do id=1,d
       ! Momentum and energy terms
       do jd=1,id-1 ! symmetry
         fgsuu(id,jd,:) = fgsuu(jd,id,:)
         gsuu(id,jd,:) = gsuu(jd,id,:)
         lm(id,jd,:,:) = lm(jd,id,:,:) 
       enddo 
       do jd=id,d       
         gsuu(id,jd,:) = gradg(jd,1+id,:) + gradg(id,1+jd,:)
         fgsuu(id,jd,:) = fgradg(jd,1+id,:) + fgradg(id,1+jd,:)
         lm(id,jd,:,1) = matmul( mg(id,:)*mg(jd,:)/rhog , &
                                 filter%pp_dyn )
         lm(id,jd,:,2) = fm(id,:)*fm(jd,:)/frho
       enddo                 
       lq(id,:,1) = matmul( mg(id,:)*Tg , filter%pp_dyn )
       lq(id,:,2) = fm(id,:)*fT
       lj(id,:,1) = matmul( mg(id,:)*sum(uug**2,1) , filter%pp_dyn )
       lj(id,:,2) = fm(id,:)*sum(fuu**2,1)
     enddo

     ! Leonard stresses (resolved)
     Leom = lm(:,:,:,1) - lm(:,:,:,2)
     Leoq = lq(:,:,1) - lq(:,:,2)
     Leoj = lj(:,:,1) - lj(:,:,2)
        
     ! Gauss nodes loop
     do l=1,base%m
       s2(l) = s2(l) + sum(sum(gsuu(:,:,l)**2,1),1)
       fs2(l) = fs2(l) + sum(sum(fgsuu(:,:,l)**2,1),1)

       ! deviatoric part of simmetric gradient
       ldivu = - 1.0_wp/3.0_wp*tr(gsuu(:,:,l))
       fldivu = - 1.0_wp/3.0_wp*tr(fgsuu(:,:,l))
       sym_d = gsuu(:,:,l)
       fsym_d = fgsuu(:,:,l)
       do id=1,d
         sym_d(id,id) = sym_d(id,id) + ldivu
         fsym_d(id,id) = fsym_d(id,id) + fldivu
       enddo

       ! deviatoric Leonard stress
       Leom_d = Leom
       do id=1,d
         Leom_d(id,id,l) = Leom_d(id,id,l) &
                         - 1.0_wp/3.0_wp*tr(Leom(:,:,l))
       enddo

       ! modelled stresses
       c1 = delta**2 * rhog(l)
       fc1 = fdelta**2 * frho(l)
       alfa_m(:,:,l,1) = - c1*sqrt( s2(l)/2.0_wp )*sym_d
       alfa_m(:,:,l,2) = - fc1*sqrt( fs2(l)/2.0_wp )*fsym_d
       alfa_i(l,1) = c1*s2(l)/2.0_wp
       alfa_i(l,2) = fc1*fs2(l)/2.0_wp
       alfa_q(:,l,1) = - phc%cp * c1*sqrt( s2(l)/2.0_wp )* &
                         gtt(:,l)
       alfa_q(:,l,2) = - phc%cp * fc1*sqrt( fs2(l)/2.0_wp )* &
                         fgtt(:,l)
       do id=1,d
         alfa_j(id,l,1) = - c1*sqrt( s2(l)/2.0_wp ) * &
                            sum( uug(:,l)*guu(:,id,l) )
         alfa_j(id,l,2) = - fc1*sqrt( fs2(l)/2.0_wp ) * &
                         sum( fuu(:,l)*fguu(:,id,l) )
         !  Pay attention to avoid small values in the denominator
         if( sum( uug(:,l)*guu(:,id,l) ).gt.10.0_wp**(-6.0_wp)) then 
           guu_check = sum( uug(:,l)*guu(id,:,l) ) / &
                       sum( uug(:,l)*guu(:,id,l) )
           cj_check(id,l) = 1.0_wp + sign( &      
                            minval( (/ 10.0_wp**3.0_wp , &
                            abs(guu_check) /) ) ,guu_check )
          else
            cj_check(id,l) = 1.0_wp
          endif
       enddo
     enddo 

     ! projection component by component
     do id=1,d
       do jd=1,id-1; alfa_m(id,jd,:,1) = alfa_m(jd,id,:,1);  enddo
       do jd=id,d
         alfa_m(id,jd,:,1) = matmul( alfa_m(id,jd,:,1) , &
                                     filter%pp_dyn )
       enddo
       alfa_q(id,:,1) = matmul( alfa_q(id,:,1) , filter%pp_dyn )
       alfa_j(id,:,1) = matmul( alfa_j(id,:,1) , filter%pp_dyn )
     enddo
     ! Leonard stress (modelled)
     Leom_mod = alfa_m(:,:,:,2) - alfa_m(:,:,:,1)      
     Leoq_mod = alfa_q(:,:,2) - alfa_q(:,:,1)
     Leoj_mod = alfa_j(:,:,2) - alfa_j(:,:,1)
     alfa_i(:,1) = matmul( alfa_i(:,1) , filter%pp_dyn )
 
     ! average constant over element     
     s2_e = sum( base%wg*s2 ) / base%me%vol

     ! Numerator and denominator are treated separately:
     ! the internal sum operations are over the components of the 
     ! tensors in order to get the gauss nodal value of the 
     ! constant; then average over the element is performed
     ! 1) Cd -> stress tensor (deviatoric) ---------------   
     cd_e = sum( base%wg * sum( sum( Leom_d*Leom_mod,1 ),1 ) ) / &
            sum( base%wg * sum( sum( Leom_mod*Leom_mod,1 ),1 ) ) 
    
     ! 3) Cd/Prsgs -> turbulent diffusion (Qj) ----------------
     ! direct estimation of the quotient
       
     cq_e = sum( base%wg * sum( Leoq*Leoq_mod,1 ) ) / &
            sum( base%wg * sum( Leoq_mod*Leoq_mod,1 ) )
         
     ! 4) Cd*Cj -> turbulent diffusion (Jj) -------------------
     ! direct estimation of the product
            
     cj_e = sum( base%wg * sum( Leoj*Leoj_mod,1 ) ) / &
            sum( base%wg * sum( Leoj_mod*Leoj_mod,1 ) )

     do id=1,d
       cj_check_e(id) = sum( base%wg*cj_check(id,:) ) / & 
                            base%me%vol / ( phc%re*delta**2* &
                            sqrt(s2_e/2.0_wp) )
     enddo          
     ! 2) Ci -> stress tensor (isotropic) ----------------
     do l=1,base%m
       ci_num(l) = tr( Leom(:,:,l) )
       ci_den(l) = (alfa_i(l,2)-alfa_i(l,1))
     enddo        
     ci_e = sum( base%wg*ci_num ) / sum( base%wg*ci_den )

     ! constraints
     ! NB: instead of using coeff_visc( x , rhog,pg ) and performing
     ! mean over the element, it's assumed here that nu_e=1.0_wp
     ! sgs viscosity must be greater than -nu in order to have 
     ! positive total kinetic dissipation
     cd_e = maxval((/ -1.0_wp/(phc%re*delta**2*sqrt(s2_e/2.0_wp)), &
                    cd_e /))

     ! sgs viscosity must be smaller than 2*nu for stability reason
     ! connected to explicit time integration
     cd_e = minval(  (/  2.0_wp/(phc%re*delta**2* & 
                       sqrt(s2_e/2.0_wp)),cd_e /))

     ! ci is clipped in this way for stability reasons
     ci_e = maxval( (/ 0.0_wp , ci_e /) )
     ci_e = minval( (/ 0.05_wp , ci_e /) )

     ! cq is the equivalent of cd/prsgs: start from the range 
     ! imposed on cd and require that 0.1<prsgs<1.5 to obtain the
     ! following limits
     cq_e = maxval((/-10.0_wp/(phc%re*delta**2*sqrt(s2_e/2.0_wp)), &
                     cq_e /))
     cq_e = minval((/ 20.0_wp/(phc%re*delta**2*sqrt(s2_e/2.0_wp)), &
                     cq_e /))
     ! sgs turbulent diffusion
     ! the magnitude of Jj and uk*taujk must be the same
     ! cj_e = sign( minval( (/ minval(abs(cj_check_e)),abs(cj_e) /)) &
     !       , cj_e ) 
 
     ! storing coefficients
     cd%coeffs(1,je) = s2_e
     cd%coeffs(2,je) = delta
     cd%coeffs(3,je) = cd_e
     cd%coeffs(4,je) = ci_e
     cd%coeffs(5,je) = cq_e
     cd%coeffs(6,je) = cj_e 
   enddo
   
   end select
 end subroutine df_compute_coeff_diags

!----------------------------------------------------------------------

 !> \warning If the optional argument \c td is present, it is assumed
 !! that the function is called for all the quadrature points in one
 !! element (i.e. not for a side).
 pure &
  subroutine df_flux(fem,tm,ie,x,bp,grid,consv,atm_ref, rho,p,uu,cc, &
                     progs,diags,td,uuu_mean,tau_mean,uu_square_mean,&
                     ht)
  class(t_dynamic_flux), intent(in) :: tm
  integer,  intent(in) :: ie
  real(wp), intent(in) :: x(:,:)  !< coordinates
  real(wp), intent(in) :: bp(:,:) !< basis functions
  !> conservative variables (deviations)
  type(t_grid),          intent(in) :: grid
  real(wp), intent(in) :: consv(:,:)
  class(t_atm_refstate), intent(in) :: atm_ref(:)
  !> Total values for density, pressure, velocity and tracers
  real(wp), intent(in) :: rho(:), p(:), uu(:,:), cc(:,:)
  class(c_turbmod_progs), allocatable, intent(in) :: progs
  class(c_turbmod_diags), intent(in) :: diags
  real(wp), intent(out) :: fem(:,:,:)
  type(t_turb_diags), intent(inout), optional :: td 
  real(wp), intent(in), optional :: uuu_mean(:,:)
  real(wp), intent(in), optional :: tau_mean(:,:,:)
  real(wp), intent(in), optional :: uu_square_mean(:,:,:)
  real(wp), intent(inout), optional :: ht(:,:)

  ! local variables
  real(wp) :: fem_les(tm%d,diags%ngrad,size(x,2))
  integer  :: i, l, id, jd
  real(wp) :: sym(tm%d,tm%d,size(x,2)), &
              gradg(tm%d,diags%ngrad,size(x,2))
   
   ! viscous contribution      
   call compute_vf_flux(fem, tm,ie,x,bp, consv,atm_ref, rho,p,uu,cc, &
                        diags, td=td, vf_sym=sym , vf_gradg=gradg)

   select type(diags); type is(t_dynamic_diags)
 
   ! LES contribution
   call compute_df_flux(fem_les, tm,ie,x,bp, consv,atm_ref, &
                        rho,p,uu,diags, sym, gradg) 

   do l=1,size(x,2)
     !Complete energy flux with turbulent diffusion (1):
     !Knight98 -> uk*tau_jk^sgs + 0.5*uj*tau_kk^sgs
     !Note: this contribution is added here in order to allow the  
     !energy equation correction required by hybrid method
     fem_les(:,1,l) = fem_les(:,1,l)                                  &
                     + 0.5_wp*uu(:,l)*tr(fem_les(:,2:1+tm%d,l))       &
                     + matmul(uu(:,l),transpose(fem_les(:,2:1+tm%d,l)))  
   enddo
 
   fem = fem_les + fem
  
   if (present(td)) then       
     do id=1,tm%d
       do jd=1,tm%d
         td%diags(2+jd+(id-1)*tm%d,:,ie) = matmul(tm%base_p , &
                                           tm%wg*fem_les(id,1+jd,:))
       enddo
     enddo
   endif
   
   end select
 end subroutine df_flux
 
!---------------------------------------------------------------------

 pure &
  subroutine compute_df_flux(fem, tm,ie,x,bp, consv,atm_ref, &
                             rho,p,uu,diags, sym,gradg) 
  class(t_dynamic_flux),      intent(in)  :: tm
  integer,                    intent(in)  :: ie
  real(wp),                   intent(in)  :: x(:,:)
  real(wp),                   intent(in)  :: bp(:,:)
  real(wp),                   intent(in)  :: consv(:,:)
  class(t_atm_refstate),      intent(in)  :: atm_ref(:)
  real(wp),                   intent(in)  :: rho(:), p(:), uu(:,:)
  class(t_dynamic_diags),     intent(in)  :: diags 
  real(wp),                   intent(in)  :: sym(:,:,:)
  real(wp),                   intent(in)  :: gradg(:,:,:)
  real(wp),                   intent(out) :: fem(:,:,:) 
                 
  ! local variables
  integer :: l, id
  real(wp) :: tau_sgs(tm%d,tm%d),tau_kk, ekin_flux(tm%d), &
              nu_sgs(size(x,2)), ldivu, sym_d(tm%d,tm%d)                   

   do l=1,size(x,2)
      
     ldivu = (-2.0_wp/3.0_wp)*0.5_wp*tr(sym(:,:,l)) 
     sym_d(:,:) = sym(:,:,l) 
     do id = 1,tm%d
       sym_d(id,id) = sym_d(id,id) + ldivu ! Deviatoric part of 
     enddo                                 ! symmetric gradient

     nu_sgs(l) = diags%coeffs(3,ie) * diags%coeffs(2,ie)**2 * &
                 sqrt(diags%coeffs(1,ie)/2.0_wp)
  
     ! momentum flux
     ! deviatoric sgs stress tensor
     tau_sgs = -rho(l)*nu_sgs(l)*sym_d
     ! isotropic sgs stress tensor
     tau_kk = diags%coeffs(4,ie) * rho(l) * diags%coeffs(2,ie)**2 * &
              diags%coeffs(1,ie)/2.0_wp 
     do id=1,tm%d
       tau_sgs(id,id) = tau_sgs(id,id) + tau_kk
     enddo
     fem(:,2:1+tm%d,l) = tau_sgs 
 
     ! energy flux
     do id=1,tm%d
       ekin_flux(id) = sum( uu(:,l)*gradg(:,id+1,l) )
     enddo
     fem(:,  1  ,l) =  &
       ! sgs heat flux
       - phc%cp * rho(l) * diags%coeffs(2,ie)**2*sqrt( & 
       diags%coeffs(1,ie)/2.0_wp) * diags%coeffs(5,ie)*gradg(:,1,l) &
       ! turbulent diffusion (2): dynamic cj*rho*nusgs*de_j(1/2*uk*uk)
       - 0.5_wp*diags%coeffs(6,ie) * rho(l) * diags%coeffs(2,ie)**2*&
       sqrt(diags%coeffs(1,ie)/2.0_wp) * ekin_flux     

   enddo 

 end subroutine compute_df_flux   

!----------------------------------------------------------------------

 pure &
  function tr(a)
  real(wp), intent(in) :: a(:,:)
  real(wp) :: tr
  integer :: i

   tr = a(1,1)
   do i=2,minval(shape(a))
     tr = tr + a(i,i)
   enddo
 end function tr

!----------------------------------------------------------------------

end module mod_dynamic_flux

