!! Copyright (C) 2016 Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> \brief
!!
!! Provides some types to define finite element coefficients
!!
!! \n
!!
!! At first glance, finite element coefficients are simply known
!! functions \f$f(t,x)\f$. However, in practice there might be some
!! complications which make defining these functions not
!! straightforward, such as:
!! <ul>
!!  <li> the function might depend nonlinearly from the finite element
!!  solution itself
!!  <li> for efficiency reasons, some computations might be cached.
!! </ul>
!! It is very convenient to hide these concerns from those parts of
!! the code which merely require the value of the coefficient, such as
!! the computation of the local matrices, to avoid passing around a
!! large number of case specific arguments. This justifies introducing
!! some classes for the problem coefficients.
!!
!! Typically, a complicated coefficient would include pointers to the
!! finite element solution, to allow implementing nonlinear terms, and
!! some private fields to precompute some data for efficiency reasons.
!! This is completely transparent to the code which needs to evaluate
!! the coefficient, and is only concerned with the interface of the
!! abstract classes provided here.
!!
!! The abstract classes provided here have a rather rich interface, in
!! order to allow various optimizations. For instance, for a
!! coefficient depending nonlinearly on the finite element solution,
!! evaluation at the quadrature nodes is much more convenient than
!! evaluation at a generic position.
!!
!! \note To ensure consistency, the results of the methods of a
!! <tt>c_*_coeff</tt> object must be used only "as such", without
!! additional processing. For instance, one could compute the values
!! of a scalar field and then use them to compute its gradient; this
!! however would not necessarily be consistent with the gradient
!! provided by the \c c_s_coeff object itself, or this gradient could
!! even not be defined at all for this particular field.
!!
!! \note The evaluation of a coefficient at the side quadrature nodes
!! can be implemented in two ways: either based on a global side, or
!! on an element and a local side. We choose the second option because
!! it seems more natural for a finite element method, especially for
!! coefficients which depend on the finite element solution itself.
!! This also implies that the side quadrature nodes are ordered using
!! the element-induced order.
module mod_h2d_coefficients

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   pure_abort, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_initialized, &
   c_h2d_me_geom, c_h2d_me_base

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_2dv, t_2ds, t_2de

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_initialized, &
   t_b_2dv, t_b_2ds, t_b_2de

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_h2d_coefficients_constructor, &
   mod_h2d_coefficients_destructor,  &
   mod_h2d_coefficients_initialized, &
   c_s_coeff, c_v_coeff, c_t_coeff, c_bc, &
   c_v_coeffs, c_t_coeffs

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Generic coefficient
 type, abstract :: c_coeff
 end type c_coeff

 !> Scalar coefficient
 type, abstract, extends(c_coeff) :: c_s_coeff
 contains
  !> evaluate the field at an arbitrary position
  procedure, pass(coeff) :: ev   => s_coeff_ev
  !> evaluate the field at the element quadrature points
  procedure, pass(coeff) :: evg  => s_coeff_evg
  !> evaluate the field at the Lagrangian nodes
  procedure, pass(coeff) :: evl  => s_coeff_evl
  !> evaluate the field at the side quadrature points
  procedure, pass(coeff) :: evgs => s_coeff_evgs
 end type c_s_coeff

 !> Vector coefficient
 type, abstract, extends(c_coeff) :: c_v_coeff
  integer :: d !< size of the vector
 contains
  !> evaluate the field at an arbitrary position
  procedure, pass(coeff) :: ev   => v_coeff_ev
  !> evaluate the field at the element quadrature points
  procedure, pass(coeff) :: evg  => v_coeff_evg
  !> evaluate the field at the Lagrangian nodes
  procedure, pass(coeff) :: evl  => v_coeff_evl
  !> evaluate the field at the side quadrature points
  procedure, pass(coeff) :: evgs => v_coeff_evgs
 end type c_v_coeff

 !> Multiple vector coefficients
 type, abstract, extends(c_coeff) :: c_v_coeffs
  integer :: d !< size of the vector
  integer :: n !< number of vectors
 contains
  !> evaluate the fields at an arbitrary position
  procedure, pass(coeff) :: ev   => v_coeffs_ev
  !> evaluate the fields at the element quadrature points
  procedure, pass(coeff) :: evg  => v_coeffs_evg
  !> evaluate the fields at the Lagrangian nodes
  procedure, pass(coeff) :: evl  => v_coeffs_evl
  !> evaluate the fields at the side quadrature points
  procedure, pass(coeff) :: evgs => v_coeffs_evgs
 end type c_v_coeffs

 !> Tensor coefficient
 type, abstract, extends(c_coeff) :: c_t_coeff
  integer :: d !< rank of the tensor
 contains
  !> evaluate the field at an arbitrary position
  procedure, pass(coeff) :: ev   => t_coeff_ev
  !> evaluate the field at the element quadrature points
  procedure, pass(coeff) :: evg  => t_coeff_evg
  !> evaluate the field at the Lagrangian nodes
  procedure, pass(coeff) :: evl  => t_coeff_evl
  !> evaluate the field at the side quadrature points
  procedure, pass(coeff) :: evgs => t_coeff_evgs
 end type c_t_coeff

 !> Multiple tensor coefficients
 type, abstract, extends(c_coeff) :: c_t_coeffs
  integer :: d !< rank of the tensor
  integer :: n !< number of tensors
 contains
  !> evaluate the tensors at an arbitrary position
  procedure, pass(coeff) :: ev   => t_coeffs_ev
  !> evaluate the tensors at the element quadrature points
  procedure, pass(coeff) :: evg  => t_coeffs_evg
  !> evaluate the tensors at the Lagrangian nodes
  procedure, pass(coeff) :: evl  => t_coeffs_evl
  !> evaluate the tensors at the side quadrature points
  procedure, pass(coeff) :: evgs => t_coeffs_evgs
 end type c_t_coeffs

 !> Boundary condition
 !!
 !! This coefficient gives two kinds of information:
 !! <ul>
 !!  <li> the boundary condition: \c b_dir or \c b_neu, together with
 !!  the type of such condition, i.e. an arbitrary integer (see
 !!  \field_fref{mod_bcs,t_b_v,btype}
 !!  <li> the value of the boundary condition.
 !! </ul>
 !!
 !! \note \c c_bcs_coeff coefficients can be used to override the
 !! values provided in the two fields \c bc and \c btype defined in \c
 !! mod_bcs; this is useful for problems with many fields and
 !! different conditions for each field.
 !!
 !! The \c coeff field can be used to represent a scalar, vector or
 !! tensor coefficient. Having such fields allows collecting in a
 !! single object the type of the boundary condition and its value.
 !! Notice that typically one will always use a select type for the \c
 !! coeff field, since scalar and tensor coefficients require
 !! different code.
 type, abstract, extends(c_coeff) :: c_bc
  class(c_coeff), allocatable :: coeff
 contains
  procedure(i_bc_bctype), pass(coeff), deferred :: bc
 end type c_bc

 abstract interface
  pure subroutine i_bc_bctype(bc,btype,coeff,breg)
   import :: c_bc
   class(c_bc), intent(in) :: coeff
   integer,     intent(in) :: breg
   integer,     intent(out) :: bc, btype
  end subroutine i_bc_bctype
 end interface

! Module variables

 ! public members
 logical, protected ::               &
   mod_h2d_coefficients_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_h2d_coefficients'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_h2d_coefficients_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false. ) .or. &
          (mod_kinds_initialized.eqv..false. ) .or. &
  (mod_h2d_master_el_initialized.eqv..false. ) .or. &
       (mod_h2d_grid_initialized.eqv..false. ) .or. &
        (mod_h2d_bcs_initialized.eqv..false. ) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_coefficients_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_coefficients_initialized = .true.
 end subroutine mod_h2d_coefficients_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_h2d_coefficients_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_h2d_coefficients_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_h2d_coefficients_initialized = .false.
 end subroutine mod_h2d_coefficients_destructor

!-----------------------------------------------------------------------

 pure function s_coeff_ev(coeff,e,bme,x) result(s)
  class(c_s_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  real(wp),             intent(in) :: x(:,:)
  real(wp) :: s(size(x,2))

   ! this function must be overridden
   call pure_abort()

 end function s_coeff_ev

 pure function s_coeff_evg(coeff,e,bme) result(s)
  class(c_s_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  real(wp) :: s(bme%m)

   s = coeff%ev( e,bme, e%me%map( bme%xig ) )

 end function s_coeff_evg

 pure function s_coeff_evl(coeff,e,bme) result(s)
  class(c_s_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  real(wp) :: s(bme%pk)

   s = coeff%ev( e,bme, e%me%map( bme%xnodes ) )

 end function s_coeff_evl

 pure function s_coeff_evgs(coeff,e,bme,isl,be) result(s)
  class(c_s_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  integer,              intent(in) :: isl   !< local side index
  !> boundary element
  !!
  !! The reason why this argument is optional is that this functions
  !! makes sense both for internal sides and for boundary sides, but a
  !! \c t_b_2de object is defined only for boundary sides. When this
  !! function is called without this argument, one can assume that it
  !! is being called for an internal side.
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: s(bme%ms)

   s = coeff%ev( e,bme, e%me%map( bme%xigb(:,:,isl) ) )

 end function s_coeff_evgs

!-----------------------------------------------------------------------

 pure function v_coeff_ev(coeff,e,bme,x) result(v)
  class(c_v_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp),             intent(in) :: x(:,:)
  real(wp) :: v(coeff%d,size(x,2))

   ! this function must be overridden
   call pure_abort()

 end function v_coeff_ev

 pure function v_coeff_evg(coeff,e,bme) result(v)
  class(c_v_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: v(coeff%d,bme%m)

   v = coeff%ev( e,bme, e%me%map( bme%xig ) )

 end function v_coeff_evg

 pure function v_coeff_evl(coeff,e,bme) result(v)
  class(c_v_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: v(coeff%d,bme%pk)

   v = coeff%ev( e,bme, e%me%map( bme%xnodes ) )

 end function v_coeff_evl

 pure function v_coeff_evgs(coeff,e,bme,isl,be) result(v)
  class(c_v_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: v(coeff%d,bme%ms)

   v = coeff%ev( e,bme, e%me%map( bme%xigb(:,:,isl) ) )

 end function v_coeff_evgs

!-----------------------------------------------------------------------

 pure function v_coeffs_ev(coeff,e,bme,x) result(v)
  class(c_v_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp),             intent(in) :: x(:,:)
  real(wp) :: v(coeff%d,coeff%n,size(x,2))

   ! this function must be overridden
   call pure_abort()

 end function v_coeffs_ev

 pure function v_coeffs_evg(coeff,e,bme) result(v)
  class(c_v_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: v(coeff%d,coeff%n,bme%m)

   v = coeff%ev( e,bme, e%me%map( bme%xig ) )

 end function v_coeffs_evg

 pure function v_coeffs_evl(coeff,e,bme) result(v)
  class(c_v_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: v(coeff%d,coeff%n,bme%pk)

   v = coeff%ev( e,bme, e%me%map( bme%xnodes ) )

 end function v_coeffs_evl

 pure function v_coeffs_evgs(coeff,e,bme,isl,be) result(v)
  class(c_v_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: v(coeff%d,coeff%n,bme%ms)

   v = coeff%ev( e,bme, e%me%map( bme%xigb(:,:,isl) ) )

 end function v_coeffs_evgs

!-----------------------------------------------------------------------

 pure function t_coeff_ev(coeff,e,bme,x) result(t)
  class(c_t_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp),             intent(in) :: x(:,:)
  real(wp) :: t(coeff%d,coeff%d,size(x,2))

   ! this function must be overridden
   call pure_abort()

 end function t_coeff_ev

 pure function t_coeff_evg(coeff,e,bme) result(t)
  class(c_t_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: t(coeff%d,coeff%d,bme%m)

   t = coeff%ev( e,bme, e%me%map( bme%xig ) )

 end function t_coeff_evg

 pure function t_coeff_evl(coeff,e,bme) result(t)
  class(c_t_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: t(coeff%d,coeff%d,bme%pk)

   t = coeff%ev( e,bme, e%me%map( bme%xnodes ) )

 end function t_coeff_evl

 pure function t_coeff_evgs(coeff,e,bme,isl,be) result(t)
  class(c_t_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: t(coeff%d,coeff%d,bme%ms)

   t = coeff%ev( e,bme, e%me%map( bme%xigb(:,:,isl) ) )

 end function t_coeff_evgs

!-----------------------------------------------------------------------

 pure function t_coeffs_ev(coeff,e,bme,x) result(t)
  class(c_t_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp),             intent(in) :: x(:,:)
  real(wp) :: t(coeff%d,coeff%d,coeff%n,size(x,2))

   ! this function must be overridden
   call pure_abort()

 end function t_coeffs_ev

 pure function t_coeffs_evg(coeff,e,bme) result(t)
  class(c_t_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: t(coeff%d,coeff%d,coeff%n,bme%m)

   t = coeff%ev( e,bme, e%me%map( bme%xig ) )

 end function t_coeffs_evg

 pure function t_coeffs_evl(coeff,e,bme) result(t)
  class(c_t_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: t(coeff%d,coeff%d,coeff%n,bme%pk)

   t = coeff%ev( e,bme, e%me%map( bme%xnodes ) )

 end function t_coeffs_evl

 pure function t_coeffs_evgs(coeff,e,bme,isl,be) result(t)
  class(c_t_coeffs),    intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: t(coeff%d,coeff%d,coeff%n,bme%ms)

   t = coeff%ev( e,bme, e%me%map( bme%xigb(:,:,isl) ) )

 end function t_coeffs_evgs

!-----------------------------------------------------------------------

end module mod_h2d_coefficients

