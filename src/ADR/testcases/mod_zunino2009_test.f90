module mod_zunino2009_test
!General comments: test case of: P. Zunino, JCP 2009, 38: 99-126
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_zunino2009_test_constructor, &
   mod_zunino2009_test_destructor,  &
   mod_zunino2009_test_initialized, &
   test_name, &
   test_description,&
   coeff_diff,&
   coeff_adv, &
   coeff_re,  &
   coeff_f,   &
   coeff_dir, &
   coeff_neu, &
   coeff_rob!, &
!   coeff_lam, &
!   coeff_q,   &
!   coeff_divq

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members
 character(len=*), parameter ::    &
   test_name = "zunino2009"

! Module variables

 ! public members
 character(len=100), protected ::    &
   test_description(2)
 logical, protected ::               &
   mod_zunino2009_test_initialized = .false.
 ! private members
 real(wp), parameter :: &
   eps1 = 1e-6_wp, &
   eps2 = 1.0_wp
 character(len=*), parameter :: &
   this_mod_name = 'mod_zunino2009_test'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_zunino2009_test_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_zunino2009_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   write(test_description(1),'(a)') &
     'Test case as in P. Zunino, ICP 2009, 38: 99-126'
   write(test_description(2),'(a,i2,a,i2,a)') &
     '  viscosities eps1 = ',eps1,', eps2 = ',eps2,'.'

   mod_zunino2009_test_initialized = .true.
 end subroutine mod_zunino2009_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_zunino2009_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_zunino2009_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_zunino2009_test_initialized = .false.
 end subroutine mod_zunino2009_test_destructor

!-----------------------------------------------------------------------
 
 pure function coeff_diff(x) result(mu)
 ! The two regions are defined by
 !  region1: y.le.x
 !  region2: y.gt.x
  real(wp), intent(in) :: x(:,:)
  real(wp) :: mu(size(x,1),size(x,1),size(x,2))

  real(wp), dimension(2,2), parameter :: & ! 2x2 identity
    i = reshape( (/ 1.0_wp , 0.0_wp , 0.0_wp , 1.0_wp /) , (/2,2/))
  integer :: l
 
   do l=1,size(x,2)
     if(x(2,l).le.x(1,l)) then
       mu(:,:,l) = eps1*i
     else
       mu(:,:,l) = eps2*i
     endif
   enddo
       
 end function coeff_diff
 
!-----------------------------------------------------------------------
 
 pure function coeff_adv(x) result(a)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: a(size(x,1),size(x,2))

  real(wp) :: fr2(size(x,2))

   fr2 = 1.0_wp/( x(1,:)**2 + x(2,:)**2 )
   a(1,:) = -fr2*x(2,:)
   a(2,:) =  fr2*x(1,:)
 
 end function coeff_adv
 
!-----------------------------------------------------------------------

 pure function coeff_re(x) result(sigma)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: sigma(size(x,2))
 
   sigma = 0.0_wp

 end function coeff_re
 
!-----------------------------------------------------------------------
 
 pure function coeff_f(x) result(f)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: f(size(x,2))

   f = 0.0_wp
 
 end function coeff_f
 
!-----------------------------------------------------------------------

 pure function coeff_dir(x,breg) result(d)
  real(wp), intent(in) :: x(:,:)
  integer, intent(in) :: breg
  real(wp) :: d(size(x,2))

   bregcase: select case(breg)
    case(1)
     d = 0.0_wp
    case(2,4,6)
     d = 1.0_wp
    case default
     d = huge(x(1,1))
   end select bregcase
 
 end function coeff_dir
 
!-----------------------------------------------------------------------

 pure function coeff_neu(x,breg) result(h)
  real(wp), intent(in) :: x(:,:)
  integer, intent(in) :: breg
  real(wp) :: h(size(x,2))

   bregcase: select case(breg)
    case(4,5,6,7)
     h = 0.0_wp
    case default
     h = huge(x(1,1))
   end select bregcase
 
 end function coeff_neu
 
!-----------------------------------------------------------------------

 pure function coeff_rob(x,breg) result(g)
  real(wp), intent(in) :: x(:,:)
  integer, intent(in) :: breg
  real(wp) :: g(size(x,2))

   g = 0.0_wp
 
 end function coeff_rob
 
!-----------------------------------------------------------------------

end module mod_zunino2009_test

