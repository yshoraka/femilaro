!>\brief
!!
!! Vlasov-Poisson equation, Hamilton-Jacobi formulation.
!!
!! \n
!!
!! This module is similar to \c mod_vp_ode, except that a different
!! form of the equation is used, namely a Hamilton-Jacobi equation.
!!
!! Specifically, we solve
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!  \displaystyle
!!  \partial_t S 
!!  + \partial_{\underline{\xi}}S\cdot\left( \underline{\eta} +
!!    \frac{1}{2} \partial_{\underline{\xi}}S \right)
!!  - C\left( \underline{\xi} -\underline{\eta}t
!!           + \partial_{\underline{\eta}} S\, ,
!!  \underline{\eta} \right) & = & -\Phi, \\
!!  \Delta \Phi & = & \displaystyle
!!  1 - \int_{\Omega_\eta}
!!   f_0 \left( \underline{\xi} -\underline{\eta}t +
!!   \partial_{\underline{\eta}}S\,,\underline{\eta} \right) \, \left|
!!   {\rm det}\left( \left[ \delta_{ij} +
!!   \partial_{\xi_i}\partial_{\eta_j} S \right] \right) \right|
!!  \,d\underline{\eta},
!!  \end{array}
!! \f}
!! where the independent variables are
!! \f$\underline{\xi}\,,\underline{\eta}\,,t\f$ and \f$C\f$ and
!! \f$f_0\f$ are known functions. The determinant is understood as the
!! determinant of the matrix
!! \f{displaymath}{
!!  \left[
!!   \begin{array}{cccc}
!!    \displaystyle 1+\frac{\partial^2 S}{\partial\xi_1\partial\eta_1} &
!!    \displaystyle \frac{\partial^2 S}{\partial\xi_1\partial\eta_2} &
!!     \ldots &
!!    \displaystyle \frac{\partial^2 S}{\partial\xi_1\partial\eta_d}
!!    \\[3mm]
!!    \displaystyle \frac{\partial^2 S}{\partial\xi_2\partial\eta_1} &
!!    \displaystyle 1+\frac{\partial^2 S}{\partial\xi_2\partial\eta_2} &
!!     \ldots &
!!    \displaystyle \frac{\partial^2 S}{\partial\xi_2\partial\eta_d}
!!    \\
!!    \vdots & \vdots & \ddots & \vdots \\
!!    \displaystyle \frac{\partial^2 S}{\partial\xi_d\partial\eta_1} &
!!    \displaystyle \frac{\partial^2 S}{\partial\xi_d\partial\eta_2} &
!!     \ldots &
!!    \displaystyle 1+\frac{\partial^2 S}{\partial\xi_d\partial\eta_d}
!!   \end{array}
!!  \right].
!! \f}
!! This system has the structure of a Hamilton-Jacobi equation
!! \f$\partial_t u + H(\nabla u) = 0\f$, where the complete gradient
!! of the unknown is \f$\left[\partial_{\underline{\xi}}S\,,
!! \partial_{\underline{\eta}} S \right]\f$, with two complications
!! arising from the presence of the integral (nonlocality) and the
!! second derivatives weighting such integral.
!! 
!! The discretization and implementation used here mostly follow \c
!! mod_vp_ode. More in details we consider the following points.
!! <ul>
!!  <li> The prognostic and diagnostic variables use the same types as
!!  in \c mod_vp_ode, since the basic layout is similar: a scalar
!!  transport coupled with a diagnostic variable which solves an
!!  elliptic problem. Nevertheless, the diagnostic type is extended to
!!  include first and second partial derivatives.
!!  <li> The prognostic equation for \f$S\f$ is of Hamilton-Jacobi
!!  type, so that its discretization is analogous to the one adopted
!!  in \c mod_hj_ode, and is based on <a
!!  href="http://dx.doi.org/10.1016/j.jcp.2010.09.022">[Yan, Osher,
!!  JCP, 2011]</a>. Basically, this requires computing a discrete
!!  approximation of \f$\left[\partial_{\underline{\xi}}S\,,
!!  \partial_{\underline{\eta}} S \right]\f$ and then solving the
!!  prognostic equation in "strong form".
!!  <li> The derivatives of \f$S\f$ are required both in the solution
!!  of the Poisson problem for the diagnostic variable and for the
!!  prognostic equation. This means that, contrary to what is done in
!!  \c mod_hj_ode, the discrete gradients can not be computed locally
!!  for each element and then discarded, but must be precomputed and
!!  stored for the whole grid.
!! </ul>
!!
!! The structure of the code thus is
!! <ul>
!!  <li> diagnose the first derivatives and the mixed second
!!  derivatives: this requires and element and a side loop for each
!!  derivative
!!  <li> diagnose the "density" and compute the potential \f$\Phi\f$
!!  <li> compute the right-hand-side of the prognostic equation: this
!!  requires a single element loop.
!! </ul>
!!
!! \section upwindderivatives Upwinding
!!
!! In this section we use \f$x\f$ to denote the collection of all the
!! independent variables (except time), and indicate by \f$K\f$ and
!! element in phase space. This is because this discussion is done for
!! a generic Hamilton-Jacobi equation.  
!!
!! We need to specify some notation. First of all, we assume that the
!! grid is Cartesian, so that we can globally define left and right
!! values in each direction. Then, \f$n\f$ will always denote the
!! outward pointing variable on \f$\partial K\f$. Finally, we denote
!! by \f$\partial_{x_k}K\f$ the subset of \f$\partial K\f$ where the
!! normal is directed along the \f$x_k\f$ coordinate (see also the
!! notation introduced in \c mod_tps_phs_grid), and denote the two
!! corresponding sides by \f$\partial_{x_k}^LK\f$ and
!! \f$\partial_{x_k}^RK\f$ (these are the sides listed in
!! <tt>t_e1\%(:,i)</tt>).
!! 
!! Then, for each partial derivative, we can define two approximations
!! using left and right values: these are the variables
!! \f$\underline{p}^+, \underline{p}^-\f$ introduced in \c mod_hj_ode.
!! The <em>Lax-Friedrichs numerical Hamiltonian</em> defined in <a
!! href="http://dx.doi.org/10.1016/j.jcp.2010.09.022">[Yan, Osher,
!! JCP, 2011]</a> is now
!! \f{displaymath}{
!!  \widehat{H}( \underline{p}^+, \underline{p}^- ) =
!!  H\left( \frac{\underline{p}^-+\underline{p}^+}{2} \right)
!!  - \frac{1}{2}\underline{\alpha}\cdot\left( \underline{p}^+ -
!!  \underline{p}^- \right),
!! \f}
!! where \f$\underline{\alpha}\f$ is an array of nonnegative 
!! stabilization coefficients. Let us consider these terms separately.
!!
!! For the average term, we have
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!  \displaystyle
!!  \frac{1}{2} \int_K \left( \underline{p}^-
!!  + \underline{p}^+ \right) \cdot \underline{q}_{k\underline{i}}\,dx
!!  & = & \displaystyle
!!  - \frac{1}{2} \int_K \left( S + S \right) \,\nabla \cdot
!!  \underline{q}_{k\underline{i}}\,dx
!!  +\frac{1}{2} \int_{\partial K} \left( S^- + S^+
!!  \right)\underline{n}\cdot
!!  \underline{q}_{k\underline{i}}\,d\sigma \\[3mm]
!!  & = & \displaystyle
!!  - \int_K S \,\nabla \cdot
!!  \underline{q}_{k\underline{i}}\,dx
!!  + \int_{\partial K} \left\{ S \right\}\underline{n}\cdot
!!  \underline{q}_{k\underline{i}}\,d\sigma,
!!  \end{array}
!! \f}
!! which is a centered approximation. For the stabilization term, we
!! have
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!  \displaystyle
!!  - \frac{1}{2} \int_K \underline{\alpha}\cdot\left( \underline{p}^+
!!  - \underline{p}^- \right) \phi_{\underline{i}}\,dx
!!  & = & \displaystyle
!!  - \frac{1}{2} \int_K \left( \underline{p}^+
!!  - \underline{p}^- \right) \cdot
!!  \Pi\underline{\alpha}\phi_{\underline{i}}\,dx \\[3mm]
!!  & = & \displaystyle
!!  \frac{1}{2} \int_K (S-S) \, \nabla\cdot
!!  \left(\Pi\underline{\alpha}\phi_{\underline{i}}\right)dx
!!  -\frac{1}{2} \int_{\partial K} \left( S^+ - S^-
!!  \right)\underline{n}\cdot
!!  \Pi\underline{\alpha}\phi_{\underline{i}}\,dx \\[3mm]
!!  & = & \displaystyle
!!  \frac{1}{2} \sum_{k=1}^d \left\{
!!  \int_{\partial^L_{x_k}K}
!!   \left( S^+ - S^- \right)\alpha_k\phi_{\underline{i}}\,
!!  d\sigma
!!  -\int_{\partial^R_{x_k}K}
!!   \left( S^+ - S^- \right)\alpha_k\phi_{\underline{i}}\,
!!  d\sigma
!!  \right\}.
!!  \end{array}
!! \f}
!! This shows that the stabilization coefficients \f$\alpha\f$ are
!! required only on the element boundaries, and only one coefficient
!! is required for each side. Therefore, when diagnosing the
!! derivatives of \f$S\f$, it is enough to store: the centered
!! derivatives and the required stabilization coefficients. In fact,
!! since the \f$\alpha_k\f$-s turn out to be single valued on each side,
!! one can store directly the "flux"
!! \f$\alpha_k\left(S^+-S^-\right)\f$ for each side.
!<----------------------------------------------------------------------
module mod_vp_hj_ode

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 !$ use omp_lib

 !$ use mod_omp_utils, only: &
 !$   mod_omp_utils_initialized, &
 !$   detailed_timing_omp, &
 !$   omput_push_key,    &
 !$   omput_pop_key,     &
 !$   omput_start_timer, &
 !$   omput_close_timer, &
 !$   omput_write_time

 use mod_linal, only: &
   mod_linal_initialized, &
   det

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_tps_phs_grid, only: &
   mod_tps_phs_grid_initialized, &
   t_tps_phs_grid, t_tps_grid, t_e1

 use mod_tps_base, only: &
   mod_tps_base_initialized, &
   t_tps_base

 use mod_time_integrators, only: &
   mod_time_integrators_initialized, &
   c_ode, c_ods

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_initialized, &
   write_octave

 use mod_f_state, only: & 
   mod_f_state_initialized, &
   t_f_state, t_e_field, new_e_field, &
   write_octave

 use mod_testcases, only: &
   mod_testcases_initialized, &
   coeff_f0 => coeff_init

 use mod_poisson_pb, only: &
   mod_poisson_pb_constructor, &
   mod_poisson_pb_destructor,  &
   mod_poisson_pb_initialized, &
   compute_electric_field, compute_c11, p0

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_vp_hj_ode_constructor, &
   mod_vp_hj_ode_destructor,  &
   mod_vp_hj_ode_initialized, &
   t_e_hj_field, new_e_hj_field, &
   t_vp_hj_ode, compute_diags, &
   compute_pdervs, compute_rho, compute_electric_field, p0, &
   new_vp_hj_ode, clear, &
   write_octave

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Extended diagnostic type including derivatives
 type, extends(t_e_field) :: t_e_hj_field
  !> <tt>d1f(k,1,l,m,ie)</tt> \f$\approx\f$ \f$\partial_{\xi_k}
  !! S(\underline{\xi}_l,\underline{\eta}_m)|_{K_{ie}}\f$;
  !! <tt>d1f(k,2,l,m,ie)</tt> \f$\approx\f$ \f$\partial_{\eta_k}
  !! S(\underline{\xi}_l,\underline{\eta}_m)|_{K_{ie}}\f$
  real(wp), allocatable :: d1f(:,:,:,:,:)
  !> <tt>d2f(k,h,l,m,ie)</tt> \f$\approx\f$ \f$\frac{\partial
  !! S}{\partial_{\xi_k}\partial_{\eta_h}}
  !! (\underline{\xi}_l,\underline{\eta}_m)|_{K_{ie}}\f$
  real(wp), allocatable :: d2f(:,:,:,:,:)
  !> <tt>alpha_stab_x(l,m,is)</tt> \f$\approx\f$ \f$\frac{1}{2}
  !! \alpha_k\left( S^+-S^-\right)|_{s_{x_{is}}}\f$
  real(wp), allocatable :: alpha_stab_x(:,:,:)
  !> <tt>alpha_stab_v(l,m,is)</tt> \f$\approx\f$ \f$\frac{1}{2}
  !! \alpha_k\left( S^+-S^-\right)|_{s_{v_{is}}}\f$
  real(wp), allocatable :: alpha_stab_v(:,:,:)
 end type t_e_hj_field

 !> Vlasov-Poisson ODE
 type, extends(c_ode) :: t_vp_hj_ode
  type(t_tps_phs_grid), pointer :: grid    => null()
  type(t_tps_base),     pointer :: base(:) => null()
 contains
  procedure, pass(ode) :: rhs  => vp_hj_rhs
 end type t_vp_hj_ode
 
 interface clear
   module procedure clear_e_hj_field, clear_vp_hj_ode
 end interface

 interface write_octave
   module procedure write_e_hj_field
 end interface write_octave

! Module variables

 real(wp), allocatable :: m_mass(:,:,:), im_mass(:,:,:) !< mass matrix

 logical, protected ::               &
   mod_vp_hj_ode_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_vp_hj_ode'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_vp_hj_ode_constructor(grid,base,fu)
  type(t_tps_phs_grid), intent(in), target :: grid
  type(t_tps_base), intent(in) :: base(2)
  integer, intent(in) :: fu
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

  integer :: ie, iv, ix
  real(wp) :: xjw, vjw

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
!$      ( (detailed_timing_omp.eqv..true.).and. &
!$ (mod_omp_utils_initialized.eqv..false.) ) .or. &
       (mod_linal_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
(mod_tps_phs_grid_initialized.eqv..false.) .or. &
    (mod_tps_base_initialized.eqv..false.) .or. &
(mod_time_integrators_initialized.eqv..false.) .or. &
(mod_octave_io_sparse_initialized.eqv..false.) .or. &
     (mod_f_state_initialized.eqv..false.) .or. &
   (mod_testcases_initialized.eqv..false.) ) then  
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_vp_hj_ode_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   allocate( m_mass(base(1)%pkd,base(2)%pkd,grid%ne))
   allocate(im_mass(base(1)%pkd,base(2)%pkd,grid%ne))
   mass_do: do ie=1,grid%ne
     do iv=1,size(m_mass,2)
       vjw = grid%e(ie)%e1(2)%p%vol*base(2)%wgld(iv)
       do ix=1,size(m_mass,1)
         xjw = grid%e(ie)%e1(1)%p%vol*base(1)%wgld(ix)

          m_mass(ix,iv,ie) =     xjw*vjw
         im_mass(ix,iv,ie) = 1.0_wp/(xjw*vjw)
       enddo
     enddo
   enddo mass_do

   call mod_poisson_pb_constructor(grid,base(1),fu,.false.)

   mod_vp_hj_ode_initialized = .true.
 end subroutine mod_vp_hj_ode_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_vp_hj_ode_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_vp_hj_ode_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   call mod_poisson_pb_destructor()

   deallocate( m_mass)
   deallocate(im_mass)

   mod_vp_hj_ode_initialized = .false.
 end subroutine mod_vp_hj_ode_destructor

!-----------------------------------------------------------------------

 subroutine new_e_hj_field(obj,grid,base)
  type(t_tps_phs_grid), intent(in), target :: grid
  type(t_tps_base),     intent(in), target :: base(2)
  type(t_e_hj_field),   intent(out) :: obj

  integer :: d

   call new_e_field(obj,grid,base)
   d = grid%d
   allocate( obj%d1f( d , 2 , base(1)%pkd,base(2)%pkd , grid%ne ) )
   allocate( obj%d2f( d , d , base(1)%pkd,base(2)%pkd , grid%ne ) )
   allocate( obj%alpha_stab_x( base(1)%spkd,base(2)%pkd , grid%nsxv ) )
   allocate( obj%alpha_stab_v( base(1)%pkd,base(2)%spkd , grid%nsvx ) )

 end subroutine new_e_hj_field
 
!-----------------------------------------------------------------------
 
 pure subroutine clear_e_hj_field(obj)
  type(t_e_hj_field), intent(out) :: obj

   ! Note: since the "clear" subroutines are overloaded with the
   ! generic name clear, it is better not to use class arguments.

   nullify( obj%grid )
   nullify( obj%base )
   ! allocatable components are implicitly deallocated
 
 end subroutine clear_e_hj_field

!-----------------------------------------------------------------------

 subroutine new_vp_hj_ode(obj,grid,base)
  type(t_tps_phs_grid), intent(in), target :: grid
  type(t_tps_base),     intent(in), target :: base(2)
  type(t_vp_hj_ode),    intent(out) :: obj

   obj%grid => grid
   obj%base => base

 end subroutine new_vp_hj_ode
 
!-----------------------------------------------------------------------
 
 pure subroutine clear_vp_hj_ode(obj)
  type(t_vp_hj_ode), intent(out) :: obj

   nullify( obj%grid )
   nullify( obj%base )
 
 end subroutine clear_vp_hj_ode
 
!-----------------------------------------------------------------------

 !> Compute the right-hand side of the Vlasov equation
 !!
 !! \note We assume that all the quadrature weights are defined on the
 !! interval \f$[0\,,1]\f$, so that the Jacobians are the element
 !! sizes.
 !!
 !! \note The mass matrix is not considered explicitly: the
 !! differential operators already include the inverse of the diagonal
 !! mass matrix. This allows some speed-up of the code.
 subroutine vp_hj_rhs(tnd,ode,t,uuu,ods,term)
  class(t_vp_hj_ode), intent(in) :: ode
  real(wp),     intent(in)    :: t
  class(c_stv), intent(in)    :: uuu
  class(c_ods), intent(inout) :: ods
  class(c_stv), intent(inout) :: tnd
  integer,      intent(in), optional :: term(2)

  integer :: ie, ix, iv, is, k ,ie1, ie2, ixs, ix1, ix2, id
  real(wp) :: ivol1, ivol2, xjw
  real(wp) :: xi(ode%grid%d,ode%base(1)%pkd), f0(ode%base(1)%pkd),    &
    eta(ode%grid%d,ode%base(1)%pkd), xideta(ode%grid%d,ode%base(1)%pkd)

   select type(uuu); type is(t_f_state)
     select type(tnd); type is(t_f_state)
       select type(ods); type is(t_e_hj_field)

   !--------------------------------------------------------------------
   ! 1.1) Diagnostic variables: partial derivatives
   call compute_pdervs(ods,uuu)
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   ! 1.2) Diagnostic variables: density
   call compute_rho(ods,t,uuu)
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   ! 1.3) Diagnostic variables: Poisson problem
   call compute_electric_field(ods,uuu , skip_rho=.true.,skip_et=.true.)
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   ! 2.1) Compute the tendency: element loop
   associate( b1 => ode%base(1) , b2 => ode%base(2) )
   elem_do: do ie=1,ode%grid%ne
     
     ! The equation is now collocated, and since we already have the
     ! partial derivatives there is not much left do do.

     associate(e1=>ode%grid%e(ie)%e1(1)%p , e2=>ode%grid%e(ie)%e1(2)%p)

     do ix=1,b1%pkd ! it is useful to precompute these values
       xi(:,ix) = e1%bw * b1%xgl( b1%mldx(ix,:) ) + e1%box(1,:)
     enddo

     do iv=1,b2%pkd

       do id=1,uuu%grid%d
         eta(id,:) = e2%bw(id) * b2%xgl( b2%mldx(iv,id) ) + e2%box(1,id) 
       enddo
       xideta = xi + ods%d1f(:,2,:,iv,ie) - eta*t
       f0 = coeff_f0( xideta , eta )

       do ix=1,b1%pkd

         tnd%f(ix,iv,ie) =                                         &
           ! \partial_xi term (all the columns of eta are identical)
           - dot_product( ods%d1f(:,1,ix,iv,ie) ,               &
                          eta(:,1)+0.5_wp*ods%d1f(:,1,ix,iv,ie) )  &
           ! C term
           + f0(ix)                                                &
           ! \Phi term
           - ods%p%pl( (e1%o-1)*b1%pkd + ix )

       enddo
     enddo
     end associate

   enddo elem_do
   end associate
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   ! 2.2) Compute the tendency: side loop
   associate( b1 => ode%base(1), b2 => ode%base(2) )
   vxside_do: do is=1,ode%grid%nsvx

     associate( s => ode%grid%sv_x(is) )

     k   = s%s1%n  ! normal space direction
     ie1 = s%ie(1) ! phase space elements
     ie2 = s%ie(2)
     ivol1 = 1.0_wp/ode%grid%e(ie1)%e1(1)%p%vol
     ivol2 = 1.0_wp/ode%grid%e(ie2)%e1(1)%p%vol

     do ixs=1,b1%spkd
       ix1 = b1%s_dofs(2,k,ixs) ! x dofs: left and right
       ix2 = b1%s_dofs(1,k,ixs)
       ! Side geometry taken from the left element
       xjw = s%s1%a/b1%wgl( b1%mldx(ix1,k) )

       tnd%f(ix1,:,ie1) = tnd%f(ix1,:,ie1)         &
            + xjw*ivol1 * ods%alpha_stab_x(ixs,:,is)
       tnd%f(ix2,:,ie2) = tnd%f(ix2,:,ie2)         &
            - xjw*ivol2 * ods%alpha_stab_x(ixs,:,is)
     enddo

     end associate

   enddo vxside_do
   end associate

   associate( b1 => ode%base(2), b2 => ode%base(1) )
   xvside_do: do is=1,ode%grid%nsxv

     associate( s => ode%grid%sx_v(is) )

     k   = s%s1%n  ! normal space direction
     ie1 = s%ie(1) ! phase space elements
     ie2 = s%ie(2)
     ivol1 = 1.0_wp/ode%grid%e(ie1)%e1(2)%p%vol
     ivol2 = 1.0_wp/ode%grid%e(ie2)%e1(2)%p%vol

     do ixs=1,b1%spkd
       ix1 = b1%s_dofs(2,k,ixs) ! x dofs: left and right
       ix2 = b1%s_dofs(1,k,ixs)
       xjw = s%s1%a/b1%wgl( b1%mldx(ix1,k) )

       tnd%f(:,ix1,ie1) = tnd%f(:,ix1,ie1)         &
            + xjw*ivol1 * ods%alpha_stab_v(:,ixs,is)
       tnd%f(:,ix2,ie2) = tnd%f(:,ix2,ie2)         &
            - xjw*ivol2 * ods%alpha_stab_v(:,ixs,is)
     enddo

     end associate

   enddo xvside_do
   end associate
   !--------------------------------------------------------------------

       end select
     end select
   end select

 end subroutine vp_hj_rhs

!-----------------------------------------------------------------------
 
 subroutine compute_pdervs(ods,uuu)
  type(t_f_state),    intent(in)    :: uuu
  type(t_e_hj_field), intent(inout) :: ods
 
  integer :: ie, iv, ix, s, k, k1,k2,k3, is, ie1,ie2, ixs, ix1,ix2
  integer :: ixvm(uuu%grid%d,2), ifs(2)
  real(wp) :: w, ivol1, ivol2, xjw, f1, f2, s_ave, alpha_k
  real(wp) :: s_s_ave(uuu%grid%d)
  type :: t_rv
   real(wp), allocatable :: f(:,:)
  end type t_rv
  type(t_rv) :: fxv(2)
  character(len=*), parameter :: &
    this_sub_name = 'compute_pdervs'
 
   !--------------------------------------------------------------------
   ! 1) Compute the first derivatives in both xi/eta; while doing
   ! this, also compute the stabilization fluxes.

   ! fxv is used to access the xi/eta sections of S with 1,2
   allocate( fxv(1)%f(uuu%base(1)%pkd,uuu%base(2)%pkd) )
   allocate( fxv(2)%f(uuu%base(2)%pkd,uuu%base(1)%pkd) )
   pder_elem_do: do ie=1,uuu%grid%ne

     ! get the xi/eta sections so that they can be indexed with 1,2
     fxv(1)%f = uuu%f(:,:,ie)
     fxv(2)%f = transpose(uuu%f(:,:,ie))

     ! volume terms
     !
     ! Compared to mod_hj_ode each loop has now two indexes, for the x
     ! and v directions.
     do iv=1,uuu%base(2)%pkd ! Guass-Lobatto nodes
       ixvm(:,2) = uuu%base(2)%mldx(iv,:) ! velocity multiindex
       do ix=1,uuu%base(1)%pkd
         ixvm(:,1) = uuu%base(1)%mldx(ix,:)

         ! Index to access fxv: 
         !  fxv(1)%(:,iv) -> \partial_xi
         !  fxv(2)%(:,ix) -> \partial_eta
         ifs(1) = iv ; ifs(2) = ix

         do s=1,2 ! xi/eta derivatives
           associate( &
               e1 => uuu%grid%e(ie)%e1(s)%p, & ! single field element
               b1 => uuu%base(s)             & ! single field base
             )
           do k=1,uuu%grid%d

             ! weight, including the inverse mass matrix
             w = 1.0_wp/( b1%wgl(ixvm(k,s)) * e1%bw(k) )

             ! get the indexes to multiply along the k-th direction
             call uuu%base(s)%dotmap(k1,k2,k3 , ixvm(:,s),k &
             !call b1%dotmap(k1,k2,k3 , ixvm(:,s),k &
                           !$ , dotmap_work &
                           )

             ! for each coordinate there are two upwind directions;
             ! the element integral is the same, while the boundary
             ! integrals are different. We store the element
             ! contributions directly in the diagnostic variable.
             ods%d1f(k,s,ix,iv,ie) = -w *                             &
       dot_product( b1%wdphi(ixvm(k,s),:) , fxv(s)%f(k1:k2:k3,ifs(s)) )

           enddo
           end associate
         enddo

       enddo
     enddo

   enddo pder_elem_do
   deallocate( fxv(1)%f , fxv(2)%f )

   ! Side loop
   !
   ! ods%d1f is completed using the centered flux, while the jumps of
   ! S are taken into account to compute the stabilization terms,
   ! stored in ods%alpha_stab.
   associate( b1 => uuu%base(1), b2 => uuu%base(2) )
   pder_vxside_do: do is=1,uuu%grid%nsvx

     associate( s => uuu%grid%sv_x(is) )

     k   = s%s1%n  ! normal space direction
     ie1 = s%ie(1) ! phase space elements
     ie2 = s%ie(2)
     ! note: velocity volume can be simplified with the mass matrix
     ivol1 = 1.0_wp/uuu%grid%e(ie1)%e1(1)%p%vol
     ivol2 = 1.0_wp/uuu%grid%e(ie2)%e1(1)%p%vol
            
     do iv=1,b2%pkd
       do ixs=1,b1%spkd
         ix1 = b1%s_dofs(2,k,ixs) ! x dofs: left and right
         ix2 = b1%s_dofs(1,k,ixs)
         ! Side geometry taken from the left element
         xjw = s%s1%a/b1%wgl( b1%mldx(ix1,k) )

         ! side traces
         f1 = uuu%f(ix1,iv,ie1)
         f2 = uuu%f(ix2,iv,ie2)

         ! centered numerical flux
         s_ave = 0.5_wp*( f1 + f2 )

         ods%d1f(k,1,ix1,iv,ie1) = ods%d1f(k,1,ix1,iv,ie1) &
              + xjw*ivol1 * s_ave
         ods%d1f(k,1,ix2,iv,ie2) = ods%d1f(k,1,ix2,iv,ie2) &
              - xjw*ivol2 * s_ave

         ! stabilization term: at this point, the k-th derivative is
         ! known already, so we can use it to determine the
         ! stabilization coefficient. In xi, we simply take the value
         ! of the derivative, motivated by the quadratic term in
         ! \partial_\xi.
         alpha_k = max( abs(ods%d1f(k,1,ix1,iv,ie1)) , &
                        abs(ods%d1f(k,1,ix2,iv,ie2)) )
         ods%alpha_stab_x(ixs,iv,is) = 0.5_wp * alpha_k * ( f2 - f1 )
       enddo
     enddo

     end associate
   enddo pder_vxside_do
   end associate

   associate( b1 => uuu%base(2), b2 => uuu%base(1) )
   pder_xvside_do: do is=1,uuu%grid%nsxv

     associate( s => uuu%grid%sx_v(is) )

     k   = s%s1%n  ! normal space direction
     ie1 = s%ie(1) ! phase space elements
     ie2 = s%ie(2)
     ivol1 = 1.0_wp/uuu%grid%e(ie1)%e1(2)%p%vol
     ivol2 = 1.0_wp/uuu%grid%e(ie2)%e1(2)%p%vol
            
     do iv=1,b2%pkd
       do ixs=1,b1%spkd
         ix1 = b1%s_dofs(2,k,ixs) ! x dofs: left and right
         ix2 = b1%s_dofs(1,k,ixs)
         ! Side geometry taken from the left element
         xjw = s%s1%a/b1%wgl( b1%mldx(ix1,k) )

         ! side traces
         f1 = uuu%f(iv,ix1,ie1)
         f2 = uuu%f(iv,ix2,ie2)

         ! centered numerical flux
         s_ave = 0.5_wp*( f1 + f2 )

         ods%d1f(k,2,iv,ix1,ie1) = ods%d1f(k,2,iv,ix1,ie1) &
              + xjw*ivol1 * s_ave
         ods%d1f(k,2,iv,ix2,ie2) = ods%d1f(k,2,iv,ix2,ie2) &
              - xjw*ivol2 * s_ave

         ! TODO one should find a good choice for the stabilization
         alpha_k = 1.0_wp
         ods%alpha_stab_v(iv,ixs,is) = 0.5_wp * alpha_k * ( f2 - f1 )
       enddo
     enddo

     end associate
   enddo pder_xvside_do
   end associate
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   ! 2) Compute the second (mixed) derivatives

   ! In principle, the order of differentiation does not matter. We
   ! choose here to compute \partial_xi[ \partial_eta ] .
   p2der_elem_do: do ie=1,uuu%grid%ne

     ! volume terms
     do iv=1,uuu%base(2)%pkd
       ! not used, since s=1: ixvm(:,2) = uuu%base(2)%mldx(iv,:)
       do ix=1,uuu%base(1)%pkd
         ixvm(:,1) = uuu%base(1)%mldx(ix,:)

         ! Since we only need \partial_xi, we always have  s = 1 .
         s = 1
         associate( &
             e1 => uuu%grid%e(ie)%e1(s)%p, & ! single field element
             b1 => uuu%base(s)             & ! single field base
           )
         do k=1,uuu%grid%d

           ! weight, including the inverse mass matrix
           w = 1.0_wp/( b1%wgl(ixvm(k,s)) * e1%bw(k) )

           ! get the indexes to multiply along the k-th direction
           call uuu%base(s)%dotmap(k1,k2,k3 , ixvm(:,s),k &
           !call b1%dotmap(k1,k2,k3 , ixvm(:,s),k &
                         !$ , dotmap_work &
                         )

           ods%d2f(k,:,ix,iv,ie) = -w *                                &
           matmul( ods%d1f(:,2,k1:k2:k3,iv,ie) , b1%wdphi(ixvm(k,s),:) )

           ! This would switch the order of derivation (one must also
           ! set ixvm(:,2) and s=2, and change the side loop
           ! p2der_vxside_do).
           !
           ! Notice that swapping the derivation order should yield
           ! identical results up to machine precision.
           ! ods%d2f(:,k,ix,iv,ie) = -w *                              &
           ! matmul(ods%d1f(:,1,ix,k1:k2:k3,ie) , b1%wdphi(ixvm(k,s),:))

         enddo
         end associate

       enddo
     enddo

   enddo p2der_elem_do

   ! Side loop
   associate( b1 => uuu%base(1), b2 => uuu%base(2) )
   p2der_vxside_do: do is=1,uuu%grid%nsvx

     associate( s => uuu%grid%sv_x(is) )

     k   = s%s1%n  ! normal space direction
     ie1 = s%ie(1) ! phase space elements
     ie2 = s%ie(2)
     ! note: velocity volume can be simplified with the mass matrix
     ivol1 = 1.0_wp/uuu%grid%e(ie1)%e1(1)%p%vol
     ivol2 = 1.0_wp/uuu%grid%e(ie2)%e1(1)%p%vol
            
     do iv=1,b2%pkd
       do ixs=1,b1%spkd
         ix1 = b1%s_dofs(2,k,ixs) ! x dofs: left and right
         ix2 = b1%s_dofs(1,k,ixs)
         ! Side geometry taken from the left element
         xjw = s%s1%a/b1%wgl( b1%mldx(ix1,k) )

         ! centered numerical flux
         s_s_ave = 0.5_wp*( ods%d1f(:,2,ix1,iv,ie1) &
                           +ods%d1f(:,2,ix2,iv,ie2) )

         ods%d2f(k,:,ix1,iv,ie1) = ods%d2f(k,:,ix1,iv,ie1) &
              + xjw*ivol1 * s_s_ave
         ods%d2f(k,:,ix2,iv,ie2) = ods%d2f(k,:,ix2,iv,ie2) &
              - xjw*ivol2 * s_s_ave
       enddo
     enddo

     end associate
   enddo p2der_vxside_do
   end associate

   ! This corresponds to switching the order of derivation
   ! associate( b1 => uuu%base(2), b2 => uuu%base(1) )
   ! p2der_xvside_do: do is=1,uuu%grid%nsxv
   !
   !   associate( s => uuu%grid%sx_v(is) )
   !
   !   k   = s%s1%n  ! normal space direction
   !   ie1 = s%ie(1) ! phase space elements
   !   ie2 = s%ie(2)
   !   ! note: velocity volume can be simplified with the mass matrix
   !   ivol1 = 1.0_wp/uuu%grid%e(ie1)%e1(2)%p%vol
   !   ivol2 = 1.0_wp/uuu%grid%e(ie2)%e1(2)%p%vol
   !          
   !   do iv=1,b2%pkd
   !     do ixs=1,b1%spkd
   !       ix1 = b1%s_dofs(2,k,ixs) ! x dofs: left and right
   !       ix2 = b1%s_dofs(1,k,ixs)
   !       ! Side geometry taken from the left element
   !       xjw = s%s1%a/b1%wgl( b1%mldx(ix1,k) )
   !
   !       ! centered numerical flux
   !       s_s_ave = 0.5_wp*( ods%d1f(:,1,iv,ix1,ie1) &
   !                         +ods%d1f(:,1,iv,ix2,ie2) )
   !
   !       ods%d2f(:,k,iv,ix1,ie1) = ods%d2f(:,k,iv,ix1,ie1) &
   !            + xjw*ivol1 * s_s_ave
   !       ods%d2f(:,k,iv,ix2,ie2) = ods%d2f(:,k,iv,ix2,ie2) &
   !            - xjw*ivol2 * s_s_ave
   !     enddo
   !   enddo
   !
   !   end associate
   ! enddo p2der_xvside_do
   ! end associate
   !--------------------------------------------------------------------

 end subroutine compute_pdervs
 
!-----------------------------------------------------------------------

 pure subroutine compute_rho(ods,t,uuu)
  real(wp),           intent(in)    :: t
  type(t_f_state),    intent(in)    :: uuu
  type(t_e_hj_field), intent(inout) :: ods

  integer :: ie1, ie2, ie, ix, iv, id, ixm(uuu%grid%d), ivm(uuu%grid%d)
  real(wp) :: vjw, mm(uuu%grid%d,uuu%grid%d), f0(uuu%base(1)%pkd),   &
    xi(uuu%grid%d,uuu%base(1)%pkd), eta(uuu%grid%d,uuu%base(1)%pkd), &
    xideta(uuu%grid%d,uuu%base(1)%pkd)

  procedure(det), pointer :: fdet

   ! Computing the determinant with the general function can be very
   ! expensive: we provide some special cases
   select case(uuu%grid%d)
    case(1)
     fdet => det1
    case(2)
     fdet => det2
    case default
     fdet => det
   end select

   associate( &
       gx => uuu%grid%gx, &
       gv => uuu%grid%gv, &
       b1 => uuu%base(1), &
       b2 => uuu%base(2)  &
     )

   do ie1=1,gx%ne
     ods%r(:,ie1) = 0.0_wp
     do ix=1,b1%pkd ! it is useful to precompute these values
       ixm = b1%mldx(ix,:)
       xi(:,ix) = gx%e(ie1)%bw * b1%xgl(ixm) + gx%e(ie1)%box(1,:) 
     enddo
     do ie2=1,gv%ne
       ie = uuu%grid%i2ie(gx%e(ie1)%i , gv%e(ie2)%i)

       do iv=1,b2%pkd

         ivm = b2%mldx(iv,:) ! velocity multiindex
         do id=1,uuu%grid%d
           eta(id,:) = gv%e(ie2)%bw(id) * b2%xgl(ivm(id)) &
                      + gv%e(ie2)%box(1,id) 
         enddo
         xideta = xi + ods%d1f(:,2,:,iv,ie) - eta*t
         f0 = coeff_f0( xideta , eta )

         vjw = gv%e(ie2)%vol * b2%wgld(iv)

         do ix=1,b1%pkd

           mm = ods%d2f(:,:,ix,iv,ie) 
           do id=1,uuu%grid%d
             mm(id,id) = mm(id,id) + 1.0_wp
           enddo

           ods%r(ix,ie1) = ods%r(ix,ie1) + vjw * f0(ix) * abs(fdet(mm))

         enddo
       enddo
     enddo
   enddo

   end associate

 end subroutine compute_rho

!-----------------------------------------------------------------------

 !> \f$1\times1\f$ determinant
 pure function det1(m) result(d)
  real(wp), intent(in) :: m(:,:)
  real(wp) :: d

   d = m(1,1)
 
 end function det1

!-----------------------------------------------------------------------

 !> \f$2\times2\f$ determinant
 pure function det2(m) result(d)
  real(wp), intent(in) :: m(:,:)
  real(wp) :: d

   d = m(1,1)*m(2,2) - m(1,2)*m(2,1)
 
 end function det2

!-----------------------------------------------------------------------
 
 !> Compute the main diagnostics
 !!
 !! To measure the jumps of a discrete variable \f$u_h\f$, we can
 !! define the associated lifting operator \f$\mathcal{R}\f$ as (see
 !! for instance equation (1.5) in <a
 !! href="http://dx.doi.org/10.1093/imanum/drn038">[Buffa, Ortner, IMA
 !! J. Num. Anal., 2009]</a>) by
 !! \f{displaymath}{
 !!  \int_K \mathcal{R}(u_h)\cdot\underline{\varphi}\,dx =
 !!  -\frac{1}{2}\int_{\partial K} 
 !!  [\hspace*{-1.6pt}[ u_h ]\hspace*{-1.6pt}] \cdot
 !!  \underline{\varphi}\,d\sigma,
 !! \f}
 !! from which we can compute
 !! \f$\|\mathcal{R}(u_h)\|_{L^2(\Omega)}\f$. This ensures that such a
 !! quantity can be directly compared with \f$\|\nabla u\|_{L^2(\Omega)}\f$.
 !!
 !! A scaling argument now shows that
 !! \f$\|\mathcal{R}(u_h)\|_{L^2(\Omega)}\f$ is in fact equivalent to
 !! a properly scaled norm of \f$[\hspace*{-1.6pt}[ u_h
 !! ]\hspace*{-1.6pt}]\f$, i.e.
 !! \f{displaymath}{
 !!  C_1 \int_{\mathcal{E}_h} h^{-1}
 !!  [\hspace*{-1.6pt}[ u_h ]\hspace*{-1.6pt}]^2 \,d\sigma \leq
 !!  \int_\Omega \mathcal{R}(u_h)^2\,dx \leq 
 !!  C_2 \int_{\mathcal{E}_h} h^{-1}
 !!  [\hspace*{-1.6pt}[ u_h ]\hspace*{-1.6pt}]^2 \,d\sigma.
 !! \f}
 !! In this subroutine, such a scaled boundary norm is computed.
 !!
 ! ! \section details Details of the scaling argument
 ! !
 ! ! To verify the above equivalence, and in particular to check that
 ! ! the proper scaling requires \f$h^{-1}\f$, consider a local
 ! ! polynomial basis \f$\underline{\phi}_{\underline{i}}\f$, denote by
 ! ! \f${\bf q}_{\mathcal{R}}\f$ and \f${\bf q}_{[\hspace*{-1.3pt}[ u_h
 ! ! ]\hspace*{-1.3pt}]}\f$ the coefficients of \f$\mathcal{R}(u_h)\f$
 ! ! and \f$[\hspace*{-1.6pt}[ u_h ]\hspace*{-1.6pt}]\f$ with respect
 ! ! to \f$\underline{\phi}_{\underline{i}}\f$ and
 ! ! \f$\underline{\phi}_{\underline{i}}|_{\partial K}\f$,
 ! ! respectively, and observe that the definition of the lifting
 ! ! operator implies
 ! ! \f{displaymath}{
 ! !  \frac{|K|}{|\widehat{K}|} \int_{\widehat{K}}
 ! !  \widehat{\mathcal{R}(u_h)} \cdot
 ! !  \widehat{\underline{\phi}}_{\underline{i}} \,d\widehat{x} =
 ! !  -\frac{1}{2}\sum_{\widehat{s}\in\partial\widehat{K}}
 ! !  \frac{|s|}{|\widehat{s}|} \int_{\widehat{s}}
 ! !  \widehat{[\hspace*{-1.6pt}[ u_h ]\hspace*{-1.6pt}]} \cdot
 ! !  \widehat{\underline{\phi}}_{\underline{i}} \,d\widehat{\sigma}
 ! ! \f}
 ! ! which implies
 ! ! \f{displaymath}{
 ! !  \frac{|K|}{|\widehat{K}|} M 
 ! ! {\bf q}_{\mathcal{R}} =
 ! !  -\frac{1}{2}\left( \sum_{\widehat{s}\in\partial\widehat{K}}
 ! !  \frac{|s|}{|\widehat{s}|} B_{\widehat{s}}\right)
 ! !  {\bf q}_{[\hspace*{-1.3pt}[ u_h ]\hspace*{-1.3pt}]},
 ! ! \f}
 ! ! for two matrices \f$M\f$ and \f$B_{\widehat{s}}\f$ which do not
 ! ! depend on the mesh. This can be rewritten as
 ! ! \f{displaymath}{
 ! !  |K| \tilde{M}
 ! ! {\bf q}_{\mathcal{R}} = -\frac{1}{2}\left(
 ! ! \sum_{\widehat{s}\in\partial\widehat{K}}|s|\tilde{B}_{\widehat{s}}
 ! ! \right)
 ! !  {\bf q}_{[\hspace*{-1.3pt}[ u_h ]\hspace*{-1.3pt}]},
 ! ! \f}
 ! ! redefining the matrices to include all the grid independent
 ! ! quantities. Now observe that
 ! ! \f{displaymath}{
 ! !  \int_K \mathcal{R}(u_h)^2\,dx =
 ! !  \frac{|K|}{|\widehat{K}|} \int_{\widehat{K}}
 ! !  \widehat{\mathcal{R}(u_h)}^2 \,d\widehat{x} =
 ! !  |K|{\bf q}_{\mathcal{R}}^T\tilde{M}{\bf q}_{\mathcal{R}}
 ! ! \f}
 ! ! and
 ! ! \f{displaymath}{
 ! !  \int_s [\hspace*{-1.6pt}[ u_h ]\hspace*{-1.6pt}]^2\,d\sigma =
 ! !  \frac{|s|}{|\widehat{s}|} \int_{\widehat{s}}
 ! !  \widehat{ [\hspace*{-1.6pt}[ u_h ]\hspace*{-1.6pt}]}^2
 ! !  \,d\widehat{\sigma} =
 ! !  |s|{\bf q}_{[\hspace*{-1.3pt}[ u_h ]\hspace*{-1.3pt}]}^T
 ! !  \tilde{B}_{\widehat{s}}
 ! !  {\bf q}_{[\hspace*{-1.3pt}[ u_h ]\hspace*{-1.3pt}]}.
 ! ! \f}
 ! ! This implies
 ! ! \f{displaymath}{
 ! !  \int_K \mathcal{R}(u_h)^2\,dx =
 ! !  \frac{1}{4|K|}
 ! !  {\bf q}_{[\hspace*{-1.3pt}[ u_h ]\hspace*{-1.3pt}]}^T
 ! !  \left(
 ! !   \sum_{\widehat{s}\in\partial\widehat{K}}|s|
 ! !     \tilde{B}_{\widehat{s}}^T
 ! !  \right)
 ! !  \tilde{M}^{-1}
 ! !  \left(
 ! !   \sum_{\widehat{s}\in\partial\widehat{K}}|s|
 ! !     \tilde{B}_{\widehat{s}}
 ! !  \right)
 ! !  {\bf q}_{[\hspace*{-1.3pt}[ u_h ]\hspace*{-1.3pt}]}.
 ! ! \f}
 subroutine compute_diags(t,uuu , e_field , grid,base , &
            momentum , ekin , l_mult ,                  &
            s_jmnorm , sxi_jmnr , set_jmnr , sd2_jmnr , &
            e_l2norm,p_jmnorm , ek)
  real(wp),             intent(in) :: t
  type(t_f_state),      intent(in) :: uuu
  type(t_e_hj_field),   intent(inout), target :: e_field
  type(t_tps_phs_grid), intent(in) :: grid
  type(t_tps_base),     intent(in) :: base(2)
  real(wp), intent(out) :: momentum(:), ekin, l_mult, &
              s_jmnorm, sxi_jmnr, set_jmnr, sd2_jmnr, &
              e_l2norm(:), p_jmnorm
  real(wp), allocatable, intent(out) :: ek(:) ! spectral components

  integer :: ie, n, is, ie1, ie2, ixs, ix1, ix2, id, jd
  real(wp) :: xvol_wx(base(1)%pkdx), c11, sxa_w(base(1)%spkd), ih, &
    ww_svx(base(2)%pkd), ww_sxv(base(1)%pkd)
  real(wp), pointer :: uuu_p(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'compute_diags'

   call compute_pdervs(e_field,uuu)

   call compute_rho(e_field,t,uuu)

   call compute_electric_field(e_field,uuu , &
              skip_rho=.true.,skip_et=.true. )

   momentum = 0.0_wp
   ekin     = 0.0_wp
   allocate(ek(0))

   ! Lagrange multiplier: last element of pl
   l_mult = e_field%p%pl(base(1)%pkd*grid%gx%ne+1)

   ! space integrals: element integrals and side integrals
   e_l2norm = 0.0_wp
   x_elem_loop: do ie=1,grid%gx%ne
     do n=1,grid%d
       xvol_wx = grid%gx%e(ie)%vol * base(1)%wgldx(:,n)
       e_l2norm(n)=e_l2norm(n)+dot_product(xvol_wx,e_field%e(n,:,ie)**2)
     enddo
   enddo x_elem_loop
   ! Notice that more precisely we compute the electrostatic energy
   e_l2norm = 0.5_wp*e_l2norm

   uuu_p(1:base(1)%pkd,1:grid%gx%ne)         &
     => e_field%p%pl(1:base(1)%pkd*grid%gx%ne)
   p_jmnorm = 0.0_wp ! jump norm
   x_side_loop1: do is=1,grid%gx%ns
     n = grid%gx%s(is)%n ! normal space direction
     ie1 = grid%gx%s(is)%ie(1)
     ie2 = grid%gx%s(is)%ie(2)
     c11 = compute_c11(is,grid%gx,base(1)%k)
     sxa_w = grid%gx%s(is)%a * base(1)%swgld(:,n)

     do ixs=1,base(1)%spkd
       ix1 = base(1)%s_dofs(2,n,ixs)
       ix2 = base(1)%s_dofs(1,n,ixs)
       p_jmnorm = p_jmnorm                                   &
         + sxa_w(ixs) * c11*(uuu_p(ix2,ie2)-uuu_p(ix1,ie1))**2
     enddo
   enddo x_side_loop1
   ! Notice that, for consistency with e_l2norm, we introduce 1/2
   p_jmnorm = 0.5_wp*p_jmnorm

   ! Jump norms
   s_jmnorm = 0.0_wp
   sxi_jmnr = 0.0_wp
   set_jmnr = 0.0_wp
   sd2_jmnr = 0.0_wp
   associate( b1 => base(1), b2 => base(2) )
   vxside_do: do is=1,grid%nsvx
     associate( s => grid%sv_x(is) , ww => ww_svx )

     n   = s%s1%n  ! normal space direction
     ie1 = s%ie(1) ! phase space elements
     ie2 = s%ie(2)
     ih = 2.0_wp/( s%s1%e(1)%p%bw(n) + s%s1%e(2)%p%bw(n) )
     ! Include the side dimension, in both x and v
     ih = ih * s%s1%a * s%e1%vol

     do ixs=1,b1%spkd
       ix1 = b1%s_dofs(2,n,ixs) ! x dofs: left and right
       ix2 = b1%s_dofs(1,n,ixs)
       ww = ih * b1%swgld(ixs,n) * b2%wgld

       s_jmnorm = s_jmnorm +                              &
         sum( ww * (uuu%f(ix2,:,ie2)-uuu%f(ix1,:,ie1))**2 )
       do id=1,grid%d
         sxi_jmnr = sxi_jmnr +                        &
           sum( ww * (e_field%d1f(id,1,ix2,:,ie2)     &
                     -e_field%d1f(id,1,ix1,:,ie1))**2 )
         set_jmnr = set_jmnr +                        &
           sum( ww * (e_field%d1f(id,2,ix2,:,ie2)     &
                     -e_field%d1f(id,2,ix1,:,ie1))**2 )
         do jd=1,grid%d
           sd2_jmnr = sd2_jmnr +                         &
             sum( ww * (e_field%d2f(id,jd,ix2,:,ie2)     &
                       -e_field%d2f(id,jd,ix1,:,ie1))**2 )
         enddo
       enddo
     enddo

     end associate
   enddo vxside_do
   end associate
   associate( b1 => base(2), b2 => base(1) )
   xvside_do: do is=1,grid%nsxv
     associate( s => grid%sx_v(is) , ww => ww_sxv )

     n   = s%s1%n
     ie1 = s%ie(1)
     ie2 = s%ie(2)
     ih = 2.0_wp/( s%s1%e(1)%p%bw(n) + s%s1%e(2)%p%bw(n) )
     ih = ih * s%s1%a * s%e1%vol

     do ixs=1,b1%spkd
       ix1 = b1%s_dofs(2,n,ixs)
       ix2 = b1%s_dofs(1,n,ixs)
       ww = ih * b1%swgld(ixs,n) * b2%wgld

       s_jmnorm = s_jmnorm +                              &
         sum( ww * (uuu%f(:,ix2,ie2)-uuu%f(:,ix1,ie1))**2 )
       do id=1,grid%d
         sxi_jmnr = sxi_jmnr +                        &
           sum( ww * (e_field%d1f(id,1,:,ix2,ie2)     &
                     -e_field%d1f(id,1,:,ix1,ie1))**2 )
         set_jmnr = set_jmnr +                        &
           sum( ww * (e_field%d1f(id,2,:,ix2,ie2)     &
                     -e_field%d1f(id,2,:,ix1,ie1))**2 )
         do jd=1,grid%d
           sd2_jmnr = sd2_jmnr +                         &
             sum( ww * (e_field%d2f(id,jd,:,ix2,ie2)     &
                       -e_field%d2f(id,jd,:,ix1,ie1))**2 )
         enddo
       enddo
     enddo

     end associate
   enddo xvside_do
   end associate
   s_jmnorm = sqrt(s_jmnorm)
   sxi_jmnr = sqrt(sxi_jmnr)
   set_jmnr = sqrt(set_jmnr)
   sd2_jmnr = sqrt(sd2_jmnr)


 
!??  integer :: ie, iv, is, id, ivm(grid%d), n, ie1, ie2, ix1, ix2, ixs
!??  real(wp) :: xvol_w(base(1)%pkd), xvol_wx(base(1)%pkdx)
!??  real(wp), dimension(base(2)%pkd) :: vvol_w, int_fx, int_afx, &
!??    int_f2x
!??  real(wp) :: vgl(grid%d), c11, sxa_w(base(1)%spkd)
!??  ! uuu_p is used to reshape the electric potential locally; see also
!??  ! section "12.6.6 Target Arguments" of the fortran handbook for the
!??  ! use of target dummy arguments associated with nontarget actual
!??  ! arguments.
!??  real(wp), pointer :: uuu_p(:,:)
!??  logical, parameter :: spectral_analysis = .true.
!??  character(len=*), parameter :: &
!??    this_sub_name = 'compute_diags'
!??
!??   call compute_electric_field(e_field,uuu)
!??
!??   ! Phase space integrals
!??   momentum = 0.0_wp
!??   ekin     = 0.0_wp
!??   f_integr = 0.0_wp
!??   f_l1norm = 0.0_wp
!??   f_l2norm = 0.0_wp
!??   f_min    =  huge(0.0_wp)
!??   f_max    = -huge(0.0_wp)
!??   elem_loop: do ie=1,grid%ne
!??
!??     ! integration in x
!??     xvol_w = grid%e(ie)%e1(1)%p%vol * base(1)%wgld
!??     int_fx  = matmul( xvol_w ,     uuu%f(:,:,ie)    )
!??     int_afx = matmul( xvol_w , abs(uuu%f(:,:,ie))   )
!??     int_f2x = matmul( xvol_w ,     uuu%f(:,:,ie)**2 )
!??
!??     ! integration in v
!??     vvol_w = grid%e(ie)%e1(2)%p%vol * base(2)%wgld
!??     do iv=1,base(2)%pkd ! Guass-Lobatto nodes
!??       ivm = base(2)%mldx(iv,:) ! velocity multiindex
!??       ! velocity at the quad. node
!??       do id=1,grid%d
!??         vgl(id) = grid%e(ie)%e1(2)%p%bw(id)*base(2)%xgl(ivm(id)) &
!??                  + grid%e(ie)%e1(2)%p%box(1,id)
!??         momentum(id) = momentum(id) + vvol_w(iv)*vgl(id)*int_fx(iv)
!??       enddo
!??       ekin = ekin + vvol_w(iv)*sum(vgl**2)*int_fx(iv) ! 1/2 added later
!??     enddo
!??     f_integr = f_integr + dot_product( vvol_w , int_fx  )
!??     f_l1norm = f_l1norm + dot_product( vvol_w , int_afx )
!??     f_l2norm = f_l2norm + dot_product( vvol_w , int_f2x ) ! sqrt later
!??     f_min    = min( f_min , minval(uuu%f(:,:,ie)) )
!??     f_max    = max( f_max , maxval(uuu%f(:,:,ie)) )
!??
!??   enddo elem_loop
!??   ekin = 0.5_wp * ekin
!??   f_l2norm = sqrt(f_l2norm)
!??
!??   ! space integrals: element integrals and side integrals
!??   e_l2norm = 0.0_wp
!??   x_elem_loop: do ie=1,grid%gx%ne
!??     do n=1,grid%d
!??       xvol_wx = grid%gx%e(ie)%vol * base(1)%wgldx(:,n)
!??       e_l2norm(n)=e_l2norm(n)+dot_product(xvol_wx,e_field%e(n,:,ie)**2)
!??     enddo
!??   enddo x_elem_loop
!??   ! Notice that more precisely we compute the electrostatic energy
!??   e_l2norm = 0.5_wp*e_l2norm
!??
!??   uuu_p(1:base(1)%pkd,1:grid%gx%ne)         &
!??     => e_field%p%pl(1:base(1)%pkd*grid%gx%ne)
!??   p_jmnorm = 0.0_wp ! jump norm
!??   x_side_loop: do is=1,grid%gx%ns
!??     n = grid%gx%s(is)%n ! normal space direction
!??     ie1 = grid%gx%s(is)%ie(1)
!??     ie2 = grid%gx%s(is)%ie(2)
!??     c11 = compute_c11(is,grid%gx,base(1)%k)
!??     sxa_w = grid%gx%s(is)%a * base(1)%swgld(:,n)
!??
!??     do ixs=1,base(1)%spkd
!??       ix1 = base(1)%s_dofs(2,n,ixs)
!??       ix2 = base(1)%s_dofs(1,n,ixs)
!??       p_jmnorm = p_jmnorm                                   &
!??         + sxa_w(ixs) * c11*(uuu_p(ix2,ie2)-uuu_p(ix1,ie1))**2
!??     enddo
!??   enddo x_side_loop
!??   ! Notice that, for consistency with e_l2norm, we introduce 1/2
!??   p_jmnorm = 0.5_wp*p_jmnorm
!??
!??   ! Now the spectral components of the electric field: this is a
!??   ! preliminary implementation, and works only for 1D problems; also,
!??   ! one has to set manually the fundamental wave number.
!??   if(spectral_analysis) &
!??     call compute_spectral_coefficients(grid%gx,base(1),e_field%e,ek)
!??
!?? contains
!??
!??  pure subroutine compute_spectral_coefficients(gx,bx,e,ek)
!??   type(t_tps_grid), intent(in) :: gx
!??   type(t_tps_base), intent(in) :: bx
!??   real(wp), intent(in) :: e(:,:,:) ! electric field, nodal values
!??   real(wp), allocatable, intent(out) :: ek(:)
!??   
!??   integer, parameter :: nmodes = 4
!??   integer, parameter :: id = 1 ! only works in 1D
!??   !real(wp), parameter :: k0 = 0.5_wp ! Landau damping
!??   real(wp), parameter :: k0 = 0.3_wp ! bump-on-tail
!??   !real(wp), parameter :: k0 = 1.0_wp ! two-stream inst.
!??   real(wp), parameter :: & ! normalization coefficient
!??     scale = sqrt(k0/3.1415926535897932384626433832795028_wp)
!??   integer :: ie, in
!??   real(wp) :: k, sin_int(nmodes), cos_int(nmodes), xgx(bx%pkx)
!??
!??    sin_int = 0.0_wp
!??    cos_int = 0.0_wp
!??    x_elem_loop: do ie=1,gx%ne
!??      ! nodes in physical space
!??      xgx = gx%e(ie)%bw(id)*bx%xglx + gx%e(ie)%box(1,id)
!??      do in=1,nmodes
!??        k = real(in,wp)*k0
!??        sin_int(in) = sin_int(in) + gx%e(ie)%bw(id) * dot_product( &
!??                                   bx%wglx , sin(k*xgx)*e(id,:,ie) )
!??        cos_int(in) = cos_int(in) + gx%e(ie)%bw(id) * dot_product( &
!??                                   bx%wglx , cos(k*xgx)*e(id,:,ie) )
!??      enddo
!??    enddo x_elem_loop
!??
!??    allocate(ek(nmodes))
!??    ek = 0.5_wp * scale**2 * ( sin_int**2 + cos_int**2 )
!??
!??  end subroutine compute_spectral_coefficients
 
 end subroutine compute_diags
 
!-----------------------------------------------------------------------

 subroutine write_e_hj_field(ef,var_name,fu)
  integer, intent(in) :: fu
  type(t_e_hj_field), intent(in) :: ef
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_e_hj_field'

   write(fu,'(a,a)') '# name: ',var_name
   write(fu,'(a)')   '# type: struct'
   write(fu,'(a)')   '# length: 6' ! (total) number of fields

   ! Write the base type
   call write_octave(ef%t_e_field,var_name,fu , skip_preamble=.true.)

   ! Write the additional fields

   ! field 05 : d1f
   write(fu,'(a)')   '# name: d1f'
   write(fu,'(a)')   '# type: cell'
   write(fu,'(a)')   '# rows: 1'
   write(fu,'(a)')   '# columns: 1'
   call write_octave(ef%d1f,'<cell-element>',fu)

   ! field 06 : d2f
   write(fu,'(a)')   '# name: d2f'
   write(fu,'(a)')   '# type: cell'
   write(fu,'(a)')   '# rows: 1'
   write(fu,'(a)')   '# columns: 1'
   call write_octave(ef%d2f,'<cell-element>',fu)

 end subroutine write_e_hj_field

!-----------------------------------------------------------------------

end module mod_vp_hj_ode

