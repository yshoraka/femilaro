!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Michele Nini                 <michelenini88@gmail.com>

!<---------------------------------------------------------------------

module mod_hybrid_flux

!----------------------------------------------------------------------

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_messages, only: &
   mod_messages_initialized, &
   error, warning, info
 
 use mod_grid, only: &
   mod_grid_initialized, &
   t_grid,el_linear_size

 use mod_base, only: &
   mod_base_initialized, &
   t_base

 use mod_sympoly, only: &
   mod_sympoly_initialized, &
   nll_sympoly

 use mod_atm_refstate, only: &
   mod_atm_refstate_initialized, &
   t_atm_refstate
 
 use mod_turb_flux_base, only: &
   mod_turb_flux_base_initialized, &
   c_turbmod, c_turbmod_progs, c_turbmod_diags, &
   t_turb_diags
  
 use mod_dgcomp_testcases, only: &
   mod_dgcomp_testcases_initialized, &
   t_phc, phc, coeff_visc

 use mod_viscous_flux, only: &
   mod_viscous_flux_initialized, &
   t_viscous_flux, compute_vf_flux
   
 use mod_anisotropic_flux, only: &
   mod_anisotropic_flux_initialized, &
   t_anisotropic_flux, af_compute_coeff_diags, &
   compute_af_flux, t_anisotropic_diags

!----------------------------------------------------------------------
 
 implicit none

!----------------------------------------------------------------------

! Module interface

 public :: &
   mod_hybrid_flux_constructor, &
   mod_hybrid_flux_destructor,  &
   mod_hybrid_flux_initialized, &
   t_hybrid_flux

  
 private

 !---------------------------------------------------------------------
 ! Module types and parameters

 ! public members


 type, extends(t_anisotropic_flux) :: t_hybrid_flux
  real(wp), allocatable :: gradp(:,:,:) ! Required by HT
  real(wp), allocatable :: w_dist(:,:)
 contains 
   procedure, pass(tm) :: init                 => hf_init
   procedure, pass(tm) :: clean                => hf_clean 
   procedure, pass(tm) :: compute_coeff_diags  => hf_compute_coeff_diags
   procedure, pass(tm) :: flux                 => hf_flux
 end type t_hybrid_flux

 type, extends(t_anisotropic_diags) :: t_hybrid_diags
  ! Up to now, this is the same of anisotrpic model 
 end type t_hybrid_diags
 
 ! Module variables
 
 ! public members
 
 logical, protected ::               &
   mod_hybrid_flux_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_hybrid_flux'
!----------------------------------------------------------------------

 contains

!----------------------------------------------------------------------

 subroutine mod_hybrid_flux_constructor()
   character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
     (mod_messages_initialized.eqv..false.) .or. &
     (mod_grid_initialized.eqv..false.) .or. &
     (mod_base_initialized.eqv..false.) .or. &
     (mod_atm_refstate_initialized.eqv..false.) .or. &
     (mod_turb_flux_base_initialized.eqv..false.).or.&
     (mod_dgcomp_testcases_initialized.eqv..false.).or.&
     (mod_viscous_flux_initialized.eqv..false.) .or. &
     (mod_anisotropic_flux_initialized.eqv..false.)) then
      call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_hybrid_flux_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif 
   mod_hybrid_flux_initialized = .true.
 end subroutine mod_hybrid_flux_constructor

!----------------------------------------------------------------------

 subroutine mod_hybrid_flux_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_hybrid_flux_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_hybrid_flux_initialized = .false.
 end subroutine mod_hybrid_flux_destructor

!----------------------------------------------------------------------

 subroutine hf_init(tm, progs,diags,td,grid,base,ntrcs)
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  integer, intent(in) :: ntrcs
  class(t_hybrid_flux), intent(inout) :: tm
  class(c_turbmod_progs), allocatable, intent(out) :: progs
  class(c_turbmod_diags), allocatable, intent(out) :: diags
  type(t_turb_diags), intent(out) :: td

  integer :: id, jd, ie    
  character :: vel(3)   
  real(wp) :: ngdl ! number of dofs, as a real

   tm%d           = grid%d
   tm%ntrcs       = ntrcs
   tm%initialized = .true.
   allocate(tm%wg(base%m));   tm%wg = base%wg
   allocate(tm%base_p(base%pk,base%m));  tm%base_p = base%p
   allocate(tm%ave(base%m));   tm%ave = base%wg/base%me%vol 
   allocate(tm%gradp(grid%d,base%pk,base%m)); tm%gradp = base%gradp  
   ! wall distance
   allocate(tm%w_dist(grid%d+1,grid%ne))
   ! Find the field "w_dist" among the element data
   do id=1,size(grid%edata_legend)+1
    if(index(grid%edata_legend(id),'w_dist').gt.0) exit
     ! If you see an error here exceeding the bounds of edata_legend,
     ! this happens because there is no field w_dist.
   enddo
   do ie=1,grid%ne
     tm%w_dist(:,ie) = grid%e(ie)%edata(id:id+grid%d) ! 1+d values
   enddo
   ! element significant length  needed by smagorinsky
!   allocate(tm%delta2(grid%ne))
!   do ie=1,grid%ne
!     ngdl = real(nll_sympoly(tm%d,base%k),wp)**(1.0_wp/real(tm%d,wp))
!     tm%delta2(ie) = (el_linear_size(grid%e(ie)) / ngdl)**2
!   enddo
   !diags
   allocate(t_hybrid_diags::diags)
   diags%ngrad    = 1 + tm%d + tm%ntrcs
   allocate(diags%grad(tm%d,diags%ngrad,base%pk,grid%ne))
   select type(diags); type is(t_hybrid_diags)
!    allocate(diags%s2(grid%ne)) 
    allocate(diags%coeffs(2,grid%ne)); 
    allocate(diags%cd(grid%d,grid%d,grid%ne))
    allocate(diags%cq(grid%d,grid%ne))
    allocate(diags%cj(grid%d,grid%ne))
   end select

   !diagnostic
   vel=['u','v','w']
   td%ndiags = 11 
   allocate( td%diag_names(td%ndiags) ,  &
      td%diags(td%ndiags,base%pk,grid%ne) )
   td%diag_names(1) = '|nabla^S u + lambda*div(u)|'
   td%diag_names(2) = 'dynamic dissipation'
   do id=1,tm%d
     do jd =1,tm%d
       td%diag_names(2+jd+(id-1)*tm%d) = 'tau_'// vel(id) // vel(jd)
     enddo
   enddo

 end subroutine hf_init

!----------------------------------------------------------------------

 pure &
  subroutine hf_clean(tm,diags,td)
  class(t_hybrid_flux), intent(inout) :: tm
  class(c_turbmod_diags), allocatable, intent(inout) :: diags
  type(t_turb_diags), intent(out) :: td
   
   deallocate(tm%wg,tm%base_p)
   deallocate(diags) ! deallocates all the components
   ! td components deallocated automatically

 end subroutine hf_clean

!----------------------------------------------------------------------
 pure &
  subroutine hf_compute_coeff_diags(tm,cd, grid,base, &
                                        uuu,progs,atm_ref)
  class(t_hybrid_flux), intent(in) :: tm
  type(t_grid),           intent(in)    :: grid
  type(t_base),           intent(in)    :: base   
  real(wp),               intent(in)    :: uuu(:,:,:)
  class(t_atm_refstate),  intent(in)    :: atm_ref(:,:)
  class(c_turbmod_progs), allocatable, intent(in) :: progs
  class(c_turbmod_diags), intent(inout) :: cd
!  write(*,*) 'hf_compute_coeff_diags' 
  select type(cd); type is(t_hybrid_diags)
  call af_compute_coeff_diags(tm,cd,grid,base,uuu,progs,atm_ref)
  end select
      
 end subroutine hf_compute_coeff_diags
 
!---------------------------------------------------------------------

 pure &
 subroutine hf_flux(fem, tm,ie,x,bp,grid,consv,atm_ref, rho,p,uu,cc, &
                    progs,diags,td,uuu_mean,tau_mean,uu_square_mean,&
                    ht)
  class(t_hybrid_flux), intent(in) :: tm
  integer,  intent(in) :: ie
  real(wp), intent(in) :: x(:,:)  !< coordinates
  real(wp), intent(in) :: bp(:,:) !< basis functions
  type(t_grid),          intent(in) :: grid
  !> conservative variables (deviations)
  real(wp), intent(in) :: consv(:,:)
  class(t_atm_refstate), intent(in) :: atm_ref(:)
  !> Total values for density, pressure, velocity and tracers
  real(wp), intent(in) :: rho(:), p(:), uu(:,:), cc(:,:)
  class(c_turbmod_progs), allocatable, intent(in) :: progs
  class(c_turbmod_diags), intent(in) :: diags
  real(wp), intent(out) :: fem(:,:,:)
  type(t_turb_diags), intent(inout), optional :: td
  real(wp), intent(in), optional :: uuu_mean(:,:)
  real(wp), intent(in), optional :: tau_mean(:,:,:)
  real(wp), intent(in), optional :: uu_square_mean(:,:,:)
  real(wp), intent(inout), optional :: ht(:,:)

  ! Local variables
  real(wp) :: k(size(x,2))
  real(wp) :: uu_f(tm%d,size(bp,2)),  &
              fem_f(tm%d,diags%ngrad,size(bp,2)),  &
              fem_visc(tm%d,diags%ngrad,size(bp,2)), &
              fem_hyb(tm%d,diags%ngrad,size(bp,2)),  & 
              tau_e_uh(tm%d,tm%d,size(bp,2)), &
              uuug_mean(size(consv,1),size(bp,2)),&
              rhog_mean(size(bp,2)), uug_mean(tm%d, &
              size(bp,2))
  real(wp) :: gradg(tm%d,diags%ngrad,size(x,2)), & 
              sym(tm%d,tm%d, size(x,2)),w_dist(size(x,2))
  integer  :: l,i,j,id,jd
 

   ! Compute viscous contribution
   call compute_vf_flux(fem_visc, tm,ie,x,bp, consv,atm_ref, rho,p,uu,cc, &
                        diags, td=td, vf_sym=sym , vf_gradg=gradg)

   !Projecting uuu_mean values
   uuug_mean = matmul(uuu_mean,bp)
   rhog_mean = uuug_mean(1,:) + atm_ref%rho

   do l=1,size(uuug_mean,2)

     ! Blending factor computation   
     ! w_dist(l) = tm%w_dist(1,ie) + dot_product(tm%w_dist(2:,ie),x(:,l))
    
     ! Blending factor k can be fixed or variable, in the latter case HT 
     ! terms appear
      
     ! k(l) = (1.0_wp-tanh(4.0_wp*3.1416_wp*w_dist(l)**2))*&
     !        0.5_wp+tanh(4.0_wp*3.1416_wp*w_dist(l)**2) 
 
     k(l) = 0.50_wp

     do i=1,tm%d
       uug_mean(i,l)  = uuug_mean(2+i,l)/rhog_mean(l)
     enddo 

     uu_f(:,l)= (uu(:,l)-(1-k(l))*uug_mean(:,l))/k(l)
     
   enddo

   select type(diags); type is(t_hybrid_diags)
   ! Compute LES contribution
   call compute_af_flux(fem_f,tm,ie,x,bp,consv,atm_ref, &
                        rho,p,uu_f,diags,sym,gradg)

   if(.not.present(tau_mean)) then
     fem = fem_f
     return
   end if

   fem_hyb = 0.0_wp

   do l=1,size(x,2)  
      !Compute \Tau_{E}(<u_{i}>_{H}<u_{j}>_{H})
      do i=1,tm%d
        do j=1,tm%d
          tau_e_uh(i,j,l) = rhog_mean(l)*(uu_square_mean(i,j,l) - &
                            uug_mean(i,l)*uug_mean(j,l))
        enddo
      enddo 
   
      !Momentum flux
      do i=1,tm%d
        do j=1,tm%d     
          fem_hyb(j,1+i,l) = k(l)*fem_f(j,1+i,l) + &                
                             (1-k(l))*tau_mean(j,i,l) + &
                             (1-k(l))/k(l)**2*tau_e_uh(j,i,l) + &
                             rhog_mean(l)*(1-k(l))/k(l)* &
                             (uu(j,l) - uug_mean(j,l)) &
                             *(uu(i,l)- uug_mean(i,l))

        enddo
      enddo 
 
     !Complete energy flux with turbulent diffusion (1):
     !Knight98 -> uk*tau_jk^sgs + 0.5*uj*tau_kk^sgs 
     fem_hyb(:,1,l) = fem_f(:,1,l) &               
                  + 0.5_wp*uu(:,l)*tr(fem_hyb(:,2:1+tm%d,l)) &                  
                  + matmul(uu(:,l),transpose(fem_hyb(:,2:1+tm%d,l)))  
   enddo
 
   if (present(td)) then       
     do id=1,tm%d
       do jd=1,tm%d
         td%diags(2+jd+(id-1)*tm%d,:,ie) = matmul(tm%base_p , &
                                           tm%wg*fem_hyb(id,1+jd,:))
             
       enddo
     enddo
   endif  
   
   ! Sum viscous and turbulent contribution
   fem=fem_hyb+fem_visc
  

   ! Compute HT
   
   ! Extra terms related to non-commutativity between spatial derivative 
   ! and hybrid filter.

   if(present(ht)) then

     ht(:,:)= 0.0_wp

     if(size(ht,2).eq.size(bp,1)) then
!       call compute_ht_elem(tm,k,w_dist,ie,grid,uu,uug_mean, &
!                            rhog_mean,fem_f(:,2:,:),tau_mean,&
!                            tau_e_uh, ht)
     else
!       call compute_ht_side(k,w_dist,grid,uu,uug_mean,rhog_mean,ht) 
     endif

   endif

   end select 
 end subroutine hf_flux

!--------------------------------------------------------------------- 

! This subroutine computes part of extra-terms related to the
! non-commutativity between H-filter and spatial derivative for the
! element. The remaining part of ht, depending of velocity derivative,
! is added in mod_dgcomp_rhs.

 pure &
  subroutine compute_ht_elem(tm,k, w_dist,ie, grid,uu, uu_mean,      &
                             rho_mean, tau_les, tau_mean,tau_e_uh, ht)
  class(t_hybrid_flux), intent(in) :: tm  
  real(wp),     intent(in) :: k(:)
  real(wp),     intent(in) :: w_dist(:)
  integer,      intent(in) :: ie
  type(t_grid), intent(in) :: grid
  real(wp),     intent(in) :: uu(:,:)
  real(wp),     intent(in) :: uu_mean(:,:)
  real(wp),     intent(in) :: rho_mean(:)
  real(wp),     intent(in) :: tau_les(:,:,:)
  real(wp),     intent(in) :: tau_mean(:,:,:)
  real(wp),     intent(in) :: tau_e_uh(:,:,:)
  real(wp),     intent(inout):: ht(:,:)
  
  ! Local variables
  integer :: l, i, j, h
  real(wp) :: dk(3,size(k)), ddk(3,size(k))
  real(wp) :: ht_g(grid%d+1,size(tm%base_p,2))
  real(wp) :: uu_les(size(uu,1),size(tm%base_p,2))
  real(wp) :: bit(grid%m,grid%d), &
              wg(size(tm%base_p,2)),gradp(grid%d,size(tm%base_p,1), &
              size(tm%base_p,2))
              

   ! gradient of the basis functions (includes the quad. weighs)
   bit = transpose( grid%e(ie)%bi )
   wg = grid%e(ie)%det_b * tm%wg
   do l=1,size(tm%base_p,2)
     ! notice: this should be changed when d.ne.m
     gradp(:,:,l) =wg(l) * matmul( bit , tm%gradp(:,:,l) )
   enddo

   ! K derivative computing, this need to be computed analytically 
   dk = 0.0_wp
   ddk = 0.0_wp
  ht_g(:,:) = 0.0_wp

 
   do l=1,size(uu,2)
     ! Reconstructed LES field
     uu_les(:,l)= ( uu(:,l) - (1-k(l))*uu_mean(:,l))/k(l)

     ! Extra terms computing
     do i=1,grid%d
       
       do j=1,grid%d
         
         do h=1,size(tm%base_p,1) 
           ! Add g(u,du)|_e  terms 
           ht(i+1,h) = ht(i+1,h) &
                     +2/phc%Re*dk(j,l)*rho_mean(l)*(uu_les(i,l)- &
                     uu_mean(i,l))*gradp(j,h,l)

         enddo  
         ! Add f(u) & h(u) 

          ht_g(i+1,l)= ht_g(i+1,l) &
                     + dk(j,l)*( rho_mean(l)* &
                       uu_les(i,l)*uu_les(j,l) - rho_mean(l)*&
                       uu_mean(i,l)*uu_mean(j,l)+tau_les(i,j,l)       &
                     - k(l)*tau_mean(i,j,l) -tau_e_uh(i,j,l)/k(l)**2 )  & 
                     - 1/phc%Re*ddk(j,l)*rho_mean(l)*(uu_les(i,l)-      &
                       uu_mean(i,l))

 
       enddo
     enddo
     
     do j=1,grid%d 
       ht_g(1,l) = ht_g(1,l) + rho_mean(l)*dk(j,l)*&
                   (uu_les(j,l) - uu_mean(j,l))
     enddo

  enddo             
  do i=1,grid%d+1
    ht_g(i,:) = wg*ht_g(i,:)
  enddo   

  ht(:,:) =  ht(:,:) + matmul(ht_g(:,:),transpose(tm%base_p))                  
  
 
 
 end subroutine compute_ht_elem
 
!--------------------------------------------------------------------- 

 pure &
  subroutine compute_ht_side(k,w_dist,grid,uu,uu_mean,rho_mean,ht)
  real(wp),     intent(in) :: k(:)
  real(wp),     intent(in) :: w_dist(:)
  type(t_grid), intent(in) :: grid
  real(wp),     intent(in) :: uu(:,:)
  real(wp),     intent(in) :: uu_mean(:,:)
  real(wp),     intent(in) :: rho_mean(:)
  real(wp),     intent(inout) :: ht(:,:)

  ! Local variables
  integer :: l,i,j
  real(wp) :: dk(3,size(w_dist))
  real(wp) :: uu_les(size(uu,1),size(uu,2))

   ! K derivative computing
   dk = 0.0_wp

   do l=1,size(uu,2)
     ! Reconstructed LES field
     uu_les(:,l) = ( uu(:,l) - (1-k(l))*uu_mean(:,l))/k(l)

     do i=1,grid%d
       do j=1,grid%d
         ! The minus sign is added by tens_side_loop subroutine 
         ! in mod_dgcomp_rhs     

         ! Add g(u,du)|_s  terms
         ht(i,l) = ht(i,l) + 2*rho_mean(l)*dk(j,l)/phc%Re* &
                   (uu_les(i,l)-uu_mean(i,l))
       enddo
     enddo

   enddo
 end subroutine compute_ht_side
 
!---------------------------------------------------------------------                       
 pure &
  function tr(a)
  real(wp), intent(in) :: a(:,:)
  real(wp) :: tr
  integer :: i

   tr = a(1,1)
   do i=2,minval(shape(a))
     tr = tr + a(i,i)
   enddo
 end function tr

!----------------------------------------------------------------------
end module mod_hybrid_flux



