# Include file for the top level Makefile

# Prefix of this test
T_PREF :=FML_GENERAL_UTILITIES

# Note: it is important to write explicitly the path relative to the
# including Makefile, bacause that is where these variables will be
# used.
PWD :=fml_general_utilities
$(T_PREF)_DIR :=./$(PWD)

# Test executable
$(T_PREF)_TEXE := fml_general_utilities.test

# Build directory for this test
$(T_PREF)_TBUILDDIR := $(TBUILDDIR)/fml_general_utilities

# Build directory for the tested modules (needed for the .mod files)
$(T_PREF)_TedBUILDDIR := $(TBUILDDIR)/../fml_general_utilities

# Object files for this test
TSRC_PF  := $(notdir $(wildcard $(PWD)/*.pf))
TSRC_F90 := $(TSRC_PF:.pf=.F90)
TOBJ     := $(TSRC_F90:.F90=.o)
$(T_PREF)_TF90 := $(TSRC_F90)
$(T_PREF)_TOBJ := $(TOBJ)

# FEMilaro libraries
$(T_PREF)_FMLLIBS := -lfml_general_utilities

# Dependece on other tests
$(T_PREF)_OTHERTESTINC :=
$(T_PREF)_OTHERTESTOBJ :=

# Accumulate the general lists
T_PREFS += $(T_PREF)
T_EXES += $$($(T_PREF)_TEXE)

# List here the dependencies among test modules.

mod_sympoly_tests.o: \
  mod_symfun_tests.o

mod_sympwfun_tests.o: \
  mod_sympoly_tests.o

# List here some target specfic rules

$(T_PREF)_SKIP_OBJ_RULE := mod_sympoly_tests.o

mod_sympoly_tests.o: %.o:%.F90 $$($(T_PREF)_FMLLIBS)
	cd $(TBUILDDIR)/fml_general_utilities && \
	  $(FC) $(filter-out -standard-semantics,$(strip $(FFLAGS))) \
	                  $(PFUNIT_INC) $(FFLAGS_RECURSIVE) $(MODFLAG)$(LIBDIR) -c $< -o $@

