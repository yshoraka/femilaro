module mod_sol_br1_ode_tests

!-----------------------------------------------------------------------

 use mod_messages, only: &
   error

 use mod_kinds, only: &
   wp

 use mod_sparse, only: &
   mod_sparse_constructor, &
   mod_sparse_destructor,  &
   get

 use mod_state_vars, only: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor

 use mod_linsolver, only: &
   mod_linsolver_constructor, &
   mod_linsolver_destructor

 use mod_time_integrators, only: &
   mod_time_integrators_constructor, &
   mod_time_integrators_destructor,  &
   c_ods

 use mod_h2d_master_el, only: &
   c_h2d_me_base, &
   t_elem_spec, me_standard, me_p1_iso_pk

 use mod_h2d_base, only: &
   t_h2d_base, new_base, clear

 use mod_h2d_grid, only: &
   t_el_collection, &
   t_2dv, t_2ds, t_2de, &
   t_h2d_grid, new_base_and_grid, clear

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_constructor_test, &
   mod_h2d_bcs_destructor, &
   t_h2d_bcs, new_bcs, clear, &
   t_b_2dv,   t_b_2ds,   t_b_2de,   &
   p_t_b_2dv, p_t_b_2ds, p_t_b_2de, &
   b_dir,   b_neu,   b_ddc
 
 use mod_h2d_coefficients, only: &
   mod_h2d_coefficients_constructor, &
   mod_h2d_coefficients_destructor,  &
   c_s_coeff, c_v_coeff, c_t_coeff, c_bc

 use mod_h2d_cgdofs, only: &
   t_h2d_cgdofs, new_cgdofs, clear

 use mod_sol_state, only: & 
   mod_sol_state_constructor_test, &
   mod_sol_state_destructor

 use mod_sol_br1_ode, only: &
   mod_sol_br1_ode_constructor_test, &
   sol_br1_ode_sys_setup,       &
   mod_sol_br1_ode_destructor,  &
   t_sol_br1_state, new_sol_br1_state, &
   t_sol_br1_ode, new_sol_br1_ode, clear, &
   mmmt, aaat, &
   sol_br1_solve

 use mod_h2d_cgdofs_tests, only: &
   mod_h2d_cgdofs_tests_setup,    &
   mod_h2d_cgdofs_tests_teardown

 use pfunit_mod

!-----------------------------------------------------------------------

 implicit none

!-----------------------------------------------------------------------

 public

!-----------------------------------------------------------------------

 type(t_h2d_grid),   target :: grid
 type(t_h2d_base),   target :: base, ubase
 type(t_h2d_cgdofs), target :: dofs, udofs
 type(t_h2d_bcs),    target :: bcs

 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_br1_ode_tests'

 real(wp), parameter :: toll = 100.0_wp*epsilon(1.0_wp)

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 @Before
 subroutine mod_sol_br1_ode_tests_setup()
  character(len=*), parameter :: &
    this_sub_name = 'setup'

  ! Note: defining the grid and the bases here means that all the
  ! tests will use the same grid/base. If you want to change this,
  ! the grid definition must be moved inside the various tests.
  !
  ! Using the hybrid grid from new_cgdofs__tri_quad_grid__works
  type(t_elem_spec) :: elem_spec
  type(t_el_collection) :: t(2)
  integer :: e(5,5)
  real(wp) :: p(2,6)

   p(1,:) = (/ real(wp) :: 0 , 1 , 0 , 1 , 2 , 1 /)
   p(2,:) = (/ real(wp) :: 0 , 0 , 2 , 2 , 2 , 4 /)

   e(1,:) = (/ 1 , 2 , 5 , 6 , 3 /)
   e(2,:) = (/ 2 , 5 , 6 , 3 , 1 /)
   e(5,:) = (/ 1 , 1 , 1 , 1 , 1 /)

   allocate( t(1)%ie(  1) ); t(1)%ie = (/ 2 /)
   allocate( t(1)%it(4,1) ); t(1)%it = reshape(  &
     (/ 1,2,4,3 /) , (/4,1/) )
   allocate( t(2)%ie(  3) ); t(2)%ie = (/ 1 , 3 , 4 /)
   allocate( t(2)%it(3,3) ); t(2)%it = reshape(  &
     (/ 3,4,6 , 5,6,4 , 2,4,5 /) , (/3,3/) )

   call mod_h2d_cgdofs_tests_setup()

   call mod_sparse_constructor()
   call mod_state_vars_constructor()
   call mod_output_control_constructor('')
   call mod_linsolver_constructor()

   call mod_time_integrators_constructor()

   call mod_h2d_bcs_constructor_test()
   call mod_h2d_coefficients_constructor()

   call mod_sol_state_constructor_test()

   elem_spec%k    = 1
   elem_spec%deg  = 2
   elem_spec%degs = 2
   elem_spec%fe_family = me_standard
   call new_base_and_grid( base,grid , p,e,t,elem_spec )

   elem_spec%k    = 2
   elem_spec%deg  = 2
   elem_spec%degs = 2
   elem_spec%fe_family = me_p1_iso_pk
   call new_base( ubase , base,elem_spec )

   call new_bcs(bcs,grid, reshape((/1,1/),(/2,1/)) )

   call new_cgdofs( dofs , grid, base)
   call new_cgdofs(udofs , grid,ubase)

   ! mod_sol_br1_ode_constructor_test -> called in the tests

 end subroutine mod_sol_br1_ode_tests_setup
   
!-----------------------------------------------------------------------

 @After
 subroutine mod_sol_br1_ode_tests_teardown()
  character(len=*), parameter :: &
    this_sub_name = 'tear-down'

   ! Notice: it is important to call the destructir here, because if
   ! there is an error the test subroutines might not complete.
   call mod_sol_br1_ode_destructor()

   call clear( bcs )
   call clear( dofs ); call clear( udofs )
   call clear( base ); call clear( ubase )
   call clear( grid )

   call mod_sol_state_destructor()

   call mod_h2d_coefficients_destructor()
   call mod_h2d_bcs_destructor()

   call mod_time_integrators_destructor()

   call mod_linsolver_destructor()
   call mod_output_control_destructor()
   call mod_state_vars_destructor()
   call mod_sparse_destructor()

   call mod_h2d_cgdofs_tests_teardown()

 end subroutine mod_sol_br1_ode_tests_teardown

!-----------------------------------------------------------------------

 @Test
 subroutine mod_sol_br1_ode__constructor__matrix_mmmt_set_correctly()

  integer :: i,j
  real(wp), allocatable :: mij(:,:)
  type(t_sol_br1_ode) :: ode

  real(wp), parameter :: ex_m11(3,3) = 2.0_wp
  real(wp), parameter :: ex_m22(3,3) = 2.0_wp + 4.0_wp
  real(wp), parameter :: ex_m44(3,3) = 1.0_wp + 2.0_wp + 3.0_wp + 4.0_wp
  real(wp), parameter :: ex_m88(3,3) = 1.0_wp + 3.0_wp

   call mod_sol_br1_ode_constructor_test(mock_loc_mass_mat, &
       mock_loc_uxb_mat0, mock_loc_uaa_mat, mock_loc_uaa_bcs)
   ! istead of calling  new_sol_br1_ode  set up a mock ode
   ode%grid  => grid
   ode%bcs   => bcs
   ode%base  => base
   ode%ubase => ubase
   ode%dofs  => dofs
   ode%udofs => udofs
   call sol_br1_ode_sys_setup( ode )

   ! There are 6+9+1 = 16 dofs and each of them has a diagonal block
   ! with 3x3 nonzero entries.
   @assertEqual( 0    , mmmt%ierr , 'mmmt%ierr: ' )
   @assertEqual( 3*16 , mmmt%n    , 'mmmt%n: '    )
   @assertEqual( 3*16 , mmmt%m    , 'mmmt%m: '    )
   @assertEqual( 9*16 , mmmt%nz   , 'mmmt%nz: '   )

   mij = reshape( (/ ( ( get(mmmt,i,j) , i=0,2 ) , j=0,2 ) /) ,(/3,3/))
   @assertEqual( ex_m11 , mij , toll , "M(1:3,1:3): " )
           
   mij = reshape( (/ ( ( get(mmmt,i,j) , i=3,5 ) , j=3,5 ) /) ,(/3,3/))
   @assertEqual( ex_m22 , mij , toll , "M(4:6,4:6): " )

   mij = reshape( (/ ( ( get(mmmt,i,j) , i=9,11 ) , j=9,11 )/),(/3,3/))
   @assertEqual( ex_m44 , mij , toll , "M(10:12,10:12): " )

   ! central node on side 2, global dof 8
   mij = reshape( (/ ( ( get(mmmt,i,j) , i=21,23 ) ,j=21,23)/),(/3,3/))
   @assertEqual( ex_m88 , mij , toll , "M(22:24,22:24): " )

   call clear( ode )

 end subroutine mod_sol_br1_ode__constructor__matrix_mmmt_set_correctly 

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine mod_sol_br1_ode__constructor__matrix_aaat_set_correctly()

  integer :: i,j
  real(wp), allocatable :: mij(:,:)
  type(t_sol_br1_ode) :: ode

  integer, parameter :: ex_nnz = & ! we count the nnz for each row
          9 + 12 + 12 + 16 + 9 + 9             & ! vertex dofs
        + 9 + 12 + 12 + 9 + 6 + 9 + 9 + 6 + 6  & ! side dofs
        + 9                                      ! element dofs

  real(wp), parameter :: ex_m11(3,3) = -20.0_wp + 0.1_wp*real(1+3,wp)
  real(wp), parameter :: ex_m44(3,3) = -10.0_wp*real(1+2+3+4,wp) &
                                 + 0.1_wp*real(2 + 1 + 2 + 1+3,wp)

   call mod_sol_br1_ode_constructor_test(mock_loc_mass_mat, &
       mock_loc_uxb_mat0, mock_loc_uaa_mat, mock_loc_uaa_bcs)
   ! istead of calling  new_sol_br1_ode  set up a mock ode
   ode%grid  => grid
   ode%bcs   => bcs
   ode%base  => base
   ode%ubase => ubase
   ode%dofs  => dofs
   ode%udofs => udofs
   call sol_br1_ode_sys_setup( ode )

   @assertEqual( 0        , aaat%ierr , 'aaat%ierr: ' )
   @assertEqual( 3*16     , aaat%n    , 'aaat%n: '    )
   @assertEqual( 3*16     , aaat%m    , 'aaat%m: '    )
   @assertEqual( 9*ex_nnz , aaat%nz   , 'aaat%nz: '   )
  
   mij = reshape( (/ ( ( get(aaat,i,j) , i=0,2 ) , j=0,2 ) /) ,(/3,3/))
   @assertEqual( ex_m11 , mij , toll , "M(1:3,1:3): " )

   mij = reshape( (/ ( ( get(aaat,i,j) , i=9,11 ) , j=9,11 )/),(/3,3/))
   @assertEqual( ex_m44 , mij , toll , "M(10:12,10:12): " )

   call clear( ode )

 end subroutine mod_sol_br1_ode__constructor__matrix_aaat_set_correctly 

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine mod_sol_br1_ode__solve__correctly_solves_system()

  integer :: i
  type(t_sol_br1_state) :: x, b, xl
  type(t_sol_br1_ode) :: ode
  type(c_ods) :: ods ! no need for diagnostics

  ! the mass matrix is diagonal=1 on each element, we must add the el.
  integer, parameter :: mass(16) = (/ 1 , 2 , 2 , 4 , 2 , 2 , &
                       2 , 2 , 2 , 2 , 1 , 1 , 1 , 1 , 1 , 1 /)
  ! each side contributes to the element 0.1*isl
  real(wp), parameter :: ex_b0(16) = 0.1_wp * real( (/              &
       1+3 , 1+3+2 , 1+3+2 , 1+3+2+2+1 , 2+1 , 2+1 ,                &
       1+3+2 , 2+1 , 1+3+2 , 2+1 , 2 , 1+3 , 1+3 , 1 , 2 , 1+3 /),wp)
  real(wp), parameter :: ex_one(16) = (/( 1.0_wp , i=1,16 )/)

   call mod_sol_br1_ode_constructor_test(mock_loc_mass_mat_1,  &
       mock_loc_uxb_mat0, mock_loc_uaa_mat_0, mock_loc_uaa_b_a0)
   ! istead of calling  new_sol_br1_ode  set up a mock ode
   ode%grid  => grid
   ode%bcs   => bcs
   ode%base  => base
   ode%ubase => ubase
   ode%dofs  => dofs
   ode%udofs => udofs
   call sol_br1_ode_sys_setup( ode )

   call new_sol_br1_state( x  , grid,bcs, base,ubase, dofs,udofs )
   call new_sol_br1_state( b  , grid,bcs, base,ubase, dofs,udofs )
   call new_sol_br1_state( xl , grid,bcs, base,ubase, dofs,udofs )

   ! b = 0
   do i=1,3
     x%vi%v(i)%s  = 1.0_wp ! initial guess
     b%vi%v(i)%s  = 0.0_wp ! rhs
     xl%vi%v(i)%s = 0.0_wp ! linearization
   enddo
   call sol_br1_solve(x,ode,t=0.0_wp,sigma=1.0_wp,b=b,xl=xl,ods=ods)
   @assertEqual( ex_b0/real(mass,wp) , x%vi%v(1)%s , toll , "b = 0: " )

   ! b = 1, sigma = 0  ->  x = 1  (sigma cancels the boundary rhs)
   do i=1,3
     x%vi%v(i)%s  = 1.0_wp ! initial guess
     b%vi%v(i)%s  = 1.0_wp ! rhs
     xl%vi%v(i)%s = 0.0_wp ! linearization
   enddo
   call sol_br1_solve(x,ode,t=0.0_wp,sigma=0.0_wp,b=b,xl=xl,ods=ods)
   @assertEqual( ex_one , x%vi%v(1)%s , toll , "homogeneous: " )

   ! TODO: one could find out some more interesting cases.

   call clear( ode )
   call clear( x  )
   call clear( b  )
   call clear( xl )

 end subroutine mod_sol_br1_ode__solve__correctly_solves_system
 
!-----------------------------------------------------------------------

 pure subroutine mock_loc_mass_mat(mmk , e,bme,gij,dx)
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  class(c_t_coeff),     intent(in) :: gij
  class(c_s_coeff),     intent(in) :: dx
  real(wp),             intent(out) :: mmk(:,:,:)

   ! constant, full blocks
   mmk = real( e%i , wp )
 end subroutine mock_loc_mass_mat

 pure subroutine mock_loc_mass_mat_1(mmk , e,bme,gij,dx)
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  class(c_t_coeff),     intent(in) :: gij
  class(c_s_coeff),     intent(in) :: dx
  real(wp),             intent(out) :: mmk(:,:,:)

  integer :: i

   ! identity matrix
   mmk = 0.0_wp
   do i=1,size(mmk,1)
     mmk(i,i,:) = 1.0_wp
   enddo
 end subroutine mock_loc_mass_mat_1

!-----------------------------------------------------------------------

 pure subroutine mock_loc_uxb_mat0(uxbk , e,bme,sth,sbb,dx)
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  class(c_s_coeff),     intent(in) :: sth
  class(c_s_coeff),     intent(in) :: sbb
  class(c_s_coeff),     intent(in) :: dx
  real(wp),             intent(out) :: uxbk(:,:,:)

   uxbk = 0.0_wp

 end subroutine mock_loc_uxb_mat0

!-----------------------------------------------------------------------

 pure subroutine mock_loc_uaa_mat(aak , e,bme,be,gij,bc,dx)
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  type(p_t_b_2de),      intent(in) :: be
  class(c_t_coeff),     intent(in) :: gij
  class(c_bc),          intent(in) :: bc
  class(c_s_coeff),     intent(in) :: dx
  real(wp),             intent(out) :: aak(:,:)

   aak = real( -10*e%i , wp )

   ! mock the presence of the boundary conditions
   if(associated(be%p)) &
     call mock_loc_uaa_bcs(aak,e=e,bme=bme,be=be%p,bc=bc,gij=gij,dx=dx)

 end subroutine mock_loc_uaa_mat

 pure subroutine mock_loc_uaa_mat_0(aak , e,bme,be,gij,bc,dx)
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  type(p_t_b_2de),      intent(in) :: be
  class(c_t_coeff),     intent(in) :: gij
  class(c_bc),          intent(in) :: bc
  class(c_s_coeff),     intent(in) :: dx
  real(wp),             intent(out) :: aak(:,:)

   aak = 0.0_wp

   ! mock the presence of the boundary conditions
   if(associated(be%p)) &
     call mock_loc_uaa_b_a0(aak,e=e,bme=bme,be=be%p,bc=bc,gij=gij,dx=dx)

 end subroutine mock_loc_uaa_mat_0

!-----------------------------------------------------------------------

 pure subroutine mock_loc_uaa_bcs( aak,bk , e,bme,be , bc , gij,dx )
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  type(t_b_2de),        intent(in) :: be
  class(c_bc),          intent(in) :: bc
  class(c_t_coeff),     intent(in) :: gij
  class(c_s_coeff),     intent(in) :: dx
  real(wp), optional, intent(inout) :: aak(:,:), bk(:)

  integer :: isl, isb

   do isb=1,be%nbs
     isl = be%bs(isb)%p%s%isl(1) ! local side index on element e
     if(present(aak)) aak = aak + 0.1_wp*real( isl , wp )
     if(present(bk))  bk  = bk  + 0.1_wp*real( isl , wp )
   enddo
 end subroutine mock_loc_uaa_bcs

 pure subroutine mock_loc_uaa_b_a0( aak,bk , e,bme,be , bc , gij,dx )
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  type(t_b_2de),        intent(in) :: be
  class(c_bc),          intent(in) :: bc
  class(c_t_coeff),     intent(in) :: gij
  class(c_s_coeff),     intent(in) :: dx
  real(wp), optional, intent(inout) :: aak(:,:), bk(:)

  integer :: isl, isb

   do isb=1,be%nbs
     isl = be%bs(isb)%p%s%isl(1) ! local side index on element e
     ! nothing to add to aak
     if(present(bk))  bk  = bk  + 0.1_wp*real( isl , wp )
   enddo
 end subroutine mock_loc_uaa_b_a0

!-----------------------------------------------------------------------

end module mod_sol_br1_ode_tests

