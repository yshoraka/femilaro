!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> SeLaLib interface to FEMilaro modules and libraries.
!!
!! This module provides a centralized access point for the whole
!! FEMilaro library, following the "SeLaLib style". A SeLaLib user
!! should access FEMilaro only via this module, without ever using any
!! specific <tt>mod_*</tt> module. This should reduce as much as possible
!! inconsistencies between the two libraries, such as, for instance,
!! different ways of accessing MPI.
!!
!! Some care must be taken concerning initializations which cannot be
!! done multiple times, typically concerning external libraries. In
!! principle, libraries like MPI or linear solvers can be used both in
!! FEMilaro modules as well as in other SeLaLib ones. These libraries
!! must be initialized before they can be used, and the problem arises
!! whether such an initialization should be done in SeLaLib or beyond
!! the present module. Here, both options are supported and the
!! constructor of this module requires passing some arguments to
!! control what is initialized internally. <em>The module destructor
!! will then finalize all the libraries which have been initialized by
!! the constructor, and nothing else.</em>
!<----------------------------------------------------------------------
module sll_femilaro_intf

!-----------------------------------------------------------------------

 use mod_messages, only: &
  mod_messages_constructor, &
  mod_messages_destructor,  &
  mod_messages_initialized, &
  error,   &
  warning, &
  info

 use mod_kinds, only: &
  mod_kinds_constructor, &
  mod_kinds_destructor,  &
  mod_kinds_initialized, &
  wp_p, wp_r

 use mod_fu_manager, only: &
  mod_fu_manager_constructor, &
  mod_fu_manager_destructor,  &
  mod_fu_manager_initialized, &
  new_file_unit

 use mod_output_control, only: &
  mod_output_control_constructor, &
  mod_output_control_destructor,  &
  mod_output_control_initialized

 use mod_linal, only: &
  mod_linal_constructor, &
  mod_linal_destructor,  &
  mod_linal_initialized

 use mod_mpi_utils, only: &
  mod_mpi_utils_constructor, &
  mod_mpi_utils_destructor,  &
  mod_mpi_utils_initialized, &
  complete_mpi_implementation, &
  mpi_init, mpi_init_thread, &
  mpi_finalize

 use mod_octave_io, only: &
  mod_octave_io_constructor, &
  mod_octave_io_destructor,  &
  mod_octave_io_initialized, &
  write_octave,   &
  read_octave,    &
  read_octave_al

 use mod_perms, only: &
  mod_perms_constructor, &
  mod_perms_destructor,  &
  mod_perms_initialized

 use mod_octave_io_perms, only: &
  mod_octave_io_perms_constructor, &
  mod_octave_io_perms_destructor,  &
  mod_octave_io_perms_initialized

 use mod_numquad, only: &
  mod_numquad_constructor, &
  mod_numquad_destructor,  &
  mod_numquad_initialized

 use mod_sympoly, only: &
  mod_sympoly_constructor, &
  mod_sympoly_destructor,  &
  mod_sympoly_initialized

 use mod_octave_io_sympoly, only: &
  mod_octave_io_sympoly_constructor, &
  mod_octave_io_sympoly_destructor,  &
  mod_octave_io_sympoly_initialized

 use mod_sparse, only: &
  mod_sparse_constructor, &
  mod_sparse_destructor,  &
  mod_sparse_initialized, &
  ! sparse types
  t_intar,     &
  t_col,       &
  t_tri,       &
  t_rp,        &
  t_pm_sk,     &
  ! construction of new objects
  new_col,     &
  new_tri,     &
  ! convertions
  col2tri,     &
  tri2col,     &
  tri2col_skeleton, &
  tri2col_skeleton_part, &
  ! overloaded operators
  operator(+), &
  operator(*), &
  sum,         &
  transpose,   &
  matmul,      &
  ! error codes
  wrong_n,     &
  wrong_m,     &
  wrong_nz,    &
  wrong_dim,   &
  ! other functions
  nnz_col,     &
  nz_col,      &
  nz_col_i,    &
  get,         &
  set,         &
  diag,        &
  spdiag,      &
  ! deallocate
  clear

 use mod_octave_io_sparse, only: &
  mod_octave_io_sparse_constructor, &
  mod_octave_io_sparse_destructor,  &
  mod_octave_io_sparse_initialized, &
  write_octave, read_octave

 use mod_state_vars, only: &
  mod_state_vars_constructor, &
  mod_state_vars_destructor,  &
  mod_state_vars_initialized, &
  c_stv

 use mod_linsolver, only: &
  mod_linsolver_constructor, &
  mod_linsolver_destructor,  &
  mod_linsolver_initialized, &
  ! solvers
  c_linpb, c_itpb, c_mumpspb, c_umfpackpb, c_pastixpb, &
  ! solver specific functions and parameters
  ! c_itpb specific names
  gmres, &
  ! c_pastixpb specific names
  api_verbose_not, api_verbose_no, api_verbose_yes, &
  api_verbose_chatterbox, api_verbose_unbearable

!-----------------------------------------------------------------------

 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   sll_femilaro_intf_constructor, &
   sll_femilaro_intf_destructor,  &
   sll_femilaro_intf_initialized, &
   ! mod_messages ------------------------------------------------------
   error, warning, info, &
   ! note: use sll_real as real kind (see sll_kinds.F90 for details)
   ! mod_octave_io -----------------------------------------------------
   write_octave, read_octave, &
   ! mod_sparse --------------------------------------------------------
   t_col, transpose, matmul, &
   new_tri, tri2col, clear,  &
   ! mod_state_vars ----------------------------------------------------
   c_stv, &
   ! mod_linsolvers ----------------------------------------------------
   ! solvers
   c_linpb, c_itpb, c_mumpspb, c_umfpackpb, c_pastixpb, &
   ! solver specific functions and parameters
   ! c_itpb specific names
   gmres, &
   ! c_pastixpb specific names
   api_verbose_not, api_verbose_no, api_verbose_yes, &
   api_verbose_chatterbox, api_verbose_unbearable

 logical, protected ::               &
   sll_femilaro_intf_initialized = .false.
 ! private members
 logical :: femilaro_init_mpi
 character(len=*), parameter :: &
   this_mod_name = 'sll_femilaro_intf'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 !> Initialization
 !!
 !! The somewhat strange dummy argument declarations mimic the Python
 !! <em>keyword-only parameters</em>, to draw the user's attention to
 !! the required initializations.
 subroutine sll_femilaro_intf_constructor(                      &
   do_not_set_this_argument_but_keyword_set_the_following_ones, &
   init_mpi, mpi_required_thread_support,                       &
   init_mumps, init_umfpack, init_pastix )
  !> Never set this argument; use <tt>keyword=.true./.false.</tt> to
  !! set the following ones.
  class(*), intent(in), optional :: &
    do_not_set_this_argument_but_keyword_set_the_following_ones
  !> Set this to <tt>.true.</tt> if FEMilaro should take care
  !! of initializing MPI; if this is <tt>.false.</tt>, than MPI should
  !! be already initialized before calling this constructor.
  !!
  !! \note For a sequential run, this argument is ignored and the
  !! dummy MPI module is always initialized.
  logical, intent(in), optional :: init_mpi
  !> If this argument is present, \c mpi_init_thread is used to
  !! initialize MPI with the required thread support, otherwise \c
  !! mpi_init is used.
  integer, intent(in), optional :: mpi_required_thread_support
  !> Set these to <tt>.true.</tt> if FEMilaro should take care
  !! of initializing the various linear solvers.
  logical, intent(in), optional :: init_mumps
  logical, intent(in), optional :: init_umfpack
  logical, intent(in), optional :: init_pastix

  integer :: provided, ierr
  character(len=*), parameter :: &
    this_sub_name = 'constructor'
   
   if(present( &
    do_not_set_this_argument_but_keyword_set_the_following_ones ))  &
     call error(this_sub_name,this_mod_name,                        &
      'Please, when calling this constructor set the required '     &
      //'"init_*" dummy arguments with the  keyword=value  syntax ' &
      //'(and do not set the first argument!).')

   ! MPI initialization
   if(complete_mpi_implementation) then 
     if(.not.present(init_mpi)) call error(this_sub_name,this_mod_name,&
      '"init_mpi" is missing.' )
     if(init_mpi) then
       femilaro_init_mpi = .true.
       if(present(mpi_required_thread_support)) then
         call mpi_init_thread(mpi_required_thread_support,provided,ierr)
       else
         call mpi_init(ierr)
       endif
     else
       femilaro_init_mpi = .false.
     endif
   else
     femilaro_init_mpi = .true.
     call mpi_init(ierr)
   endif

   ! Some other constructors from fml_general_utilities
   call mod_messages_constructor()
   call mod_kinds_constructor()
   call mod_fu_manager_constructor()
   call mod_output_control_constructor('')
   call mod_linal_constructor()
   call mod_mpi_utils_constructor()
   call mod_octave_io_constructor()
   call mod_perms_constructor()
   call mod_octave_io_perms_constructor()
   call mod_numquad_constructor()
   call mod_sympoly_constructor()
   call mod_octave_io_sympoly_constructor()
   call mod_sparse_constructor()
   call mod_octave_io_sparse_constructor()
   call mod_state_vars_constructor()

   ! Linear solvers
   if(.not.present(init_umfpack)) &
    call error(this_sub_name,this_mod_name,'"init_umfpack" is missing.')
   if(.not.present(init_mumps)) &
    call error(this_sub_name,this_mod_name,'"init_mumps" is missing.')
   if(.not.present(init_pastix)) &
    call error(this_sub_name,this_mod_name,'"init_pastix" is missing.')
   call mod_linsolver_constructor(init_umfpack,init_mumps,init_pastix)

   sll_femilaro_intf_initialized = .true.
 end subroutine sll_femilaro_intf_constructor

!-----------------------------------------------------------------------
 
 subroutine sll_femilaro_intf_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'

  integer :: ierr
   
   !Consistency checks ---------------------------
   if(sll_femilaro_intf_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   call mod_linsolver_destructor()

   call mod_state_vars_destructor()
   call mod_octave_io_sparse_destructor()
   call mod_sparse_destructor()
   call mod_octave_io_sympoly_destructor()
   call mod_sympoly_destructor()
   call mod_numquad_destructor()
   call mod_octave_io_perms_destructor()
   call mod_perms_destructor()
   call mod_octave_io_destructor()
   call mod_mpi_utils_destructor()
   call mod_linal_destructor()
   call mod_output_control_destructor()
   call mod_fu_manager_destructor()
   call mod_kinds_destructor()
   call mod_messages_destructor()

   if(femilaro_init_mpi) call mpi_finalize(ierr)

   sll_femilaro_intf_initialized = .false.
 end subroutine sll_femilaro_intf_destructor

!-----------------------------------------------------------------------

end module sll_femilaro_intf

