// Shape parameters (all dimensions in cm )
r0 = 165.0;
a  =  60.0;
rmin = 0.8*a;
rmax = 1.2*a;

// extent of the region gridded with triangles
theta = 60.0 * (3.1415/180.0);

// grid resolution
n_upper = 15;
n_width = 15;
n_lower = (3.1415/2-theta) * rmin/(rmax-rmin) * n_width;

// Build the upper and the lower parts of the ring
cth = Cos(theta);
sth = Sin(theta);

// magnetic axix
C = newp; Point(C) = {r0,0.0,0.0};
// points on the internal boundary (east, north, ...)
Ei = newp; Point(Ei) = { r0 + cth*rmin , -sth*rmin , 0.0 };
Ni = newp; Point(Ni) = {    r0         ,      rmin , 0.0 };
Wi = newp; Point(Wi) = { r0 - cth*rmin , -sth*rmin , 0.0 };
Si = newp; Point(Si) = {    r0         ,     -rmin , 0.0 };
// points on the external boundary
Ee = newp; Point(Ee) = { r0 + cth*rmax , -sth*rmax , 0.0 };
Ne = newp; Point(Ne) = {    r0         ,      rmax , 0.0 };
We = newp; Point(We) = { r0 - cth*rmax , -sth*rmax , 0.0 };
Se = newp; Point(Se) = {    r0         ,     -rmax , 0.0 };

a1i = newreg; Circle(a1i) = {Ei,C,Ni};
a2i = newreg; Circle(a2i) = {Ni,C,Wi};
a3i = newreg; Circle(a3i) = {Wi,C,Si};
a4i = newreg; Circle(a4i) = {Si,C,Ei};

a1e = newreg; Circle(a1e) = {Ee,C,Ne};
a2e = newreg; Circle(a2e) = {Ne,C,We};
a3e = newreg; Circle(a3e) = {We,C,Se};
a4e = newreg; Circle(a4e) = {Se,C,Ee};

sE = newc; Line(sE) = {Ei,Ee}; 
sW = newc; Line(sW) = {Wi,We}; 

NorthLine = newreg; Line Loop(NorthLine) = {sE,a1e,a2e,-sW,-a2i,-a1i}; 
SouthLine = newreg; Line Loop(SouthLine) = {sE,-a4e,-a3e,-sW,a3i,a4i}; 

North = news; Plane Surface(North) = {NorthLine};
South = news; Plane Surface(South) = {SouthLine};

// Define the 1D grid on the curves
//Transfinite Line{sE} = n_width Using Bump 4.0; // symmetric ref.
//Transfinite Line{sW} = n_width Using Bump 4.0;
prog = 0.85;
Transfinite Line{sE} = n_width Using Progression prog; // side ref.
Transfinite Line{sW} = n_width Using Progression prog;

// Upper region: rectangular, structured
Transfinite Line{a1i} = n_upper;
Transfinite Line{a2i} = n_upper;
Transfinite Line{a1e} = n_upper;
Transfinite Line{a2e} = n_upper;
// specify the limits of the transfinite (structured) grid
Transfinite Surface{North} = {Ei,Ee,We,Wi};
// make quadrilaterals
Recombine Surface{North};

// Lower region: trinagular, unstructured
nsub_tri_i = Ceil(  prog^(n_width/2) * n_lower );
nsub_tri_e = Ceil( 1.0/(prog^n_width) * rmax/rmin * nsub_tri_i );
Transfinite Line{a3i} = nsub_tri_i;
Transfinite Line{a4i} = nsub_tri_i;
Transfinite Line{a3e} = nsub_tri_e;
Transfinite Line{a4e} = nsub_tri_e;

// Display boundary labels
View "Boundary labels" {
  T2(10, 20, 0){ StrCat( 
    "Internal labels:    ",Sprintf("%.0f, %.0f, %.0f, %.0f",a1i,a2i,a3i,a4i)) };
  T2(10, 40, 0){ StrCat( 
    "External labels:    ",Sprintf("%.0f, %.0f, %.0f, %.0f",a1e,a2e,a3e,a4e)) };
};

