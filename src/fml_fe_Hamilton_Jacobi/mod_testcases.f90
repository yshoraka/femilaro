!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! This module provides a unified interface to the specific test case
!! modules for the Hamilton-Jacobi equation. See
!! <tt>NS/mod_dgcomp_testcases</tt> for implementation details.
!<----------------------------------------------------------------------
module mod_testcases

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_burgers_test, only: &
   mod_burgers_test_constructor, &
   mod_burgers_test_destructor,  &
   burgers_test_name        => test_name, &
   burgers_test_description => test_description, &
   burgers_coeff_init       => coeff_init, &
   burgers_coeff_h          => coeff_h, &
   burgers_coeff_a          => coeff_a

 use mod_advection_test, only: &
   mod_advection_test_constructor, &
   mod_advection_test_destructor,  &
   advection_test_name        => test_name, &
   advection_test_description => test_description, &
   advection_coeff_init       => coeff_init, &
   advection_coeff_h          => coeff_h, &
   advection_coeff_a          => coeff_a

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_testcases_constructor, &
   mod_testcases_destructor,  &
   mod_testcases_initialized, &
   test_name,       & ! name of the test case
   test_description,& ! short description of the test case
   coeff_init, & ! initial condition for time dependent problems
   coeff_h,    & ! Hamiltonian function
   coeff_a       ! coefficient alpha for the Lax-Friedrichs stab.

 private

!-----------------------------------------------------------------------

! Module types and parameters

 abstract interface
  pure function i_coeff_init(x) result(phi0)
   import :: wp
   implicit none
   real(wp), intent(in) :: x(:,:)
   real(wp) :: phi0(size(x,2))
  end function i_coeff_init
 end interface
 abstract interface
  pure function i_coeff_h(p) result(h)
   import :: wp
   implicit none
   real(wp), intent(in) :: p(:,:)
   real(wp) :: h(size(p,2))
  end function i_coeff_h
 end interface
 abstract interface
  pure function i_coeff_a(p) result(a)
   import :: wp
   implicit none
   real(wp), intent(in) :: p(:,:,:)
   real(wp) :: a(size(p,2),size(p,3))
  end function i_coeff_a
 end interface

! Module variables

 character(len=10000), protected              :: test_name
 character(len=10000), allocatable, protected :: test_description(:)
 procedure(i_coeff_init), pointer :: coeff_init => null()
 procedure(i_coeff_h   ), pointer :: coeff_h    => null()
 procedure(i_coeff_a   ), pointer :: coeff_a    => null()

 logical, protected ::               &
   mod_testcases_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_testcases'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_testcases_constructor(testname,d)
  character(len=*), intent(in) :: testname
  integer, intent(in) :: d

  integer :: i
  character(len=100+len_trim(testname)+len(test_name)) :: message
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_testcases_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   casename: select case(testname)

    case(burgers_test_name)
     call mod_burgers_test_constructor(d)
     test_name = trim(burgers_test_name)
     allocate(test_description(size(burgers_test_description)))
     do i=1,size(test_description)
       test_description(i) = trim(burgers_test_description(i))
     enddo
     coeff_init => burgers_coeff_init
     coeff_h    => burgers_coeff_h
     coeff_a    => burgers_coeff_a

    case(advection_test_name)
     call mod_advection_test_constructor(d)
     test_name = trim(advection_test_name)
     allocate(test_description(size(advection_test_description)))
     do i=1,size(test_description)
       test_description(i) = trim(advection_test_description(i))
     enddo
     coeff_init => advection_coeff_init
     coeff_h    => advection_coeff_h
     coeff_a    => advection_coeff_a

    case default
     write(message,'(a,a,a)') 'Unknown test case "',trim(testname),'"'
     call error(this_sub_name,this_mod_name,message)
   end select casename

   write(message,'(a,a,a)') 'Test case: "',trim(test_name),'"'
   call info(this_sub_name,this_mod_name,message)

   mod_testcases_initialized = .true.
 end subroutine mod_testcases_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_testcases_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_testcases_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   casename: select case(trim(test_name))
    case(burgers_test_name)
     call mod_burgers_test_destructor()
    case(advection_test_name)
     call mod_advection_test_destructor()
   end select casename

   ! nullify here the procedure pointers
   nullify( coeff_init , coeff_h , coeff_a )
   deallocate( test_description )

   mod_testcases_initialized = .false.
 end subroutine mod_testcases_destructor

!-----------------------------------------------------------------------

end module mod_testcases

