/*
Copyright (C) 2012 Carlo de Falco

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Octave; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>.  

Author: Carlo de Falco <carlo@guglielmo.local>
Created: 2012-11-22

*/

#ifndef OCTAVE_FILE_IO_H
# define OCTAVE_FILE_IO_H

# ifdef __cplusplus
extern "C" 
{
# endif
  
  int
  octave_io_open (const char*, int*, int*);

  int
  octave_io_close (void);

  int 
  octave_load (const char*, double**, int*, int*);

  int 
  octave_save (const char*, const double*, const int*, const int*);

  int
  octave_clear (void);

# ifdef __cplusplus
}
# endif

#endif
