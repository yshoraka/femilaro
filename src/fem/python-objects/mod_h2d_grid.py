"""
Python copy of mod_h2d_grid.f90
"""

import numpy as np
from mod_h2d_master_el import *

#----------------------------------------------------------------------

class t_2dv():

  def __init__(self,data):
    self.i  = data['i']-1
    copy_array_field( self , 'x'  , data , (2,)                  )
    self.ns = data['ns']                                
    copy_array_field( self , 'is' , data , (self.ns,) , shift=-1 )
    self.ne = data['ne']
    copy_array_field( self , 'ie' , data , (self.ne,) , shift=-1 )

#----------------------------------------------------------------------

class t_2ds():

  def __init__(self,data):
    self.i = data['i']-1
    copy_array_field( self , 'iv'  , data , (2,) , shift=-1 )
    copy_array_field( self , 'ie'  , data , (2,) , shift=-1 )
    copy_array_field( self , 'isl' , data , (2,) , shift=-1 )
    copy_array_field( self , 'xb'  , data , (2,)            )
    self.a = data['a']

#----------------------------------------------------------------------

class t_2de():

  def __init__(self,data):
    self.i = data['i']-1
    self.poly = data['poly']
    copy_array_field( self , 'iv'  , data , (self.poly,) , shift=-1 )
    copy_array_field( self , 'is'  , data , (self.poly,) , shift=-1 )
    copy_array_field( self , 'pi'  , data , (self.poly,) , shift=-1 )
    copy_array_field( self , 'ip'  , data , (self.poly,) , shift=-1 )
    copy_array_field( self , 'ie'  , data , (self.poly,) , shift=-1 )
    copy_array_field( self , 'iel' , data , (self.poly,) , shift=-1 )
    copy_array_field( self , 'xb'  , data , (2,)                    )
    self.vol = data['vol']
    copy_array_field( self , 'n'   , data , (2,self.poly)           )
    if self.poly == 3:
      self.me = t_h2d_me_geom_tri(data["me"])
    elif self.poly == 4:
      self.me = t_h2d_me_geom_quad(data["me"])
    else:
      raise Exception('Unknown element shape "poly = '+str(self.poly))

#----------------------------------------------------------------------

class t_h2d_grid():

  def __init__(self,data):
    self.ne = data['ne']
    self.nv = data['nv']
    self.ns = data['ns']
    self.nb = data['nb']
    self.ni = data['ni']
    self.v  = np.array( list( map(t_2dv,data["v"]) ) )
    self.s  = np.array( list( map(t_2ds,data["s"]) ) )
    self.e  = np.array( list( map(t_2de,data["e"]) ) )
    self.vol = data['vol']

#----------------------------------------------------------------------

