!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Common interface of the time integrators
!!
!! \n
!!
!! This module simply provides a unified interface to all the time
!! integrators; see \c mod_time_integrators_base for further details.
!<----------------------------------------------------------------------
module mod_time_integrators

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_time_integrators_base, only: &
   mod_time_integrators_base_constructor, &
   mod_time_integrators_base_destructor,  &
   c_ode, c_ods, c_tint, t_ti_init_data, t_ti_step_diag

 use mod_rk, only: &
   mod_rk_constructor, &
   mod_rk_destructor,  &
   t_ee, t_hm, t_rk4, t_ssprk54

 use mod_multistep, only: &
   mod_multistep_constructor, &
   mod_multistep_destructor,  &
   t_thetam, t_bdf1, t_bdf2, t_bdf3, t_bdf2ex

 use mod_expo, only: &
   mod_expo_constructor, &
   mod_expo_destructor,  &
   t_erb2kry, t_erb2lej

 use mod_spexpo, only: &
   mod_spexpo_constructor, &
   mod_spexpo_destructor,  &
   t_pcexpo

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_time_integrators_constructor, &
   mod_time_integrators_destructor,  &
   mod_time_integrators_initialized, &
   c_ode, c_ods, c_tint, t_ti_init_data, t_ti_step_diag, &
   t_ee, t_hm, t_rk4, t_ssprk54, &
   t_thetam, t_bdf1, t_bdf2, t_bdf3, t_bdf2ex, &
   t_erb2kry, t_erb2lej, &
   t_pcexpo

 private

!-----------------------------------------------------------------------

! Module types and parameters

! Module variables

 ! public members
 logical, protected ::               &
   mod_time_integrators_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_time_integrators'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_time_integrators_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized   .eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_time_integrators_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   call mod_time_integrators_base_constructor()
   call mod_rk_constructor()
   call mod_multistep_constructor()
   call mod_expo_constructor()
   call mod_spexpo_constructor()

   mod_time_integrators_initialized = .true.
 end subroutine mod_time_integrators_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_time_integrators_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_time_integrators_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   call mod_spexpo_destructor()
   call mod_expo_destructor()
   call mod_multistep_destructor()
   call mod_rk_destructor()
   call mod_time_integrators_base_destructor()

   mod_time_integrators_initialized = .false.
 end subroutine mod_time_integrators_destructor

!-----------------------------------------------------------------------

end module mod_time_integrators

