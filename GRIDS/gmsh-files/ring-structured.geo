// Shape parameters (all dimensions in cm )
r0 = 165.0;
a  =  60.0;
rmin = 0.8*a;
rmax = 1.2*a;
//rmin = 0.9*a;
//rmax = 1.1*a;

// grid resolution
n_poloi = 32 * 020;  // # el. in the poloidal direction
n_width = 32 * 005;  // # el. in the radial direction
bump = 1.0;

// magnetic axix
C = newp; Point(C) = {r0,0.0,0.0};
// points on the internal boundary (east, north, ...)
Ei = newp; Point(Ei) = { r0 + rmin ,  0.0  , 0.0 };
Wi = newp; Point(Wi) = { r0 - rmin ,  0.0  , 0.0 };
// points on the external boundary
Ee = newp; Point(Ee) = { r0 + rmax ,  0.0  , 0.0 };
We = newp; Point(We) = { r0 - rmax ,  0.0  , 0.0 };

Ni = newreg; Circle(Ni) = {Ei,C,Wi};
Si = newreg; Circle(Si) = {Wi,C,Ei};
Ne = newreg; Circle(Ne) = {Ee,C,We};
Se = newreg; Circle(Se) = {We,C,Ee};
le = newreg; Line(le) = {Ei,Ee};
lw = newreg; Line(lw) = {Wi,We};

// Define the 1D grid on the curves
Transfinite Line{Ni,Ne} = n_poloi/2+1;
Transfinite Line{Si,Se} = n_poloi/2+1;
Transfinite Line{le,lw} = n_width+1 Using Bump bump;

// Define the 2D domain
NorthLine = newreg; Line Loop(NorthLine) = {le,Ne,-lw,-Ni};
North = news; Plane Surface(North) = {NorthLine};
SouthLine = newreg; Line Loop(SouthLine) = {lw,Se,-le,-Si};
South = news; Plane Surface(South) = {SouthLine};

// Define the 2D grid
Transfinite Surface{North} = {Ei,Ee,We,Wi};
Recombine Surface{North};
Transfinite Surface{South} = {Ei,Ee,We,Wi};
Recombine Surface{South};

// Display boundary labels
View "Boundary labels" {
  T2(10, 20, 0){ StrCat( 
    "Internal labels:    ",Sprintf("%.0f, %.0f",Ni,Si)) };
  T2(10, 40, 0){ StrCat( 
    "External labels:    ",Sprintf("%.0f, %.0f",Ne,Se)) };
};

