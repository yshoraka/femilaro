#!/bin/bash


#--------------------------CONFIGURATIONS-----------------------------

# Name of the executable
execroot="./../bin"
executable="lc-cn"

# Number of cores
num_cores=1
# Number of MPI processes (per core) - maximum 8
num_MPI_proc=1

# Name specifier (used to identify different experiments)
name_spec="simple-HYDRA-test"

# Input file
input_file=${executable}.in

# Required time: seconds | hours:minutes:seconds
#  -> see time_specifier in "man sge_types"
#real_time="24:00:00"
real_time="30:00"

#------------------------END CONFIGURATIONS---------------------------


#-----------------------------MAIN OUTPUT-----------------------------

# POE script to submit the job
job_file="poe_script_to_run_${executable}.cmd"

# job name
job_qual="${num_cores}x${num_MPI_proc}"
job_name="${executable}-${job_qual}"

# stdout file
job_out="out-${executable}-${name_spec}-N${job_qual}.txt"

#---------------------------END MAIN OUTPUT---------------------------


# We make a copy of the input file so that the original file can be
# changed even while the job is waiting in the queue.
input_file_copy=""
if [ -n "${input_file}" ]; # notice that we need "" in the test!
 then
  input_file_copy="temp-${job_name}-${RANDOM}.in"
  cp ${input_file} "${input_file_copy}"
fi

# Total number of processes: num_cores*num_MPI_proc
tot_num_procs=`echo ${num_cores} ${num_MPI_proc} \
               | awk '{ N = \$1*\$2 ; print N }'`

# Write the POE input file
cat > ${job_file} << EOF
# @ shell=/bin/bash
#
# Set the runtime
# @ wall_clock_limit = ${real_time}
#
# @ node = ${num_cores}
# @ tasks_per_node = ${num_MPI_proc}
#
# Never send an e-mail
# @ notification = never
#
# Job name
# @ job_name = ${name_spec}
#
# Output/error file
# @ output = ${job_out}
# @ error = ${job_out}.err
#
# @ job_type = parallel
# @ node_usage = not_shared
# @ resources = ConsumableCpus(1)
# @ network.MPI = sn_all,not_shared,us
#
# @ queue

echo
echo \
"#><><><><><><><><><><><><><><><> Job file <><><><><><><><><><><><><><>#"
cat ${job_file}
echo \
"#><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>#"
echo

echo \
"**************** Preamble of the job ${name_spec} *********************"

echo
echo "hostname: \`hostname\`"
echo "    date: \`date\`"
echo "     pwd: \`pwd\`"

echo
echo "      HOSTNAME: \${HOSTNAME}"
echo "      HOSTTYPE: \${HOSTTYPE}"
echo "LOADL_JOB_NAME: \${LOADL_JOB_NAME}"
echo " LOADEDMODULES: \${LOADEDMODULES}"
echo " LOADL_STEP_ID: \${LOADL_STEP_ID}"

echo
echo \
"***************** Starting the job ${name_spec} ***********************"
echo

poe ${execroot}/${executable} ${input_file_copy}

echo
echo "DONE! -- date: \`date\`"

EOF

# qsub appends the output, which can be confusing
if [ -e "${job_out}" ]
 then
  rm ${job_out}
fi

llsubmit ${job_file}

