"""
Read an octave ASCII file storing the data in dictionaries


The main functions provided by this module are read_var and
read_all_vars; everything else should be considered part of the
internal implementation.


Octave tends to add/remove indexes from arrays when they are 1; this
is fine because octave is very permissive about omitting 1 indexes.
However, in python an array of dimension (1,1) is different from an
array with dimension (1,) as well as from a scalar. Here, we suppress
completely indexes 1, since we have no means to decide whether they
are "correct" or whether they have been introduced by octave. If an
array A is read and must be assigned to an array B which is known to
have, say, three dimensions, the safest way is

B[:,:,:] = np.reshape(np.array(A),B.shape)

This would restore any index which might have been suppressed by this
module.


Concerning strings: for octave character arrays with more than two
indexes, problems can arise if the string contains characters
requiring more than one unicode point. The reason is that the
character array is transposed in the octave file, so the bytes
representing a single character are interspersed with the other bytes
and byte sequences can appear which have no meaning in unicode. This
generates an error when reading the file in text mode from octave.


NOTE: Most of this code follows the implementation in
mod_octave_io.f90 (more specifically, the "read" back-end implemented
in such a module).
"""

from enum import Enum
import re

import numpy as np
import scipy as sp
import scipy.sparse

# This denotes an error of this module (as opposed to errors occurring
# in the input file, which should be labeled more specifically)
def internal_module_error():
  raise Exception('This should never happen - module internal error')

# Some constants to classify the octave types and structures
class FT(Enum):
  x = -1 # error
  l =  1 # logical
  i =  2 # integer
  r =  3 # real(wp)
  z =  4 # complex(wp)
  c =  5 # character with arbitrary length
 
class OS(Enum):
  x = -1 # error
  u =  0 # undefined
  s =  1 # scalar
  m =  2 # matrix
  sm = 3 # sparse matrix
  t =  4 # struct
  c =  5 # cell
  h =  6 # function handle

# Some regular expressions used to parse octave files
OC_TYPE_BOOL_RE    = re.compile('bool')
OC_TYPE_INT_RE     = re.compile('u?int(8|16|32|64)')
OC_TYPE_COMPLEX_RE = re.compile('(sparse\s*)?(float\s*)?complex')
OC_TYPE_STRING_RE  = re.compile('(sq_)?string')
# If nothing of the above is specified, real is understood (this
# treats also structures as reals - which doesn't matter since
# structures have no type anyway)
#
# Note: more precisely, octave distinguishes among the default double
# precision and "float" single precision reals (and complex). In any
# case, we convert all reals to python double precision.

# scalar struct are treated as struct (not as scalar!)
OC_STRC_SCALAR_RE = re.compile('scalar(?!\s*struct)')
# this excludes sparse matrices... still looking for nicer solutions
MAT_RE_INTERNAL = [ re.compile('matrix') , re.compile('sparse') ]
OC_STRC_MATRIX_RE = lambda s: [ bool(re.search(s)) \
                       for re in MAT_RE_INTERNAL ] == [True,False]
OC_STRC_SPMTRX_RE = re.compile('sparse\s*(complex)?\s*matrix')
OC_STRC_STRUCT_RE = re.compile('struct')
OC_STRC_CELL_RE   = re.compile('cell')
OC_STRC_FUHN_RE   = re.compile('function handle')

# Complex numbers can come either as real numbers or as pairs
# (real,imag). This re isolates what should correspond to a real
# number, so that it can be passed to "float". Notice that in case the
# number includes invalid characters they will produce an error when
# passed to float.
COMPLEX_RE = re.compile('\(([^,]*),([^,]*)\)|([^,\(\)]*)')

# Select among the two possible forms to express dimensions
OC_NAME_RE = re.compile('^#\s*name:\s*(\w+|<cell-element>)')
OC_TYPE_RE = re.compile('^#\s*type:\s*(.*)$')
OC_DIMS_RE = re.compile('^#\s*(ndims)?(rows)?:\s*([0-9]*)')
OC_COLS_RE = re.compile('^#\s*columns:\s*([0-9]*)')
OC_STR_NDIMS_RE = re.compile('^#\s*ndims:\s*([0-9]*)')
OC_STR_LENGT_RE = re.compile('^#\s*length:\s*([0-9]*)')
OC_SPM_SIZES_RE = re.compile('^#\s*(nnz|rows|columns):\s*([0-9]*)')
OC_CHAR_DIMS_RE = re.compile('^#\s*(ndims)?(elements)?:\s*([0-9]*)')
OC_CHAR_LENG_RE = re.compile('^#\s*length:\s*([0-9]*)')

# Some functions to convert a single scalar from a string
read_scal = { \
  FT.l : lambda s: not( s.strip() == "0" ) , \
  FT.i : lambda s: int( s.strip() )        , \
  FT.r : lambda s: float( s.strip() )      , \
  FT.z : lambda s: complex( *[ float(x)                  \
           for x in COMPLEX_RE.match(s.strip()).groups() \
           if x is not None ] )            , \
  # in octave all the strings are arrays
  FT.c : lambda s: internal_module_error()   \
}

#----------------------------------------------------------------------

def locate_var(file,var_name,norewind=False):
  """
  Locate a variable in file

  If the variable is found, on return the file pointer points to the
  line immediately following the variable specification. To get this
  line, use next(file).
  """
  
  if not norewind:
    file.seek(0,0)

  s_var_name = var_name.strip()

  for line in file:
    match = OC_NAME_RE.match(line.strip())
    if match and (match.group(1).strip() == s_var_name):
      return True
  
  return False # variable not found

#----------------------------------------------------------------------

def locate_next_var(file):
  """
  Locate the next variable in file
  """

  for line in file:
    match = OC_NAME_RE.match(line.strip())
    if match:
      return match.group(1).strip()
  
  return None

#----------------------------------------------------------------------

def parse_octave_type(oc_type):
  """
  Parse the octave type specifier
  """

  match = OC_TYPE_RE.match(oc_type)
  if match:
    oc_type = match.group(1)
  else:
    raise Exception('Unable to parse type "'+oc_type.strip()+'"!')

  # check the type
  if OC_TYPE_BOOL_RE.match(oc_type):
    ft = FT.l
  elif OC_TYPE_INT_RE.match(oc_type):
    ft = FT.i
  elif OC_TYPE_COMPLEX_RE.match(oc_type):
    ft = FT.z
  elif OC_TYPE_STRING_RE.match(oc_type):
    ft = FT.c
    # strings have no additional specification
    os = OS.u
    return ft, os
  else:
    ft = FT.r
  
  # check the structure
  if OC_STRC_SCALAR_RE.search(oc_type):
    os = OS.s
  elif OC_STRC_MATRIX_RE(oc_type):
    os = OS.m
  elif OC_STRC_SPMTRX_RE.search(oc_type):
    os = OS.sm
  elif OC_STRC_STRUCT_RE.search(oc_type):
    os = OS.t
  elif OC_STRC_CELL_RE.search(oc_type):
    os = OS.c
  elif OC_STRC_FUHN_RE.search(oc_type):
    os = OS.h
  else:
    # bool objects do not include "scalar" when they are scalar...
    if ft is FT.l:
      os = OS.s
    else:
      raise Exception('Unknown structure "'+oc_type.strip()+'"!')

  return ft, os

#----------------------------------------------------------------------

def get_ndarray_dims(file):

  # Read the matrix dimensions
  oc_dims = next(file).strip()
  match = OC_DIMS_RE.match(oc_dims)
  if match.group(1) is not None: # ndims syntax
    if (match.group(2) is not None) or (match.group(3) is None):
      raise Exception('Can not understand dimensions "'+oc_dims+'"!')

    dims = np.array([ int(i) for i in next(file).split() ])
    if len(dims) != int(match.group(3)):
      raise Exception(match.group(3)+' dimensions are declared ' \
                      'but %d are provided!' % len(dims))
    nlines = np.prod(dims)
  else:                          # rows/columns syntax
    if (match.group(2) is None) or (match.group(3) is None):
      raise Exception('Can not understand dimensions "'+oc_dims+'"!')
    
    dims = np.array(                                                 \
            [ int(           match.group(3)                      ) , \
              int(OC_COLS_RE.match( next(file).strip() ).group(1)) ] )
    nlines = dims[0]
  
  # compress the dimensions eliminating 1s
  dims = [ i for i in dims if i != 1 ]
  
  return dims, nlines

#----------------------------------------------------------------------

def read_oc_scalar(ft,file):
  return read_scal[ft]( next(file) )

#----------------------------------------------------------------------

def read_oc_matrix(ft,file):

  # Read the matrix dimensions
  dims, nlines = get_ndarray_dims(file)

  # Read the matrix elements
  mat = np.array( [ [ read_scal[ft](i) for i in next(file).split() ] \
                       for l in range(nlines) ] )
  
  if len(dims) == 0: # this is in fact a scalar
    mat = mat[0][0]
  else:
    mat = np.reshape(mat,dims,order='F')
  
  return mat

#----------------------------------------------------------------------

def read_oc_spmatrix(ft,file):

  nnz  = int(OC_SPM_SIZES_RE.match(next(file).strip()).group(2))
  rows = int(OC_SPM_SIZES_RE.match(next(file).strip()).group(2))
  cols = int(OC_SPM_SIZES_RE.match(next(file).strip()).group(2))

  def parse_ijx(i,j,x):
    # parse the three arguments and pass to zero-based indexes
    return int(i)-1, int(j)-1, read_scal[ft](x)

  def reshape_args(i,j,x):
    # rearrange the arguments as required by coo_matrix
    return (x,(i,j))

  return \
   sp.sparse.coo_matrix( reshape_args(                                \
    *zip( *[ parse_ijx( *next(file).split() ) for i in range(nnz) ] ) \
   ) , [rows,cols] )

#----------------------------------------------------------------------

def read_oc_string(ft,file):
 
  # Get the size
  oc_dims = next(file).strip()
  match = OC_CHAR_DIMS_RE.match(oc_dims)
  if match.group(1) is not None: # ndims syntax
    if (match.group(2) is not None) or (match.group(3) is None):
      raise Exception('Can not understand dimensions "'+oc_dims+'"!')
    
    dims = np.array([ int(i) for i in next(file).split() ])
    if len(dims) != int(match.group(3)):
      raise Exception(match.group(3)+' dimensions are declared ' \
                      'but %d are provided!' % len(dims))
    
    # Read all the available bytes (including possible \n)
    to_be_read = np.product(dims)
    bST = b"" # empty byte string
    while to_be_read > 0:
      line = next(file).encode("utf-8")
      nbyt = min( len(line) , to_be_read )
      bST += line[:nbyt]
      to_be_read -= nbyt

    # Partially reshape the byte list
    if len(dims) == 1:
      ST = bST.decode("utf-8")
    else:
      # let's make an array so we can reshape it
      intST = np.array([ bytes([c]) for c in bST ])

      # extend the 2D case to avoid special-casing later
      if len(dims) == 2:
        dims = np.append(dims,[1])

      # The second index defines the strings, the remaining
      # indexes are used to sort the strings in a 1D list
      dims[2] = np.prod(dims[2:])

      # Reshape, get the strings and make one single list
      intST = np.reshape(intST,dims[:3],order='F')
      ST = sum([ [ bytes(bs).decode("utf-8") for bs in intST[:,:,i] ] \
                 for i in range(dims[2]) ] , [])
    
  else:                          # elements/columns syntax
    if (match.group(2) is None) or (match.group(3) is None):
      raise Exception('Can not understand dimensions "'+oc_dims+'"!')

    # These are always 2D matrices with nelem rows, which we read as
    # a 1D list, one string for each row.
    nelem = int(match.group(3))
    ST = [ "" for i in range(nelem) ]
    for i in range(nelem):

      # Read the length specification
      match = OC_CHAR_LENG_RE.match( next(file).strip() )
      if not match or (match.group(1) is None):
        raise Exception('Can not understand the string length!')
      length = int(match.group(1))

      # Here we need to read exactly "length" bytes; the idea is:
      # 1) read a line and convert it to bytes
      # 2) determine the available number of bytes (the octave
      #   string can span multiple lines including \n)
      # 3) collect the available nbyt bytes
      # 4) check whether more bytes are required and repeat
      to_be_read = length
      ST[i] = ""
      while to_be_read > 0:
        line = next(file).encode("utf-8")
        nbyt = min( len(line) , to_be_read )
        ST[i] = ST[i] + line[:nbyt].decode("utf-8")
        to_be_read -= nbyt
    
    # Compress the dimensions for a single string
    if nelem == 1:
      ST = ST[0]
          
  return ST

#----------------------------------------------------------------------

def read_oc_struct(ft,file):
  
  # Check the structure shape
  oc_dims = next(file).strip()

  # ndims is optional and if absent a scalar is assumed
  match = OC_STR_NDIMS_RE.match(oc_dims)
  if match:
    dims = np.array([ int(i) for i in next(file).split() ])
    oc_dims = next(file).strip() # read the length
  else:
    dims = np.array([1,1])
    # the length has been read already in oc_dims

  # trim the dimensions to avoid 1s
  dims = [ i for i in dims if i != 1 ]

  # length must be present and gives the number of fields
  match = OC_STR_LENGT_RE.match(oc_dims)
  if match:
    nfields = int(match.group(1))
  else:
    raise Exception('Missing "length" specification!')

  # Fill the structure(s)
  if len(dims) != 0:
    # The trick here is that the dimension information in octave is
    # redundant: it is specified both for the structure and for each
    # cell array. Thanks to this, we can afford reading the cell
    # arrays as 1D and then apply the reshape afterwards.

    # This creates independent empty dictionary objects
    S = [ {} for i in range(np.prod(dims)) ]
    for f in range(nfields):
      f_name = locate_next_var(file)
      # This field must be a cell array: we read the whole cell array
      # as a list and store it in S
      cell_array = read_var(file,norewind=True,return_iterator=True)
      for s_dic,cell in zip(S,cell_array):
        s_dic[f_name] = cell
    # now we can convert and reshape the list to an nparray
    S = np.reshape(np.array(S),dims)
  else:
    S = {}
    for f in range(nfields):
      f_name = locate_next_var(file)
      S[f_name] = read_var(file,norewind=True)
  
  return S

#----------------------------------------------------------------------

def read_oc_cell(ft,file,return_iterator=False):
  """
  Read a cell array and return it as a multidimensional list.

  If return_iterator is true, two things are done:
  1) the list is not reshaped, so that it is a 1D list
  2) the list is not really built, but rather an iterator is returned
    which reads a list element whenever a new value is required.

  The version returning an iterator should be used only inside this
  module when filling the fields of an array of structures, to improve
  efficiency and to avoid large temporary lists.
  """
  
  # A small function to reshape lists
  def list_reshape_add_one_dim(l,dim):
    # create dim iterators to the list and pass them to zip; since zip
    # returns tuples, we then map them to lists
    return map( list , zip( *( [iter(l)]*dim ) ) )

  # The cell-array shape is specified as the matrix shape, except that
  # the elements are then aways listed one after the other
  dims = get_ndarray_dims(file)[0]

  if len(dims) != 0:

    # read the elements as one list

    # Implementation note: we could build a list like this:
    #
    # C = [ read_var(file,'<cell-element>',norewind=True) \
    #        for i in range(np.prod(dims)) ]
    # 
    # however, building an iterator should be more efficient
    C = map( lambda x: read_var(file,'<cell-element>',norewind=True) ,\
             range(np.prod(dims)) )

    # Unless returning an iterator, reshape and list
    if not return_iterator:
      for d in dims[:-1]:
        C = list_reshape_add_one_dim( C , d )
      C = list(C)

    return C
  
  else: 
    return read_var(file,'<cell-element>',norewind=True)

#----------------------------------------------------------------------

def read_oc_function_handle(ft,file):
  return "Function handle not yet supported"

#----------------------------------------------------------------------

def read_var(file,var_name=None,norewind=False,return_iterator=False):
  """
  Read the required variable from file

  FILE: stream object returned by open

  See read_al_impl in mod_octave_io.f90 for further details.

  If var_name is None, we assume that the file is already positioned
  at the right location and just start reading the variable.
  """

  # Using here short-circuiting of "or"
  if (var_name is None) or (locate_var(file,var_name,norewind)):

    oc_type = next(file).strip()
    ft, os = parse_octave_type(oc_type)

    if os is OS.s:
      return read_oc_scalar(ft,file)
    elif os is OS.m:
      return read_oc_matrix(ft,file)
    elif os is OS.sm:
      return read_oc_spmatrix(ft,file)
    elif os is OS.u:
      # is this a string?
      if ft is FT.c:
        return read_oc_string(ft,file)
      else:
        internal_module_error()
    elif os is OS.t:
      return read_oc_struct(ft,file)
    elif os is OS.c:
      return read_oc_cell(ft,file,return_iterator=return_iterator)
    elif os is OS.h:
      return read_oc_function_handle(ft,file)
    else:
      if var_name is None:
        var_name = "<not specified>"
      raise Exception(                                       \
        'Variable '+var_name+': unknown type "'+str(os)+'" !')

  else:
    raise Exception('Variable '+var_name+' not found!') 

#----------------------------------------------------------------------

def read_all_vars(file):
  """
  Read all the variables in a file, returning them in a dictionary

  FILE: stream object returned by open
  """

  file.seek(0,0)

  all_vars = {}
  while True:
    f_name = locate_next_var(file)
    if f_name is None:
      break
    all_vars[f_name] = read_var(file,norewind=True)
  
  return all_vars

#----------------------------------------------------------------------
