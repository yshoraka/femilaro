program swdg_main

 !$ use omp_lib

 use mod_utils, only: &
   my_second

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   mod_octave_io_initialized, &
   write_octave, &
   read_octave

! use mod_sympoly, only: &
!   mod_sympoly_constructor, &
!   mod_sympoly_destructor,  &
!   mod_sympoly_initialized, &
!   show
!
! use mod_linal, only: &
!   mod_linal_constructor, &
!   mod_linal_destructor,  &
!   mod_linal_initialized, &
!   invmat_nop, &
!   invmat_chol
!
! use mod_umfpack, only: &
!   mod_umfpack_constructor, &
!   mod_umfpack_destructor,  &
!   mod_umfpack_initialized, &
!   umfpack_control, &
!   umfpack_info,    &
!   umf_dp,          &
!   umf_void,        &
!   umfpack_a,       &
!   umf4def,         &
!   umf4pcon,        &
!   umf4sym,         &
!   umf4num,         &
!   umf4sol,         &
!   umf4fsym,        &
!   umf4fnum
!
! use mod_gmres, only: &
!   mod_gmres_constructor, &
!   mod_gmres_destructor,  &
!   mod_gmres_initialized, &
!   gmres,  &
!   identity
!
! use mod_sparse, only: &
!   mod_sparse_constructor, &
!   mod_sparse_destructor,  &
!   mod_sparse_initialized, &
!   ! sparse types
!   t_col,       &
!   t_tri,       &
!   ! construction of new objects
!   new_col,     &
!   new_tri,     &
!   ! convertions
!   col2tri,     &
!   tri2col,     &
!   ! overloaded operators
!   operator(+), &
!   operator(*), &
!   sum,         &
!   transpose,   &
!   matmul,      &
!   clear
!
! use mod_octave_io_sparse, only: &
!   mod_octave_io_sparse_constructor, &
!   mod_octave_io_sparse_destructor,  &
!   mod_octave_io_sparse_initialized, &
!   write_octave
!
! use mod_octave_io_sympoly, only: &
!   mod_octave_io_sympoly_constructor, &
!   mod_octave_io_sympoly_destructor,  &
!   mod_octave_io_sympoly_initialized, &
!   write_octave

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   mod_output_control_initialized, &
   elapsed_format, &
   base_name

! use mod_grid, only: &
!   mod_grid_constructor, &
!   mod_grid_destructor,  &
!   mod_grid_initialized, &
!   p,e,t,   &
!   t_grid,  &
!   grid,    &
!   affmap
!
! use mod_base, only: &
!   mod_base_constructor, &
!   mod_base_destructor,  &
!   mod_base_initialized, &
!   t_base,       &
!   write_octave
!
! use mod_bcs, only: &
!   mod_bcs_constructor, &
!   mod_bcs_destructor,  &
!   mod_bcs_initialized, &
!   b_dir,          &
!   b_e2be,         &
!   t_b_s, b_side,  &
!   p_t_b_s, b_s2bs
!
! use mod_fe_spaces, only: &
!   mod_fe_spaces_constructor, &
!   mod_fe_spaces_destructor,  &
!   mod_fe_spaces_initialized, &
!   rt,      & ! complete f.e. space RT
!   ldgh,    & ! complete f.e. space LDG-H
!   bdm,     & ! complete f.e. space BDM
!   rt_vect, & ! vector f.e. space RT (orthonormal)
!   dg_vect, & ! vector f.e. space DG (orthonormal)
!   dg_scal, & ! scalar f.e. space DG (orthonormal)
!   legendre   ! one-dmensional Legendre polynomials
!
! use mod_testcases, only: &
!   mod_testcases_constructor, &
!   mod_testcases_destructor,  &
!   mod_testcases_initialized, &
!   test_name,   &
!   test_description,&
!   coeff_dir,   &
!   coeff_xiadv, &
!   coeff_lam,   &
!   coeff_q, coeff_divq
!
! use mod_tau, only: &
!   mod_tau_constructor, &
!   mod_tau_destructor,  &
!   mod_tau_initialized, &
!   taus
!
! use mod_ldgh_locmat, only: &
!   mod_ldgh_locmat_constructor, &
!   mod_ldgh_locmat_destructor,  &
!   mod_ldgh_locmat_initialized, &
!   ldgh_locmat, t_bcs_error
!
! use mod_ldgh_lhs, only: &
!   mod_ldgh_lhs_constructor, &
!   mod_ldgh_lhs_destructor,  &
!   mod_ldgh_lhs_initialized, &
!   ldgh_lhs_setup, &
!   ldgh_lhs,       &
!   precon_diag,    &
!   precon_tridiag, &
!   precon_multidiag
!
! use mod_ldgh_reconstructions, only: &
!   mod_ldgh_reconstructions_constructor, &
!   mod_ldgh_reconstructions_destructor,  &
!   mod_ldgh_reconstructions_initialized, &
!   uuu, qqq, qhn, &
!   bases, uuus, uuusef, xiadv_el, &
!   base_qsdg, qqqsdg,   &
!   base_qsrt, qqqsrt
!
! use mod_error_norms, only: &
!   mod_error_norms_constructor, &
!   mod_error_norms_destructor,  &
!   mod_error_norms_initialized, &
!   hybrid_err, &
!   primal_err, &
!   primal_err_sef, &
!   dual_err

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------
 
 ! Parameters
 integer, parameter :: max_char_len = 1000
! character(len=*), parameter :: input_file_name = 'ldgh.in'
 character(len=*), parameter :: this_prog_name = 'swdg'

! ! Main variables
! logical :: write_grid, write_sys, compute_error_norms, compute_usef
! integer :: r, stab_tauk, stab_bartauk, nbound, err_extra_deg
! integer, allocatable :: bc_type(:,:)
! character(len=max_char_len) :: method, grid_file, in_testname, &
!   stab_method, in_basename, cbc_type
! type(t_base) :: base
!
! ! Local matrixes
! integer :: mk, pk, nk
! integer :: pos_shift_ie, pos_shift_isl, pos_shift_igdll, &
!   pos_shift_jsl, pos
! integer :: ie, isl, igdll, is, i, il, jsl, jgdll, js, j, jl
! !$ integer :: ne_omp
! real(wp), allocatable :: &
!   aak(:,:) , bbk(:,:) , cck(:,:) ,         &
!   ddk(:,:) , eek(:,:) , hhk(:,:) , f2k(:), &
!   ffk(:,:) , ggk(:,:) , llk(:,:) , f3k(:), &
!  aaki(:,:) , wwk(:,:) , qqk(:,:) ,         &
!  ddkaaki(:,:) , vvk(:,:) , uuk(:,:) ,      &
!  mmmk(:,:) , fffk(:)
! 
! ! Global matrix
! integer :: sdim
! integer, allocatable :: mmmi(:), mmmj(:)
! real(wp), allocatable :: mmmx(:), fff(:), fff1(:), fff2(:), &
!  lam(:), lam1(:), rhs(:)
! type(t_col) :: mmm, mmm11, mmm12, mmm21, mmm22
!
! ! bcs variables
! integer :: is_dir, is_int
! integer, allocatable :: gdl_dir(:), gdl_int(:)
! real(wp), allocatable :: lam2(:), norm_e2(:)
!
! ! Linear system solver
! character(len=max_char_len) :: linsolver
! ! UMFPACK related variables
! integer(umf_void) :: symbolic, numeric
! real(umf_dp) :: umf_contr(umfpack_control), umf_info(umfpack_info)
! real(umf_dp), allocatable :: lam1_dp(:)
! ! GMRES related variables
 integer, parameter :: gmres_max = 500
! logical :: gmres_max_iter, gmres_abstol
! integer :: gmres_iterm, precon_ndiags
! real(wp) :: gmres_toll
! real(wp), allocatable :: gmres_res(:)
! character(len=11) :: cformat
! character(len=max_char_len) :: preconditioner
! logical :: initial_guess_umfpack
!
! ! error computations
! real(wp) :: e_l2e, e_l2k, e_pl2e, e_pl2k, e_u_l2, e_us_l2, e_usef_l2, &
!   e_q_l2, e_q_hdiv, e_qsdg_l2, e_qsdg_hdiv, e_qsrt_l2, e_qsrt_hdiv
!
! ! IO variables
! character(len=*), parameter :: out_file_nml_suff = '-nml.octxt'
! character(len=10000+len(out_file_nml_suff)) :: out_file_nml_name
! character(len=*), parameter :: out_file_base_suff = '-base.octxt'
! character(len=10000+len(out_file_base_suff)) :: out_file_base_name
! character(len=*), parameter :: out_file_results_suff = '-results.octxt'
! character(len=10000+len(out_file_results_suff)) :: out_file_results_name
!
! ! Auxiliary variables
! integer :: fu, ierr
 real :: t00, t0, t1
 !$ double precision :: t00_omp, t0_omp, t1_omp
 character(len=10*max_char_len + 10*gmres_max) :: message(4)
! type(t_bcs_error) :: b_err
!
! ! Input namelist
! namelist /input/ &
!   ! test case
!   in_testname, &
!   ! output base name
!   in_basename, &
!   ! base
!   r, method,   &
!   ! grid
!   write_grid,  &
!   grid_file,   &
!   ! stabilization
!   stab_method, stab_tauk, stab_bartauk, &
!   ! boundary conditions
!   nbound, cbc_type, &
!   ! linear system solver
!   linsolver,   &
!   ! GMRES control variables (used only if linsolver.eq."gmres")
!   gmres_abstol, gmres_toll, &
!   preconditioner, precon_ndiags, &
!   initial_guess_umfpack, &
!   ! results
!   write_sys,   &
!   ! postprocessing
!   compute_usef,&
!   ! errors
!   compute_error_norms, err_extra_deg

 !-----------------------------------------------------------------------
 ! Preliminary initializations
 t00 = my_second()
 !$ t00_omp = omp_get_wtime()
 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

! call mod_sympoly_constructor()
! call mod_octave_io_sympoly_constructor()
!
! call mod_linal_constructor()
!
! call mod_sparse_constructor()
! call mod_octave_io_sparse_constructor()
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Read input file
! call new_file_unit(fu,ierr)
! open(fu,file=trim(input_file_name), &
!      status='old',action='read',form='formatted',iostat=ierr)
! read(fu,input)
! close(fu,iostat=ierr)
! ! echo the input namelist
! out_file_nml_name = trim(in_basename)//out_file_nml_suff
! open(fu,file=trim(out_file_nml_name), &
!      status='replace',action='write',form='formatted',iostat=ierr)
! write(fu,input)
! close(fu,iostat=ierr)
!
! allocate(bc_type(nbound,2))
! read(cbc_type,*) bc_type
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Setup for the linear system solver
! linsolver_init: select case(trim(linsolver))
!  case('umfpack')
!   !---------------------------------------------------------------------
!   ! UMFPACK setup and initialization
!   call mod_umfpack_constructor()
!   !---------------------------------------------------------------------
!  case('gmres')
!   call mod_gmres_constructor()
!   ! mod_umfpack can be initialized in mod_ldgh_lhs_constructor,
!   ! depending on the chosen preconditioner
!   call mod_ldgh_lhs_constructor(trim(preconditioner))
!   if(initial_guess_umfpack.and.(.not.mod_umfpack_initialized)) then
!     !---------------------------------------------------------------------
!     ! UMFPACK setup and initialization
!     call mod_umfpack_constructor()
!     !---------------------------------------------------------------------
!   endif
!  case default
!   write(message(1),'(A,A,A)') &
!     'Unknown linear solver "',trim(linsolver),'".'
!   call error(this_prog_name,'',message(1))
! end select linsolver_init
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! output control setup
! call mod_output_control_constructor(in_basename)
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Test case setup (including the definition of the dimension)
! call mod_testcases_constructor(in_testname)
! !-----------------------------------------------------------------------
!
! !$ t0_omp = omp_get_wtime()
!
! !$omp parallel sections default(shared) private(t0,t1,t1_omp,message)
! !-----------------------------------------------------------------------
! !$omp section
! ! Define the finite element space
! t0 = my_second()
!
! call mod_base_constructor()
!
! call mod_fe_spaces_constructor()
! select case(trim(method))
!  case('rt')
!   base = rt(r)
!  case('ldgh')
!   base = ldgh(r)
!  case('bdm')
!   base = bdm(r)
!  case default
!   write(message(1),'(A,A,A)') 'Unknown method "',trim(method),'".'
!   call error(this_prog_name,'',message(1))
! end select
!
! ! write the octave output
! out_file_base_name = trim(base_name)//out_file_base_suff
! call new_file_unit(fu,ierr)
! open(fu,file=trim(out_file_base_name), &
!      status='replace',action='write',form='formatted',iostat=ierr)
!   call write_octave(base,'base',fu)
! close(fu,iostat=ierr)
!
! t1 = my_second()
! !$ t1_omp = omp_get_wtime()
! write(message(1),elapsed_format) &
!   'Completed FE basis construction: elapsed time ',t1-t0,'s.'
! call info(this_prog_name,'',message(1))
! !$ write(message(1),elapsed_format) &
! !$   'Completed FE basis construction: OpenMP elapsed time ', &
! !$   t1_omp-t0_omp,'s.'
! !$ call info(this_prog_name,'',message(1))
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! !$omp section
! ! Construct the grid
! t0 = my_second()
!
! call mod_grid_constructor(trim(grid_file),write_grid)
!
! t1 = my_second()
! !$ t1_omp = omp_get_wtime()
! write(message(1),elapsed_format) &
!   'Completed grid construction: elapsed time ',t1-t0,'s.'
! call info(this_prog_name,'',message(1))
! !$ write(message(1),elapsed_format) &
! !$   'Completed grid construction: OpenMP elapsed time ', &
! !$   t1_omp-t0_omp,'s.'
! !$ call info(this_prog_name,'',message(1))
! !-----------------------------------------------------------------------
! !$omp end parallel sections
!
! !$ t1_omp = omp_get_wtime()
! !$ write(message(1),elapsed_format) &
! !$   'Completed FE basis and grid construction: OpenMP elapsed time ', &
! !$   t1_omp-t0_omp,'s.'
! !$ call info(this_prog_name,'',message(1))
!
! !-----------------------------------------------------------------------
! ! Collect the information concerning the boundary conditions
! call mod_bcs_constructor(bc_type)
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Define the stabilization parameter tau
! call mod_tau_constructor(trim(stab_method),stab_tauk,stab_bartauk, &
!                          base,write_grid)
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Assemble matrix and right hand side
! call mod_ldgh_locmat_constructor(base)
!
! t0 = my_second()
! !$ t0_omp = omp_get_wtime()
!
! !$omp parallel private(mk , pk , nk ,         &
! !$omp                  aak , bbk , cck ,      &
! !$omp                  ddk , eek , hhk , f2k, &
! !$omp                  ffk , ggk , llk , f3k, &
! !$omp                 aaki , wwk , qqk ,      &
! !$omp              ddkaaki , vvk , uuk ,      &
! !$omp                 mmmk , fffk,            &
! !$omp                  ie , b_err , pos_shift_ie , &
! !$omp                  isl , pos_shift_isl , igdll , &
! !$omp                  pos_shift_igdll , is, i , il , &
! !$omp                  jsl , pos_shift_jsl , jgdll , &
! !$omp                  js, j , jl , pos,      &
! !$omp                  t1_omp, ne_omp, message)&
! !$omp           shared(base,grid,sdim,mmmi,mmmj,mmmx,fff, &
! !$omp                  t0_omp, taus,b_e2be)   &
! !$omp          default(none)
!
! mk = base%mk
! pk = base%pk
! nk = base%nk
! allocate( aak(  mk,mk) , bbk(  mk,pk) , cck(  mk,3*nk) ,            &
!           ddk(  pk,mk) , eek(  pk,pk) , hhk(  pk,3*nk) , f2k(  pk), &
!           ffk(3*nk,mk) , ggk(3*nk,pk) , llk(3*nk,3*nk) , f3k(3*nk), &
!           aaki( mk,mk) , wwk(  mk,pk) , qqk(  mk,3*nk) ,            &
!         ddkaaki(pk,mk) , vvk(  pk,pk) , uuk(  pk,3*nk) ,            &
!          mmmk(3*nk,3*nk), fffk(3*nk) )
! !$omp single
!  sdim = grid%ns*nk ! problem dimension
!  allocate( mmmi(grid%ne*(3*nk)**2) , mmmj(grid%ne*(3*nk)**2) )
!  allocate( mmmx(grid%ne*(3*nk)**2) , fff(sdim) )
!  fff = 0.0_wp
! !$omp end single
!
! !$ ne_omp = 0
! !$omp do schedule(static)
! elem_loop: do ie=1,grid%ne
!
!   !---------------------------------------------------------------------
!   !1) compute local matrices
!   call ldgh_locmat( aak , bbk , cck ,      &
!                     ddk , eek , hhk , f2k, &
!                     ffk , ggk , llk , f3k, &
!                     base, grid%e(ie), taus(ie), &
!                     b_e2be(ie), b_err )
!
!   if(b_err%lerr) &
!     call error(this_prog_name,'',b_err%message)
!   !---------------------------------------------------------------------
!
!   !---------------------------------------------------------------------
!   !2) static condensation
!
!   ! aaki = inv(aak)
!   call invmat_chol( aak , aaki )
!   ! ddkaaki = ddk*aaki
!   ddkaaki = matmul(ddk,aaki)
!
!   ! vvk  = inv(eek - ddk*aaki*bbk)
!   call invmat_nop( eek - matmul(ddkaaki,bbk) , vvk )
!   ! uuk  = vvk*(ddk*aaki*cck - hhk)
!   uuk = matmul( vvk , matmul(ddkaaki,cck)-hhk )
!    
!   ! qqk  = -aaki*(bbk*uuk + cck)
!   qqk = -matmul( aaki , matmul(bbk,uuk)+cck )
!   ! wwk  = -aaki*bbk*vvk
!   wwk = -matmul(aaki,matmul(bbk,vvk))
!    
!   ! mmmk = ffk*qqk + ggk*uuk + llk
!   mmmk = matmul(ffk,qqk) + matmul(ggk,uuk) + llk
!   ! fffk = f3k - (ffk*wwk + ggk*vvk)*f2k
!   fffk = f3k - matmul( matmul(ffk,wwk)+matmul(ggk,vvk) , f2k)
!   !---------------------------------------------------------------------
!
!   !---------------------------------------------------------------------
!   !3.1) side oriented summation of the element contributions
!   ! The summation is done in two steps, exploting the operations
!   ! defined for sparse matrices:
!   ! a) the contributions of each element are collected into three
!   !  vectors: indices and value, without doing any summation of the
!   !  contributions of different elements;
!   ! b) the sparse matrix is constructed and reduced: the reduction
!   !  operation sums the element contributions.
!   ! Notice that step b) has to be done outside the element loop.
!   pos_shift_ie = (ie-1)*(3*nk)**2
!   iside_do: do isl=1,3
!     pos_shift_isl = pos_shift_ie + (isl-1)*nk*3*nk
!     igdl_do: do igdll=1,nk
!       pos_shift_igdll = pos_shift_isl + (igdll-1)*3*nk
!       ! global side index
!       is = grid%e(ie)%is(isl)
!       ! matrix index i
!       i  =  (is-1)*nk + igdll
!       ! local matrix index i
!       il = (isl-1)*nk + igdll
!       jside_do: do jsl=1,3
!         pos_shift_jsl = pos_shift_igdll + (jsl-1)*nk
!         jgdl_do: do jgdll=1,nk
!           ! global side index
!           js = grid%e(ie)%is(jsl)
!           ! matrix index i
!           j  =  (js-1)*nk + jgdll
!           ! local matrix index i
!           jl = (jsl-1)*nk + jgdll
!
!           ! store the values in the unreduced matrix
!           pos = pos_shift_jsl + jgdll
!           mmmi(pos) = i
!           mmmj(pos) = j
!           mmmx(pos) = mmmk(il,jl)
!
!         enddo jgdl_do
!       enddo jside_do
!
!       ! right hand side
!       !$omp atomic
!        fff(i) = fff(i) + fffk(il)
!
!     enddo igdl_do
!   enddo iside_do
!   !---------------------------------------------------------------------
!
!   !$ ne_omp = ne_omp+1
! enddo elem_loop
! !$omp end do nowait
!
! deallocate( aak , bbk , cck ,      &
!             ddk , eek , hhk , f2k, &
!             ffk , ggk , llk , f3k, &
!            aaki , wwk , qqk ,      &
!         ddkaaki , vvk , uuk ,      &
!            mmmk , fffk )
!
! !$ t1_omp = omp_get_wtime()
! !$ write(message(1),'(a)') &
! !$   'Completed parallel assembling of the linear system:'
! !$ write(message(2),'(a,i4,a)') &
! !$   '        thread ', omp_get_thread_num(), ';'
! !$ write(message(3),'(a,i7,a,i3,a)') &
! !$   '      computed ',ne_omp,' elements, i.e. ', &
! !$   int(100.0_wp*real(ne_omp,wp)/real(grid%ne,wp)),'%;'
! !$ write(message(4),elapsed_format) &
! !$   '  elapsed time ',t1_omp-t0_omp,'s.'
! !$omp critical(omp_info_write_critical)
! !$ call info(this_prog_name,'',message(1:4))
! !$omp end critical(omp_info_write_critical)
!
! !$omp end parallel
! t1 = my_second()
! !$ t1_omp = omp_get_wtime()
! write(message(1),elapsed_format) &
!   'Completed computation of the local matrices: elapsed time ',t1-t0,'s.'
! call info(this_prog_name,'',message(1))
! !$ write(message(1),elapsed_format) &
! !$   'Completed computation of the local matrices: OpenMP elapsed time ', &
! !$   t1_omp-t0_omp,'s.'
! !$ call info(this_prog_name,'',message(1))
!
! !-----------------------------------------------------------------------
! !3.2) complete the side oriented summation of the element contributions
! ! notice that indexes in sparse matrices start from 0
! mmm = tri2col(new_tri(sdim,sdim,mmmi-1,mmmj-1,mmmx))
! deallocate( mmmi , mmmj , mmmx )
!
! t1 = my_second()
! !$ t1_omp = omp_get_wtime()
! write(message(1),elapsed_format) &
!   'Completed assembling of the linear system: elapsed time ',t1-t0,'s.'
! call info(this_prog_name,'',message(1))
! !$ write(message(1),elapsed_format) &
! !$   'Completed assembling of the linear system: OpenMP elapsed time ', &
! !$   t1_omp-t0_omp,'s.'
! !$ call info(this_prog_name,'',message(1))
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Partition mmm and the rhs according to the Dirichlet bcs
! t0 = my_second()
! allocate(gdl_dir( base%nk * count(b_side%bc.eq.b_dir) ))
! allocate(lam2( size(gdl_dir) ))
! allocate(gdl_int( base%nk*(grid%ns) - size(gdl_dir) ))
! is_dir = 0
! is_int = 0
! do is=1,grid%ni
!   is_int = is_int+1
!   gdl_int( (is_int-1)*base%nk+1 : is_int*base%nk ) = &
!      (/ (igdll, igdll=(is-1)*base%nk,is*base%nk-1) /)
! enddo
! allocate(norm_e2(base%nk)) ! squared norm of the eta basis functions
! do igdll=1,base%nk
!   norm_e2(igdll) = sum(base%wge * base%e(igdll,:)**2)
! enddo
! do is=grid%ni+1,grid%ns
!   if(b_s2bs(is)%p%bc.eq.b_dir) then ! Dirichlet side
!     is_dir = is_dir+1
!     gdl_dir( (is_dir-1)*base%nk+1 : is_dir*base%nk ) = &
!        (/ (igdll, igdll=(is-1)*base%nk,is*base%nk-1) /)
!     do igdll=1,base%nk ! projections (the eta must be orthogonal)
!       lam2( (is_dir-1)*base%nk+igdll ) = &
!          sum( base%wge                                          * &
!               coeff_dir(affmap(grid%s(is)%e(1)%p,                   &
!                                base%xigb(:,:,grid%s(is)%isl(1))), &
!                         -grid%s(is)%ie(2))                      * &
!               base%e(igdll,:) )/norm_e2(igdll)
!     enddo
!   else
!     is_int = is_int+1
!     gdl_int( (is_int-1)*base%nk+1 : is_int*base%nk ) = &
!        (/ (igdll, igdll=(is-1)*base%nk,is*base%nk-1) /)
!   endif
! enddo
! deallocate(norm_e2)
!
! !         row             column
! mmm11 = gdl_int * (mmm * gdl_int)
! mmm12 = gdl_int * (mmm * gdl_dir)
! mmm21 = gdl_dir * (mmm * gdl_int)
! mmm22 = gdl_dir * (mmm * gdl_dir)
!
! allocate(fff1(size(gdl_int)))
! fff1 = fff(gdl_int+1)
!
! t1 = my_second()
! write(message(1),elapsed_format) &
!   'Completed partitioning of the linear system: elapsed time ',t1-t0,'s.'
! call info(this_prog_name,'',message(1))
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Solution of the linear system
! t0 = my_second()
!
! allocate(lam1(mmm11%n),rhs(mmm11%n))
! rhs = fff1 - matmul(mmm12,lam2)
!
! ! the solution of the linear system is stored in lam1
! linsolver_solve: select case(trim(linsolver))
!
!  case('umfpack')
!   ! Inizialize umf_contr with the default UMFPACK parameters
!   call umf4def(umf_contr)
!   call umf4pcon(umf_contr)
!   ! pre-order and symbolic analysis
!   call umf4sym(mmm11%m,mmm11%n,mmm11%ap,mmm11%ai,real(mmm11%ax,umf_dp),&
!                symbolic,umf_contr,umf_info)
!   ! numeric factorization
!   call umf4num(mmm11%ap,mmm11%ai,real(mmm11%ax,umf_dp), &
!                symbolic,numeric,umf_contr,umf_info)
!   call umf4fsym(symbolic)
!   ! solve
!   allocate(lam1_dp(size(lam1)))
!   call umf4sol(umfpack_a,lam1_dp,real(rhs,umf_dp), &
!                numeric,umf_contr,umf_info)
!   call umf4fnum(numeric)
!   lam1 = real(lam1_dp,wp)
!   deallocate(lam1_dp)
!
!  case('gmres')
!   if(initial_guess_umfpack) then
!     ! Inizialize umf_contr with the default UMFPACK parameters
!     call umf4def(umf_contr)
!     call umf4pcon(umf_contr)
!     ! pre-order and symbolic analysis
!     call umf4sym(mmm11%m,mmm11%n,mmm11%ap,mmm11%ai,real(mmm11%ax,umf_dp),&
!                  symbolic,umf_contr,umf_info)
!     ! numeric factorization
!     call umf4num(mmm11%ap,mmm11%ai,real(mmm11%ax,umf_dp), &
!                  symbolic,numeric,umf_contr,umf_info)
!     call umf4fsym(symbolic)
!     ! solve
!     allocate(lam1_dp(size(lam1)))
!     call umf4sol(umfpack_a,lam1_dp,real(rhs,umf_dp), &
!                  numeric,umf_contr,umf_info)
!     call umf4fnum(numeric)
!     lam1 = real(lam1_dp,wp)
!     deallocate(lam1_dp)
!   else
!     lam1 = 0.0_wp
!   endif
!   call ldgh_lhs_setup(mmm11,trim(preconditioner),precon_ndiags)
!   ! the following could be simplified with a procedure pointer
!   prec: select case(trim(preconditioner))
!    case('none')
!     call gmres(lam1,ldgh_lhs,rhs,identity,         &
!                gmres_toll,gmres_abstol,gmres_max,  &
!                gmres_max_iter,gmres_iterm,gmres_res)
!    case('diagonal')
!     call gmres(lam1,ldgh_lhs,rhs,precon_diag,      &
!                gmres_toll,gmres_abstol,gmres_max,  &
!                gmres_max_iter,gmres_iterm,gmres_res)
!    case('tridiagonal')
!     call gmres(lam1,ldgh_lhs,rhs,precon_tridiag,   &
!                gmres_toll,gmres_abstol,gmres_max,  &
!                gmres_max_iter,gmres_iterm,gmres_res)
!    case('multidiagonal')
!     call gmres(lam1,ldgh_lhs,rhs,precon_multidiag, &
!                gmres_toll,gmres_abstol,gmres_max,  &
!                gmres_max_iter,gmres_iterm,gmres_res)
!    case default
!    write(message(1),'(a,a,a)') &
!      'Unknown preconditioner "',trim(preconditioner),'".'
!    call error(this_prog_name,'',message(1))
!   end select prec
!
!   write(message(1),'(a)')&
!        'GMRES diagnostics'
!   write(message(2),'(a,l1)') &
!        ' reached maximum number of iterations: ',gmres_max_iter
!   write(message(3),'(a,i3)') &
!        ' number of iterations                : ',gmres_iterm
!   if(gmres_iterm.gt.0) then
!     write(cformat,'(a,i3,a)') '(a,',gmres_iterm,'e9.1)'
!     write(message(4),cformat) &
!        ' residuals:',gmres_res
!   else
!     message(4) = ''
!   endif
!   call info(this_prog_name,'',message(1:4))
!
! end select linsolver_solve
! deallocate(rhs)
!
! ! boundary terms
! allocate(fff2(mmm22%n))
! fff2 = matmul(mmm21,lam1) + matmul(mmm22,lam2)
!
! ! pack back the values
! allocate(lam(size(lam1)+size(lam2)))
! lam(gdl_int+1) = lam1
! lam(gdl_dir+1) = lam2
! fff(gdl_dir+1) = fff2
!
! t1 = my_second()
! write(message(1),elapsed_format) &
!   'Completed solution of the linear system: elapsed time ',t1-t0,'s.'
! call info(this_prog_name,'',message(1))
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Clear the variables used in the solution of the linear system
! call clear( mmm11 )
! call clear( mmm12 )
! call clear( mmm21 )
! call clear( mmm22 )
! deallocate( fff1 , fff2 )
! deallocate( lam1 , lam2 )
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Postprocessings
! call mod_ldgh_reconstructions_constructor(base, lam ,            &
!                                           aak , bbk , cck ,      &
!                                           ddk , eek , hhk , f2k, &
!                                           ffk , ggk , llk , f3k, &
!                                          aaki , wwk , qqk ,      &
!                                       ddkaaki , vvk , uuk ,      &
!                                           compute_usef)
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Error computation
! t0 = my_second()
!
! errnorms: if(compute_error_norms) then
!   call mod_error_norms_constructor()
!   
!   call hybrid_err(base,lam,coeff_lam,err_extra_deg, &
!                   e_l2e,e_l2k,e_pl2e,e_pl2k)
!   call primal_err(base,uuu,coeff_lam,err_extra_deg, &
!                   e_u_l2)
!   call primal_err(bases,uuus,coeff_lam,err_extra_deg, &
!                   e_us_l2)
!   if(compute_usef) &
!     call primal_err_sef(bases,uuusef,coeff_xiadv,xiadv_el, &
!                         coeff_lam,err_extra_deg,e_usef_l2)
!   call dual_err(base,qqq,coeff_q,coeff_divq,err_extra_deg, &
!                 e_q_l2,e_q_hdiv)
!   if(base%r.gt.0) &
!     call dual_err(base_qsdg,qqqsdg,coeff_q,coeff_divq,err_extra_deg, &
!                   e_qsdg_l2,e_qsdg_hdiv)
!   call dual_err(base_qsrt,qqqsrt,coeff_q,coeff_divq,err_extra_deg, &
!                 e_qsrt_l2,e_qsrt_hdiv)
!
!   call mod_error_norms_destructor()
! endif errnorms
!
! t1 = my_second()
! write(message(1),elapsed_format) &
!   'Completed error computations: elapsed time ',t1-t0,'s.'
! call info(this_prog_name,'',message(1))
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Write the results
! call new_file_unit(fu,ierr)
! out_file_results_name = trim(in_basename)//out_file_results_suff
! open(fu,file=trim(out_file_results_name), &
!      status='replace',action='write',form='formatted',iostat=ierr)
!   ! preamble
!   call write_octave(test_name       ,'test_name'       ,fu)
!   call write_octave(test_description,'test_description',fu)
!   ! data
!   if(write_sys) then
!     call write_octave(gdl_int,'c','gdl_int',fu)
!     call write_octave(gdl_dir,'c','gdl_dir',fu)
!     call write_octave(mmm,'mmm',fu)
!     call write_octave(fff,'c','fff',fu)
!   endif
!   call write_octave(lam,'c','lam',fu)
!   call write_octave(uuu,'c','uuu',fu)
!   call write_octave(qqq,'c','qqq',fu)
!   call write_octave(qhn,'qhn',fu)
!   call write_octave(bases,'bases',fu)
!   call write_octave(uuus,'c','uuus',fu)
!   if(compute_usef) then
!     call write_octave(uuusef,'c','uuusef',fu)
!     call write_octave(xiadv_el,'c','xiadv_el',fu)
!   endif
!   if(base%r.gt.0) then
!     call write_octave(base_qsdg,'base_qsdg',fu)
!     call write_octave(qqqsdg,'c','qqqsdg',fu)
!   endif
!   call write_octave(base_qsrt,'base_qsrt',fu)
!   call write_octave(qqqsrt,'c','qqqsrt',fu)
!   if(compute_error_norms) then
!     call write_octave(e_l2e ,'e_l2e' ,fu)
!     call write_octave(e_l2k ,'e_l2k' ,fu)
!     call write_octave(e_pl2e,'e_pl2e',fu)
!     call write_octave(e_pl2k,'e_pl2k',fu)
!     call write_octave(e_u_l2,'e_u_l2',fu)
!     call write_octave(e_us_l2,'e_us_l2',fu)
!     if(compute_usef) &
!       call write_octave(e_usef_l2,'e_usef_l2',fu)
!     call write_octave(e_q_l2,'e_q_l2',fu)
!     call write_octave(e_q_hdiv,'e_q_hdiv',fu)
!     if(base%r.gt.0) then
!       call write_octave(e_qsdg_l2,'e_qsdg_l2',fu)
!       call write_octave(e_qsdg_hdiv,'e_qsdg_hdiv',fu)
!     endif
!     call write_octave(e_qsrt_l2,'e_qsrt_l2',fu)
!     call write_octave(e_qsrt_hdiv,'e_qsrt_hdiv',fu)
!   endif
! close(fu,iostat=ierr)
! !-----------------------------------------------------------------------
!
! !-----------------------------------------------------------------------
! ! Cleanup
! call mod_ldgh_reconstructions_destructor()
!
! deallocate( gdl_int , gdl_dir , fff , lam )
! call clear( mmm )
! deallocate( bc_type )
!
! call mod_ldgh_locmat_destructor()
!
! call mod_tau_destructor()
!
! call mod_bcs_destructor()
!
! call mod_grid_destructor()
!
! call mod_fe_spaces_destructor()
!
! call mod_base_destructor()
!
! call mod_testcases_destructor()
!
! call mod_output_control_destructor()
!
! call mod_octave_io_sparse_destructor()
! call mod_sparse_destructor()
!
! linsolver_clean: select case(trim(linsolver))
!  case('umfpack')
!   call mod_umfpack_destructor()
!  case('gmres')
!   ! the following migth call mod_umfpack_destructor
!   call mod_ldgh_lhs_destructor()
!   call mod_gmres_destructor()
!   if(initial_guess_umfpack.and.mod_umfpack_initialized) &
!     call mod_umfpack_destructor()
! end select linsolver_clean
!
! call mod_linal_destructor()
!
! call mod_octave_io_sympoly_destructor()
! call mod_sympoly_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 call mod_kinds_destructor()

 t1 = my_second()
 !$ t1_omp = omp_get_wtime()
 write(message(1),elapsed_format) &
   'Done: elapsed time ',t1-t00,'s.'
 call info(this_prog_name,'',message(1))
 !$ write(message(1),elapsed_format) &
 !$   'Done: OpenMP elapsed time ',t1_omp-t00_omp,'s.'
 !$ call info(this_prog_name,'',message(1))

 call mod_messages_destructor()
 !-----------------------------------------------------------------------

end program swdg_main

