!> \file
!! Main program for the LDG-H method.
!!
!! \n
!!
!! This program provides a unified implementation of the following
!! three hybrid methods for the steady diffusion-advection-reaction
!! equation:
!! <ul>
!!  <li> Raviart-Thomas method \f$\mathbb{RT}_k\f$
!!  <li> Local Discontinuous Galerkin Hybridizable method
!!  \f$\mathbb{LDG-H}_k\f$
!!  <li> Brezzi-Douglas-Marini method \f$\mathbb{BDM}_k\f$.
!! </ul>
!! The three methods are implemented with hybridization and static
!! condensation; see \c mod_ldgh_locmat for the details of the
!! variational forms.
!!
!! \note The current MPI parallelism is very limited: it is used
!! only in the solution of the linear system when the MUMPS
!! solver is selected. As a consequence, most of the code is executed
!! by the master thread, with the remaining ones doing only part
!! of the initializations and the calls to the MUMPS linear
!! solver.
!<
program ldgh_main

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   mod_octave_io_initialized, &
   write_octave, &
   read_octave

 use mod_mpi_utils, only: &
   mod_mpi_utils_constructor, &
   mod_mpi_utils_destructor,  &
   mod_mpi_utils_initialized, &
   mpi_comm_world, &
   mpi_init, mpi_finalize, &
   mpi_comm_size, mpi_comm_rank, &
   mpi_allgather, wp_mpi

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor,  &
   mod_sympoly_initialized

 use mod_octave_io_sympoly, only: &
   mod_octave_io_sympoly_constructor, &
   mod_octave_io_sympoly_destructor,  &
   mod_octave_io_sympoly_initialized, &
   write_octave

 use mod_linal, only: &
   mod_linal_constructor, &
   mod_linal_destructor,  &
   mod_linal_initialized, &
   invmat_nop, &
   invmat_chol

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor,  &
   mod_perms_initialized

 use mod_octave_io_perms, only: &
   mod_octave_io_perms_constructor, &
   mod_octave_io_perms_destructor,  &
   mod_octave_io_perms_initialized

 use mod_state_vars, only: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor,  &
   mod_state_vars_initialized
 
 use mod_sparse, only: &
   mod_sparse_constructor, &
   mod_sparse_destructor,  &
   mod_sparse_initialized, &
   ! sparse types
   t_col,       &
   t_tri,       &
   ! construction of new objects
   new_col,     &
   new_tri,     &
   ! convertions
   col2tri,     &
   tri2col,     &
   ! overloaded operators
   operator(+), &
   operator(*), &
   sum,         &
   transpose,   &
   matmul,      &
   clear

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_constructor, &
   mod_octave_io_sparse_destructor,  &
   mod_octave_io_sparse_initialized, &
   write_octave

 use mod_linsolver, only: &
   mod_linsolver_constructor, &
   mod_linsolver_destructor,  &
   c_linpb

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   mod_output_control_initialized, &
   elapsed_format, &
   base_name

 use mod_master_el, only: &
   mod_master_el_constructor, &
   mod_master_el_destructor,  &
   mod_master_el_initialized

 use mod_grid, only: &
   mod_grid_constructor, &
   mod_grid_destructor,  &
   mod_grid_initialized, &
   t_grid, t_ddc_grid,   &
   new_grid, clear,      &
   affmap,               &
   write_octave
 
 use mod_numquad, only: &
   mod_numquad_constructor, &
   mod_numquad_destructor,  &
   mod_numquad_initialized

 use mod_base, only: &
   mod_base_constructor, &
   mod_base_destructor,  &
   mod_base_initialized, &
   t_base, clear, &
   write_octave

 use mod_bcs, only: &
   mod_bcs_constructor, &
   mod_bcs_destructor,  &
   mod_bcs_initialized, &
   b_dir,                 &
   t_bcs, t_b_s, p_t_b_s, &
   new_bcs, clear

 use mod_fe_spaces, only: &
   mod_fe_spaces_constructor, &
   mod_fe_spaces_destructor,  &
   mod_fe_spaces_initialized, &
   rt,      & ! complete f.e. space RT
   ldgh,    & ! complete f.e. space LDG-H
   bdm        ! complete f.e. space BDM

 use mod_testcases, only: &
   mod_testcases_constructor, &
   mod_testcases_destructor,  &
   mod_testcases_initialized, &
   test_name,   &
   test_description,&
   coeff_dir,   &
   coeff_xiadv, &
   coeff_lam,   &
   coeff_q, coeff_divq

 use mod_tau, only: &
   mod_tau_constructor, &
   mod_tau_destructor,  &
   mod_tau_initialized, &
   taus

 use mod_ldgh_locmat, only: &
   mod_ldgh_locmat_constructor, &
   mod_ldgh_locmat_destructor,  &
   mod_ldgh_locmat_initialized, &
   ldgh_locmat, t_bcs_error

 use mod_ldgh_reconstructions, only: &
   mod_ldgh_reconstructions_constructor, &
   mod_ldgh_reconstructions_destructor,  &
   mod_ldgh_reconstructions_initialized, &
   uuu, qqq, qhn, &
   bases, uuus, uuusef, xiadv_el, &
!   base_qsdg, qqqsdg,   &
   base_qsrt, qqqsrt

 use mod_error_norms, only: &
   mod_error_norms_constructor, &
   mod_error_norms_destructor,  &
   mod_error_norms_initialized, &
   hybrid_err, &
   primal_err, &
   primal_err_sef, &
   dual_err
 
 use mod_ldgh_linpb, only: &
   mod_ldgh_linpb_constructor, &
   mod_ldgh_linpb_destructor,  &
   mod_ldgh_linpb_initialized, &
   dofs_dir, dofs_nat, gdofs_nat, &
   t_lambda, lam, lam1, lam2,     &
   fff, fff1, fff2, rhs,       &
   edim, eldim, sdim, nnz_mmm11,  &
   mmmi, mmmj, mmmx, mmm, mmm11, mmm12, mmm21, mmm22, &
   linpb,                      &
   me_g2l

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------
 
 ! Parameters
 integer, parameter :: max_char_len = 1000
 character(len=*), parameter :: input_file_name = 'ldgh.in'
 character(len=*), parameter :: this_prog_name = 'ldgh'

 ! Main variables
 integer :: mpi_id, mpi_nd
 logical :: write_grid, write_sys, compute_error_norms, compute_usef
 logical :: zero_mean
 integer :: k, stab_tauk, stab_bartauk, nbound, err_extra_deg
 integer, allocatable :: bc_type_t(:,:)
 character(len=max_char_len) :: method, grid_file, in_testname, &
   stab_method, in_basename, cbc_type
 type(t_base) :: base
 type(t_grid), target :: grid
 type(t_ddc_grid) :: ddc_grid
 type(t_bcs), target :: bcs

 ! Local matrixes
 integer :: dp, mk, pk, nk
 integer :: pos_shift_ie, pos_shift_isl, pos_shift_igdll, &
   pos_shift_jsl, pos
 integer :: ie, isl, igdll, is, i, il, jsl, jgdll, js, j, jl
 real(wp), allocatable :: &
   aak(:,:) , bbk(:,:) , cck(:,:) ,         &
   ddk(:,:) , eek(:,:) , hhk(:,:) , f2k(:), &
   ffk(:,:) , ggk(:,:) , llk(:,:) , f3k(:), &
  aaki(:,:) , wwk(:,:) , qqk(:,:) ,         &
  ddkaaki(:,:) , vvk(:,:) , uuk(:,:) ,      &
  mmmk(:,:) , fffk(:) ,                     &
   mmk(:,:) ,                               &
  bbbk(:,:) , ccck(:,:) , dddk(:,:) , ffpk(:)
 
 ! Global matrix: defined in mod_ldgh_linpb

 ! bcs variables
 real(wp), allocatable :: norm_e2(:)

 ! error computations
 real(wp) :: e_l2s, e_l2k, e_pl2s, e_pl2k, e_u_l2, e_us_l2, e_usef_l2, &
   e_q_l2, e_q_hdiv, e_qsdg_l2, e_qsdg_hdiv, e_qsrt_l2, e_qsrt_hdiv,   &
   all_errors_s(11)
 real(wp), allocatable :: all_errors_r(:,:)

 ! IO variables
 character(len=*), parameter :: out_file_nml_suff = '-nml.octxt'
 character(len=10000+len(out_file_nml_suff)) :: out_file_nml_name
 character(len=*), parameter :: out_file_grid_suff = '-grid.octxt'
 character(len=10000+len(out_file_grid_suff)) :: out_file_grid_name
 character(len=*), parameter :: out_file_base_suff = '-base.octxt'
 character(len=10000+len(out_file_base_suff)) :: out_file_base_name
 character(len=*), parameter :: out_file_results_suff = '-results.octxt'
 character(len=10000+len(out_file_results_suff)) :: out_file_results_name

 ! Auxiliary variables
 integer :: fu, ierr
 real(t_realtime) :: t00, t0, t1
 character(len=10*max_char_len) :: message(3)
 type(t_bcs_error) :: b_err

 ! Input namelist
 namelist /input/ &
   ! test case
   in_testname, &
   ! output base name
   in_basename, &
   ! base
   k, method,   &
   ! grid
   write_grid,  &
   grid_file,   &
   ! stabilization
   stab_method, stab_tauk, stab_bartauk, &
   ! boundary conditions
   nbound, cbc_type, &
   ! enforze zero mean
   zero_mean,   &
   ! results
   write_sys,   &
   ! postprocessing
   compute_usef,&
   ! errors
   compute_error_norms, err_extra_deg

 !----------------------------------------------------------------------
 ! Setup of all the MPI processes
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Preliminary initializations
 t00 = my_second()
 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mpi_init(ierr)
 call mod_mpi_utils_constructor()
 call mpi_comm_size(mpi_comm_world,mpi_nd,ierr)
 call mpi_comm_rank(mpi_comm_world,mpi_id,ierr)

 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_sympoly_constructor()
 call mod_octave_io_sympoly_constructor()

 call mod_linal_constructor()

 call mod_perms_constructor()
 call mod_octave_io_perms_constructor()

 call mod_state_vars_constructor()

 call mod_sparse_constructor()
 call mod_octave_io_sparse_constructor()
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Read input file
 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
 read(fu,input)
 close(fu,iostat=ierr)
 ! If there are more processes, each of them needs its own basename
 ! and grid
 if(mpi_nd.gt.1) then
   write(in_basename,'(a,a,i3.3)') trim(in_basename),'-P',mpi_id
   write(grid_file  ,'(a,a,i3.3)') trim(grid_file)  ,'.' ,mpi_id
 endif
 ! echo the input namelist
 out_file_nml_name = trim(in_basename)//out_file_nml_suff
 open(fu,file=trim(out_file_nml_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 write(fu,input)
 close(fu,iostat=ierr)

 allocate(bc_type_t(nbound,2))
 read(cbc_type,*) bc_type_t ! transposed, for simplicity
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! output control setup (and modules using it)
 call mod_output_control_constructor(in_basename)

 call mod_linsolver_constructor()
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Construct the grid
 t0 = my_second()

 call mod_master_el_constructor()

 call mod_grid_constructor()
 call new_grid(grid,trim(grid_file),ddc_grid,mpi_comm_world)

 ! write the octave output
 if(write_grid) then
   out_file_grid_name = trim(base_name)//out_file_grid_suff
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), &
        status='replace',action='write',form='formatted',iostat=ierr)
    call write_octave(grid    ,'grid'    ,fu)
    call write_octave(ddc_grid,'ddc_grid',fu)
   close(fu,iostat=ierr)
 endif

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed grid construction: elapsed time ',t1-t0,'s.'
 call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Define the finite element space
 t0 = my_second()

 call mod_numquad_constructor()

 call mod_base_constructor()

 call mod_fe_spaces_constructor()
 select case(trim(method))
  case('rt')
   base = rt(grid%me,k)
  case('ldgh')
   base = ldgh(grid%me,k)
  case('bdm')
   base = bdm(grid%me,k)
  case default
   write(message(1),'(A,A,A)') 'Unknown method "',trim(method),'".'
   call error(this_prog_name,'',message(1))
 end select

 ! write the octave output
 out_file_base_name = trim(base_name)//out_file_base_suff
 call new_file_unit(fu,ierr)
 open(fu,file=trim(out_file_base_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
   call write_octave(base,'base',fu)
 close(fu,iostat=ierr)

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed FE basis construction: elapsed time ',t1-t0,'s.'
 call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Collect the information concerning the boundary conditions
 call mod_bcs_constructor()
 call new_bcs(bcs,grid,transpose(bc_type_t) , ddc_grid,mpi_comm_world)
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Test case setup
 call mod_testcases_constructor(in_testname,grid%m)
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Define the stabilization parameter tau
 !> \bug check the definition of the \f$\tau\f$ parameter
 call mod_tau_constructor(trim(stab_method),stab_tauk,stab_bartauk, &
                          grid,base,write_grid)
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Define the linear system
 call mod_ldgh_linpb_constructor(grid,ddc_grid,base,bcs,mpi_comm_world,&
                                  zero_mean )
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Assemble matrix and right hand side
 call mod_ldgh_locmat_constructor(base)

 t0 = my_second()

 dp = base%me%d+1
 mk = base%mk
 pk = base%pk
 nk = base%nk
 allocate( aak(   mk,mk) , bbk(   mk,pk) , cck(   mk,dp*nk),           &
           ddk(   pk,mk) , eek(   pk,pk) , hhk(   pk,dp*nk),f2k(   pk),&
           ffk(dp*nk,mk) , ggk(dp*nk,pk) , llk(dp*nk,dp*nk),f3k(dp*nk),&
           aaki(  mk,mk) , wwk(   mk,pk) , qqk(   mk,dp*nk),           &
          ddkaaki(pk,mk) , vvk(   pk,pk) , uuk(   pk,dp*nk),           &
          mmmk(dp*nk,dp*nk), fffk(dp*nk) )
 sdim = grid%ns*nk  ! problem dimension
 eldim = (dp*nk)**2 ! size of local matrices
 if(zero_mean) then
   sdim = sdim + 1
   eldim = eldim + 2*dp*nk + 1
   allocate( mmk(1,pk) )
   allocate( bbbk(1,dp*nk) , ccck(dp*nk,1) , dddk(1,1) , ffpk(1) )
 endif
 edim = grid%ne * eldim
 allocate( mmmi(edim) , mmmj(edim) , mmmx(edim) , fff(sdim) )
 fff = 0.0_wp

 elem_loop: do ie=1,grid%ne

   !---------------------------------------------------------------------
   !1) compute local matrices
   call ldgh_locmat( aak , bbk , cck ,      &
                     ddk , eek , hhk , f2k, &
                     ffk , ggk , llk , f3k, &
                     base, grid%e(ie), taus(ie), &
                     bcs%b_e2be(ie), b_err, &
                     ddc_grid, me_g2l,      &
                     zero_mean , mmk)

   if(b_err%lerr) &
     call error(this_prog_name,'',b_err%message)
   !---------------------------------------------------------------------

   !---------------------------------------------------------------------
   !2) static condensation

   ! aaki = inv(aak)
   call invmat_chol( aak , aaki )
   ! ddkaaki = ddk*aaki
   ddkaaki = matmul(ddk,aaki)

   ! vvk  = inv(eek - ddk*aaki*bbk)
   call invmat_nop( eek - matmul(ddkaaki,bbk) , vvk )
   ! uuk  = vvk*(ddk*aaki*cck - hhk)
   uuk = matmul( vvk , matmul(ddkaaki,cck)-hhk )
    
   ! qqk  = -aaki*(bbk*uuk + cck)
   qqk = -matmul( aaki , matmul(bbk,uuk)+cck )
   ! wwk  = -aaki*bbk*vvk
   wwk = -matmul(aaki,matmul(bbk,vvk))
    
   ! mmmk = ffk*qqk + ggk*uuk + llk
   mmmk = matmul(ffk,qqk) + matmul(ggk,uuk) + llk
   ! fffk = f3k - (ffk*wwk + ggk*vvk)*f2k
   fffk = f3k - matmul( matmul(ffk,wwk)+matmul(ggk,vvk) , f2k)

   if(zero_mean) then
     ! bbbk = mmk*uuk
     bbbk = matmul(mmk,uuk)
     ! ccck = -(ffk*wwk + ggk*vvk)*mmk^T
     ccck = -matmul( matmul(ffk,wwk)+matmul(ggk,vvk) , transpose(mmk) )
     ! dddk = -mmk*vvk*mmk^T
     dddk = -matmul(matmul(mmk,vvk),transpose(mmk))
     ! rhs
     ffpk = -matmul(mmk,matmul(vvk,f2k))
   endif
   !---------------------------------------------------------------------

   !---------------------------------------------------------------------
   !3.1) side oriented summation of the element contributions
   ! The summation is done in two steps, exploting the operations
   ! defined for sparse matrices:
   ! a) the contributions of each element are collected into three
   !  vectors: indices and value, without doing any summation of the
   !  contributions of different elements;
   ! b) the sparse matrix is constructed and reduced: the reduction
   !  operation sums the element contributions.
   ! Notice that step b) has to be done outside the element loop.
   pos_shift_ie = (ie-1)*eldim
   iside_do: do isl=1,dp
     pos_shift_isl = pos_shift_ie + (isl-1)*nk*dp*nk
     igdl_do: do igdll=1,nk
       pos_shift_igdll = pos_shift_isl + (igdll-1)*dp*nk
       ! global side index
       is = grid%e(ie)%is(isl)
       ! matrix index i
       i  =  (is-1)*nk + igdll
       ! local matrix index i
       il = (isl-1)*nk + igdll
       jside_do: do jsl=1,dp
         pos_shift_jsl = pos_shift_igdll + (jsl-1)*nk
         jgdl_do: do jgdll=1,nk
           ! global side index
           js = grid%e(ie)%is(jsl)
           ! matrix index i
           j  =  (js-1)*nk + jgdll
           ! local matrix index i
           jl = (jsl-1)*nk + jgdll

           ! store the values in the unreduced matrix
           pos = pos_shift_jsl + jgdll
           mmmi(pos) = i
           mmmj(pos) = j
           mmmx(pos) = mmmk(il,jl)

         enddo jgdl_do
       enddo jside_do

       ! right hand side
       fff(i) = fff(i) + fffk(il)

       ! zero mean correction
       if(zero_mean) then
         ! last row
         pos = pos_shift_ie + (dp*nk)**2 + (isl-1)*nk + igdll
         mmmi(pos) = sdim
         mmmj(pos) = i
         mmmx(pos) = bbbk(1,il)
         ! last column
         pos = pos_shift_ie + (dp*nk)**2 + dp*nk + (isl-1)*nk + igdll
         mmmi(pos) = i
         mmmj(pos) = sdim
         mmmx(pos) = ccck(il,1)
         ! 1x1 block
         pos = pos_shift_ie + (dp*nk)**2 + 2*dp*nk + 1
         mmmi(pos) = sdim
         mmmj(pos) = sdim
         mmmx(pos) = dddk(1,1)
         ! rhs
         fff(sdim) = fff(sdim) + ffpk(1)
       endif

     enddo igdl_do
   enddo iside_do
   !---------------------------------------------------------------------

 enddo elem_loop

 deallocate( aak , bbk , cck ,      &
             ddk , eek , hhk , f2k, &
             ffk , ggk , llk , f3k, &
            aaki , wwk , qqk ,      &
         ddkaaki , vvk , uuk ,      &
            mmmk , fffk )
 if(zero_mean) &
   deallocate(mmk,bbbk,ccck,dddk,ffpk)

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed computation of the local matrices: elapsed time ',t1-t0,'s.'
 call info(this_prog_name,'',message(1))

 !-----------------------------------------------------------------------
 !3.2) complete the side oriented summation of the element contributions
 ! notice that indexes in sparse matrices start from 0
 mmm = tri2col(new_tri(sdim,sdim,mmmi-1,mmmj-1,mmmx))
 deallocate( mmmi , mmmj , mmmx )

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed assembling of the linear system: elapsed time ',t1-t0,'s.'
 call info(this_prog_name,'',message(1))
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Evaluate the Dirichlet boundary conditions
 allocate( lam(sdim) , lam2(size(dofs_dir)) )
 allocate(norm_e2(base%nk)) ! squared norm of the eta basis functions
 do igdll=1,base%nk
   norm_e2(igdll) = sum(base%wgs * base%e(igdll,:)**2)
 enddo
 do is=grid%ni+1,grid%ns
   if(bcs%b_s2bs(is)%p%bc.eq.b_dir) then ! Dirichlet side
     do igdll=1,base%nk ! projections (the eta must be orthogonal)
       lam( (is-1)*base%nk+igdll ) = &
          sum( base%wgs                                          * &
               coeff_dir(affmap(grid%s(is)%e(1)%p,                 &
                                base%xigb(:,:,grid%s(is)%isl(1))), &
                         -grid%s(is)%ie(2))                      * &
               base%e(igdll,:) )/norm_e2(igdll)
     enddo
   endif
 enddo
 lam2 = lam( dofs_dir+1 )
 deallocate(norm_e2)
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Partition mmm and the rhs according to the Dirichlet bcs
 t0 = my_second()

 !         row             column
 mmm11 = dofs_nat * (mmm * dofs_nat)
 mmm12 = dofs_nat * (mmm * dofs_dir)
 mmm21 = dofs_dir * (mmm * dofs_nat)
 mmm22 = dofs_dir * (mmm * dofs_dir)

 allocate(fff1(size(dofs_nat)))
 fff1 = fff(dofs_nat+1)

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed partitioning of the linear system: elapsed time ',t1-t0,'s.'
 call info(this_prog_name,'',message(1))
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Solution of the linear system
 t0 = my_second()

 allocate(lam1%l(mmm11%n))
 rhs = fff1 - matmul(mmm12,lam2)

 call linpb%factor()
 lam1%l = 0.0_wp ! initial guess for iterative solvers
 call linpb%solve(lam1)
 call linpb%clean()

 ! boundary terms
 allocate(fff2(mmm22%n))
 fff2 = matmul(mmm21,lam1%l) + matmul(mmm22,lam2)

 ! pack back the values
 lam(dofs_nat+1) = lam1%l
 lam(dofs_dir+1) = lam2
 fff(dofs_dir+1) = fff2

 ! Before doing anything with these numbers, it is better to convert
 ! them in terms of the local degrees of freedom, so that in the
 ! following we don't have to worry about ddc sides.
 do i=1,ddc_grid%nns
   if(ddc_grid%ns(i)%id.lt.ddc_grid%id) then
     is = ddc_grid%ns(i)%i     ! local side
     j  = ddc_grid%ns(i)%p_s2s ! permutation
     ! global -> local transformation
     lam( (is-1)*base%nk+1 : is*base%nk ) =                         &
       matmul( me_g2l(:,:,j) , lam( (is-1)*base%nk+1 : is*base%nk ) )
   endif
 enddo

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed solution of the linear system: elapsed time ',t1-t0,'s.'
 call info(this_prog_name,'',message(1))
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Clear the variables used in the solution of the linear system
 nnz_mmm11 = mmm11%nz
 call clear( mmm11 )
 call clear( mmm12 )
 call clear( mmm21 )
 call clear( mmm22 )
 deallocate( fff1 , fff2 )
 deallocate( lam1%l , lam2 )
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Postprocessings
 call mod_ldgh_reconstructions_constructor(grid,bcs,base,lam, &
                                        compute_usef,zero_mean)
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Error computation
 t0 = my_second()

 errnorms: if(compute_error_norms) then
   call mod_error_norms_constructor()
   
   ! To use a single reduction, the subdomain contributions are added
   ! afterwards.

   call hybrid_err(grid,base,lam,coeff_lam,err_extra_deg, &
                  e_l2s,e_l2k,e_pl2s,e_pl2k , bcs,ddc_grid)
   call primal_err(grid,base,uuu,coeff_lam,err_extra_deg, &
                   e_u_l2)
   call primal_err(grid,bases,uuus,coeff_lam,err_extra_deg, &
                   e_us_l2)
   if(compute_usef) then
     call primal_err_sef(grid,bases,uuusef,coeff_xiadv,xiadv_el, &
                         coeff_lam,err_extra_deg,e_usef_l2)
   else
     e_usef_l2 = 0.0_wp ! avoid undefined in the following MPI calls
   endif
   call dual_err(grid,base,qqq,coeff_q,coeff_divq,err_extra_deg, &
                 e_q_l2,e_q_hdiv)
!   if(base%r.gt.0) &
!     call dual_err(base_qsdg,qqqsdg,coeff_q,coeff_divq,err_extra_deg, &
!                   e_qsdg_l2,e_qsdg_hdiv)
   call dual_err(grid,base_qsrt,qqqsrt,coeff_q,coeff_divq, &
                 err_extra_deg, e_qsrt_l2,e_qsrt_hdiv)

   all_errors_s = (/ e_l2s,e_l2k,e_pl2s,e_pl2k , e_u_l2 , e_us_l2 , &
             e_usef_l2 , e_q_l2,e_q_hdiv , e_qsrt_l2,e_qsrt_hdiv /)
   ! The proper form should use mpi_allreduce, however this has
   ! problem with quadruple precision:
   !   https://trac.mcs.anl.gov/projects/mpich2/ticket/258
   ! Thus we communicate all the values and use the fortran intrinsic
   ! sum to compute the reduction
   !call mpi_allreduce( all_errors_s,all_errors_r ,            &
   !  size(all_errors_s) , wp_mpi,mpi_sum , mpi_comm_world,ierr)
   allocate(all_errors_r(size(all_errors_s),ddc_grid%nd))
   call mpi_allgather( all_errors_s,size(all_errors_s),wp_mpi, &
                       all_errors_r,size(all_errors_s),wp_mpi, &
                       mpi_comm_world, ierr )
   all_errors_s = sqrt(sum(all_errors_r**2,2))
   deallocate(all_errors_r)
   e_l2s       = all_errors_s( 1)
   e_l2k       = all_errors_s( 2)
   e_pl2s      = all_errors_s( 3)
   e_pl2k      = all_errors_s( 4)
   e_u_l2      = all_errors_s( 5)
   e_us_l2     = all_errors_s( 6)
   e_usef_l2   = all_errors_s( 7)
   e_q_l2      = all_errors_s( 8)
   e_q_hdiv    = all_errors_s( 9)
   e_qsrt_l2   = all_errors_s(10)
   e_qsrt_hdiv = all_errors_s(11)

   call mod_error_norms_destructor()
 endif errnorms

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed error computations: elapsed time ',t1-t0,'s.'
 call info(this_prog_name,'',message(1))
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Write the results
 call new_file_unit(fu,ierr)
 out_file_results_name = trim(in_basename)//out_file_results_suff
 open(fu,file=trim(out_file_results_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
   ! preamble
   call write_octave(test_name       ,'test_name'       ,fu)
   call write_octave(test_description,'test_description',fu)
   call write_octave(nnz_mmm11       ,'nnz_mmm11'       ,fu)
   ! data
   if(write_sys) then
     call write_octave(dofs_nat,'c','dofs_nat',fu)
     call write_octave(dofs_dir,'c','dofs_dir',fu)
     call write_octave(mmm,'mmm',fu)
     call write_octave(fff,'c','fff',fu)
   endif
   if(zero_mean) then
     call write_octave(lam(1:size(lam)-1),'c','lam',fu)
     call write_octave(lam(  size(lam)  ),'Pi',fu)
   else
     call write_octave(lam,'c','lam',fu)
   endif
   call write_octave(uuu,'c','uuu',fu)
   call write_octave(qqq,'c','qqq',fu)
   call write_octave(qhn,'qhn',fu)
   call write_octave(bases,'bases',fu)
   call write_octave(uuus,'c','uuus',fu)
   if(compute_usef) then
     call write_octave(uuusef,'c','uuusef',fu)
     call write_octave(xiadv_el,'c','xiadv_el',fu)
   endif
   !> \bug add reconstructions and error computations
!   if(base%k.gt.0) then
!     call write_octave(base_qsdg,'base_qsdg',fu)
!     call write_octave(qqqsdg,'c','qqqsdg',fu)
!   endif
   call write_octave(base_qsrt,'base_qsrt',fu)
   call write_octave(qqqsrt,'c','qqqsrt',fu)
   if(compute_error_norms) then
     call write_octave(e_l2s ,'e_l2s' ,fu)
     call write_octave(e_l2k ,'e_l2k' ,fu)
     call write_octave(e_pl2s,'e_pl2s',fu)
     call write_octave(e_pl2k,'e_pl2k',fu)
     call write_octave(e_u_l2,'e_u_l2',fu)
     call write_octave(e_us_l2,'e_us_l2',fu)
     if(compute_usef) &
       call write_octave(e_usef_l2,'e_usef_l2',fu)
     call write_octave(e_q_l2,'e_q_l2',fu)
     call write_octave(e_q_hdiv,'e_q_hdiv',fu)
!     if(base%k.gt.0) then
!       call write_octave(e_qsdg_l2,'e_qsdg_l2',fu)
!       call write_octave(e_qsdg_hdiv,'e_qsdg_hdiv',fu)
!     endif
     call write_octave(e_qsrt_l2,'e_qsrt_l2',fu)
     call write_octave(e_qsrt_hdiv,'e_qsrt_hdiv',fu)
   endif
 close(fu,iostat=ierr)
 !-----------------------------------------------------------------------

 !-----------------------------------------------------------------------
 ! Cleanup
 call mod_ldgh_reconstructions_destructor()

 deallocate( fff , lam )
 call clear( mmm )

 call mod_ldgh_locmat_destructor()

 call mod_ldgh_linpb_destructor()

 call mod_tau_destructor()

 call mod_testcases_destructor()

 call clear( bcs )
 call mod_bcs_destructor()

 call clear( base )
 call mod_fe_spaces_destructor()

 call mod_base_destructor()

 call mod_numquad_destructor()

 call clear( grid )
 call mod_grid_destructor()

 call mod_master_el_destructor()

 call mod_linsolver_destructor()

 call mod_output_control_destructor()

 deallocate( bc_type_t )

 call mod_octave_io_sparse_destructor()
 call mod_sparse_destructor()

 call mod_state_vars_destructor()

 call mod_octave_io_perms_destructor()
 call mod_perms_destructor()

 call mod_linal_destructor()

 call mod_octave_io_sympoly_destructor()
 call mod_sympoly_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 call mod_mpi_utils_destructor()
 call mpi_finalize(ierr)

 call mod_kinds_destructor()

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Done: elapsed time ',t1-t00,'s.'
 call info(this_prog_name,'',message(1))

 call mod_messages_destructor()
 !-----------------------------------------------------------------------

end program ldgh_main

