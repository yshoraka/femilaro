"""
This is a module to collect some simple functions for post-processing
of 2d hybrid grids and results.

We should collect here all the functions which are too specific or
simple or preliminary to be placed elsewhere.
"""

# Add the python-methods folder
import sys
sys.path.insert(0,'../src/fem/python-objects')
import mod_h2d_grid as mg
import mod_h2d_base as mb
import re
import read_octxt as ro

#----------------------------------------------------------------------

def subst_partition(fn,i):
  """
  Substitute the partition index i in the file name fn
  """
  return re.sub( '%',"{0:0>3d}".format(i) , fn )

def subst_data(fn,i):
  """
  Substitute the grid/base/res-i tag in the file name fn
  """
  if type(i) is int: i = "res-{0:0>4d}".format(i)
  return re.sub( '\$',i ,fn ) 

#----------------------------------------------------------------------

def read_grid_base(input_file,iMPI):
  """
  See pack_MPI_files, which has a very similar interface.
  """

  try:
    g_files = [ subst_partition( subst_data(input_file,'grid') , i ) \
                for i in range(iMPI) ]
    b_files = [ subst_partition( subst_data(input_file,'base') , i ) \
                for i in range(iMPI) ]
  except:
    g_files = [ subst_data(input_file,'grid') ]
    b_files = [ subst_data(input_file,'base') ]
  
  G = []
  B = []
  for gf,bf in zip(g_files,b_files):

    # read the grid
    with open(gf,"r") as fG:
      G.append( mg.t_h2d_grid(ro.read_var(fG,"grid")) )

    # read the base
    with open(bf,"r") as fB:
      B.append( mb.t_h2d_base(ro.read_var(fB,"base")) )
  
  return G, B

#----------------------------------------------------------------------

def read_res(input_file,iMPI,itimes):
  """
  See pack_MPI_files.m, which has a very similar interface.

  iMPI:   number of MPI partitions
  itimes: can be either a single time level, or an arbitrary
          collection of time levels
  """

  # read a single partition for a single time
  def readi(fn):
    with open(fn,"r") as f:
      return ro.read_all_vars(f)

  # read a single time (all the partitions)
  def read(fn,it):
    try:
      return [ readi( subst_partition( subst_data(fn,it) , i ) ) \
               for i in range(iMPI) ]
    except:
      return [ readi( subst_data(fn,it) ) ]

  # read all the required times, all the partitions
  try:
    return [ read(input_file,it) for it in itimes ]
  except:
    return read(input_file,itimes)

