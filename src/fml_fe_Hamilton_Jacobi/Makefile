# Copyright (C) 2014  Marco Restelli
#
# This file is part of:
#   FEMilaro -- Finite Element Method toolkit
#
# FEMilaro is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# FEMilaro is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
#
# author: Marco Restelli                   <marco.restelli@gmail.com>

# List of the object files of the present folder
OBJ_HJ_TEST= mod_burgers.o \
             mod_advection.o
OBJ_HJ= mod_phi_state.o \
     mod_hj_ode.o \
     mod_testcases.o \
     hj_dg.o \
     $(OBJ_HJ_TEST)

# General settings -----------------------------------

PWD:=$(shell pwd)
BUILDDIR:=$(BUILDDIR)/fml_fe_Hamilton_Jacobi

LIBS_HJ:=-lode -lfem -lfml_general_utilities $(LIBS)
LDFLAGS:=$(LDFLAGS) -L$(LIBDIR)

# VPATH includes:
#   PWD for the source files (included by default)
#   PWD/testcases for the source files of the testcases
#   BUILDDIR for the .o prerequisites
#   LIBDIR for the various libraries
#   BINDIR for the final executable
VPATH:=./testcases $(BUILDDIR) $(LIBDIR) $(BINDIR)

# clear unfinished targets
.DELETE_ON_ERROR:
# define the phony targets
.PHONY: clean

# Main targets ---------------------------------------

hj-dg: $(OBJ_HJ)
	cd $(BUILDDIR) && \
	  $(LD) $(LDFLAGS) $(OBJ_HJ) $(LIBS_HJ) -o $(BINDIR)/hj-dg

# General rule: all the .o depend on the various libraries
%.o: %.f90 libfml_general_utilities.a libode.a libfem.a
	cd $(BUILDDIR) && \
	  $(FC) $(FFLAGS) $(MODFLAG)$(LIBDIR) -c $(PWD)/$<

clean:
	$(RM) $(BUILDDIR)/*.o $(BUILDDIR)/*.mod \
	  $(BINDIR)/hj-dg

# Specific dependencies: because of USE directives ---
# Notice: here we list only the prerequisites which are not already
# included in the libfml_general_utilities.a library (nor in any other
# library), which is a prerequisite of all the .o files listed in this
# Makefile

mod_hj_ode.o: \
  mod_phi_state.o \
  mod_testcases.o

mod_testcases.o: $(OBJ_HJ_TEST)

hj_dg.o: \
  mod_phi_state.o \
  mod_hj_ode.o
  
