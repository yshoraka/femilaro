function sgradp = geom_tri_scale_gradp(me,xig,gradp)
% sgradp = geom_tri_scale_gradp(me,xig,gradp)
%
% See mod_h2d_master_el::geom_tri_scale_gradp

 for l=1,size(xig,2)
   sgradp(:,:,l) = me.b_it * gradp(:,:,l);
 end

return
