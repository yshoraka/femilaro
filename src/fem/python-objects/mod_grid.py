"""
Python copy of mod_grid.f90
"""

import numpy as np
import utilities as ut
import mod_master_el as me

#----------------------------------------------------------------------

class t_v():
  def __init__(self,data):
    self.m  = data['m' ]
    self.i  = data['i' ]-1
    ut.copy_array_field( self , 'x'  , data , ( -1, ) )
    self.ns = data['ns']
    ut.copy_array_field( self , 'is' , data , ( -1, ) , shift=-1 )
    self.ne = data['ne']
    ut.copy_array_field( self , 'ie' , data , ( -1, ) , shift=-1 )

#----------------------------------------------------------------------

class t_s():
  def __init__(self,data):
    self.d  = data['d' ]
    self.m  = data['m' ]
    self.i  = data['i' ]-1
    ut.copy_array_field( self , 'iv'  , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'ie'  , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'isl' , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'xb'  , data , ( -1, ) )
    self.a  = data['a' ]
    ut.copy_array_field( self , 'b'   , data , ( self.d,-1 ) )
    ut.copy_array_field( self , 'x0'  , data , ( -1, ) )

#----------------------------------------------------------------------

class t_e():
  def __init__(self,data):
    self.d = data['d' ]
    self.m = data['m' ]
    self.i = data['i' ]-1
    ut.copy_array_field( self , 'iv'  , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'is'  , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'pi'  , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'ip'  , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'ie'  , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'iel' , data , ( -1, ) , shift=-1 )
    ut.copy_array_field( self , 'xb'  , data , ( -1, ) )
    self.vol   = data['vol']
    ut.copy_array_field( self , 'b'   , data , ( self.d,-1 ) )
    self.det_b = data['det_b']
    ut.copy_array_field( self , 'bi'  , data , ( self.d,-1 ) )
    ut.copy_array_field( self , 'x0'  , data , ( -1, ) )
    ut.copy_array_field( self , 'n'   , data , ( self.m,-1 ) )
    ut.copy_array_field( self , 'edata' , data , ( -1, ) )

#----------------------------------------------------------------------

class t_grid():
  def __init__(self,data):
    self.d   = data['d' ]
    self.m   = data['m' ]
    self.ne  = data['ne']
    self.nv  = data['nv']
    self.ns  = data['ns']
    self.nb  = data['nb']
    self.ni  = data['ni']
    self.me  = me.t_me( data['me'] )
    self.v = ut.make_np_array( t_v , data["v"] )
    self.s = ut.make_np_array( t_s , data["s"] )
    self.e = ut.make_np_array( t_e , data["e"] )
    self.vol = data['vol']
    self.edata_legend = data['edata_legend']

#----------------------------------------------------------------------

