program hj_dg

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 !$ use mod_omp_utils, only: &
 !$   mod_omp_utils_constructor, &
 !$   mod_omp_utils_destructor,  &
 !$   detailed_timing_omp

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   real_format,    &
   write_octave,   &
   read_octave,    &
   read_octave_al, &
   locate_var
 
 use mod_tps_phs_grid, only: &
   mod_tps_phs_grid_constructor, &
   mod_tps_phs_grid_destructor,  &
   t_real_lists, &
   t_tps_grid, new_tps_grid, clear, &
   write_octave, read_octave_al
 
 use mod_linal, only: &
   mod_linal_constructor, &
   mod_linal_destructor

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor

 use mod_numquad, only: &
   mod_numquad_constructor, &
   mod_numquad_destructor
 
 use mod_mpi_utils, only: &
   mod_mpi_utils_constructor, &
   mod_mpi_utils_destructor,  &
   mpi_comm_world, &
   mpi_init, mpi_thread_single, mpi_thread_multiple, &
   mpi_finalize, mpi_init_thread

 use mod_tps_base, only: &
   mod_tps_base_constructor, &
   mod_tps_base_destructor,  &
   t_tps_base, new_tps_base, clear, &
   write_octave
 
 use mod_state_vars, only: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor,  &
   c_stv

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   elapsed_format, &
   base_name

 use mod_phi_state, only: &
   mod_phi_state_constructor, &
   mod_phi_state_destructor,  &
   t_phi_state, new_phi_state, clear, &
   write_octave
 
 use mod_testcases, only: &
   mod_testcases_constructor, &
   mod_testcases_destructor,  &
   test_name, test_description, &
   coeff_init

 use mod_time_integrators, only: &
   mod_time_integrators_constructor, &
   mod_time_integrators_destructor,  &
   c_ode, c_ods, c_tint, t_ti_init_data, t_ti_step_diag, &
   t_ee, t_hm, t_rk4, t_ssprk54, &
   t_thetam, t_bdf1, t_bdf2, t_bdf3, t_bdf2ex, &
   t_erb2kry, t_erb2lej, &
   t_pcexpo
 
 use mod_hj_ode, only: &
   mod_hj_ode_constructor, &
   mod_hj_ode_destructor,  &
   t_hj_ode, t_hj_ods, &
   new_hj_ode, clear
   

 implicit none


 ! Define some general parameters
 integer, parameter :: max_char_len = 10000
 character(len=*), parameter :: this_prog_name = 'hj_dg'

 ! IO variables
 character(len=*), parameter :: input_file_name_def = 'hj-dg.in'
 character(len=max_char_len) :: input_file_name
 character(len=*), parameter :: out_file_nml_suff = '-nml.octxt'
 character(len=max_char_len+len(out_file_nml_suff)):: out_file_nml_name
 character(len=max_char_len) :: basename
 logical :: write_grid
 character(len=*), parameter :: out_file_grid_suff = '-grid.octxt'
 character(len=max_char_len+len(out_file_grid_suff))::out_file_grid_name
 character(len=*), parameter :: out_file_base_suff = '-base.octxt'
 character(len=max_char_len+len(out_file_base_suff))::out_file_base_name
 integer :: fu, ierr

 ! Grid
 type(t_real_lists), allocatable :: xx(:)
 type(t_tps_grid), target :: grid
 character(len=max_char_len) :: grid_file

 ! FE basis
 integer :: k ! polynomial degree
 type(t_tps_base), target :: base

 ! Some grid/base related variables useful for visualization
 real(wp), allocatable :: x_points(:,:,:)
 
 ! Test case
 character(len=max_char_len) :: testname
 character(len=max_char_len) :: integrator

 ! Unknowns
 type(t_phi_state) :: uuu0, uuun

 ! Initial condition
 integer :: i, id, ie
 real(wp), allocatable :: xe(:,:)

 ! Time integration
 integer :: nstep, n, n_out
 real(wp) :: dt, tt_sta, tt_end, t_nm1, t_n
 logical :: l_output1, l_output2
 integer :: nout1, nout2
 type(t_hj_ode)  :: hj_ode
 type(t_hj_ods)  :: hj_ods
 type(t_ti_init_data) :: ti_init_data
 type(t_ti_step_diag) :: ti_step_diag
 class(c_tint), allocatable :: tint

 ! Messages
 character(len=max_char_len) :: message(1)

 ! Timing
 real(t_realtime) :: t00, t0, t1, t0step

 ! MPI variables
 logical, parameter :: master_proc = .true.

 ! Input namelist
 namelist /input/ &
   ! test case
   testname, &
   ! output base name
   basename, &
   ! grid
   write_grid, &
   grid_file,  &
   ! base
   k, &
   ! time stepping
   tt_sta, tt_end, dt, &
   integrator, &
   nout1, nout2  ! output frequency


 call mod_messages_constructor()

 call mod_kinds_constructor()

 !$ if(detailed_timing_omp) call mod_omp_utils_constructor()
 
 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_linal_constructor()

 call mod_perms_constructor()

 call mod_numquad_constructor()

 call mod_state_vars_constructor()

 !call mpi_init(ierr)
 call mpi_init_thread(mpi_thread_multiple,i,ierr)
 call mod_mpi_utils_constructor()

 !----------------------------------------------------------------------
 ! Read input file
 if(command_argument_count().gt.0) then
   call get_command_argument(1,value=input_file_name)
 else
   input_file_name = input_file_name_def
 endif
 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
 read(fu,input)
 close(fu,iostat=ierr)
 !! If there are more processes, each of them needs its own basename
 !! and grid
 !if(mpi_nd.gt.1) then
 !  write(in_basename,'(a,a,i3.3)') trim(in_basename),'-P',mpi_id
 !  write(grid_file  ,'(a,a,i3.3)') trim(grid_file)  ,'.' ,mpi_id
 !endif
 ! echo the input namelist
 out_file_nml_name = trim(basename)//out_file_nml_suff
 open(fu,file=trim(out_file_nml_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 write(fu,input)
 close(fu,iostat=ierr)
 !----------------------------------------------------------------------

 call mod_output_control_constructor(basename)

 !----------------------------------------------------------------------
 ! Define the grid
 t0 = my_second()

 call mod_tps_phs_grid_constructor()

 call new_file_unit(fu,ierr)
 open(fu,file=trim(grid_file), &
      status='old',action='read',form='formatted',iostat=ierr)
 call read_octave_al(xx,'xx',fu) ! implies allocate(xx)
 close(fu,iostat=ierr)
 call new_tps_grid(grid,xx)

 ! write the octave output
 if(write_grid) then
   out_file_grid_name = trim(base_name)//out_file_grid_suff
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), &
        status='replace',action='write',form='formatted',iostat=ierr)
   call write_octave(grid,'grid',fu)
   close(fu,iostat=ierr)
 endif

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed grid construction: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !---------------------------------------------------------------------
 ! Define the basis
 t0 = my_second()

 call mod_tps_base_constructor()

 call new_tps_base(base,k,grid%d)

 ! Write some additional output which is useful for the postprocessing
 out_file_base_name = trim(base_name)//out_file_base_suff
 call new_file_unit(fu,ierr)
 open(fu,file=trim(out_file_base_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 call write_octave(base,'base',fu)
 ! We don't close here this file since some additional output will be
 ! added later.

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed FE basis construction: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Allocations and initial condition
 t0 = my_second()

 call mod_time_integrators_constructor()

 call mod_testcases_constructor(testname,grid%d)

 call mod_phi_state_constructor()
 call mod_hj_ode_constructor()

 call new_phi_state(uuu0,grid,base)
 call new_phi_state(uuun,grid,base)
 call new_hj_ode( hj_ode,hj_ods ,grid,base)

 allocate( xe( grid%d , base%pkd ) )
 if(write_grid) allocate(x_points(grid%d,base%pkd,grid%ne))
 do ie=1,grid%ne

   ! For each element, we need to generate the nodal points
   do i=1,base%pkd
     do id=1,grid%d
       xe(id,i) = &
         grid%e(ie)%bw(   id)*base%xgl(base%mldx(i,id)) &
       + grid%e(ie)%box(1,id)
     enddo
   enddo

   uuu0%phi(:,ie) = coeff_init(xe)

   if(write_grid) x_points(:,:,ie) = xe

 enddo
 deallocate( xe )

 ! Complete writing the base output file
 if(write_grid) then
   call write_octave((/( size(xx(i)%x)-1,i=1,size(xx) )/),'r','nex',fu)
   call write_octave(x_points ,'x_points' ,fu)
   deallocate( x_points )
 endif
 deallocate(xx)
 call write_octave(test_name       ,'test_name'       ,fu)
 call write_octave(test_description,'test_description',fu)

 call write_octave(uuu0%phi,"phi0",fu)
 close(fu,iostat=ierr)

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed start-up phase: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Time integration
 !
 !   0        1                 n             nstep
 !   |--------|--------|--------|--------|------|
 ! tt_sta            t_nm1     t_n            tt_end
 !                     | step_n |
 !

 ! Set time integration method
 time_int_case: select case(trim(integrator))
  case('rk4')
   allocate(t_rk4::tint)
  case('ssprk54')
   allocate(t_ssprk54::tint)
  case('erb2kry')
   allocate(t_erb2kry::tint)
   ti_init_data%dim = 150       ! size of the Krylov space
   ti_init_data%tol = 1.0e-3_wp ! tolerance
  case('erb2lej')
   allocate(t_erb2lej::tint)
   ti_init_data%dim = 150       ! maximum number of Leja points
   ti_init_data%tol = 1.0e-2_wp ! tolerance
   allocate(ti_init_data%ir1(2)); ti_init_data%ir1 = (/-76.0_wp,0.0_wp/)
  case('pcexpo')
   allocate(t_pcexpo::tint)
   ti_init_data%dim = 50
   ti_init_data%tol = 1.0e-6_wp
   ti_init_data%mpi_id = 0
   ti_init_data%mpi_comm = mpi_comm_world
   allocate(ti_init_data%ii1(1)); ti_init_data%ii1 = (/1/) ! Kry/Lej
   allocate(ti_init_data%ir1(2)); ti_init_data%ir1 = (/-20.0_wp,0.1_wp/)
  case default
   call error(this_prog_name, '',&
    'Unknown integration method "'//trim(integrator)//'".')
 end select time_int_case
 
 nstep = ceiling(tt_end/dt)
 n_out = 0
 call write_out2(grid,base,tt_sta,uuu0,.true. ,ti_step_diag)
 call write_out2(grid,base,tt_sta,uuu0,.false.,ti_step_diag)
 call write_out1(0,tt_sta,n_out,uuu0,ti_step_diag)

 call tint%init( hj_ode,dt,0.0_wp,uuu0,hj_ods,          &
        init_data=ti_init_data , step_diag=ti_step_diag )
 t0 = my_second()
 time_loop: do n=1,nstep

   t_nm1 = tt_sta + real(n-1,wp)*dt
   t_n   = t_nm1 + dt

   call tint%step(uuun,hj_ode,t_nm1,uuu0,hj_ods,ti_step_diag)
   uuu0%phi = uuun%phi

   ! Select how often the full distribution is written
   l_output1 = (mod(n,nout1).eq.0).or.(n.eq.1).or.(n.eq.nstep)
   if(l_output1) then
     write(*,*) 'Step ',n,' of ',nstep
     n_out = n_out + 1
     call write_out1(n,t_n,n_out,uuu0,ti_step_diag)
   endif

   ! Select how often the integrated diagnostics are written
   l_output2 = (mod(n,nout2).eq.0).or.(n.eq.1).or.(n.eq.nstep)
   if(l_output2) &
     call write_out2(grid,base,t_n,uuu0,.false.,ti_step_diag)

 enddo time_loop
 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed time loop: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))

 call tint%clean(hj_ode,ti_step_diag)
 deallocate(tint)

 call clear(hj_ode,hj_ods)
 call mod_hj_ode_destructor()

 call clear(uuu0)
 call clear(uuun)
 call mod_phi_state_destructor()

 call mod_testcases_destructor()

 call mod_time_integrators_destructor()

 call clear(base)
 call mod_tps_base_destructor()

 call clear(grid)
 call mod_tps_phs_grid_destructor()

 call mod_output_control_destructor()

 call mod_mpi_utils_destructor()
 call mpi_finalize(ierr)

 call mod_state_vars_destructor()

 call mod_numquad_destructor()

 call mod_perms_destructor()

 call mod_linal_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 !$ if(detailed_timing_omp) call mod_omp_utils_destructor()

 call mod_kinds_destructor()

 call mod_messages_destructor()

contains

 subroutine write_out1(n,t,n_out,uuu,sd)
  integer, intent(in) :: n, n_out
  real(wp), intent(in) :: t
  type(t_phi_state), intent(in) :: uuu
  type(t_ti_step_diag), intent(in) :: sd

  integer :: fu, ierr
  character(len=*), parameter :: time_stamp_format = '(i4.4)'
  character(len=4) :: time_stamp
  character(len=*), parameter :: suff1 = '-res-'
  character(len=*), parameter :: suff2 = '.octxt'
  character(len= len_trim(base_name) + len(suff1) + 4 + len(suff2)) :: &
     out_file_res_name

   call new_file_unit(fu,ierr)
   write(time_stamp,time_stamp_format) n_out
   out_file_res_name = trim(base_name)//suff1//time_stamp//suff2
   open(fu,file=out_file_res_name, &
        status='replace',action='write',form='formatted',iostat=ierr)
    call write_octave(n  ,'n'  ,fu)
    call write_octave(t  ,'t'  ,fu)
    call write_octave(uuu,'uuu',fu)
    call write_octave(sd%max_iter,'ti_max_iter',fu)
    call write_octave(sd%iterations,'ti_iter',fu)
    if(allocated(sd%d1)) &
      call write_octave(sd%d1,'r',trim(sd%name_d1),fu)
    if(allocated(sd%d2)) &
      call write_octave(sd%d2,    trim(sd%name_d2),fu)
    if(allocated(sd%d3)) &
      call write_octave(sd%d3,    trim(sd%name_d3),fu)
   close(fu,iostat=ierr)

 end subroutine write_out1

 !> Write "high-frequency" output
 !!
 !! The output is collected in one array and written as a row in an
 !! ASCII file. There is only one output file, with one row per time
 !! level.
 subroutine write_out2(grid,base,t,uuu,init,sd)
  type(t_tps_grid),     intent(in) :: grid
  type(t_tps_base),     intent(in) :: base
  real(wp),             intent(in) :: t
  type(t_phi_state),    intent(in) :: uuu
  logical,              intent(in) :: init
  type(t_ti_step_diag), intent(in) :: sd

  character(len=*), parameter :: suff = '-fast-output.octxt'

   ! todo

 end subroutine write_out2

end program hj_dg

