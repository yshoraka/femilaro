// Set the mesh size
//h = 1.0/(64.0*8.0);
h = 1.0/3.0;

// Build the mesh
Point(1) = {0.0,0.0,0.0,h};
Point(2) = {1.0,0.0,0.0,h};
Point(3) = {1.0,1.0,0.0,h};
Point(4) = {0.0,1.0,0.0,h};
Line(2) = {1,2};
Line(3) = {2,3};
Line(1) = {3,4};
Line(4) = {4,1};
Line Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};

