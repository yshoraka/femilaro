"""
Python copy of mod_h2d_master_el.f90
"""

import numpy as np

#----------------------------------------------------------------------

abs_err = "This is an abstract class - should not be istantiated!"

def copy_array_field(obj,f,data,dims,shift=0):
  """
  This function has three goals:
  1) ensures the correct number of indices (see read_octxt.py)
  2) ensures that new objects are build, rather than references
  3) if necessary, corrects the indexes passing to zero based
  """
  setattr( obj , f , np.reshape( data[f]+shift , dims ) )

#----------------------------------------------------------------------

class t_perm():
  def __init__(self,data):
    self.d = data['d']
    self.p = data['p']
    copy_array_field( self , 'i' , data , (self.d,) , shift=-1 )

#----------------------------------------------------------------------

class c_h2d_me():
  def __init__(self,data):
    raise Exception(abs_err)

#----------------------------------------------------------------------

class c_h2d_me_geom(c_h2d_me):
  def __init__(self,data):
    raise Exception(abs_err)

class c_h2d_me_base(c_h2d_me):
  def __init__(self,data):
    if type(self) is c_h2d_me_base:
      raise Exception(abs_err)
    else:
      self.k    = data['k']
      copy_array_field( self , 'xnodes' , data , (2,-1)            )
      copy_array_field( self , 'snodes' , data , (2,-1) , shift=-1 )
      self.m    = data['m']
      self.ms   = data['ms']
      self.deg  = data['deg']
      self.degs = data['degs']
      copy_array_field( self , 'xig' , data , (2,-1) )
      copy_array_field( self , 'wg'  , data , (-1,)  )
      self.pi_tab = np.array( list( map(t_perm,data["pi_tab"]) ) )
      copy_array_field( self , 'stab' , data , (-1,self.ms) , shift=-1)
      copy_array_field( self , 'xigs' , data , (1,self.ms   ) )
      copy_array_field( self , 'wgs'  , data , (  self.ms   ) )
      copy_array_field( self , 'xigb' , data , (2,self.ms,-1) )
      self.pk   = data['pk']
      copy_array_field(self, 'p'      , data , (  self.pk,self.m    ) )
      copy_array_field(self, 'gradp'  , data , (2,self.pk,self.m    ) )
      copy_array_field(self, 'pb'     , data , (  self.pk,self.ms,-1) )
      copy_array_field(self, 'gradpb' , data , (2,self.pk,self.ms,-1) )

#----------------------------------------------------------------------

class t_h2d_me_geom_tri(c_h2d_me_geom):

  def __init__(self,data):
    copy_array_field( self , 'b'    , data , (2,2) )
    self.det_b = data['det_b']
    copy_array_field( self , 'x0'   , data , (2,)  )
    copy_array_field( self , 'b_it' , data , (2,2) )

  def map(self,csi):
    x = np.dot(self.b,csi)
    for i in range(x.shape[0]):
      x[i,:] += self.x0[i]
    return x

  def scale_wg(self,xig,wg):
    # very easy: the Jacobian is constant
    return self.det_b * wg

  def scale_gradp(self,xig,gradp):
    sgradp = np.ndarray(gradp.shape)
    # very easy: the Jacobian is constant
    for l in range(xig.shape[1]):
      sgradp[:,:,l] = np.dot( self.b_it , gradp[:,:,l] )
    return sgradp

#----------------------------------------------------------------------

class t_h2d_me_geom_quad(c_h2d_me_geom):

  def __init__(self,data):
    copy_array_field( self , 'v1' , data , (2,)  )
    copy_array_field( self , 'b'  , data , (2,2) )
    copy_array_field( self , 'c'  , data , (2,)  )
    # some internal variables
    self._jac_it = np.ndarray( (2,2) )

  def map(self,csi):
    x = np.dot( self.b , csi )
    for i in range(csi.shape[1]):
      x[:,i] += self.v1 + self.c*np.prod(csi[:,i])
    return x

  def scale_wg(self,xig,wg):
    det_j = \
       (self.b[0,0] + self.c[0]*xig[1,:])    \
        * (self.b[1,1] + self.c[1]*xig[0,:]) \
     - (self.b[0,1] + self.c[0]*xig[0,:])    \
        * (self.b[1,0] + self.c[1]*xig[1,:])
    return det_j * wg

  def scale_gradp(self,xig,gradp):
    sgradp = np.ndarray(gradp.shape)
    for l in range(xig.shape[1]):
      # 2x2 inverse, including transposition
      self._jac_it[0,0] =   self.b[1,1] + self.c[1]*xig[0,l]
      self._jac_it[0,1] = - self.b[1,0] - self.c[1]*xig[1,l]
      self._jac_it[1,0] = - self.b[0,1] - self.c[0]*xig[0,l]
      self._jac_it[1,1] =   self.b[0,0] + self.c[0]*xig[1,l]

      # notice that the 2x2 det formula can be applied also to jac_it
      det =  self._jac_it[0,0]*self._jac_it[1,1] \
           - self._jac_it[0,1]*self._jac_it[1,0]
      self._jac_it /= det

      sgradp[:,:,l] = np.dot( self._jac_it , gradp[:,:,l] )
    return sgradp

#----------------------------------------------------------------------

class t_h2d_me_base_tri(c_h2d_me_base):
  # We can use the constructor of the abstract class
  pass

class t_h2d_me_base_quad(c_h2d_me_base):
  # We can use the constructor of the abstract class
  pass

#----------------------------------------------------------------------

