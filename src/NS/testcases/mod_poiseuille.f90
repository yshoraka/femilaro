!> \brief
!! Poiseuille flow patch test for 2D and 3D Stokes and Navier-Stokes
!! methods.
!!
!! \n
!!
!! Th computational domain is
!! \f$\Omega=[-A,A]\times[-B,B]\times[-s,s]\f$, with the
!! two-dimensional case obtained for \f$B\to\infty\f$. The viscosity
!! is \f$\mu=1\f$ and there is no forcing term. The test case has
!! three degrees of freedom:
!! <ol>
!!  <li> the angle \f$\theta\f$ of the flow in the \f$(x,y)\f$ plane
!!  (this is only used in the 3D case);
!!  <li> the coefficient \f$U_1\f$ of the first momentum of the flow
!!  (corresponding to the velocity of the two plates at \f$\pm s\f$);
!!  <li> the coefficient \f$U_2\f$ of the second momentum of the
!!  flow (corresponding to the pressure gradient).
!! </ol>
!! The analytic solution is
!! \f{displaymath}{
!!  \underline{u} = \left[\begin{array}{c}
!!   U\cos\theta \\[2mm]
!!   U\sin\theta \\[2mm]
!!   0
!!  \end{array}\right], \qquad
!!  p = -\frac{2U_2}{s^2}\left( \cos\theta\,x + \sin\theta\, y
!!  \right),
!! \f}
!! where \f$U_1\f$ and \f$U_2\f$ are two independent constants and
!! \f{displaymath}{
!!  U = U_1\frac{z}{s}+U_2\left(1-\frac{z^2}{s^2}\right).
!! \f}
!!
!! On the \f$z=\pm s\f$ boundaries a Dirichlet boundary condition
!! should be enforced, while on the remaining boundaries either
!! Dirichlet or Neumann boundary conditions can be specified.
!!
!! \note The constants \f$s\f$, \f$\theta\f$, \f$U_1\f$ and \f$U_2\f$
!! are presently implemented as hardwired values.
!<----------------------------------------------------------------------
module mod_poiseuille_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_poiseuille_test_constructor, &
   mod_poiseuille_test_destructor,  &
   mod_poiseuille_test_initialized, &
   test_name, &
   test_description,&
   coeff_visc,&
   coeff_f,   &
   coeff_dir, &
   coeff_u,   &
   coeff_gradu,&
   coeff_p,   &
   coeff_gradp

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members
 character(len=*), parameter ::    &
   test_name = "poiseuille"
 ! private members
 real(wp), parameter :: &
   theta = 0.5236_wp, &
   u1 = 1.0_wp, &
   u2 = 1.0_wp, &
   s = 1.0_wp

! Module variables

 ! public members
 character(len=100), protected ::    &
   test_description(2)
 logical, protected ::               &
   mod_poiseuille_test_initialized = .false.
 ! private members
 integer :: d ! dimension
 character(len=*), parameter :: &
   this_mod_name = 'mod_poiseuille_test'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_poiseuille_test_constructor(id)
  integer, intent(in) :: id

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_poiseuille_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   d = id

   write(test_description(1),'(a,i1)') &
     'Poiseuille test case, dimension d = ',d
   testdim: select case(d)
    case(2)
     write(test_description(2),'(a,e9.3,a,a,a,e9.3,a)') &
       '  parameters: U1 = ',u1,', U2 = ',u2,'.'
    case(3)
     write(test_description(2),'(a,e9.3,a,e9.3,a,e9.3,a)') &
       '  parameters: theta = ',theta,', U1 = ',u1,', u2 = ',u2,'.'
    case default
     call error(this_sub_name,this_mod_name, &
       'This test case is only implemented for dimensions 2 and 3.')
   end select testdim

   mod_poiseuille_test_initialized = .true.
 end subroutine mod_poiseuille_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_poiseuille_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_poiseuille_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_poiseuille_test_initialized = .false.
 end subroutine mod_poiseuille_test_destructor

!-----------------------------------------------------------------------
 
 pure function coeff_visc(x) result(nu_x)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: nu_x(size(x,2))

   nu_x = 1.0_wp
 end function coeff_visc
 
!-----------------------------------------------------------------------
 
 pure function coeff_f(x) result(f)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: f(size(x,1),size(x,2))

   f = 0.0_wp
 end function coeff_f
 
!-----------------------------------------------------------------------

 pure function coeff_dir(x,breg) result(d)
  real(wp), intent(in) :: x(:,:)
  integer, intent(in) :: breg
  real(wp) :: d(size(x,1),size(x,2))

   d = coeff_u(x)
 end function coeff_dir
 
!-----------------------------------------------------------------------

 pure function coeff_u(x) result(u)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: u(size(x,1),size(x,2))

  real(wp) :: uu(size(x,2))

   if(d.eq.2) then
     uu = u1*x(2,:)/s + u2*(1.0_wp-x(2,:)**2/s**2)
     u(1,:) = uu
     u(2,:) = 0.0_wp
   else
     uu = u1*x(3,:)/s + u2*(1.0_wp-x(3,:)**2/s**2)
     u(1,:) = cos(theta)*uu
     u(2,:) = sin(theta)*uu
     u(3,:) = 0.0_wp
   endif

 end function coeff_u
 
!-----------------------------------------------------------------------

 pure function coeff_gradu(x) result(gu)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: gu(size(x,1),size(x,1),size(x,2))

   if(d.eq.2) then
     ! first row
     gu(1,1,:) = 0.0_wp
     gu(1,2,:) = u1/s - 2.0_wp*u2*x(2,:)/s**2
     ! second row
     gu(2,1,:) = 0.0_wp
     gu(2,2,:) = 0.0_wp
   else
     ! first row
     gu(1,1,:) = 0.0_wp
     gu(1,2,:) = 0.0_wp
     gu(1,3,:) = cos(theta)*( u1/s - 2.0_wp*u2*x(3,:)/s**2 )
     ! second row
     gu(2,1,:) = 0.0_wp
     gu(2,2,:) = 0.0_wp
     gu(2,3,:) = sin(theta)*( u1/s - 2.0_wp*u2*x(3,:)/s**2 )
     ! third row
     gu(3,1,:) = 0.0_wp
     gu(3,2,:) = 0.0_wp
     gu(3,3,:) = 0.0_wp
   endif

 end function coeff_gradu
 
!-----------------------------------------------------------------------

 pure function coeff_p(x) result(p)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: p(size(x,2))

   if(d.eq.2) then
     p = -2.0_wp*u2/s**2 * x(1,:)
   else
     p = -2.0_wp*u2/s**2 * (cos(theta)*x(1,:)+sin(theta)*x(2,:))
   endif

 end function coeff_p
 
!-----------------------------------------------------------------------

 pure function coeff_gradp(x) result(gp)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: gp(size(x,1),size(x,2))

   if(d.eq.2) then
     gp(1,:) = -2.0_wp*u2/s**2
     gp(2,:) =      0.0_wp
   else
     gp(1,:) = -2.0_wp*u2/s**2 * cos(theta)*x(1,:)
     gp(2,:) = -2.0_wp*u2/s**2 * sin(theta)*x(2,:)
     gp(3,:) =             0.0_wp
   endif

 end function coeff_gradp
 
!-----------------------------------------------------------------------

end module mod_poiseuille_test

