!> \file
!! This program generates a complete quadrature rule from a .html file
!! with the format described in
!! http://www.cs.kuleuven.ac.be/~nines/research/ecf/mtables.html .
!<
program cools

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   write_octave, &
   read_octave,  &
   read_octave_al

 use mod_linal, only: &
   mod_linal_constructor, &
   mod_linal_destructor

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor,  &
   t_perm,     &
   perm_table

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor,  &
   t_sympol,     &
   nll_sympoly,  &
   all_sympoly,  &
   ev,           &
   me_int

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

 integer, parameter :: default_char_length = 100000
 character(len=*), parameter :: this_prog_name='cools-encyclopedia'

 logical :: node_exists
 integer :: ierr, fu, nargin
 integer :: d, npoints, degree
 integer :: i, ii, j, k, h, nn
 integer, allocatable :: rule_struct(:), symb(:), psymb(:), &
   node_symb(:,:), stab(:,:)
 real(wp) :: w, err, err_max, err_mean
 real(wp), allocatable :: xg(:,:), wg(:), gen(:), bcoords(:,:)
 character(len=default_char_length) :: message(5), &
   input_file, output_file, line, aword
 type(t_perm), allocatable :: ptab(:)
 type(t_sympol), allocatable :: polys(:)

 !----------------------------------------------------------------------
 ! Call constructors
 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_linal_constructor()

 call mod_perms_constructor()

 call mod_sympoly_constructor()
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! 0) parse input arguments
 nargin = command_argument_count()
 select case(nargin)
  case(0)
   write(*,*) 'Input file name:'
   read(*,'(A)') input_file
   write(*,*) 'Output file name'
   read(*,'(A)') output_file
  case(2)
   call get_command_argument(1,value=input_file)
   call get_command_argument(2,value=output_file)
  case default
   write(message(1),'(a)') 'Wrong number of arguments.'
   write(message(2),'(a)') &
     ' This program can be called either without input arguments or'
   write(message(3),'(a)') &
     ' with two input arguments specifying:'
   write(message(4),'(a)') &
     '  input file (.html, as in http://www.cs.kuleuven.ac.be/~nines/&
        &research/ecf/mtables.html)'
   write(message(5),'(a)') &
     '  output file (.octxt)'
   call error(this_prog_name,'',message(1:5))
 end select
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! 1) parse the input file
 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file), &
      status='old',action='read',form='formatted',iostat=ierr)
 if(ierr.ne.0) &
   call error(this_prog_name,'', &
     'Problems opening the input file "'//trim(input_file)//'".')

 ! check that we are working on a Simplex
 call parse_until(line,fu,'Region')
 read(line(len('Region:')+1:),'(a)') aword
 if(trim(adjustl(aword)).ne.'Simplex') &
   call error(this_prog_name,'', &
     'This function only works for "Simplex" quadrature formulas.')

 ! dimension
 call parse_until(line,fu,'Dimension')
 read(line(len('Dimension:')+1:),*) d

 ! degree
 call parse_until(line,fu,'Degree')
 read(line(len('Degree:')+1:),*) degree

 ! npoints
 call parse_until(line,fu,'Points')
 read(line(len('Points:')+1:),*) npoints

 ! check that the structure is fully symmetric
 call parse_until(line,fu,'Structure')
 read(line(len('Structure:')+1:),'(a)') aword
 if(trim(adjustl(aword)).ne.'Fully symmetric') &
   call error(this_prog_name,'', &
     'This function only works for "Fully symmetric" quadrature formulas.')

 ! rule structure: the size depends on the dimension
 dim_case_1: select case(d)
  case(2)
   allocate(rule_struct(6))
  case(3)
   allocate(rule_struct(11))
  case(4)
   allocate(rule_struct(18))
  case default
   write(line,*) 'Formulas for dimension ',d,' currently unsupported.'
   call error(this_prog_name,'',line)
 end select dim_case_1
 call parse_until(line,fu,'Rule struct')
 read(line(len('Rule struct:')+1:),*) rule_struct
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! 2) allocations
 allocate( xg(d,npoints) , wg(npoints) , gen(d+1) )
 call perm_table(ptab,d+1)
 allocate( symb(d+1) , psymb(d+1) , node_symb(d+1,size(ptab,1)) )
 allocate( stab(size(ptab,1),npoints) )
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! 3) we now loop over all the generators
 npoints = 0
 do i=1,size(rule_struct)
   do ii=1,rule_struct(i) ! generate the corresponding points

     call parse_until(line,fu,'Generator',ierr)
     read(line(len('Generator:')+1:),'(a)') aword
     if(trim(adjustl(aword)).ne.'[ Fully symmetric ]') &
       call error(this_prog_name,'', &
         'Generators must be "[ Fully symmetric ]".')
     ! In the choice of the barycentric coordinates we have to be
     ! consistent with the vertex numbering of mod_master_el. So the
     ! first barycentric coordinate is associated with the origin, the
     ! second with x_1 and so on.
     call read_generator(gen(2:d+1),fu,line)

     call parse_until(line,fu,'Corresponding',ierr)
     call read_weight(w,fu,line)

     ! the last barycentric coordinate must sum up to 1
     gen(1) = 1.0_wp - sum(gen(2:d+1))

     ! 4) we now generate all the corresponding nodes

     ! This select generates the symbols corresponding the rule
     ! structure. The symbol indicate which barycentric coordinate are
     ! repeated and which are different; being integers, the symbols
     ! can be used to make comparisons, which would not be robust
     ! using directly the barycentric coordinates. The value of the
     ! symbols is not relevant, the only relevant aspect is which
     ! numbers are repeated and in which positions. The barycentric
     ! coordinate of the origin is the first one.
     symbols: select case(d)
      case(2)
       dim2_rules: select case(i)
        case(1)
         symb = (/ 2 , 1 , 2 /)
        case(2)        
         symb = (/ 2 , 1 , 1 /)
        case(3)        
         symb = (/ 3 , 1 , 2 /)
        case(4)        
         symb = (/ 1 , 1 , 1 /)
        case(5)        
         symb = (/ 2 , 1 , 1 /)
        case(6)      
         symb = (/ 3 , 1 , 2 /)
       end select dim2_rules
      case(3)
       dim3_rules: select case(i)
        case(1)
         symb = (/ 2 , 1 , 2 , 2 /)
        case(2)
         symb = (/ 2 , 1 , 1 , 2 /)
        case(3)
         symb = (/ 3 , 1 , 2 , 3 /)
        case(4)
         symb = (/ 2 , 1 , 1 , 1 /)
        case(5)
         symb = (/ 3 , 1 , 1 , 2 /)
        case(6)
         symb = (/ 4 , 1 , 2 , 3 /)
        case(7)
         symb = (/ 1 , 1 , 1 , 1 /)
        case(8)
         symb = (/ 2 , 1 , 1 , 1 /)
        case(9)
         symb = (/ 2 , 1 , 1 , 2 /)
        case(10)
         symb = (/ 3 , 1 , 1 , 2 /)
        case(11)
         symb = (/ 4 , 1 , 2 , 3 /)
       end select dim3_rules
      case(4)
       dim4_rules: select case(i)
        case(1)
         symb = (/ 2 , 1 , 2 , 2 , 2 /)
        case(2)
         symb = (/ 2 , 1 , 1 , 2 , 2 /)
        case(3)
         symb = (/ 3 , 1 , 2 , 3 , 3 /)
        case(4)
         symb = (/ 2 , 1 , 1 , 1 , 2 /)
        case(5)
         symb = (/ 3 , 1 , 1 , 2 , 3 /)
        case(6)
         symb = (/ 4 , 1 , 2 , 3 , 4 /)
        case(7)
         symb = (/ 2 , 1 , 1 , 1 , 1 /)
        case(8)
         symb = (/ 3 , 1 , 1 , 1 , 2 /)
        case(9)
         symb = (/ 3 , 1 , 1 , 2 , 2 /)
        case(10)
         symb = (/ 4 , 1 , 1 , 2 , 3 /)
        case(11)
         symb = (/ 5 , 1 , 2 , 3 , 4 /)
        case(12)
         symb = (/ 1 , 1 , 1 , 1 , 1 /)
        case(13)
         symb = (/ 2 , 1 , 1 , 1 , 1 /)
        case(14)
         symb = (/ 2 , 1 , 1 , 1 , 2 /)
        case(15)
         symb = (/ 3 , 1 , 1 , 1 , 2 /)
        case(16)
         symb = (/ 3 , 1 , 1 , 2 , 2 /)
        case(17)
         symb = (/ 4 , 1 , 1 , 2 , 3 /)
        case(18)
         symb = (/ 5 , 1 , 2 , 3 , 4 /)
       end select dim4_rules
      case default
     end select symbols

     nn = 0
     ! apply the permutations to the symbol and generate the nodes
     do j=1,size(ptab)
       psymb = symb(ptab(j)%i)
       node_exists = .false.
       search_do_1: do k=1,nn
         if(all(node_symb(:,k).eq.psymb)) then ! existing node
           node_exists = .true.
           stab(j,npoints+1) = npoints+k
           exit search_do_1
         endif
       enddo search_do_1

       if(.not.node_exists) then ! new node
         nn = nn+1
         xg(:,npoints+nn) = gen(ptab(j)%i(2:d+1))
         wg(npoints+nn) = w
         node_symb(:,nn) = psymb
         stab(j,npoints+1) = npoints+nn
       endif
     enddo

     ! fill stab (brute force approach)
     do j=1,size(ptab)
       do k=2,nn
         psymb = node_symb(ptab(j)%i,k)
         search_do_2: do h=1,nn
           if(all(node_symb(:,h).eq.psymb)) then
             stab(j,npoints+k) = npoints+h
             exit search_do_2
           endif
         enddo search_do_2
       enddo
     enddo

     ! update npoints
     npoints = npoints + nn

   enddo
 enddo
 close(fu,iostat=ierr)
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! 5) tests
 ! test the quadrature formula
 allocate(polys(nll_sympoly(d,degree)))
 polys = all_sympoly(d,degree)
 err_max = -1.0_wp
 err_mean = 0.0_wp
 do i=1,size(polys)
   err = abs(me_int(d,polys(i)) - sum(wg*ev(polys(i),xg)))
   err_max = max(err_max,err)
   err_mean = err_mean + err
 enddo
 err_mean = err_mean/size(polys)
 write(message(1),'(a)') &
   'Integration errors:'
 write(message(2),'(a,e9.3)') &
   '  maximum absolute error: ',err_max
 write(message(3),'(a,e9.3)') &
   '  mean absolute error:    ',err_mean
 call info(this_prog_name,'',message(1:3))
 deallocate(polys)
 ! test the permutation table
 allocate(bcoords(size(xg,1)+1,size(xg,2)))
 bcoords(2:,:) = xg
 bcoords(1,:) = 1.0_wp - sum(xg,1)
 err_max = -1.0_wp
 err_mean = 0.0_wp
 do i=1,size(stab,1)
   do j=1,size(stab,2)
     err = sqrt(sum((bcoords(ptab(i)%i,j)-bcoords(:,stab(i,j)))**2))
     err_max = max(err_max,err)
     err_mean = err_mean + err
   enddo
 enddo
 err_mean = err_mean/size(stab)
 write(message(1),'(a)') &
   'Permutation errors:'
 write(message(2),'(a,e9.3)') &
   '  maximum absolute error: ',err_max
 write(message(3),'(a,e9.3)') &
   '  mean absolute error:    ',err_mean
 call info(this_prog_name,'',message(1:3))
 deallocate(bcoords)
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! 6) output
 open(fu,file=trim(output_file), &
      status='new',action='write',form='formatted',iostat=ierr)
 if(ierr.ne.0) &
   call error(this_prog_name,'', &
     'Problem creating the output file "'//trim(output_file)//'".')
 call write_octave(xg,'x',fu)
 call write_octave(wg,'r','w',fu)
 call write_octave(stab,'stab',fu)
 close(fu,iostat=ierr)
  
 !----------------------------------------------------------------------
 ! 7) cleanup
 deallocate( xg , wg , gen )
 deallocate( ptab )
 deallocate( symb , psymb , node_symb )
 deallocate( stab )

 call mod_sympoly_destructor()
 call mod_perms_destructor()
 call mod_linal_destructor()
 call mod_octave_io_destructor()
 call mod_fu_manager_destructor()
 call mod_kinds_destructor()
 call mod_messages_destructor()
 !----------------------------------------------------------------------

contains

 !> Parse a file until \c match is found.
 subroutine parse_until(newline,fu,match,ierr)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: match
  character(len=*), intent(out) :: newline
  integer, optional, intent(out) :: ierr

   if(present(ierr)) then
     parse_loop_ierr: do
       read(fu,'(a)',iostat=ierr) newline
      if( (line(1:len_trim(match)).eq.match).or.(ierr.ne.0) ) &
        exit parse_loop_ierr
     enddo parse_loop_ierr
   else
     parse_loop: do
       read(fu,'(a)') newline
      if(line(1:len_trim(match)).eq.match) exit parse_loop
     enddo parse_loop
   endif

 end subroutine parse_until

 !> This subroutine reads a generator. It looks for size(gen) real
 !! numbers, which can be placed on one or more lines, possibly
 !! interleaved with text lines.
 !<
 subroutine read_generator(gen,fu,workline)
  integer, intent(in) :: fu
  real(wp), intent(out) :: gen(:)
  character(len=*), intent(out) :: workline

  integer :: nread, ierr
  real(wp) :: realtemp

   nread = 0
   read_loop: do

     read(fu,'(a)') workline
     ! now we process a line, which may contain 0, 1 or more numbers
     line_loop: do
       call mpread(realtemp,workline,ierr)
       if(ierr.eq.0) then
         nread = nread + 1
         gen(nread) = realtemp
       else
         exit line_loop ! line processed
       endif
     enddo line_loop

    if(nread.eq.size(gen)) exit read_loop
   enddo read_loop

 end subroutine read_generator

 !> This subroutine reads a weight. It looks for one real number in
 !! all the following lines.
 !<
 subroutine read_weight(w,fu,workline)
  integer, intent(in) :: fu
  real(wp), intent(out) :: w
  character(len=*), intent(out) :: workline

  integer :: ierr
  real(wp) :: realtemp

   read_loop: do

     read(fu,'(a)') workline
     ! now we process a line, which may contain 0, 1 or more numbers
     line_loop: do
       call mpread(realtemp,workline,ierr)
       if(ierr.eq.0) then
         w = realtemp
         return
       else
         exit line_loop ! line processed
       endif
     enddo line_loop

   enddo read_loop

 end subroutine read_weight

 !> This subroutine processes a character string containing a
 !! multi-precision real and reads it. The number is then cleared from
 !! the character string. According to
 !! http://www.cs.kuleuven.ac.be/~nines/research/ecf/datafiles.html,
 !! multi-precision reals are written as follows:
 !! <tt> 10^m x fortran_real, </tt>
 !! where \c m is an integer, \c x is the character 'x' and \c
 !! fortran_real is a number which can be read by a
 !! <tt>read(...,*)</tt> fortran statement. The <tt> 10^m x</tt> part
 !! can be omitted, and spaces are ignored. The number is always
 !! terminated by a comma.
 !!
 !! To read such a number, we proceed here as follow:
 !! <ol>
 !!  <li> strip any leading character which is not in the range 0-9
 !!  <li> isolate the text until the first ',' is encountered
 !!  <li> if there is a 'x', read the exponent
 !!  <li> read the mantissa
 !!  <li> compute the number as a <tt>real(wp)</tt>
 !! </ol>
 !! A nonzero \c ierr value indicates that no valid number has been
 !! read.
 !<
 pure subroutine mpread(x,xchar,ierr)
  character(len=*), intent(inout) :: xchar
  real(wp), intent(out) :: x
  integer, intent(out) :: ierr

  integer :: i, i_x, i_comma, base, ex
  real(wp) :: mantissa
  character(len=len(xchar)) :: chartemp

   !1) strip initial characters
   xchar = adjustl(xchar)
   strip_do: do
    select case(xchar(1:1))
     case('-','.','0','1','2','3','4','5','6','7','8','9')
      exit strip_do
     case default
      xchar = adjustl(xchar(2:))
      if(len_trim(xchar).eq.0) then
        ierr = -1
        return
      endif
    end select
   enddo strip_do

   !2) look for a comma, and check whether there is a 'x'
   i_comma = -1
   i_x = -1
   comma_do: do i=1,len_trim(xchar)
     select case(xchar(i:i))
      case('x')
       i_x = i
      case(',')
       i_comma = i
       exit comma_do
      case default
       ! nothing to do
     end select
   enddo comma_do

   !3) if there is no comma, no number can be read
   if(i_comma.le.0) then
     ierr = -1
     return
   endif

   !4) if there is 'x' read the exponent
   if(i_x.ge.1) then
     read(xchar(1:i_x-1),*,iostat=ierr) base, chartemp, ex
     if(ierr.ne.0) return
   else
     i_x = 0
     base = 1
     ex = 1
   endif

   !5) read the mantissa
   read(xchar(i_x+1:i_comma-1),*,iostat=ierr) mantissa
   if(ierr.ne.0) return

   !6) convert the number
   x = mantissa * (real(base,wp)**ex)

   !7) clear the number
   xchar = xchar(i_comma+1:)

 end subroutine mpread

end program cools

