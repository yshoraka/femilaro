function PV_cgns_series(fname,itime,N,k,outfile)
% PV_cgns_series(fname,itime,N,k,outfile)
%
% Call PV_cgns for a series of files and prepare a .pvd file to
% collect the generated vtk files.
%
% fname nust contain a '%' to indicate the subdomain number and a '$'
% to indicate the suffix 'grid', 'base' or 'res-XXXX'.

 % Summary file
 fnameNX = [outfile,'.pvd'];
 fid = fopen(fnameNX,'w');
 
 line = '<?xml version="1.0"?>';
 fprintf(fid,'%s\n',line);
 line = '<VTKFile type="Collection" version="0.1" byte_order="BigEndian">';
 fprintf(fid,'%s\n',line);
 line = ' <Collection>';
 fprintf(fid,'%s\n',line);

 % Part loop
 Npos = find(fname=='%');
 for i=0:N
   fnameN = [fname(1:Npos-1),num2str(i,'%.3i'),fname(Npos+1:end)];
   Xpos = find(fnameN=='$');
   fnameNX = [fnameN(1:Xpos-1),'grid',fnameN(Xpos+1:end)];
   grid = load(fnameNX);
   fnameNX = [fnameN(1:Xpos-1),'base',fnameN(Xpos+1:end)];
   base = load(fnameNX);
   fnameNX = [fnameN(1:Xpos-1),'res-',num2str(itime,'%.4i'), ...
              fnameN(Xpos+1:end)];
   res = load(fnameNX);
   fnameNX = [outfile,'-',num2str(i,'%.3i'),'.vtu'];
   PV_cgns(grid.grid,base.ubase,base.pbase,base.udofs,base.pdofs, ...
           res.uuu,k,fnameNX,res.test_name);
   line = ['  <DataSet timestep="" group="" part="', num2str(i,'%i'), ...
           '" file="',fnameNX,'"/>'];
   fprintf(fid,'%s\n',line);
 end

 line = ' </Collection>';
 fprintf(fid,'%s\n',line);
 line = '</VTKFile>';
 fprintf(fid,'%s\n',line);

 fclose(fid);

return

